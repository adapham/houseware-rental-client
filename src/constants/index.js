import {
  spfOne,
  spfTwo,
  spfThree,
  spfFour,
  bestSellerOne,
  bestSellerTwo,
  bestSellerThree,
  bestSellerFour,
  newArrOne,
  newArrTwo,
  newArrThree,
  newArrFour,
} from "../assets/images/index";

// =================== NavBarList Start here ====================
export const navBarList = [
  {
    _id: 1001,
    title: "Trang chủ",
    link: "/",
  },
  {
    _id: 1002,
    title: "Sản phẩm",
    link: "/shop",
  },
  {
    _id: 1005,
    title: "Blog",
    link: "/blog",
  },
  {
    _id: 1003,
    title: "Sổ tay chủ tiệm",
    link: "/about",
  },
  // {
  //   _id: 1004,
  //   title: "Contact",
  //   link: "contact",
  // },
];
// =================== NavBarList End here ======================
// =================== Special Offer data Start here ============
export const SplOfferData = [
  {
    _id: "201",
    img: spfOne,
    productName: "Stove",
    price: "35.00",
    color: "Blank and White",
    badge: true,
    des: "Short Desc",
  },
  {
    _id: "202",
    img: newArrFour,
    productName: "Air Fryer",
    price: "180.00",
    color: "Gray",
    badge: true,
    des: "Short Desc",
  },
  {
    _id: "203",
    img: spfThree,
    productName: "Microwave",
    price: "25.00",
    color: "Mixed",
    badge: true,
    des: "Short Desc",
  },
  {
    _id: "204",
    img: spfFour,
    productName: "Vacuum",
    price: "220.00",
    color: "Black",
    badge: true,
    des: "Short Desc",
  },
];
// =================== Special Offer data End here ==============

// =================== PaginationItems Start here ===============

export const paginationItems = [
  {
    _id: 1001,
    img: spfOne,
    productName: "Stove",
    price: "35.00",
    color: "Blank and White",
    badge: true,
    des: "Short Desc",
  },
  {
    _id: 1002,
    img: spfTwo,
    productName: "Air Fryer",
    price: "180.00",
    color: "Gray",
    badge: true,
    des: "Short Desc",
  },
  {
    _id: 1003,
    img: spfThree,
    productName: "Microwave",
    price: "25.00",
    color: "Mixed",
    badge: true,
    des: "Short Desc",
  },
  {
    _id: 1004,
    img: spfFour,
    productName: "Vacuum",
    price: "220.00",
    color: "Black",
    badge: true,
    des: "Short Desc",
  },
  {
    _id: 1005,
    img: bestSellerOne,
    productName: "Wardrobe",
    price: "35.00",
    color: "Blank and White",
    badge: true,
    des: "Short Desc",
  },
  {
    _id: 1006,
    img: bestSellerTwo,
    productName: "Chair",
    price: "180.00",
    color: "Gray",
    badge: false,
    des: "Short Desc",
  },
  {
    _id: 1007,
    img: bestSellerThree,
    productName: "Desk",
    price: "25.00",
    color: "Mixed",
    badge: true,
    des: "Short Desc",
  },
  {
    _id: 1008,
    img: bestSellerFour,
    productName: "Fan",
    price: "220.00",
    color: "Black",
    badge: false,
    des: "A bed is a furniture item used for sleeping or resting, providing a comfortable surface for relaxation and sleep.",
  },
  {
    _id: 1009,
    img: newArrOne,
    productName: "Bed",
    price: "44.00",
    color: "White",
    badge: true,
    des: "A bed is a furniture item used for sleeping or resting, providing a comfortable surface for relaxation and sleep.",
  },
  {
    _id: 1010,
    img: newArrTwo,
    productName: "Washing machine",
    price: "250.00",
    color: "White",
    badge: true,
    des: "A washing machine is an appliance used for cleaning clothes by automatically agitating them in water with detergent, rinsing, and spinning to remove excess water, offering convenience and efficiency in laundry tasks.",
  },
  {
    _id: 1011,
    img: newArrFour,
    productName: "Fridge",
    price: "80.00",
    color: "Mixed",
    badge: true,
    des: "A fridge, short for refrigerator, is a kitchen appliance designed to cool and preserve food by maintaining a low temperature, typically below the ambient room temperature, helping to extend the shelf life of perishable items and keep them fresh for longer periods of time.",
  },
  {
    _id: 1012,
    img: newArrThree,
    productName: "Television",
    price: "60.00",
    color: "Mixed",
    badge: false,
    des: "A television, often referred to as a TV, is an electronic device that receives broadcast signals or streams media content to display audio and visual information, offering entertainment, news, educational programming, and a range of audiovisual experiences for viewers.",
  },
  {
    _id: 1013,
    img: newArrTwo,
    productName: "Washing machine",
    price: "60.00",
    color: "Mixed",
    badge: false,
    des: "Short Desc",
  },

  {
    _id: 1014,
    img: newArrTwo,
    productName: "Washing machine",
    price: "250.00",
    color: "Black",
    badge: true,
    des: "Short Desc",
  },
  {
    _id: 1015,
    img: newArrFour,
    productName: "Fridge",
    price: "60.00",
    color: "Mixed",
    badge: false,
    des: "Short Desc",
  },
  {
    _id: 1016,
    img: newArrTwo,
    productName: "Washing machine",
    price: "250.00",
    color: "Black",
    badge: true,
    des: "Short Desc",
  },
  {
    _id: 1017,
    img: bestSellerFour,
    productName: "Travel Bag",
    price: "220.00",
    color: "Black",
    badge: false,
    des: "Short Desc",
  },
  {
    _id: 1018,
    img: newArrOne,
    productName: "Bed",
    price: "44.00",
    color: "Black",
    badge: true,
    des: "Short Desc",
  },
  {
    _id: 1019,
    img: newArrTwo,
    productName: "Washing machine",
    price: "250.00",
    color: "Black",
    badge: true,
    des: "Short Desc",
  },
  {
    _id: 1020,
    img: newArrThree,
    productName: "Television",
    price: "80.00",
    color: "Mixed",
    badge: true,
    des: "Short Desc",
  },
  {
    _id: 1021,
    img: spfThree,
    productName: "Microwave",
    price: "25.00",
    color: "Mixed",
    badge: true,
    des: "Short Desc",
  },
  {
    _id: 1022,
    img: spfFour,
    productName: "Vacuum",
    price: "220.00",
    color: "Black",
    badge: true,
    des: "Short Desc",
  },
  {
    _id: 1023,
    img: bestSellerOne,
    productName: "Flower Base",
    price: "35.00",
    color: "Blank and White",
    badge: true,
    des: "Short Desc",
  },
  {
    _id: 1024,
    img: spfOne,
    productName: "Stove",
    price: "35.00",
    color: "Blank and White",
    badge: true,
    des: "Short Desc",
  },
  {
    _id: 1025,
    img: spfTwo,
    productName: "Tea Table",
    price: "180.00",
    color: "Gray",
    badge: true,
    des: "Short Desc",
  },
  {
    _id: 1026,
    img: spfThree,
    productName: "Microwave",
    price: "25.00",
    color: "Mixed",
    badge: true,
    des: "Short Desc",
  },
  {
    _id: 1027,
    img: spfFour,
    productName: "Vacuum",
    price: "220.00",
    color: "Black",
    badge: true,
    des: "Short Desc",
  },
  {
    _id: 1028,
    img: bestSellerOne,
    productName: "Flower Base",
    price: "35.00",
    color: "Blank and White",
    badge: true,
    des: "Short Desc",
  },
  {
    _id: 1029,
    img: bestSellerTwo,
    productName: "New Backpack",
    price: "180.00",
    color: "Gray",
    badge: false,
    des: "Short Desc",
  },
  {
    _id: 1030,
    img: bestSellerThree,
    productName: "Household materials",
    price: "25.00",
    color: "Mixed",
    badge: true,
    des: "Short Desc",
  },
  {
    _id: 1031,
    img: bestSellerFour,
    productName: "Travel Bag",
    price: "220.00",
    color: "Black",
    badge: false,
    des: "Short Desc",
  },
  {
    _id: 1032,
    img: newArrOne,
    productName: "Bed",
    price: "44.00",
    color: "Black",
    badge: true,
    des: "Short Desc",
  },
  {
    _id: 1033,
    img: newArrTwo,
    productName: "Washing machine",
    price: "250.00",
    color: "Black",
    badge: true,
    des: "Short Desc",
  },
  {
    _id: 1034,
    img: newArrThree,
    productName: "Television",
    price: "80.00",
    color: "Mixed",
    badge: true,
    des: "Short Desc",
  },
  {
    _id: 1035,
    img: newArrFour,
    productName: "Fridge",
    price: "60.00",
    color: "Mixed",
    badge: false,
    des: "Short Desc",
  },
  {
    _id: 1036,
    img: newArrTwo,
    productName: "Fridge",
    price: "60.00",
    color: "Mixed",
    badge: false,
    des: "Short Desc",
  },
  {
    _id: 1037,
    img: newArrFour,
    productName: "Fridge",
    price: "60.00",
    color: "Mixed",
    badge: false,
    des: "Short Desc",
  },
  {
    _id: 1038,
    img: newArrTwo,
    productName: "Washing machine",
    price: "250.00",
    color: "Black",
    badge: true,
    des: "Short Desc",
  },
  {
    _id: 1039,
    img: bestSellerFour,
    productName: "Travel Bag",
    price: "220.00",
    color: "Black",
    badge: false,
    des: "Short Desc",
  },
  {
    _id: 1040,
    img: newArrOne,
    productName: "Bed",
    price: "44.00",
    color: "Black",
    badge: true,
    des: "Short Desc",
  },
  {
    _id: 1041,
    img: newArrTwo,
    productName: "Washing machine",
    price: "250.00",
    color: "Black",
    badge: true,
    des: "Short Desc",
  },
  {
    _id: 1042,
    img: newArrThree,
    productName: "Television",
    price: "80.00",
    color: "Mixed",
    badge: true,
    des: "Short Desc",
  },
  {
    _id: 1043,
    img: spfThree,
    productName: "Microwave",
    price: "25.00",
    color: "Mixed",
    badge: true,
    des: "Short Desc",
  },
  {
    _id: 1044,
    img: spfFour,
    productName: "Sun glasses",
    price: "220.00",
    color: "Black",
    badge: true,
    des: "Short Desc",
  },
  {
    _id: 1045,
    img: bestSellerOne,
    productName: "Flower Base",
    price: "35.00",
    color: "Blank and White",
    badge: true,
    des: "Short Desc",
  },
  {
    _id: 1046,
    img: spfOne,
    productName: "Stove",
    price: "35.00",
    color: "Blank and White",
    badge: true,
    des: "Short Desc",
  },
];
// =================== PaginationItems End here =================
