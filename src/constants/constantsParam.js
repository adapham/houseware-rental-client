export const regexPhoneNumber = /^(09|03|07|08|05)?([0-9]{8})\b$/;
export const regexEmail =  /^([a-zA-Z0-9_-]+)@([a-zA-Z0-9-]+\.?[a-zA-Z0-9-]+\.?[a-zA-Z0-9-]+)\.?[a-zA-Z0-9-]+\.?[a-zA-Z0-9-]+$/;