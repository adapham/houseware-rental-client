import axios from "axios";

const API = {
  request: (config) => {
    const {
      method = "GET",
      url,
      params,
      baseURL = process.env.REACT_APP_BASE_URL,
    } = config;

    let requestConfig = {
      method,
      url,
      baseURL,
      headers: {
        "Content-Type": "application/json",
        // Authorization: `Bearer ${accessToken}`,
      },
      timeout: 20000, //timeout error message 20s.
      timeoutErrorMessage: "Quá thời gian chờ dịch vụ",
    };

    const accessToken = localStorage.getItem("access-token");
    if (accessToken) {
      const newHeader = {
        Authorization: `Bearer ${accessToken}`,
      };
      requestConfig = {
        ...requestConfig,
        headers: {
          ...requestConfig.headers,
          ...newHeader,
        },
      };
    }

    if (params) {
      if (typeof method === "string" && method.toLowerCase().trim() === "get") {
        requestConfig.params = params;
      } else {
        requestConfig.data = params;
      }
    }
    return axios(requestConfig)
      .then((response) => {
        return response?.data;
      })
      .catch((e) => {
        const error = e?.response?.data || {};
        return Promise.reject({ ...error, message: error.error });
      });
  },
};

export default API;
