export const formatToMoney = (input) => {
  if(input == null){
    return 0;
  }
  return input.toLocaleString("vi-VN", { style: "currency", currency: "VND" });
};
