import httpRequest from "../redux/apiConfig";

const BASE_URL = process.env.REACT_APP_BASE_URL;

const request = (options) => {
    const headers = new Headers();

    if (options.setContentType !== false) {
        headers.append("Content-Type", "application/json");
    }

    if (localStorage.getItem("access-token")) {
        headers.append(
            "Authorization",
            "Bearer " + localStorage.getItem("access-token")
        );
    }

    const defaults = {headers: headers};
    options = Object.assign({}, defaults, options);
    return fetch(options.url, options)
        .then((response) =>
            response.json().then((json) => {
                if (!response.ok) {
                    return Promise.reject(json);
                }
                return json;
            })
        );
};

export function countNewMessages(senderId, recipientId) {
    const url = BASE_URL + "/api/chat/messages/" + senderId + "/" + recipientId + "/count";
    return httpRequest.get(url, {}, {})
}

//Get All User ById
// export function getAllUsersBySenderId(senderId) {
//     const url = BASE_URL + "/api/chat/messages/" + senderId + "/users";
//     return httpRequest.get(url)
// }
export const getAllUsersBySenderId = async (senderId) => {
    try {
        const endpoint = "api/chat/messages/" + senderId + "/users";
        const res = await httpRequest.getToken(endpoint);
        return res;
    } catch (err) {
        console.log(err);
    }
};

export function findChatMessages(senderId, recipientId) {
    const url = BASE_URL + "/api/chat/messages/" + senderId + "/" + recipientId;
    return httpRequest.get(url)
}

export function findChatMessage(id) {
    const url = BASE_URL + "/api/chat/messages/" + id;
    return httpRequest.get(url)
}

//Get User Hiện tại
export function getCurrentUser() {
    if (!localStorage.getItem("access-token")) {
        return Promise.reject("No access token set.");
    }
    return request({
        url: BASE_URL + "/api/users/profile",
        method: "GET",
    });
}

//Get All User
export function getUsers() {
    if (!localStorage.getItem("accessToken")) {
        return Promise.reject("No access token set.");
    }

    return request({
        url: BASE_URL + "/api/users",
        method: "GET",
    });
}

export const getAllUsers = async () => {
    try {
        const param = {
            status: "UNBAN"
        }
        const res = await httpRequest.getToken("api/users",param);
        return res;
    } catch (err) {
        console.log(err);
    }
};