
export const formatDate = (dateString)=> {
  const date = new Date(dateString);
  const options = { day: '2-digit', month: 'short', year: 'numeric' };
  const formattedDate = date.toLocaleDateString('en-US', options);
  return formattedDate;
}

export const convertToTimestamps=(timestampString)=> {
  // Split the timestamp string into its components.
  const timestampComponents = timestampString.split(/[-T:]/);
  // Create a new Date object from the timestamp components.
  const timestampDate = new Date(timestampComponents[0], timestampComponents[1] - 1, timestampComponents[2], timestampComponents[3], timestampComponents[4], timestampComponents[5], timestampComponents[6]);
  // Return the timestamp in milliseconds.
  return timestampDate.getTime();
}
export const convertMoney=(number)=>{
  if(number == null){
    return 0;
  }
    return number.toLocaleString("vi-VN", {
        currency: "VND",
        useGrouping: true,
      });
}
export const isTimestampWithinOneWeek=(timestamp)=> {
    // Lấy ra thời gian hiện tại.
    const now = new Date();
    // So sánh timestamp với thời gian hiện tại.
    const delta = now.getTime() - timestamp;
    // Trả về kết quả.
    return delta < (7 * 24 * 60 * 60 * 1000);
  }

  export const  convertDateFormToTimestamp=(dateString)=> {
    const date = new Date(dateString);
    const timestamp = date.getTime();
    return timestamp;
  }

  export const convertToDate = (dateString) => {
    const date = new Date(dateString);
  
    if (isNaN(date)) {
      return ''; // Return an empty string if the input string is not a valid date
    }
  
    const day = String(date.getDate()).padStart(2, '0');
    const month = String(date.getMonth() + 1).padStart(2, '0');
    const year = date.getFullYear();
  
    return `${day}/${month}/${year}`;
  };

  export const convertTimestamp =(timestamp)=> {
    // Chuyển timestamp về dạng số nguyên
    const timestampAsNumber = parseInt(timestamp, 10);
    // Tạo chuỗi timestamp mới với độ dài 13 chữ số
    const timestampIn13Digits = `${timestampAsNumber}`.padStart(13, "0");
    return timestampIn13Digits;
  }