import {createSlice} from "@reduxjs/toolkit";

const authSlide = createSlice({
    name: "auth2",
    initialState: {
        login: {
            accessToken: null,
            error: false,
        },
        register: {
            error: false,
            success: false
        },
        loadUser: {
            user: null,
            error: false,
            success: false
        }
    },
    reducers: {
        loginSuccess: (state, action) => {
            localStorage.setItem("access-token", action.payload);
        },
        loginFailed: (state) => {
            state.login.error = true;
        },
        registerSuccess: (state) => {
            state.register.success = true;
        },
        registerFailed: (state) => {
            state.register.error = true;
        },
        loadUserSuccess: (state, action) => {
            state.loadUser.success = true;
            state.loadUser.user = action.payload;
        },
        updateUser:(state,action)=>{

            state.loadUser.user = action.payload;
        },
        loadUserFailed: (state) => {
            state.loadUser.error = true;
        }
    }
});

export const {
    loginSuccess,
    loginFailed,
    registerSuccess,
    registerFailed,
    loadUserSuccess,
    updateUser
} = authSlide.actions;

export default authSlide.reducer
