import httpRequest from "./apiConfig";
import {loadUserSuccess, loginFailed, loginSuccess, registerFailed, registerSuccess} from "./authSlide";

const BASE_URL = process.env.REACT_APP_BASE_URL;
const API_URL = `${BASE_URL}/api/auth/`;

export const login = async (loginRequest, dispatch) => {
    try {
        const res = httpRequest.post(API_URL + "signin", loginRequest)
        const response = await res;
        console.log(response.accessToken);
        dispatch(loginSuccess(response.accessToken))
        const resUser = httpRequest.getToken("api/users/profile")
        const responseUser = await resUser;
        dispatch(loadUserSuccess(responseUser));
        localStorage.setItem("user", JSON.stringify(responseUser));
    } catch (err) {
        dispatch(loginFailed())
    }
}

export const loginGG = async () => {
    try {
        const res = httpRequest.get(`${BASE_URL}/oauth2/authorization/google`)
        const response = await res;

    } catch (err) {
        console.log(err)
    }
}

export const register = async (registerRequest, dispatch, navigate) => {
    try {
        await httpRequest.post(API_URL + "signup", registerRequest);
        dispatch(registerSuccess())
        navigate("/signin")
    } catch (err) {
        dispatch(registerFailed())
    }
}