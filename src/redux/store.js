import {combineReducers, configureStore} from "@reduxjs/toolkit";
import authReducer from "./Slide/authSlide"
import {FLUSH, PAUSE, PERSIST, persistReducer, persistStore, PURGE, REGISTER, REHYDRATE,} from "redux-persist";
import storage from "redux-persist/lib/storage";
import orebiReducer from "./Slide/orebiSlice";
import productReducer from "./Slide/productSlice";
import productUserReducer from "./Slide/productUserSlide";
import cateoryReducer from "./Slide/categorySlice";
import authSlide from "./authSlide";

const persistConfig = {
    key: "root",
    version: 1,
    blacklist: ["product"],
    storage,
};

const rootReducer = combineReducers({
    orebiReducer: orebiReducer,
    product: productReducer,
    productUser: productUserReducer,
    category: cateoryReducer,
    auth: authReducer,
    auth2:authSlide
});
const persistedReducer = persistReducer(persistConfig, rootReducer);

export const store = configureStore({
    reducer: persistedReducer,
    middleware: (getDefaultMiddleware) =>
        getDefaultMiddleware({
            serializableCheck: {
                ignoredActions: [FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER],
            },
        }),
});

export let persistor = persistStore(store);
