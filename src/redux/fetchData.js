import httpRequest from "./apiConfig";

export const fetchUser = async () => {
    const token = localStorage.getItem("access-token");
    if (token) {
        try {
            return await httpRequest.getToken('api/users/profile');
        } catch (error) {
        }
    }
};