import {createSlice} from "@reduxjs/toolkit";

const userSlide = createSlice({
    name: "user",
    initialState: {
        user: {
            profile: null,
            error: false
        }

    },
    reducers: {
        getUserSuccess: (state, action) => {
            state.user.profile = action.payload;
        },
        getUserFailed: (state) => {
            state.user.error = true;
        }
    }
});

export const {
    getUserSuccess,
    getUserFailed
} = userSlide.actions;

export default userSlide.reducer
