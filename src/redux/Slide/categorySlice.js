import { createSlice } from "@reduxjs/toolkit";

const categorySlice = createSlice({
    name:"category",
    initialState:{
        isFetchinng: false,
        categories:[],
    },
    reducers:{
        getCategories:(state,action)=>{
            state.isFetchinng=true;
            state.categories=action.payload;
        },
        getCategoriesSuccess:(state,action)=>{
            state.isFetchinng=false;
            state.categories=action.payload;
        }
    }
})
export const{getCategories,getCategoriesSuccess}=categorySlice.actions;
export default categorySlice.reducer;