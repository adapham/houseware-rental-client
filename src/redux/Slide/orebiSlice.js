import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  userInfo: [],
  products: [],
};

export const orebiSlice = createSlice({
  name: "orebi",
  initialState,
  reducers: {
    addToCart: (state, action) => {

      const item = state.products.find(
        (item) => item?.productId === action.payload.productId
      );
      if (item) {
        item.quantity = action.payload.quantity;
        item.rentalPeriod = action.payload.rentalPeriod;
      } else {
        state.products.push(action.payload);
      }
    },
    increaseQuantity: (state, action) => {
      
      const item = state.products.find(
        (item) => item?.productId === action.payload.productId
      );
      console.log(state.products)

      if (item) {
        item.quantity++;
      }
    },
    decreaseQuantity: (state, action) => {
      const item = state.products.find(
        (item) => item?.productId === action.payload.productId
      );
        if(item){
          if (item?.quantity === 1) {
            item.quantity = 1;
          } else {
            item.quantity--;
          }
        }
      
    },
    increasePeriod: (state, action) => {
      const item = state.products.find(
        (item) => item?.productId === action.payload.productId
      );


      if (item) {
        item.rentalPeriod++;
      }
    },
    decreasePeriod: (state, action) => {
      const item = state.products.find(
        (item) => item?.productId === action.payload.productId
      );
      if(item){
        if (item?.rentalPeriod === 1) {
          item.rentalPeriod = 1;
        } else {
          item.rentalPeriod--;
        }
      }
     
    },
    deleteItem: (state, action) => {
      state.products = state.products.filter(
        (item) => item?.productId !== action.payload
      );
    },
    resetCart: (state) => {
      state.products = [];
    },
  },
});

export const {
  addToCart,
  increaseQuantity,
  decreaseQuantity,
  increasePeriod,
  decreasePeriod,
  deleteItem,
  resetCart,
} = orebiSlice.actions;
export default orebiSlice.reducer;
