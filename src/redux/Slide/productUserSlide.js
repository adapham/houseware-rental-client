import { createSlice } from "@reduxjs/toolkit";

const productUserSlide = createSlice({
    name: "productUser",
    initialState: {
        isFetchinng: false,
        products: [],
        pageable: {
            pageIndex: 1,
            pageSize: 5,
        },
        filters: {
            rentalDate: {
                from: null,
                to: null,
            },
            keyword: null,
        },
    },
    reducers: {
        updateProductUser: (state, action) => {
            state.products = action.payload;
        },
        updateFilter: (state, action) => {
            state.filters = { ...state.filters, ...action.payload };

        },
        emptyFilter: (state, action) => {
            state.filters = {};
        },
        updatePageable: (state, action) => {
            state.pageable = { ...state.pageable, ...action.payload };
        },
    },
});

export const {
    updateProduct,
    updatePageble,
    updateFilter,
    emptyFilter,
} = productUserSlide.actions;

export default productUserSlide.reducer;
