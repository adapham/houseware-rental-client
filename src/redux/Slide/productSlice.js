import { createSlice } from "@reduxjs/toolkit";

const productSlice = createSlice({
  name: "product",
  initialState: {
    isFetchinng: false,
    products: [],
    pageable: {
      pageIndex: 1,
      pageSize: 12,
    },
    filters: {
      categoryId: null,
      price: {
        from: null,
        to: null,
      },
      sortby: "BEST_SELLER",
      rentalDate: {
        from: null,
        to: null,
      },
      keyword: null,
      userId:null
    },
  },
  reducers: {
    getProducts: (state, action) => {
      state.isFetchinng = true;
    },
    getProductsSuccess: (state, action) => {
      state.isFetchinng = false;
    },
    getProductsFailed: (state, action) => {
      state.isFetchinng = false;
    },
    updateProduct: (state, action) => {
      state.products = action.payload;
    },
    updateFilter: (state, action) => {
      state.filters = { ...state.filters, ...action.payload };

    },
    emptyFilter:(state,action)=>{     
      state.filters ={};
    },
    updatePageble: (state, action) => {
      state.pageable = { ...state.pageable,...action.payload};
    },
  },
});

export const {
  getProductsStart,
  getProductsSuccess,
  getProductsFailed,
  updateProduct,
  updatePageble,
  updateFilter,
  emptyFilter,
} = productSlice.actions;

export default productSlice.reducer;
