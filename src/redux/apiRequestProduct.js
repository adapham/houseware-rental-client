import axios from "axios";

import httpRequest from "./apiConfig";


const accessToken = localStorage.getItem("access-token");

const url = process.env.REACT_APP_BASE_URL + "/api/";

export const getAllProduct = async (prop) => {

  try {
   
    const param ={
      pageIndex:prop?.pageIndex,
      pageSize:prop?.pageSize,
      categoryId:prop?.categoryId,
      rentalPeriodFrom:prop?.rentalDate?.from,
      rentalPeriodTo:prop?.rentalDate?.to,
      priceFrom:prop?.price?.from,
      priceTo:prop?.price?.to,
      sortBy:prop?.sortby,
      keyword:prop?.keyword
    }
    
    

    const res=httpRequest.get("products",param);
   
    return res;
  } catch (err) {
    console.log(err);
  }
};

export const getAllCateory = async () => {
  try {
    const res = await axios.get(url + "categories");
    return res.data;
  } catch (err) {
    console.log(err);
  }
};

export const getProductById = async (id) => {
  try {
    const res = await axios.get(url + "products/"+id);

    return res.data;
  } catch (err) {
    console.log(err);
  }
};

export const getProvince = async () => {
  try {
    const res = await httpRequest.get(url+"province");
    return res;
  } catch (err) {
    console.log(err);
  }
};
export const getUser = async (id) => {
  try {
    const res = await httpRequest.get(url+"users/"+id);
    return res;
  } catch (err) {
    console.log(err);
  }
};
export const getDistrictByProvince = async (id) => {
  try {
    const res = await axios.get(url + "district/"+id);

    return res.data;
  } catch (err) {
    console.log(err);
  }
};
export const getWardByDistrict = async (id) => {
  try {
    const res = await axios.get(url + "ward/"+id);

    return res.data;
  } catch (err) {
    console.log(err);
  }
};

export const getCategories = async () => {
  try {
    const res = await axios.get(url + "categories");

    return res.data;
  } catch (err) {
    console.log(err);
  }
};

export const getReviewsProduct = async (id)=>{
  try {
    const res = await axios.get(url + "reviews/product/"+id);
    return res.data;
  } catch (err) {
    console.log(err);
  }
}

export const getNewArrivals = async ()=>{
  try{
      const res =await axios.get(url + "products/newArrival");
      
      return res.data;
  }catch(err){
    console.log(err);
  }
}
export const getTopSellers = async ()=>{
  try{
      const res =await axios.get(url + "products/topSeller");
      return res.data;
  }catch(err){
    console.log(err);
  }
}
export const getMinMaxPrice = async ()=>{
  try{
      const res =await axios.get(url + "products/price");
      return res.data;
  }catch(err){
    console.log(err);
  }
}

export const getRelateProducts = async (categoryId)=>{
  try{
      // const res =httpRequest.get(url+"products/related",{categoryId:categoryId});
      const res =await axios.get(url + `products/related?categoryId=${categoryId}`); 

      return res.data;
  }catch(err){
    console.log(err);
  }
}

export const addProductToCard = async (product)=>{

  const params ={
    title: product.title.trim(),
    imageText: product.imageText.trim(),
    price: product.price,
    depositPrice: product.depositPrice,
    rentalPeriod: product.rentalPeriod,
    productId: product.productId,
    userId: product.userId,
    quantity:product?.quantity || 1
  }
  try{
      const res =await httpRequest.postToken("api/carts",params);

      return res;
  }catch(err){
    console.log(err);
  }
}
export const addProductToCardHome = async (product)=>{

  const params ={
    title: product.title.trim(),
    imageText: product.imageText.trim(),
    price: product.price,
    depositPrice: product.depositPrice,
    rentalPeriod: product.rentalPeriod,
    productId: product.productId,
    userId: product.userId,
    quantity:product?.quantity || 1
  }
  try{
      const res =await httpRequest.postToken("api/carts/home",params);

      return res;
  }catch(err){
    console.log(err);
  }
}

export const updateProductInCard = async (id,product)=>{
  try{
      const res =await httpRequest.putToken("api/carts/"+id,product);
      return res;
  }catch(err){
    console.log(err);
  }
}

export const removeProductInCard = async (id)=>{
  try{
      const res =await httpRequest.deleteWithToken("api/carts/"+id);
      return res;
  }catch(err){
    console.log(err);
  }
}
export const removeAllProductInCard = async (userId)=>{
  try{
    console.log(userId,"ggg");
      const res =await httpRequest.deleteWithToken("api/carts/"+userId+"/user");
      return res;
  }catch(err){
    console.log(err);
  }
}

export const getProductsInCard = async (id)=>{
  try{
      const res =await httpRequest.getToken("api/carts/"+id+"/user");
      return res;
  }catch(err){
    console.log(err);
  }
}
export const getCheckoutSubmit = async (infor,items)=>{

  var today = new Date();
  try{
    const itemProcducts = items.map((item)=> ({
      cartId:item?.id,
      price: item?.price,
      quantity: item?.quantity,
      expiredDate: (new Date(today.getTime() - item?.rentalPeriod *30 * 24 * 60 * 60 * 1000)).toISOString(),
      rentalPeriod: item?.rentalPeriod,
      depositPrice: item?.depositPrice,
      productId: item?.productId,
      storeId:item?.storeId
    }))
    
    const params = {

        totalprice:infor?.totalPrice,
        totalDepositPrice: infor?.totalDepositPrice,
        shipAddress:infor?.address.trim(),
        type:infor?.typePayment.trim(),
        phone: infor?.phone.trim(),
        fullName: infor?.fullName.trim(),
        email: infor?.email.trim(),
        userId: infor?.userID,
        orderDetailReqs: itemProcducts ?? null
    }
      const res =await httpRequest.postToken("api/orders/checkout",params);
      return res;
  }catch(err){
    console.log(err);
  }
}

export const getRechargeAndWithdraw = async (infor,type)=>{
  try{
    var params = null;
    if(type ==="CREDIT"){
      params={
        typePayment: infor?.typePayment.trim(),
        creditReq: {
          userId: infor?.userId,
          money: infor?.money
        }
      }
    }else if(type==="DEBIT"){
      params={
        typePayment: infor?.typePayment.trim(),
        debitReq: {
          userId: infor?.userId,
          money: infor?.money
        }
      }
    }


      const res =await httpRequest.postToken("api/transactions/submitOrder",params);
      return res;
  }catch(err){
    console.log(err);
  }
}

export const getHistoryTransaction = async (userID,infor) => {
  try {
    const param = {
      fromDate: infor?.fromDate,
      toDate: infor?.toDate,
      type: infor?.type.trim()
    }
    const res = await httpRequest.get(url+"transactions/"+userID+"/user",param);
    return res;
  } catch (err) {
    console.log(err);
  }
};

export const getCommentReview = async (infor)=>{
  try{
    const params = {
        productId:infor?.productId,
        userId: infor?.userId,
        content: infor?.content.trim(),
        rating: infor?.rating
    }
      const res =await httpRequest.postToken("api/reviews",params);
      return res;
  }catch(err){
    console.log(err);
  }
}
export const getOrderLessor = async (userID,infor,status) => {
  try {
  
    const param = {
      fromDate: infor?.dateFrom ,
      toDate: infor?.dateTo,
      types: status.join(',')
    }
    const res = await httpRequest.getToken("api/orders/"+userID+"/lessor",param);
    return res;
  } catch (err) {
    console.log(err);
  }
};
export const getOrderLessee = async (userID,infor) => {
  try {
    const param = {
      fromDate: infor?.dateFrom ? infor?.dateFrom :'',
      toDate: infor?.dateTo ? infor?.dateTo : ''
    }
    
    const res = await httpRequest.getToken("api/orders/"+userID+"/lessee",param);
    return res;
  } catch (err) {
    console.log(err);
  }
};

export const getHistoryOrdersLessor= async (userID,infor) => {
  try {
    const param = {
      fromDate: infor?.dateFrom ? infor?.dateFrom :'',
      toDate: infor?.dateTo ? infor?.dateTo : ''
    }
    
    const res = await httpRequest.getToken("api/orders/"+userID+"/history-lessor",param);
    return res;
  } catch (err) {
    console.log(err);
  }
};

export const getHistoryOrdersLessee= async (userID,infor) => {
  try {
    const param = {
      fromDate: infor?.dateFrom ? infor?.dateFrom :'',
      toDate: infor?.dateTo ? infor?.dateTo : ''
    }
    const res = await httpRequest.getToken("api/orders/"+userID+"/history-lessee",param);
    return res;
  } catch (err) {
    console.log(err);
  }
};

export const getProductOfUser = async (userID,infor) => {
  try {
    const param = {
      keyword: infor.keyword,
      pageIndex:infor?.pageIndex,
      pageSize:infor?.pageSize,
    }
    const res = await httpRequest.getTokenParam("api/products/"+userID+"/user",param);
    return res;
  } catch (err) {
    console.log(err);
  }
};

export const getOrderSuccess = async (id,userId)=>{
  try{
      const res =await httpRequest.putToken("api/orders/"+id+"/success?userId="+userId);
      return res;
  }catch(err){
    console.log(err);
  }
}
export const getOrderDelete= async (id, content)=>{
  try{
      const param={
        reason: content.trim()
      }
      
      const res =await httpRequest.putToken("api/orders/"+id+"/cancel", param);
      return res;
  }catch(err){
    console.log(err);
  }
}
export const getOrderApprove= async (id)=>{
  try{
      const res =await httpRequest.putToken("api/orders/"+id+"/approve");
      return res;
  }catch(err){
    console.log(err);
  }
}
export const getOrderDone= async (id)=>{
  try{
      const res =await httpRequest.putToken("api/orders/"+id+"/done");
      return res;
  }catch(err){
    console.log(err);
  }
}

export const getReport= async (id,userId,content,type)=>{
  try{
    const param={
      content:content,
      type:type
    }
      const res =await httpRequest.post("requests/"+id+"/report/"+userId+"/user",param,{Authorization: `Bearer ${accessToken}`});
      return res;
  }catch(err){
    console.log(err);
  }
}
export const getReviewDelete= async (id)=>{
  try{
      const res =await httpRequest.deleteWithToken("api/reviews/"+id);
      return res;
  }catch(err){
    console.log(err);
  }
}
export const getReviewEdit= async (id,infor)=>{
  try{
    const param={
      productId: infor.productId,
      userId: infor.userID,
      content: infor.content.trim(),
      rating: infor.rating
    }
      const res =await httpRequest.putToken("api/reviews/"+id,param);
      return res;
  }catch(err){
    console.log(err);
  }
}

export const getUpdateAvatar= async (id,infor)=>{
  try{
    
      const res =await httpRequest.putToken("api/users/"+id+"/avatar",infor);
      return res;
  }catch(err){
    console.log(err);
  }
}

export const getRequest = async (infor)=>{
  try{
    
      const res =await httpRequest.postToken("api/requests",infor);
      return res;
  }catch(err){
    console.log(err);
  }
}

export const getDetailStore = async (userID) => {
  try {
    const res = await httpRequest.getToken("api/stores/"+userID+"/user");
    return res;
  } catch (err) {
    console.log(err);
  }
};

export const getOrderStatusLessor = async (userID) => {
  try {
    const res = await httpRequest.getToken("api/orders/"+userID+"/status-lessor");
    return res;
  } catch (err) {
    console.log(err);
  }
};
export const getOrderStatusLessee = async (userID) => {
  try {
    const res = await httpRequest.getToken("api/orders/"+userID+"/status-lessee");
    return res;
  } catch (err) {
    console.log(err);
  }
};
export const getStoreByUser= async (userID) => {
  try {
    const res = await httpRequest.getToken("api/stores/"+userID+"/user");
    return res;
  } catch (err) {
    console.log(err);
  }
};
export const getTotalTransactions = async (userID) => {
  try {
    const res = await httpRequest.getToken("api/transactions/"+userID+"/total");
    return res;
  }catch(err){
    console.log(err);
  }
};

export const getUpdateStore= async (id,infor)=>{
  try{
    
      const res =await httpRequest.putToken("api/stores/"+id,infor);
      return res;
  }catch(err){
    console.log(err);
  }
}
export const getTransactionWithDraw = async (userId)=>{
    try{
      const res =await httpRequest.getToken("api/transactions/"+userId+"/withdraw");
      return res;
    }catch(err){
      console.log(err);
    }
}
export const getStatusRequest = async (userId)=>{
  try{
    const res =await httpRequest.getToken("api/stores/"+userId+"/user/status");
    return res;
  }catch(err){
    console.log(err);
  }
}
export const getStaticDashboard = async (storeId)=>{
  try{
    const res =await httpRequest.getToken("api/stores/"+storeId+"/dashboard");
    return res;
  }catch(err){
    console.log(err);
  }
}
export const getStaticDashbordTotalOrder = async (storeId,type)=>{
  try{

    const param = {type :type}
    const res =await httpRequest.getToken("api/stores/"+storeId+"/dashboad-order",param);
    return res;
  }catch(err){
    console.log(err);
  }
}
export const getStaticDashbordIncome = async (storeId,type)=>{
  try{
    const param = {type :type}
    const res =await httpRequest.getToken("api/stores/"+storeId+"/dashboard-money",param);
    return res;
  }catch(err){
    console.log(err);
  }
}
export const getConfirmReturn = async (orderDetailId)=>{
  try{
    const res =await httpRequest.putToken("api/orders/"+orderDetailId+"/returns");
    return res;
  }catch(err){
    console.log(err);
  }
}

export const getTransferMoney= async (id,infor)=>{
  try{
    
      const res =await httpRequest.putToken(`api/stores/${id}/transfer-money`,infor);
      return res;
  }catch(err){
    console.log(err);
  }
}

export const getStaticTotalRented = async (storeId)=>{
  try{
    const res =await httpRequest.getToken("api/stores/"+storeId+"/dashboard-rented");
    return res.totalProductCategory;
  }catch(err){
    console.log(err);
  }
}

export const getStaticAdmin = async ()=>{
  try{
    const res =await httpRequest.getToken("api/admin/dashbroad");
    
    return res;
  }catch(err){
    console.log(err);
  }
}
export const getStaticCategoryAdmin = async ()=>{
  try{
    const res =await httpRequest.getToken("api/admin/dashboard-categorry");
    
    return Object.entries(res.productOfCategory);
  }catch(err){
    console.log(err);
  }
}
export const getStaticUserRented = async (storeId,type)=>{
  try{
    const param = {type :type}
    const res =await httpRequest.getToken("api/stores/"+storeId+"/dashboard-user-rented",param);
    return res;
  }catch(err){
    console.log(err);
  }
}
export const getStaticUserAdmin = async (type)=>{
  try{
    const param = {type :type}
    const res =await httpRequest.getToken("api/admin/dashboard-user",param);
    return res;
  }catch(err){
    console.log(err);
  }
}

export const getStaticAdminIncome = async (type)=>{
  try{
    const param = {type :type}
    const res =await httpRequest.getToken("api/admin/dashboard-income",param);
    return res;
  }catch(err){
    console.log(err);
  }
}
export const getNotification = async (id)=>{
  try{
    const res =await httpRequest.getToken(`api/users/${id}/notifications`);
    return res;
  }catch(err){
    console.log(err);
  }
}
export const exportProductByUser = async (id,keyword)=>{
  try{
    window.location.href = `${url}products/${id}/export?keyword=${keyword}`;
  }catch(err){
    console.log(err);
  }
}

export const exportTransaction= async (id,type,toDate,fromDate)=>{
  try{
    
    window.location.href = `${url}transactions/${id}/export?fromDate=${fromDate}&toDate=${toDate}&type=${type}`;
  }catch(err){
    console.log(err);
  }
}

export const exportOrderByUser = async (id)=>{
  try{
    window.location.href = `${url}order/${id}/export`;
  }catch(err){
    console.log(err);
  }
}

export const getUserProfile = async ()=>{
  try{
    const res =await httpRequest.getToken("api/users/profile");
    return res;
  }catch(err){
    console.log(err);
  }
}

export const getUpdateIsRead= async (id)=>{
  try{
    
      const res =await httpRequest.putToken(`api/users/${id}/is-read`);
      return res;
  }catch(err){
    console.log(err);
  }
}

export const getOrderConfirm= async (id,userId)=>{
  try{
      const res =await httpRequest.putToken("api/orders/"+id+"/confirm?userId="+userId);
      return res;
  }catch(err){
    console.log(err);
  }
}