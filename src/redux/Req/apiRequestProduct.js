import axios from "axios";

import httpRequest from "./apiConfig";

const BASE_URL = process.env.REACT_APP_BASE_URL;
const API_URL = `${BASE_URL}/api/`;

export const getAllProduct = async (prop) => {
  try {
    
    const param ={
      pageIndex:prop?.pageIndex,
      pageSize:prop?.pageSize,
      categoryId:prop?.categoryId,
      rentalPeriodFrom:prop?.rentalDate?.from,
      rentalPeriodTo:prop?.rentalDate?.to,
      priceFrom:prop?.price?.from,
      priceTo:prop?.price?.to,
      sortBy:prop?.sortby,
      userId:prop?.userId
    }
    // const res = await axios.get(url + `products?pageIndex`);
    const res=httpRequest.get("products",param);
    return res;
  } catch (err) {
    console.log(err);
  }
};

export const getAllCateory = async () => {
  try {
    const res = await axios.get(API_URL + "categories");
    return res.data;
  } catch (err) {
    console.log(err);
  }
};

export const getProductById = async (id) => {
  try {
    const res = await axios.get(API_URL + "products/"+id);

    return res.data;
  } catch (err) {
    console.log(err);
  }
};