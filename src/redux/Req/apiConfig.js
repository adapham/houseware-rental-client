import axios from 'axios';

const BASE_URL = process.env.REACT_APP_BASE_URL;

export const instanceCTDRequest = axios.create({
    baseURL: BASE_URL + "/api/",
    responseType: 'json',
    withCredentials: true,
});
export default class httpRequest {
    static post(url, params = {}, headers = {}) {
        return instanceCTDRequest
            .post(url, {...params}, {headers})
            .then(res => {
                if (res.data) {
                    var resJSON = null;
                    if (res.data.data) {
                        resJSON = JSON.stringify(res.data.data);
                    }
                    if (
                        resJSON != null &&
                        resJSON?.error &&
                        resJSON?.error == 'invalid_token'
                    ) {
                        //Alert error here
                    }
                    return res.data;
                } else {
                    return {};
                }
            })
            .catch(error => {

                console.log('error', JSON.stringify(error));
                return error?.response;
            });
    }

    static get(url, params = {}, headers = {}) {
        return instanceCTDRequest
            .get(url, {
                headers: headers ? headers : {},
                params: params ? params : {},
            })
            .then(res => {
                if (res.data) {
                    return res.data;
                } else {
                    return {};
                }
            })
            .catch(error => {
                return error?.response;
            });
    }

    static delete(url, params = {}, headers = {}) {
        return instanceCTDRequest
            .delete(url, {
                headers: headers ? headers : {},
                params: params ? params : {},
            })
            .then(res => {

                if (res.data) {
                    return res.data;
                } else {
                    return {};
                }
            })
            .catch(error => {
                return error?.response;
            });
    }

    static postToken(url, params) {
        const accessToken = localStorage.getItem("access-token");
        return instanceCTDRequest
            .post(`${BASE_URL}/` + url, params, {Authorization: `Bearer ${accessToken}`})
            .then(res => {
                if (res.data) {
                    var resJSON = null;
                    if (res.data.data) {
                        resJSON = JSON.stringify(res.data.data);
                    }
                    if (
                        resJSON != null &&
                        resJSON?.error &&
                        resJSON?.error == 'invalid_token'
                    ) {
                        // Redirect to /signin page here
                        window.location.href = '/signin';
                    }
                    return res.data;
                } else {
                    return {};
                }
            })
            .catch(error => {

                console.log('error', JSON.stringify(error));
                return error?.response;
            });
    }


    static postFormDataToken(url, formData = new FormData()) {
        const accessToken = localStorage.getItem("access-token");
        return instanceCTDRequest
            .post(`${BASE_URL}/` + url, formData, {
                Authorization: `Bearer ${accessToken}`,
            })
            .then(res => {
                if (res.data) {
                    var resJSON = null;
                    if (res.data.data) {
                        resJSON = JSON.stringify(res.data.data);
                    }
                    if (
                        resJSON != null &&
                        resJSON?.error &&
                        resJSON?.error == "invalid_token"
                    ) {
                        //Alert error here
                    }
                    return res.data;
                } else {
                    return {};
                }
            })
            .catch(error => {
                console.log("error", JSON.stringify(error));
                return error?.response;
            });
    }

    static async getToken(url, params = {}) {
        const accessToken = localStorage.getItem("access-token");
        try {

            const response = await instanceCTDRequest
                .get(`${BASE_URL}/` + url, {
                    headers: {Authorization: `Bearer ${accessToken}`},
                    params: params ? params : {},
                });
            if (response.status === 401) {
                // Redirect to /signin page here
                window.location.href = '/signin';
                return;
            }
            if (response.data) {
                return response.data;
            } else {
                return {};
            }
        } catch (error) {
            return error?.response;
        }
    }

    static async getUserById(url, params = {}) {
        try {
            const accessToken = localStorage.getItem("access-token");
            const response = await instanceCTDRequest
                .get(`${BASE_URL}/users/` + url, {
                    headers: {Authorization: `Bearer ${accessToken}`},
                    params: params ? params : {},
                });

            if (response.status === 401) {
                // Redirect to /signin page here
                window.location.href = '/signin';
                return;
            }

            if (response.data) {
                return response.data;
            } else {
                return {};
            }
        } catch (error) {
            return error?.response;
        }
    }

    static async putToken(url, body = {}) {
        const accessToken = localStorage.getItem("access-token");
        if (!accessToken) {
            // Redirect to /signin page here
            window.location.href = '/signin';
            return;
        }

        try {
            const response = await instanceCTDRequest.put(`${BASE_URL}/${url}`,
                body,
                {
                    headers: {Authorization: `Bearer ${accessToken}`}
                }
            );

            if (response.status === 401) {
                // Redirect to /signin page here
                window.location.href = '/signin';
                return;
            }

            if (response.data) {
                return response.data;
            } else {
                return {};
            }
        } catch (error) {
            return error?.response;
        }
    }

}

