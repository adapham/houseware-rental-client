import {loadUserSuccess, loginFailed, loginSuccess, registerFailed, registerSuccess} from "../Slide/authSlide";
import httpRequest from "./apiConfig";

const BASE_URL = process.env.REACT_APP_BASE_URL;
const API_URL = `${BASE_URL}/api/auth/`;

export const login = async (loginRequest, dispatch) => {
    try {
        const res = httpRequest.post(API_URL + "signin", loginRequest)
        const response = await res;
        if (response.accessToken) {
            dispatch(loginSuccess(response.accessToken))
        }
        const resUser = httpRequest.getToken("api/users/profile")
        const responseUser = await resUser;
        if (responseUser && response.accessToken ) {
            console.log("responseUser", responseUser)
            dispatch(loadUserSuccess(responseUser));
            localStorage.setItem("user", JSON.stringify(responseUser));
        }
        return res;
    } catch (err) {
        dispatch(loginFailed())
    }
}

export const register = async (registerRequest, dispatch, navigate) => {
    try {
        const result =  await httpRequest.post(API_URL + "signup", registerRequest);
        if(result){
            dispatch(registerSuccess())

        }
        return result;
    } catch (err) {
        dispatch(registerFailed())
        return err;
    }
}