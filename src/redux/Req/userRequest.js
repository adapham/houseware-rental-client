import {getUserFailed, getUserSuccess} from "../Slide/userSlide";
import httpRequest from "./apiConfig";

const BASE_URL = process.env.REACT_APP_BASE_URL;
const API_URL = `${BASE_URL}/api/users/`;

export const getUser = async (id,dispatch,token) => {
    try {
       const res = httpRequest.post(API_URL + `${id}`,{},{
            headers:{Authorization: `Bearer ${token}`}
        });
        dispatch(getUserSuccess(res.data))
    }catch(err) {
        dispatch(getUserFailed())
    }
}
