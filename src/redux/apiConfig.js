import axios from 'axios';

const BASE_URL = process.env.REACT_APP_BASE_URL;

export const instanceCTDRequest = axios.create({
    baseURL: BASE_URL + "/api/",
    responseType: 'json',
    withCredentials: true,
});
export default class httpRequest {
    static post(url, params = {}, headers = {}) {
        return instanceCTDRequest
            .post(url, {...params}, {headers})
            .then(res => {
                if (res.data) {
                    var resJSON = null;
                    if (res.data.data) {
                        resJSON = JSON.stringify(res.data.data);
                    }
                    if (
                        resJSON != null &&
                        resJSON?.error &&
                        resJSON?.error === 'invalid_token'
                    ) {
                        //Alert error here
                    }
                    return res.data;
                } else {
                    return {};
                }
            })
            .catch(error => {
                return error?.response;
            });
    }


    static get(url, params = {}, headers = {}) {

        return instanceCTDRequest
            .get(url, {
                headers: headers ? headers : {},
                params: params ? params : {},
            })
            .then(res => {
                if (res.data) {

                    return res.data;
                } else {
                    return {};
                }
            })
            .catch(error => {
                return error?.response;
            });
    }

    static put(url, data = {}, headers = {}) {
        return instanceCTDRequest
            .put(url, data, {
                headers: headers,
            })
            .then(res => {
                if (res.data) {
                    console.log("res", res.data);
                    return res.data;
                } else {
                    return {};
                }
            })
            .catch(error => {
                console.log(error?.response);
                return error?.response;
            });
    }

    static delete(url, params = {}, headers = {}) {
        return instanceCTDRequest
            .delete(url, {
                headers: headers ? headers : {},
                params: params ? params : {},
            })
            .then(res => {

                if (res.data) {
                    return res.data;
                } else {
                    return {};
                }
            })
            .catch(error => {
                return error?.response;
            });
    }

    static postToken(url, params = {}) {
        const accessToken = localStorage.getItem("access-token");

        return instanceCTDRequest
            .post(`${BASE_URL}/` + url, params,
             {Authorization: `Bearer ${accessToken}`})
            .then(res => {
                if (res.data) {
                    var resJSON = null;
                    if (res.data.data) {
                        resJSON = JSON.stringify(res.data.data);
                    }
                    if (
                        resJSON != null &&
                        resJSON?.error &&
                        resJSON?.error === 'invalid_token'
                    ) {
                        //Alert error here
                    }
                    return res.data;
                } else {
                    return "ok";
                }
            })
            .catch(error => {
                return error?.response;
            });
    }

    static async getToken(url, params = {}) {
        try {
            const accessToken = localStorage.getItem("access-token");
            //const jsonString = JSON.stringify(params);
            const response = await instanceCTDRequest
                .get(`${BASE_URL}/` + url, {
                    headers: {
                        Authorization: `Bearer ${accessToken}`,
                    },
                    params: params ? params : {},
                });
                
            if (response.data) {
                return response.data;
            } else {
                return {};
            }
        } catch (error) {
            return error?.response;
        }
    }

    static async getTokenParam(url, params = {}) {
        try {
            const accessToken = localStorage.getItem("access-token");
            console.log("params", params)
            const response = await instanceCTDRequest
                .get(`${BASE_URL}/` + url, {
                    headers: {
                        Authorization: `Bearer ${accessToken}`,
                    },
                    params: params ? params : {},
                });

            if (response.data) {
                return response.data;
            } else {
                return {};
            }
        } catch (error) {
            return error?.response;
        }
    }

    static async deleteWithToken(url, params = {}) {
        try {
            const accessToken = localStorage.getItem("access-token");
            const response = await instanceCTDRequest
                .delete(`${BASE_URL}/` + url, {
                    headers: {Authorization: `Bearer ${accessToken}`},
                    params: params ? params : {},
                });
            if (response.data) {
                return response.data;
            } else {
                return {};
            }
        } catch (error) {
            return error?.response;
        }
    }

    static putToken(url, data = {}) {
        const accessToken = localStorage.getItem("access-token");
        return instanceCTDRequest
            .put(`${BASE_URL}/` + url, data, {Authorization: `Bearer ${accessToken}`})
            .then(res => {
                if (res.data) {
                    var resJSON = null;
                    if (res.data.data) {
                        resJSON = JSON.stringify(res.data.data);
                    }
                    if (
                        resJSON != null &&
                        resJSON?.error &&
                        resJSON?.error === 'invalid_token'
                    ) {
                        // Gửi thông báo lỗi ở đây
                    }
                    return res.data;
                } else {
                    return res.status;
                }
            })
            .catch(error => {
                return error?.response;
            });
    }

}

