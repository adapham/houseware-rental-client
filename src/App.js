import React from "react";
import {
    createBrowserRouter,
    createRoutesFromElements,
    Outlet,
    Route,
    RouterProvider,
    ScrollRestoration,
} from "react-router-dom";

import {toast, ToastContainer} from "react-toastify";

import AdminStyle from "./components/AdminStyle";
import Chat from "./components/Profile/chat/Chat";
import SpecialCase from "./components/SpecialCase/SpecialCase";
import ErrorPaymentPage from "./components/checkoutDetail/errorPayment";
import SuccessPayment from "./components/checkoutDetail/successPayment";
import Footer from "./components/home/Footer/Footer";
import Header from "./components/home/Header/Header";
import About from "./pages/About/About";
import Faq from "./pages/About/Faq";
import GuideLessor from "./pages/About/GuideLessor";
import PoliCy from "./pages/About/Policy";
import ForgotPassword from "./pages/Account/ForgotPassword";
import ResetPassword from "./pages/Account/ResetPassword";
import SignIn from "./pages/Account/SignIn";
import SignUp from "./pages/Account/SignUp";
import Cart from "./pages/Cart/Cart";
import Contact from "./pages/Contact/Contact";
import Home from "./pages/Home/Home";
import Journal from "./pages/Journal/Journal";
import Offer from "./pages/Offer/Offer";
import ProductDetails from "./pages/ProductDetails/ProductDetails";
import Profile from "./pages/Profile/Profile";
import Shop from "./pages/Shop/Shop";
import Store from "./pages/Store/Store";
import NotFound from "./pages/examples/NotFound";
import ServerError from "./pages/examples/ServerError";
import Payment from "./pages/payment/Payment";
import AdminCategory from "./scenes/admin-category/";
import AdminHistoryPayment from "./scenes/admin-history-payment/index.jsx";
import AdminProduct from "./scenes/admin-product";
import AdminRequest from "./scenes/admin-request";
import AdminStore from "./scenes/admin-store";
import AdminUser from "./scenes/admin-user";
import Dashboard from "./scenes/dashboard";

import {useSelector} from "react-redux";
import AdminCreateUpdateCategory from "./scenes/admin-category/screens/create-update-category.js";

import AdminDetailProduct from "./scenes/admin-product/screens/detail-product.js";

import AdminDetailReportRequest from "./scenes/admin-request/screens/detail-report-request.js";
import AdminDetailRequest from "./scenes/admin-request/screens/detail-request.js";
import AdminDetailRoleUpRequest from "./scenes/admin-request/screens/detail-role-up-request.js";

import AdminDetailStore from "./scenes/admin-store/screens/detail-store.js";

import AdminDetailUser from "./scenes/admin-user/screens/detail-user.js";
import "react-toastify/dist/ReactToastify.css";
import AdminMessage from "./scenes/admin-message/index.js";
import WithdrawSuccess from "./components/checkoutDetail/WithdrawSuccess.js";
import WithdrawFail from "./components/checkoutDetail/WithdrawFail.js";
import {getCurrentUser} from "./utils/ApiUtil.js";
import SockJS from "sockjs-client";
import Stomp from "stompjs";

const Layout = () => {
    return (
        <div>
            <Header/>
            <SpecialCase/>
            <ScrollRestoration/>
            <Outlet/>
            <Footer/>
        </div>
    );
};

var stompClient = null;
var orderStompClient = null;


function App() {
    const user = useSelector((state) => state?.auth?.loadUser?.user);
    const isLoggedIn = user?.roleId;
    const BASE_URL = process.env.REACT_APP_BASE_URL

    console.log("isLoggedIn", isLoggedIn);

    //Connect Noti Message
    const connect = () => {
        const Stomp = require("stompjs");
        var SockJS = require("sockjs-client");

        SockJS = new SockJS(BASE_URL + "/ws");
        stompClient = Stomp.over(SockJS);
        stompClient.connect({}, onConnected, (e) => console.error(e));
    };

    const onConnected = async () => {
        console.log("===> Connect Websocket Tin nhắn");
        const data = await getCurrentUser();
        console.log("===> User: ", data);
        stompClient.subscribe(
            "/user/" + data?.id + "/queue/messages",
            onMessageReceived
        );
    };

    const onMessageReceived = (msg) => {
        const notification = JSON.parse(msg.body);
        console.log("notification", notification)
        toast.info("Bạn vừa nhận được tin nhắn mới!", {
            position: "top-right",
            autoClose: 2000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
            theme: "light",
        });
    };

    //Connect Socket Noti Order
    const connectOrder = () => {
        const Stomp = require("stompjs");
        var SockJS = require("sockjs-client");

        SockJS = new SockJS(BASE_URL + "/ws");
        stompClient = Stomp.over(SockJS);
        stompClient.connect({}, onConnectedOrder, (e) => console.error(e));
    };

    const onConnectedOrder = async () => {
        console.log("===> Connect Websocket Đơn hàng");
        const data = await getCurrentUser();
        console.log("===> User: ", data);
        stompClient.subscribe(
            "/user/" + data?.id + "/queue/orders",
            onMessageReceivedOrder
        );
    };

    const onMessageReceivedOrder = (msg) => {
        const notification = JSON.parse(msg.body);
        console.log("notification", notification)
        toast.info("Thông báo " + notification.content, {
            position: "top-right",
            autoClose: 2000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
            theme: "light",
        });
    };


    if (isLoggedIn) {
        connect();
        connectOrder();
    }
    // useEffect(()=>{
    //     window.location.href ='/';
    // },[])
    const router = createBrowserRouter(
        createRoutesFromElements(
            <>
                <Route>
                    <Route
                        path="/"
                        element={
                            isLoggedIn !== 3 ? (
                                <Layout/>
                            ) : (
                                <AdminStyle component={<Dashboard/>} title="Tổng quan"/>
                            )
                        }
                    >
                        {/* ==================== Header Navlink Start here =================== */}
                        <Route index element={<Home/>}></Route>
                        <Route path="/shop" element={<Shop/>}></Route>
                        <Route path="/about" element={<About/>}></Route>
                        <Route path="/contact" element={<Contact/>}></Route>
                        <Route path="/blog" element={<Journal/>}></Route>
                        {/* ==================== Header Navlink End here ===================== */}
                        <Route path="/offer" element={<Offer/>}></Route>
                        <Route path="/product/:_id" element={<ProductDetails/>}></Route>
                        <Route path="/cart" element={<Cart/>}></Route>
                        <Route path="/paymentgateway" element={<Payment/>}></Route>
                        <Route path="/profile" element={<Profile/>}></Route>
                        <Route path="/store/:idStore" element={<Store/>}></Route>

                        <Route path="/policy" element={<PoliCy/>}></Route>
                        <Route path="/faq" element={<Faq/>}></Route>
                        <Route path="/guideLessor" element={<GuideLessor/>}></Route>

                        <Route path="/withdrawRechargeFail" element={<WithdrawFail/>}></Route>
                        {/* <Route path="/rechargeFail" element={<RechargeFail/>}></Route> */}

                        <Route path="/withDrawRechargSuccess" element={<WithdrawSuccess/>}></Route>
                        {/* <Route path="/rechargeSuccess" element={<RechargeSuccess />}></Route> */}

                        <Route path="/errorPayment" element={<ErrorPaymentPage/>}></Route>
                        <Route path="/successPayment" element={<SuccessPayment/>}></Route>

                    </Route>
                    <Route path="/notFound" element={<NotFound/>}></Route>
                    <Route path="/serverError" element={<ServerError/>}></Route>

                    <Route path="/signin" element={<SignIn/>}></Route>

                    <Route path="/signup" element={<SignUp/>}></Route>
                    <Route path="/forgotpassword" element={<ForgotPassword/>}></Route>
                    <Route path="/reset-password" element={<ResetPassword/>}></Route>
                    <Route path="/chat" element={<Chat/>}></Route>

                    {/* user route */}
                    <Route
                        path="/admin/user"
                        element={
                            isLoggedIn !== 3 ? (
                                <NotFound/>
                            ) : (
                                <AdminStyle
                                    component={<AdminUser/>}
                                    title="Quản lý người dùng"
                                />
                            )
                        }
                    />
                    <Route
                        path="/admin/user/:id"
                        element={
                            isLoggedIn !== 3 ? (
                                <NotFound/>
                            ) : (
                                <AdminStyle
                                    component={<AdminDetailUser/>}
                                    title="Chi tiết người dùng"
                                />
                            )
                        }
                    />

                    {/* request route */}
                    <Route
                        path="/admin/request"
                        element={
                            isLoggedIn !== 3 ? (
                                <NotFound/>
                            ) : (
                                <AdminStyle
                                    component={<AdminRequest/>}
                                    title="Quản lý yêu cầu"
                                />
                            )
                        }
                    />
                    <Route
                        path="/admin/request/detail/:id"
                        element={
                            isLoggedIn !== 3 ? (
                                <NotFound/>
                            ) : (
                                <AdminStyle
                                    component={<AdminDetailRequest/>}
                                    title="Chi tiết yêu cầu"
                                />
                            )
                        }
                    />
                    <Route
                        path="/admin/request/role-up/:id"
                        element={
                            isLoggedIn !== 3 ? (
                                <NotFound/>
                            ) : (
                                <AdminStyle
                                    component={<AdminDetailRoleUpRequest/>}
                                    title="Chi tiết cửa hàng đăng ký"
                                />
                            )
                        }
                    />
                    <Route
                        path="/admin/request/report/:id"
                        element={
                            isLoggedIn !== 3 ? (
                                <NotFound/>
                            ) : (
                                <AdminStyle
                                    component={<AdminDetailReportRequest/>}
                                    title="Chi tiết yêu cầu"
                                />
                            )
                        }
                    />

                    {/* product route */}
                    <Route
                        path="/admin/product"
                        element={
                            isLoggedIn !== 3 ? (
                                <NotFound/>
                            ) : (
                                <AdminStyle
                                    component={<AdminProduct/>}
                                    title="Quản lý sản phẩm"
                                />
                            )
                        }
                    />
                    <Route
                        path="/admin/product/detail/:id"
                        element={
                            isLoggedIn !== 3 ? (
                                <NotFound/>
                            ) : (
                                <AdminStyle
                                    component={<AdminDetailProduct/>}
                                    title="Chi tiết sản phẩm"
                                />
                            )
                        }
                    />
                    {/* store route */}
                    <Route
                        path="/admin/store"
                        element={
                            isLoggedIn !== 3 ? (
                                <NotFound/>
                            ) : (
                                <AdminStyle
                                    component={<AdminStore/>}
                                    title="Quản lí cửa hàng"
                                />
                            )
                        }
                    />
                    <Route
                        path="/admin/store/:id"
                        element={
                            isLoggedIn !== 3 ? (
                                <NotFound/>
                            ) : (
                                <AdminStyle
                                    component={<AdminDetailStore/>}
                                    title="Chi tiết cửa hàng"
                                />
                            )
                        }
                    />
                    {/* category route */}
                    <Route
                        path="/admin/category"
                        element={
                            isLoggedIn !== 3 ? (
                                <NotFound/>
                            ) : (
                                <AdminStyle
                                    component={<AdminCategory/>}
                                    title="Quản lý danh mục"
                                />
                            )
                        }
                    />
                    <Route
                        path="/admin/category/create"
                        element={
                            isLoggedIn !== 3 ? (
                                <NotFound/>
                            ) : (
                                <AdminStyle
                                    component={<AdminCreateUpdateCategory/>}
                                    title="Thêm mới danh mục"
                                />
                            )
                        }
                    />
                    <Route
                        path="/admin/category/edit/:id"
                        element={
                            isLoggedIn !== 3 ? (
                                <NotFound/>
                            ) : (
                                <AdminStyle
                                    component={<AdminCreateUpdateCategory/>}
                                    title="Chỉnh sửa danh mục"
                                />
                            )
                        }
                    />
                    {/* history payment route */}
                    <Route
                        path="/admin/history-payment"
                        element={
                            isLoggedIn !== 3 ? (
                                <NotFound/>
                            ) : (
                                <AdminStyle
                                    component={<AdminHistoryPayment/>}
                                    title="Lịch sử giao dịch"
                                />
                            )
                        }
                    />
                    {/* message payment route */}
                    <Route
                        path="/admin/message"
                        element={
                            isLoggedIn !== 3 ? (
                                <NotFound/>
                            ) : (
                                <AdminStyle component={<AdminMessage/>} title="Tin nhắn"/>
                            )
                        }
                    />
                    <Route path="*" element={<NotFound/>}/>
                </Route>
            </>
        )
    );
    return (
        <div className="font-bodyFont">
            <RouterProvider router={router}/>
            <ToastContainer/>
        </div>
    );
}

export default App;
