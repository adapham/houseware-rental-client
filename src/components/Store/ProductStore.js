import React, { useEffect, useState } from "react";

import Pagination from "../../components/pageProps/shopPage/Pagination";
import { Tabs } from "antd";
import { useDispatch, useSelector } from "react-redux";
import { getAllProduct } from "../../redux/Req/apiRequestProduct";
import { updateFilter, updateProduct } from "../../redux/Slide/productSlice";

function ProductStore({idStore}) {
  const dispatch = useDispatch();
  const [totalPage, setTotalPage] = useState(1);
  const produtStore = useSelector((state) => state?.product);
  const categoryStore = useSelector((state) => state.category);
  const { categories } = categoryStore;
  const { pageable, filters, products } = produtStore;
  const [keySelect, setKeySelect] = useState();
  

  useEffect(()=>{
    if(idStore){
      dispatch(updateFilter({userId:idStore}));
    }
    
  },[idStore])
  useEffect(() => {
    if(idStore){
      getProducts();
    }
    
  }, [pageable, filters]);

  const getProducts = async () => {
    const response = await getAllProduct({ ...filters, ...pageable });

    if (response) {
      dispatch(updateProduct(response.content));

      setTotalPage(response.totalPages);
    }
  };

  const onChange = (key) => {
    dispatch(updateFilter({categoryId:key}));
  };
  const items =
    Array.isArray(categories) && categories.length > 0
      ? categories.map((item) => ({
          key: item.id,
          label: item.categoryName,
        }))
      : [];
  items.unshift({
    key: null,
    label: "Tất cả sản phẩm",
  });

  return (
    <div>
      <Tabs
        defaultActiveKey="1"
        items={items}
        onChange={onChange}
        indicatorSize={(origin) => origin - 16}
        size="large"
        className="font-medium"
      />
      {products.length > 0 && (
        <Pagination            
            pageable={pageable}
            products={products}
            totalPage={totalPage}
            choose={keySelect}
          />
      )
      }
    </div>
  );
}

export default ProductStore;
