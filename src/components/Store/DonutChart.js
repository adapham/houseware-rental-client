import React from 'react'
import Highcharts from 'highcharts';
import Highcharts3D from 'highcharts/highcharts-3d'; // Import the 3D module

Highcharts3D(Highcharts); // Initialize the 3D module
function DonutChart({staticTotalProductRented}) {

    const chartConfig = {
        chart: {
            type: 'pie',
            options3d: {
                enabled: true,
                alpha: 45
            }
        },
        title: {
            text: 'Tổng sản phẩm đã thuê theo thể loại',
            align: 'left'
        },
        
        plotOptions: {
            pie: {
                innerSize: 100,
                depth: 45
            }
        },
        series: [{
            name: 'Tổng sản phẩm: ',
            data: Object.entries(staticTotalProductRented)
        }]
    }
    
      React.useEffect(() => {
        Highcharts.chart('donutChart', chartConfig);
      }, []);
    
      return <div id="donutChart"></div>;
}

export default DonutChart