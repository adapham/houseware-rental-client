import {  Col, Row } from "antd";
import React from "react";
import AvatarStore from "./AvatarStore";
import FeatureStore from "./FeatureStore";


const HeaderStore = ({dataDetail,setDataStore,storeId}) => {

  return (
    <div className="mt-10">
      <Row gutter={16}>
        <Col span={8}>
            {/* <div className="rounded-lg bg-gradient-to-r from-cyan-500 to-blue-500 h-full py-10 max-h-40" >
              <div>
                <AvatarStore storeData={dataDetail}/>
              </div>
            </div> */}
            {dataDetail?.imageTextUri ? (
                <div
                  className="rounded-lg h-full py-10 max-h-40"
                  style={{ backgroundImage: `url(${dataDetail?.imageTextUri})`, backgroundSize: 'cover', backgroundPosition: 'center' }}
                >
                  <div>
                    <AvatarStore storeData={dataDetail}/>
                  </div>
                </div>
              ) : (
                <div className="rounded-lg bg-gradient-to-r from-cyan-500 to-blue-500 h-full py-10 max-h-40">
                  <div>
                    <AvatarStore storeData={dataDetail}/>
                  </div>
                </div>
              )}
        </Col>
        <Col span={16}>
            <FeatureStore storeData={dataDetail} setDataStore={setDataStore} storeId={storeId}/>
        </Col>
      </Row>
    </div>
  );
};

export default HeaderStore;