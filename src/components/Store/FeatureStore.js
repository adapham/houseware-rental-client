import React, { useEffect, useState } from "react";
import { Avatar, Button, Col, List, Row } from "antd";
import { BiStore } from "react-icons/bi";
import { BsFillPersonPlusFill } from "react-icons/bs";
import { LiaCommentDotsSolid } from "react-icons/lia";
import { AiOutlinePhone, AiOutlineSolution, AiOutlineStar } from "react-icons/ai";

import UpdateStore from "./UpdateStore";
import { FaRegAddressBook } from "react-icons/fa";
import { EditTwoTone } from '@ant-design/icons';
import { useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import { FaMoneyBillTransfer } from "react-icons/fa6";
import { getTotalTransactions, getUser } from "../../redux/apiRequestProduct";
import CustomizedDialogs from "../Profile/CustomizedDialogs";
import { convertMoney } from "../../utils/formatConfig";

const FeatureStore = ({storeData,setDataStore,storeId}) => {
  const totalIncome = storeData?.money;
  const user = useSelector((state) => state?.auth?.loadUser?.user);
  const [userD, setUserD] = useState([]);
  const [modal2Open, setModal2Open] = useState(false);
  const [isModalOpen, setOpen] = useState(false);
  const [totalPrice, setTotalPrice] = useState(0);
  const param = useParams();
  const idStore = param.idStore;
  const openDepositDialog = () => {
    setOpen(true);
  };
  const getMyUser = async (userId) => {
    const response = await getUser(userId);
    if (response) {
      setUserD(response);
    }
  };
  useEffect(()=>{
    if (user?.id) {
      getTotalTransaction()
      getMyUser(user?.id);
    }
  },[])
  const getTotalTransaction = async () => {
    const response = await getTotalTransactions(user?.id);
    if (response) {
      setTotalPrice(response);
    }
  }
  const data = [
    {
      title: (
        <span>
          Sản Phẩm: <strong>{storeData?.totalProduct}</strong>
        </span>
      ),
      icon: <BiStore className="text-lg" />,
    },
    {
      title: (
        <span>
          Người đã thuê: <strong>{storeData?.totalPersonRented}</strong>
        </span>
      ),
      icon: <BsFillPersonPlusFill className="text-lg" />,
    },
    {
      title: (
        <span>
          Tỷ Lệ Xác Nhận Đơn: <strong>{storeData?.orderReceivingRate}</strong>
        </span>
      ),
      icon: <LiaCommentDotsSolid className="text-lg" />,
    },
    {
      title: (
        <span>
          Tỷ Lệ Shop Huỷ Đơn: <strong>{storeData?.orderCancellationRate}</strong>
        </span>
      ),
      icon: <AiOutlineSolution className="text-lg" />,
    },
  ];
  
  const handleRechargeMoney = () => {
    setOpen(true);
  };
  const closeDepositDialog = () => {
    setOpen(false);
  };
  
  const data2 = [
    (user?.roleId === 2 && user?.storeId === storeId) && {
      title: (
        <span>
          Tổng doanh thu: <strong>{convertMoney(totalIncome)} đ</strong>
        </span>
      ),
      icon: <FaMoneyBillTransfer className="text-lg" />,
    },
    {
      title: (
        <span>
          Số điện thoại: <strong>{storeData?.phone}</strong>
        </span>
      ),
      icon: <AiOutlinePhone className="text-lg" />,
    },
    {
      title: (
        <span>
          Địa chỉ: <strong>{storeData?.address}- {storeData?.ward} - {storeData?.district} - {storeData?.province}</strong> 
        </span>
      ),
      icon: <FaRegAddressBook className="text-lg" />,
    },
  ];
  

  return (
    <>
    <Row gutter={16}>
      <Col span={12}>
        <List
          size="small"
          itemLayout="horizontal"
          dataSource={data}
          renderItem={(item, index) => (
            <List.Item>
              <List.Item.Meta
                avatar={<>{item.icon}</>}
                title={<span>{item.title}</span>}
              />
            </List.Item>
          )}
        />
      </Col>
      <Col span={12}>
      <List
          itemLayout="horizontal"
          dataSource={data2}
          renderItem={(item, index) => (
            <List.Item>
              <List.Item.Meta
                avatar={<>{item.icon}</>}
                title={<span>{item.title}</span>}
              />
            </List.Item>
          )}
        />
        <div className="flex justify-end">
        {(user?.roleId ===2 && user?.storeId===storeId) &&
        <div className="flex ">
        
        <Button onClick={() => setModal2Open(true)} icon={<EditTwoTone />} className="mt-10 flex justify-end" size="large">
            Chỉnh sửa thông tin
        </Button>
          <Button className="ml-10 mt-10 bg-[#2980b9] text-white"size="large" onClick={openDepositDialog}>Chuyển tiền</Button>
        </div>

          }
        </div>
      </Col>
    </Row>
    <UpdateStore modal2Open={modal2Open} setModal2Open={setModal2Open} setDataStore={setDataStore}/>
    <CustomizedDialogs
           isModalOpen={isModalOpen}
           closeModal={closeDepositDialog}
           
           title={"Chuyển tiền"}
           label={"Số tiền chuyển"}
           balances={userD?.money ? userD.money.toLocaleString() + " đ" : 0}
           id={userD?.id}
           transfer={true}
           storeId={storeId}
           totalIncome={totalIncome}
           setDataStore={setDataStore}
           openModal={openDepositDialog}
         />
    </>
  );
};
export default FeatureStore;
