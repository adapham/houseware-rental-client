import React, { useEffect, useState } from 'react';
import Highcharts from 'highcharts';
import { DataArray } from '@mui/icons-material';
import { Select } from 'antd';

function convertToDate(dateString) {
  if (Array.isArray(dateString)) {
    // If dateString is an array, map each date in the array
    return dateString.map(date => {
      if (typeof date === 'string') {
        const dateArray = date.split("-");
        return dateArray[2];
      }
      return null;
    });
  } else if (typeof dateString === 'string') {
    // If dateString is a single string, split it
    const dateArray = dateString.split("-");
    return dateArray[2];
  } else {
    return null;
  }
}

const AreaSpline = ({ staticDataTotalOrder,setTypeOrder }) => {

  const [tickInterval,setTickInterval]=useState(5);
  
 

  const fromDate = convertToDate(staticDataTotalOrder?.date?.[0]);
  const toDate = convertToDate(staticDataTotalOrder?.date && staticDataTotalOrder.date.length > 0 ? 
    staticDataTotalOrder.date[staticDataTotalOrder.date.length - 1] : null); 
     const plotBands = [{
    from: fromDate,
    to: toDate
  }];
  const handleChange = (value) => {
    setTypeOrder(value);
    switch (value) { 
      case 'ONE_WEEK':
        setTickInterval(2);
        break;
      case 'ONE_MONTH':
        setTickInterval(5);
        break;
      case 'ONE_YEAR':
        setTickInterval(100);
        break;
      default:
        setTickInterval(1);
        break;
    }
  };
  const rentedMax = Array.isArray(staticDataTotalOrder.rented)
  ? staticDataTotalOrder.rented.reduce((a, b) => Math.max(a, b), 0)
  : 0;

const doneMax = Array.isArray(staticDataTotalOrder.done)
  ? staticDataTotalOrder.done.reduce((a, b) => Math.max(a, b), 0)
  : 0;

const maxY = Math.max(rentedMax, doneMax);
  const chartHeight = 400;
  const chartConfig = {
    chart: {
      type: 'areaspline',
      height: chartHeight
    },
    title: {
      text: 'Thống kê số đơn hàng',
      align: 'left'
    },

    xAxis: {
     
      categories: staticDataTotalOrder?.date,
      tickInterval: tickInterval,
      plotBands: plotBands
    },
    yAxis: {
      title: {
        text: 'Số lượng'
      },
      min: 0,
      max: maxY
    },
    tooltip: {
      shared: true,
      headerFormat: '<b>đơn hàng cho thuê trong {point.x}</b><br>'
    },
    credits: {
      enabled: true
    },
   
    series: [{
      name: 'Tổng đơn hàng đang thuê',
      data: staticDataTotalOrder?.rented
    }, {
      name: 'Tổng đơn hàng đã thuê',
      data: staticDataTotalOrder?.done
    }]
  };

  React.useEffect(() => {
    Highcharts.chart('areaSpline', chartConfig);
  }, [staticDataTotalOrder,tickInterval]);

  return (
    <div>
      <div>
      <Select
      defaultValue="ONE_MONTH"
      style={{
        width: 120,
      }}
      onChange={handleChange}
      options={[
        {
          value: 'ONE_DAY',
          label: 'Một ngày',
        },
        {
          value: 'THREE_DAY',
          label: 'Ba ngày',
        },
        {
          value: 'ONE_WEEK',
          label: 'Một tuần',
        },
        {
          value: 'ONE_MONTH',
          label: 'Một Tháng',
        },
        {
          value: 'ONE_YEAR',
          label: 'Một năm',
        },
      ]}
    />
      </div>
      <div id="areaSpline" style={{ height: chartHeight }}></div>
    </div>
  );
};

export default AreaSpline;
