import React, { useEffect, useState } from 'react';
import Highcharts from 'highcharts';
import { Select } from 'antd';

export default function ColumnChart({statisUserRented,setTypeUserRented}) {
  const [tickInterval,setTickInterval]=useState(5);
  const handleChange = (value) => {
    setTypeUserRented(value);
    switch (value) { 
      case 'ONE_WEEK':
        setTickInterval(2);
        break;
      case 'ONE_MONTH':
        setTickInterval(5);
        break;
      case 'ONE_YEAR':
        setTickInterval(100);
        break;
      default:
        setTickInterval(1);
        break;
    }
  };

  const chartHeight = 400;
  const chartConfig = {
    chart: {
      type: 'column', // Use 'column' type for 3D column chart
      options: {
        enabled: true,
        alpha: 15,
        beta: 15,
        depth: 50,
        viewDistance: 25,
      },
      height: chartHeight
    },
    title: {
      text: 'Số người thuê đồ',
    },
    xAxis: {
      categories: statisUserRented?.date,
      tickInterval: tickInterval,
      title: {
        text: 'Thời gian',
      },
      labels: {
        skew: true,
      },
    },
    yAxis: {
      title: {
        margin: 20,
        text: 'Số lượng người',
      },
      labels: {
        skew3d: true,
      },
    },
    tooltip: {
      headerFormat: '<b>Thời gian: {point.x}</b><br>',
    },
    plotOptions: {
      column: {
        depth: 25,
        colorByPoint: true,
      },
    },
    series: [
      {
        data: statisUserRented?.userRented,
        name: 'Số người',
        showInLegend: false,
      },
    ],
  };

  useEffect(() => {
    Highcharts.chart('columnChart', chartConfig);
  }, [statisUserRented,setTypeUserRented]);


  return (
    <div>
      <div>
      <Select
      defaultValue="ONE_MONTH"
      style={{
        width: 120,
      }}
      onChange={handleChange}
      options={[
        {
          value: 'ONE_DAY',
          label: 'Một ngày',
        },
        {
          value: 'THREE_DAY',
          label: 'Ba ngày',
        },
        {
          value: 'ONE_WEEK',
          label: 'Một tuần',
        },
        {
          value: 'ONE_MONTH',
          label: 'Một Tháng',
        },
        {
          value: 'ONE_YEAR',
          label: 'Một năm',
        },
      ]}
    />
      </div>
      <div style={{ height: chartHeight }} id="columnChart"></div>;
    </div>
  );
}
