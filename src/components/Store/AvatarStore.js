import * as React from 'react';
import { styled } from '@mui/material/styles';
import Badge from '@mui/material/Badge';
import Avatar from '@mui/material/Avatar';
import Stack from '@mui/material/Stack';
import { useSelector } from 'react-redux';

const StyledBadge = styled(Badge)(({ theme }) => ({
  '& .MuiBadge-badge': {
    backgroundColor: '#44b700',
    color: '#44b700',
    boxShadow: `0 0 0 2px ${theme.palette.background.paper}`,
    '&::after': {
      position: 'absolute',
      top: 0,
      left: 0,
      width: '100%',
      height: '100%',
      borderRadius: '50%',
      animation: 'ripple 1.2s infinite ease-in-out',
      border: '1px solid currentColor',
      content: '""',
    },
  },
  '@keyframes ripple': {
    '0%': {
      transform: 'scale(.8)',
      opacity: 1,
    },
    '100%': {
      transform: 'scale(2.4)',
      opacity: 0,
    },
  },
}));



export default function AvatarStore({storeData}) {
  const user = useSelector((state) => state?.auth?.loadUser?.user);
  return (
    <Stack direction="row" spacing={2} className='bg-gray-500 p-2 opacity-75'>
      <StyledBadge
        overlap="circular"
        anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
        variant="dot"
      >
        <Avatar alt="IMAGE" 
        style={{ width: 50, height: 50 }}
        src={user?.imageUrl} />        
      </StyledBadge>
      <div>
        <h1 className='text-xl font-bold text-white'>{storeData?.title}</h1>
        <p className='text-sm font-semibold text-white'>{storeData?.description}</p>
      </div>
    </Stack>
  );
}