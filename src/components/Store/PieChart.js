import React from 'react';
import Highcharts from 'highcharts';

const PieChart = ({data}) => {

  const chartConfig = {
    chart: {
      type: 'pie',
      credits: false
      
    },
    title: {
      text: 'Tổng sản phẩm theo thể loại'
    },
    tooltip: {
      valueSuffix: '%'
    },
    
    plotOptions: {
      series: {
        allowPointSelect: true,
        cursor: 'pointer',
        dataLabels: [
          {
            enabled: true,
            distance: 20
          },
          {
            enabled: true,
            distance: -40,
            format: '{point.percentage:.1f}%',
            style: {
              fontSize: '1.2em',
              textOutline: 'none',
              opacity: 0.7
            },
            filter: {
              operator: '>',
              property: 'percentage',
              value: 10
            }
          }
        ]
      }
    },
    series: [
      {
        name: 'Percentage',
        colorByPoint: true,
        data: [
          {
            name: 'Tủ lạnh',
            y: data?.["Tủ Lạnh"]
          },
          {
            name: 'Máy giặt',
            sliced: true,
            selected: true,
            y: data?.["Máy Giặt"]
          },
          {
            name: 'Điều hoà',
            y: data?.["Điều Hòa"]
          },
          {
            name: 'Tivi',
            y: data?.["Tivi"]
          },
          {
            name: 'Quạt',
            y: data?.["Quạt"]
          }
        ]
      }
    ]
  };

  React.useEffect(() => {
    Highcharts.chart('pieChart', chartConfig);
  }, []);

  return <div id="pieChart"></div>;
};

export default PieChart;
