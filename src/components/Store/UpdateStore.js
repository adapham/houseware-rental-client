import { Modal } from "antd";
import React, { useEffect, useMemo, useRef, useState } from "react";
import TextField from "@mui/material/TextField";
import { Container, Grid } from "@material-ui/core";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import Select from "@mui/material/Select";
import FormControl from "@mui/material/FormControl";
import "resize-observer-polyfill"; // Import the ResizeObserver polyfill
import Avatar from "@mui/material/Avatar";
import {
  getDetailStore,
  getDistrictByProvince,
  getProvince,
  getStoreByUser,
  getUpdateStore,
  getWardByDistrict,
} from "../../redux/apiRequestProduct";
import { useSelector } from "react-redux";
import { Button } from "@mui/material";
import { toast } from "react-toastify";
import { regexPhoneNumber } from "../../constants/constantsParam";
const imageDefault =
  "https://inkythuatso.com/uploads/thumbnails/800/2023/03/6-anh-dai-dien-trang-inkythuatso-03-15-26-36.jpg";

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};
const UpdateStore = ({
  modal2Open,
  setModal2Open,
  setDataStore,
}) => {
  const user = useSelector((state) => state?.auth?.loadUser?.user);
  const [ward, setWard] = useState();
  const [district, setDistrict] = useState();
  const [province, setProvince] = useState();
  const [storeName, setStoreName] = useState("");
  const [phoneNumber, setPhoneNumber] = useState("");
  const [wardSelect, setWardSelect] = useState("");
  const [districtSelect, setDistrictSelect] = useState("");
  const [provinceSelect, setProvinceSelect] = useState("");
  const [image, setImage] = useState("");
  const [errStoreName, setErrStoreName] = useState("");
  const [errPhoneNumber, setErrPhoneNumber] = useState("");
  const [addressDetail, setAddressDetail] = useState("");
  const [errAddressDetail, setErrAddressDetail] = useState("");
  const [descriptionDetail, setDescriptionDetail] = useState("");
  const [errDescriptionDetail, setErrDescriptionDetail] = useState("");
  const [storeInfor, setStoreInfor] = useState([]);
  const [imageSelected, setImageSelected] = useState();

  const profileImage = useRef(null);
  const openChooseImage = () => {
    profileImage.current.click();
  };
  var formData = new FormData();
  const changeProfileImage = (event) => {
    console.log(event, "fileImage");
    const ALLOWED_TYPES = ["image/png", "image/jpeg", "image/jpg"];
    const selected = event.target.files[0];
    console.log(selected, "IMAGEFILE");
    if (selected && ALLOWED_TYPES.includes(selected.type)) {
      setImageSelected(selected);
      let reader = new FileReader();
      reader.onloadend = () => setImage(reader.result);
      reader.readAsDataURL(selected);
    } else {
      // Handle case when selected file type is not allowed
      console.log("Selected file type is not allowed");
      // Add your error handling logic here
    }
  };
  const getStore = async () => {
    if (user?.id) {
      const response = await getStoreByUser(user?.id);

      if (response) {
        setStoreName(response?.title);
        setPhoneNumber(response?.phone);
        setWardSelect(response.ward);
        setAddressDetail(response?.address);
        setDescriptionDetail(response?.description);
        setStoreInfor(response);
        setImage(response?.imageTextUri);
      }
    }
  };
  useEffect(() => {
    const fetchData = async () => {
      await getProvinces();
      await getStore();
    };
    fetchData();
  }, []);

  const getProvinces = async () => {
    try {
      const response = await getProvince();
      if (response) setProvince(response);
    } catch (error) {
      console.error("Error fetching provinces:", error);
    }
  };

  const getDistrict = async (provinceId) => {
    try {
      const response = await getDistrictByProvince(provinceId);
      if (response) {
        setDistrict(response);
      }
    } catch (error) {
      console.error("Error fetching districts:", error);
    }
  };

  const getWard = async (districtId) => {
    try {
      const response = await getWardByDistrict(districtId);
      if (response) {
        setWard(response);
      }
    } catch (error) {
      console.error("Error fetching wards:", error);
    }
  };

  useEffect(() => {
    if (storeInfor?.province && province) {
      const idProvince = province?.find(
        (item) => item.name == storeInfor?.province
      )?.id;
      setProvinceSelect(idProvince);
    }
  }, [province, storeInfor]);

  useEffect(() => {
    if (provinceSelect) {
      getDistrict(provinceSelect);
    }
  }, [provinceSelect]);

  useEffect(() => {
    if (storeInfor?.district && district) {
      const idDistrict = district?.find(
        (item) => item.name == storeInfor?.district
      )?.id;
      setDistrictSelect(idDistrict);
    }
  }, [district, storeInfor]);

  useEffect(() => {
    if (districtSelect) {
      getWard(districtSelect);
    }
  }, [districtSelect]);

  useEffect(() => {
    if (storeInfor?.ward) {
      const idWar = ward?.find((item) => item.name == storeInfor?.ward)?.id;
      setWardSelect(idWar);
    }
  }, [ward, storeInfor]);

  const handleApprove = () => {
    formData.set("title", storeName.trim());
    formData.set("phone", phoneNumber.trim());
    formData.set(
      "ward",
      ward?.filter((item) => item.id == wardSelect)[0]?.name
    );
    formData.set(
      "district",
      district?.filter((item) => item.id == districtSelect)[0]?.name
    );
    formData.set(
      "province",
      province?.filter((item) => item.id == provinceSelect)[0]?.name
    );
    formData.set("address", addressDetail.trim());
    formData.set("userId", user?.id);
    formData.set("description", descriptionDetail.trim());
    formData.set("image", imageSelected);

    if (phoneNumber && addressDetail && storeName && descriptionDetail
      && provinceSelect && districtSelect && wardSelect
      && storeName.trim() != "" && addressDetail.trim() != ""
      && descriptionDetail.trim() != "" && errPhoneNumber.trim() == "") {
      updateInforStore(formData);
    } else {
      toast.error("Không điền đầy đủ/đúng thông tin", {
        position: "top-right",
        autoClose: 800,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "light",
      });
    }


  };
  const getDetailInforStore = async (userID) => {
    const response = await getDetailStore(userID);
    if (response) {

      setDataStore(response);

    }
  }
  const updateInforStore = async (infor) => {
    const response = await getUpdateStore(user?.id, infor);
    if (response) {
      getDetailInforStore(user?.id);
      setModal2Open(false);
      toast.success("Sửa công ty cửa hàng thành công", {
        position: "top-right",
        autoClose: 800,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "light",
      });
    }
  }

  return (
    <>
      <Modal
        title="Chỉnh sửa thông tin cửa hàng"
        centered
        open={modal2Open}
        footer={null}
        onOk={() => setModal2Open(false)}
        onCancel={() => setModal2Open(false)}
      >
        <form className="needs-validation mt-2">
          <Grid container spacing={2}>
            <Grid item xs={12} sm={6}>
              <TextField
                margin="normal"
                label={
                  <span>
                    Tên cửa hàng<span style={{ color: 'red' }}>*</span>
                  </span>
                }
                variant="outlined"
                size="small"
                type="text"
                className="w-full"
                placeholder="Nhập tại đây"
                InputLabelProps={{
                  shrink: true,
                }}
                onChange={(e) => {
                  const storeN = e.target.value;
                  setStoreName(storeN);

                  if (storeN.trim() === '') {
                    setErrStoreName("Vui lòng nhập tên cửa hàng");
                  } else {
                    setErrStoreName("");
                  }
                }}
                value={storeName}
                error={!!errStoreName}
                helperText={errStoreName}
                required
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                margin="normal"
                label={
                  <span>
                    Số điện thoại cửa hàng<span style={{ color: 'red' }}>*</span>
                  </span>
                }
                variant="outlined"
                size="small"
                type="text"
                className="w-full"
                placeholder="Nhập tại đây"
                InputLabelProps={{
                  shrink: true,
                }}
                onChange={(e) => {
                  const trimmedPhoneNumber = e.target.value.trim();
                  setPhoneNumber(trimmedPhoneNumber);

                  if (trimmedPhoneNumber === '') {
                    setErrPhoneNumber("Vui lòng nhập số điện thoại");
                  } else {
                    const regexMatch = regexPhoneNumber.test(trimmedPhoneNumber);
                    if (!regexMatch) {
                      setErrPhoneNumber("Vui lòng nhập số điện thoại hợp lệ");
                    } else {
                      setErrPhoneNumber("");
                    }
                  }
                }}
                value={phoneNumber}
                error={!!errPhoneNumber}
                helperText={errPhoneNumber}
              />
            </Grid>

            <Grid item xs={12} sm={6}>
              {useMemo(() => {
                return (
                  <>
                    <FormControl sx={{ mt: 1 }} size="small" className="w-full">
                      <InputLabel id="demo-select-small-label-province">

                        <span>
                          Tỉnh/Thành phố<span style={{ color: 'red' }}>*</span>
                        </span>
                      </InputLabel>
                      <Select
                        labelId="demo-select-small-label-province"
                        id="demo-select-small"
                        value={provinceSelect}
                        label="Tỉnh/Thành phố"
                        onChange={(e) => setProvinceSelect(e.target.value)}
                        MenuProps={MenuProps}
                      >
                        {/* <MenuItem value={-1}>
                          <em>Chọn Tỉnh/Thành phố</em>
                        </MenuItem> */}
                        {Array.isArray(province) &&
                          province.map((item) => (
                            <MenuItem key={item.id} value={item?.id}>
                              {item.name}
                            </MenuItem>
                          ))}
                      </Select>
                    </FormControl>
                  </>
                );
              }, [province, provinceSelect])}
            </Grid>
            <Grid item xs={12} sm={6}>
              {useMemo(() => {
                return (
                  <>
                    <FormControl sx={{ mt: 1 }} size="small" className="w-full">
                      <InputLabel id="demo-select-small-label-district">

                        <span>
                          Quận/Huyện<span style={{ color: 'red' }}>*</span>
                        </span>
                      </InputLabel>
                      <Select
                        labelId="demo-select-small-label-district"
                        id="demo-select-small"
                        value={districtSelect}
                        label="Quận/Huyện"
                        onChange={(e) => setDistrictSelect(e.target.value)}
                        MenuProps={MenuProps}
                      >
                        {/* <MenuItem value={-1}>
                          <em>Chọn Quận/Huyện</em>
                        </MenuItem> */}
                        {Array.isArray(district) &&
                          district.map((item) => (
                            <MenuItem key={item.id} value={item?.id}>
                              {item.name}
                            </MenuItem>
                          ))}
                      </Select>
                    </FormControl>
                  </>
                );
              }, [district, districtSelect])}
            </Grid>
            <Grid item xs={12} sm={6}>
              {useMemo(() => {
                return (
                  <>
                    <FormControl sx={{ mt: 2 }} size="small" className="w-full">
                      <InputLabel id="demo-select-small-label-ward">

                        <span>
                          Xã/Phường<span style={{ color: 'red' }}>*</span>
                        </span>
                      </InputLabel>
                      <Select
                        labelId="demo-select-small-label-ward"
                        id="demo-select-small"
                        value={wardSelect}
                        label="Xã/Phường"
                        onChange={(e) => setWardSelect(e.target.value)}
                        MenuProps={MenuProps}
                      >
                        {/* <MenuItem value={-1}>
                          <em>Chọn Xã/Phường</em>
                        </MenuItem> */}
                        {Array.isArray(ward) &&
                          ward.map((item) => (
                            <MenuItem key={item.id} value={item?.id}>
                              {item.name}
                            </MenuItem>
                          ))}
                      </Select>
                    </FormControl>
                  </>
                );
              }, [ward, wardSelect])}
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                margin="normal"
                label={
                  <span>
                    Địa chỉ<span style={{ color: 'red' }}>*</span>
                  </span>
                }
                variant="outlined"
                size="small"
                type="text"
                className="w-full"
                placeholder="Nhập tại đây"
                InputLabelProps={{
                  shrink: true,
                }}
                onChange={(e) => {

                  setAddressDetail(e.target.value);

                  if (e.target.value.trim() === '') {
                    setErrAddressDetail("Vui lòng nhập địa chỉ");
                  } else {
                    setErrAddressDetail("");
                  }
                }}
                value={addressDetail}
                error={!!errAddressDetail}
                helperText={errAddressDetail}
              />
            </Grid>
            <Grid item xs={12} sm={12}>
              <TextField
                label={
                  <span>
                    Mô tả cửa hàng<span style={{ color: 'red' }}>*</span>
                  </span>
                }
                placeholder="Nhập mô tả cửa hàng tại đây"
                multiline
                rows={2}
                className="w-full"
                InputLabelProps={{
                  shrink: true,
                }}
                onChange={(e) => {
                  const trimmedDescription = e.target.value;
                  setDescriptionDetail(trimmedDescription);

                  if (trimmedDescription.trim() === '') {
                    setErrDescriptionDetail("Vui lòng nhập mô tả");
                  } else {
                    // Additional validation logic if needed

                    setErrDescriptionDetail("");
                  }
                }}
                value={descriptionDetail}
                error={!!errDescriptionDetail}
                helperText={errDescriptionDetail}
              />
            </Grid>
            <span class="block mb-2 text-lg font-medium text-gray-900 light:text-white">
              Ảnh của hàng (nhấn vào ảnh để chọn)
            </span>
            {/* <UploadImageStore fileList={fileList}
                  setFileList={setFileList} image={image}/> */}
            <Avatar
              sx={{
                width: "50%",
                height: "30%",
                cursor: "pointer",
                borderRadius: "0%",
              }}
              alt="User Avatar"
              src={image}
              onClick={openChooseImage}
            ></Avatar>
            <input
              hidden
              type="file"
              ref={profileImage}
              onChange={changeProfileImage}
            />
          </Grid>
        </form>
        <div className="flex justify-end mt-5">
          <Button
            variant="contained"
            key="ok"
            type="primary"
            onClick={() => handleApprove()}
            style={{ marginRight: "60px" }}
          >
            Xác nhận
          </Button>

          <Button
            key="cancel"
            type="default"
            onClick={() => setModal2Open(false)}
          >
            Huỷ
          </Button>
        </div>
      </Modal>
    </>
  );
};
export default UpdateStore;
