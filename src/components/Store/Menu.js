import React, { useState } from 'react';
import {
  ContainerOutlined,
  MenuFoldOutlined,
  MenuUnfoldOutlined,
} from '@ant-design/icons';
import { LiaStoreAltSolid } from 'react-icons/lia';
import { MdProductionQuantityLimits } from 'react-icons/md';
import { AiOutlineHome, AiOutlineShopping } from 'react-icons/ai';
import { Button, Menu } from 'antd';
import { RiChatHistoryLine } from "react-icons/ri";

const MenuStore = ({setPage,setReload,reload,selectKey,setSelectKey}) => {
  const [collapsed, setCollapsed] = useState(false);
  const toggleCollapsed = () => {
    setCollapsed(!collapsed);
  };
  const getItem=(label, key, icon, children, type)=> {
    return {
      key,
      icon,  
      children,    
      label,
      type,
      onClick: () => handleSelect(key), // Thêm sự kiện onClick
    };
  }

  const handleSelect = (key)=>{
      setReload(!reload);
    setPage(key);
    setSelectKey(String(key))
  }
  const items = [
    getItem('Cửa hàng', '1', < AiOutlineHome className="text-lg"/>),
    getItem('Quản lý sản phẩm', '2', < MdProductionQuantityLimits className="text-lg"/>),
    getItem('Quản lý đơn hàng cho thuê', '3', <AiOutlineShopping className="text-lg"/>),
    getItem('Quản lý sản phẩm cho thuê', '4', <RiChatHistoryLine className="text-lg"/>),
    getItem('Thống kê', '5', <ContainerOutlined className="text-lg"/>)
  ];
  return (
    <div>
      <Button
        className='bg-[#27ae60]'
        onClick={toggleCollapsed}
        style={{
          marginBottom: 16,
        }}
      >
        {collapsed ? <MenuUnfoldOutlined /> : <MenuFoldOutlined />}
      </Button>
      <Menu
        defaultSelectedKeys={[1]}
        selectedKeys={selectKey}
        defaultOpenKeys={['sub1']}
        mode="inline"
        theme="light"
        inlineCollapsed={collapsed}
        items={items}
        className='border-white text-base'
      />
    </div>
  );
};
export default MenuStore;