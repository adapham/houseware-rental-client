import React, { useState } from 'react';
import Highcharts from 'highcharts';
import { Select } from 'antd';

const AirChart = ({ staticDataIncome,setTypeIncome }) => {
    const [tickInterval,setTickInterval]=useState(5);
    const handleChange = (value) => {
        setTypeIncome(value);
        switch (value) { 
            case 'ONE_WEEK':
              setTickInterval(2);
              break;
            case 'ONE_MONTH':
              setTickInterval(5);
              break;
            case 'ONE_YEAR':
              setTickInterval(100);
              break;
            default:
              setTickInterval(1);
              break;
          }
    };
    const maxY = staticDataIncome.money ? Math.max(
        staticDataIncome.money.reduce((a, b) => Math.max(a, b), 0)
    ) : 0;
    

    const chartHeight = 400;
    const chartConfig = {
        chart: {
            type: 'area',
            height: chartHeight
        },
        accessibility: {
            description: ''
        },
        title: {
            text: 'Thống kê doanh thu'
        },

        xAxis: {
            categories: staticDataIncome?.date || [],
            tickInterval: tickInterval,
            plotBands: [{
                // Highlight the two last years
                from: staticDataIncome?.date?.[0],
                to: staticDataIncome?.date && staticDataIncome.date.length > 0 ? staticDataIncome.date[staticDataIncome.date.length - 1] : undefined,
                color: 'rgba(68, 170, 213, .2)'
            }]
        },
        yAxis: {
            title: {
                text: 'Số tiền'
            },
            min: 0,
            max: maxY
        },
        tooltip: {
            pointFormat: '{series.name} <b>{point.y:,.0f}đ</b><br/>trong ngày {point.x}'
        },

        plotOptions: {
            area: {

                marker: {
                    enabled: false,
                    symbol: 'circle',
                    radius: 2,
                    states: {
                        hover: {
                            enabled: true
                        }
                    }
                }
            }
        },
        series: [{
            name: 'Tổng tiền đạt được là',
            data: staticDataIncome?.money
        }]
    }

    React.useEffect(() => {
        Highcharts.chart('airChart', chartConfig);
    }, [staticDataIncome]);

    return (
        <div>
          <div>
          <Select
          defaultValue="ONE_MONTH"
          style={{
            width: 120,
          }}
          onChange={handleChange}
          options={[
            {
              value: 'ONE_DAY',
              label: 'Một ngày',
            },
            {
              value: 'THREE_DAY',
              label: 'Ba ngày',
            },
            {
              value: 'ONE_WEEK',
              label: 'Một tuần',
            },
            {
              value: 'ONE_MONTH',
              label: 'Một Tháng',
            },
            {
              value: 'ONE_YEAR',
              label: 'Một năm',
            },
          ]}
        />
          </div>
          <div style={{ height: chartHeight }} id="airChart"></div>
        </div>
      );
    
};

export default AirChart;
