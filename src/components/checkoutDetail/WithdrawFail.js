import React from 'react'
import { useNavigate } from 'react-router-dom';

function WithdrawFail() {
    const navigate = useNavigate();
  return (
    <div className="flex items-center justify-center h-96 "style={{marginBottom:'160px',marginTop:'160px'}}>
    <div className="p-1 rounded shadow-lg bg-gradient-to-r from-red-500 via-yellow-500 to-pink-500">
        <div className="flex flex-col items-center p-6 space-y-2 bg-white">
            <svg
                xmlns="http://www.w3.org/2000/svg"
                className="text-red-600 w-28 h-28"
                fill="none"
                viewBox="0 0 24 24"
                stroke="currentColor"
                strokeWidth={1}
            >
                <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    d="M6 18L18 6M6 6l12 12"
                />
            </svg>
            <h1 className="text-4xl font-bold text-transparent bg-clip-text bg-gradient-to-r from-pink-500 to-red-500">
                Giao dịch thất bại
            </h1>
            <p>
                Rất tiếc, có vấn đề xảy ra trong quá trình giao dịch. Vui lòng thử lại hoặc liên hệ với chúng tôi để được hỗ trợ.
            </p>
            <button className="inline-flex items-center px-4 py-2 text-white bg-red-600 border border-red-600 rounded rounded-full hover:bg-red-700 focus:outline-none focus:ring">
                <svg
                    xmlns="http://www.w3.org/2000/svg"
                    className="w-3 h-3 mr-2"
                    fill="none"
                    viewBox="0 0 24 24"
                    stroke="currentColor"
                    strokeWidth={2}
                >
                    <path
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        d="M7 16l-4-4m0 0l4-4m-4 4h18"
                    />
                </svg>
                <span className="text-sm font-medium" onClick={()=> navigate('/profile')}>Quay lại</span>
            </button>
        </div>
    </div>
</div>

  )
}

export default WithdrawFail