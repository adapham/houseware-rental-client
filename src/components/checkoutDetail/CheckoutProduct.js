import React, { useEffect, useMemo, useState } from "react";
import {
  Box,
  Container,

  Grid,
  List,
  ListItem,

  Typography,
} from "@material-ui/core";
import { convertMoney } from "../../utils/formatConfig";
import { regexEmail, regexPhoneNumber } from "../../constants/constantsParam";
import {
  getCheckoutSubmit,
  getDistrictByProvince,

  getProvince,
  getUser,
  getWardByDistrict,
} from "../../redux/apiRequestProduct";
import { useNavigate } from "react-router-dom";
import TextField from "@mui/material/TextField";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import Select from "@mui/material/Select";
import FormControl from "@mui/material/FormControl";
import { Button, Col, Image, Modal, Row } from "antd";
import { ToastContainer, toast } from "react-toastify";
import { useSelector } from "react-redux";
import CustomizedDialogs from "../Profile/CustomizedDialogs";

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};

const CheckoutProduct = ({ cartItems }) => {
  const [openC, setOpenC] = useState(false);
  const [clientName, setClientName] = useState();
  const [phoneNumber, setPhoneNumber] = useState();

  const [email, setEmail] = useState("");
  const [ward, setWard] = useState();
  const [wardSelect, setWardSelect] = useState("");
  const [districtSelect, setDistrictSelect] = useState("");
  const [provinceSelect, setProvinceSelect] = useState("");
  const [district, setDistrict] = useState();
  const [province, setProvince] = useState();
  const [addressDetail, setAddressDetail] = useState();
  const [errClientName, setErrClientName] = useState();
  const [errPhoneNumber, setErrPhoneNumber] = useState("");
  const [errEmail, setErrEmail] = useState("");
  const [errAddressDetail, setErrAddressDetail] = useState();
  const user = useSelector((state) => state?.auth?.loadUser?.user);
  const navigate = useNavigate();
  const [selectedValue, setSelectedValue] = useState("ONLINE");
  const [isModalOpen, setOpen] = useState(false);
  const [userD, setUserD] = useState([]);
  const handleRadioChange = (event) => {
    setSelectedValue(event.target.value);
  };
  if (!user?.id) {
    navigate("/signin");
  }
  const handleName = (e) => {
    setClientName(e.target.value);

    // Your validation logic (replace this with your actual validation)
    if (e.target.value.trim() === '') {
      setErrClientName('Họ và tên là trường bắt buộc');
    } else {
      setErrClientName('');
    }
  };
  const openDepositDialog = () => {
    setOpen(true);
  };
  const closeDepositDialog = () => {
    setOpen(false);
  };

  const getUpdateUser = async (userId) => {
    const response = await getUser(userId);
    if (response) {
      setUserD(response);
    }
  };

  useEffect(() => {
    setClientName(user?.name);
    setPhoneNumber(user?.phone);
    setEmail(user?.email);
    setAddressDetail(user?.address);

    getProvinces();
    getUpdateUser(user?.id);
  }, []);
  useEffect(() => {
    if (provinceSelect) getDistrict(provinceSelect);
  }, [provinceSelect]);
  useEffect(() => {
    if (districtSelect) getWard(districtSelect);
  }, [districtSelect]);
  const getProvinces = async () => {
    const response = await getProvince();
    if (response) setProvince(response);
  };
  const getDistrict = async (provinceId) => {
    const response = await getDistrictByProvince(provinceId);
    if (response) {
      setDistrict(response);
    }
  };
  const getWard = async (districtId) => {
    const response = await getWardByDistrict(districtId);
    if (response) {
      setWard(response);
    }
  };



  useEffect(() => {
    if (user?.province && province) {
      const idProvince = province?.find(
        (item) => item.name == user?.province
      )?.id;
      setProvinceSelect(idProvince);
    }
  }, [province]);

  useEffect(() => {
    if (provinceSelect) {
      getDistrict(provinceSelect);
    }
  }, [provinceSelect]);

  useEffect(() => {
    if (user?.district && district) {
      const idDistrict = district?.find(
        (item) => item.name == user?.district
      )?.id;
      setDistrictSelect(idDistrict);
    }
  }, [district]);

  useEffect(() => {
    if (districtSelect) {
      getWard(districtSelect);
    }
  }, [districtSelect]);

  useEffect(() => {
    if (user?.ward) {
      const idWar = ward?.find((item) => item.name == user?.ward)?.id;
      setWardSelect(idWar);
    }
  }, [ward]);
  const organizedDataObject = {};

  if (cartItems && Array.isArray(cartItems)) {
    cartItems.forEach((item) => {
      if (organizedDataObject.hasOwnProperty(item.storeId)) {
        organizedDataObject[item.storeId].push(item);
      } else {
        organizedDataObject[item.storeId] = [item];
      }
    });
  }


  const calculateTotalPrice = (products) => {
    if (!products || typeof products !== "object") {
      return 0; // Return 0 if products is undefined, null, or not an object
    }
    let total = 0;
    let totalDepositePrice = 0;
    const items = Object.values(products);
    for (const product of items) {
      total +=
        product.price * product.quantity * product.rentalPeriod +
        product.depositPrice * product.quantity;
      //totalDepositePrice += product.depositPrice;
    }
    return total;
  };
  const totalBill = (products) => {
    if (!products || typeof products !== "object") {
      return 0; // Return 0 if products is undefined, null, or not an object
    }
    let total = 0;
    const items = Object.values(products);
    for (const product of items) {
      total +=
        product.price * product.quantity * product.rentalPeriod +
        product.depositPrice * product.quantity;
    }
    return convertMoney(total);
  };
  const calculateTotalDepositePrice = (products) => {
    if (!products || typeof products !== "object") {
      return 0; // Return 0 if products is undefined, null, or not an object
    }
    let total = 0;
    const items = Object.values(products);
    for (const product of items) {
      total += product.depositPrice * product.quantity;
    }
    return total;
  };
  const handleRechargeMoney = () => {
    setOpen(true);
  };

  const handlePost = (e) => {
    e.preventDefault();
    if (!clientName) {
      setErrClientName("Nhập tên của bạn");
    }
    if (!addressDetail) {
      setErrAddressDetail("Nhập địa chỉ của bạn");
    }
    if (!phoneNumber) {
      const regexMatch = regexPhoneNumber.test(phoneNumber);
      if (!regexMatch) {
        setErrPhoneNumber("Vui lòng nhập số điện thoại hợp lệ");
      }
    }
    if (!email) {
      const regexMatch = regexEmail.test(email);
      if (!regexMatch) {
        setErrEmail("Vui lòng nhập email có domain hợp lệ");
      }
    }
    if (email && phoneNumber && addressDetail && clientName
      && provinceSelect && districtSelect && wardSelect && email.trim() != ""
      && clientName.trim() != "" && phoneNumber.trim() != "" && addressDetail.trim() != ""
       && errPhoneNumber?.trim() == "" && errEmail?.trim()==""  ) {
      if (userD?.money < calculateTotalPrice(cartItems)) {
        toast.warning("Số dư không đủ", {
          position: "top-right",
          autoClose: 2000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
          theme: "light",
        });
      } else {
        const infor = {
          totalPrice: calculateTotalPrice(cartItems),
          totalDepositPrice: calculateTotalDepositePrice(cartItems),

          address: `${addressDetail}-${ward?.filter((item) => item.id === wardSelect)[0]?.name
            }-${district?.filter((item) => item.id === districtSelect)[0]?.name
            }-${province?.filter((item) => item.id === provinceSelect)[0]?.name}`,
          phone: phoneNumber.trim(),
          fullName: clientName.trim(),
          email: email.trim(),
          userID: user?.id,
          typePayment: selectedValue,
        };
        checkoutCart(infor, cartItems);
      }
    } else {
      toast.warning("Vui lòng nhập đầy đủ thông tin", {
        position: "top-right",
        autoClose: 2000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "light",
      });
    }
    setOpenC(false);
  };
  const checkoutCart = async (infor, items) => {
    try {
      const response = await getCheckoutSubmit(infor, items);
      if (response.status != 400) {
        window.location.href = "/successPayment";
        //navigate("/successPayment");
      }
    } catch (error) {
      navigate("/errorPayment")
      toast.error("Có lỗi xảy ra khi thanh toán", {
        position: "top-right",
        autoClose: 800,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "light",
      });
    }
  };
console.log(errEmail,"hoang");
  return (
    <div className="maincontainer">
      <Container>
        <Grid container  >
          <Grid
            item
            md={5} xs={12}
            className="order-md-2 mx-8 border-2 border-[#dde8da] px-8 py-4"
          >
            <Typography variant="h4" component="h4">
              Đơn hàng của bạn
            </Typography>

            <Box
              className="mt-4"
              display="flex"
              justifyContent="space-between"
              alignItems="center"
              mb={3}
            >

              <Typography
                variant="h6"
                component="span"
                className="badge badge-secondary badge-pill"
              >
                Tổng sản phẩm: {cartItems?.length > 0 ? cartItems?.length : 0}
              </Typography>
            </Box>

            <List component="ul" className="list-group mb-3">
              {Object.keys(organizedDataObject).map((storeIndex) => (
                <React.Fragment key={storeIndex}>
                  <div className="relative">
                    <p className="text-2xl ml-2 text-black inline font-bold mt-2">
                      {organizedDataObject[storeIndex][0]?.storeName}
                    </p>
                  </div>

                  {organizedDataObject[storeIndex].map((item, index) => (
                    <ListItem
                      component="li"
                      key={item?.id}
                      className="list-group-item border-dashed border-t border-gray-300 flex items-center w-full space-x-4"
                    >
                      <div className="flex">
                        <div className="flex items-center">
                          <Image.PreviewGroup
                            preview={{
                              onChange: (current, prev) =>
                                console.log(`current index: ${current}, prev index: ${prev}`),
                            }}
                          >
                            <Image width={100} src={item.imageText} />
                          </Image.PreviewGroup>

                          <div className="flex flex-col ml-4">
                            <div>
                              <p className="text-lg font-semibold">{item.title}</p>
                            </div>
                            <Grid container spacing={2} className="mt-2">
                              <Grid item xs={6} sm={6}>
                                <Typography variant="body1">
                                  Số lượng: {item.quantity}
                                </Typography>
                              </Grid>
                              <Grid item xs={6} sm={6}>
                                <Typography variant="body1">
                                  Giá cọc: {item?.depositPrice && convertMoney(item.depositPrice)} đ
                                </Typography>
                              </Grid>
                              <Grid item xs={6} sm={6}>
                                <Typography variant="body1">
                                  Tháng thuê: {item.rentalPeriod}
                                </Typography>
                              </Grid>
                              <Grid item xs={6} sm={6}>
                                <Typography variant="body1">
                                  Giá sản phẩm: {convertMoney(item.price)} đ
                                </Typography>
                              </Grid>
                            </Grid>

                          </div>
                        </div>
                      </div>
                    </ListItem>
                  ))}

                </React.Fragment>
              ))}
              <ListItem
                component="li"
                className="list-group-item border-dashed border-t border-gray-300 bg-gray-100 flex  items-center mt-5"
                style={{ justifyContent: "space-between" }}
              >
                <div className="text-success">
                  <h5 className="my-0 text-xl font-medium">
                    Tổng Tiền Thanh toán
                  </h5>
                </div>
                <div className="text-success">
                  <h5 className="my-0 text-xl font-medium">
                    {totalBill(cartItems)}
                  </h5>
                </div>
              </ListItem>
            </List>
          </Grid>

          <Grid
            item
            md={6} xs={12}
            className="order-md-1 mx-8 border-2 border-[#dde8da] px-8 py-4"
          >
            <Typography variant="h4" component="h4" className="mb-3">
              Thông tin thanh toán
              <div className="bg-gray-700 max-w-[40%] h-0.5 block"></div>
            </Typography>
            <form className="needs-validation mt-2" noValidate>
              <Grid container spacing={2}>
                <Grid item xs={12} sm={6}>
                  <TextField
                    margin="dense"
                    label={
                      <span>
                        Họ và tên<span style={{ color: 'red' }}>*</span>
                      </span>
                    }
                    variant="outlined"
                    size="small"
                    onChange={handleName}
                    value={clientName}
                    type="text"
                    className="w-full"
                    placeholder="Nhập họ và tên"
                    error={!!errClientName}
                    helperText={errClientName}
                  />
                </Grid>
                <Grid item xs={12} sm={6}>
                  <TextField
                    margin="dense"

                    label={
                      <span>
                        Số điện thoại<span style={{ color: 'red' }}>*</span>
                      </span>
                    }
                    variant="outlined"
                    size="small"
                    onChange={(e) => {
                      const trimmedPhoneNumber = e.target.value.trim();
                      setPhoneNumber(trimmedPhoneNumber);

                      if (trimmedPhoneNumber === '') {
                        setErrPhoneNumber("Vui lòng nhập số điện thoại");
                      } else {
                        const regexMatch = regexPhoneNumber.test(trimmedPhoneNumber);
                        if (!regexMatch) {
                          setErrPhoneNumber("Vui lòng nhập số điện thoại hợp lệ");
                          
                        } else {
                          setErrPhoneNumber("");
                        }
                      }
                    }}

                    value={phoneNumber}
                    type="text"
                    className="w-full"
                    placeholder="Nhập số điện thoại của bạn"
                    error={!!errPhoneNumber}
                    helperText={errPhoneNumber}
                  />
                </Grid>
                <Grid item xs={12} sm={6}>
                  <TextField
                    margin="dense"

                    label={
                      <span>
                        Email<span style={{ color: 'red' }}>*</span>
                      </span>
                    }
                    variant="outlined"
                    size="small"
                    onChange={(e) => {
                      const emailTrim = e.target.value.trim();
                      setEmail(emailTrim);
                    
                      if (emailTrim === '') {
                        setErrEmail("Vui lòng nhập địa chỉ");
                      } else {
                        const regexMatch = regexEmail.test(emailTrim);
                        if (!regexMatch) {
                          setErrEmail("Vui lòng nhập địa chỉ email hợp lệ");
                        } else {
                          setErrEmail("");
                        }
                      }
                    }}

                    value={email}
                    type="text"
                    className="w-full"
                    placeholder="Nhập email nhận hàng của bạn"
                    error={!!errEmail}
                    helperText={errEmail}
                  />
                </Grid>
                <Grid item xs={12} sm={6}>
                  {useMemo(() => {
                    return (
                      <>
                        <FormControl
                          sx={{ mt: 1 }}
                          size="small"
                          className="w-full"
                        >
                          <InputLabel id="demo-select-small-label-province">

                            <span>
                              Tỉnh/Thành phố<span style={{ color: 'red' }}>*</span>
                            </span>
                          </InputLabel>
                          <Select
                            labelId="demo-select-small-label-province"
                            id="demo-select-small"
                            value={provinceSelect}

                            label={
                              <span>
                                Tỉnh/Thành phố<span style={{ color: 'red' }}>*</span>
                              </span>
                            }
                            onChange={(e) =>
                              setProvinceSelect(e.target.value)
                            }
                            MenuProps={MenuProps}
                          >
                            {Array.isArray(province) &&
                              province.map((item) => (
                                <MenuItem key={item.id} value={item?.id}>
                                  {item.name}
                                </MenuItem>
                              ))}
                          </Select>
                        </FormControl>
                      </>
                    );
                  }, [province, provinceSelect])}
                </Grid>
                <Grid item xs={12} sm={6}>
                  {useMemo(() => {
                    return (
                      <>
                        <FormControl
                          sx={{ mt: 1 }}
                          size="small"
                          className="w-full"
                        >
                          <InputLabel id="demo-select-small-label-district">
                            
                            <span>
                            Quận/Huyện<span style={{ color: 'red' }}>*</span>
                            </span>
                          </InputLabel>
                          <Select
                            labelId="demo-select-small-label-district"
                            id="demo-select-small"
                            value={districtSelect}

                            label={
                              <span>
                                Quận/Huyện<span style={{ color: 'red' }}>*</span>
                              </span>
                            }
                            onChange={(e) =>
                              setDistrictSelect(e.target.value)
                            }
                            MenuProps={MenuProps}
                          >

                            {Array.isArray(district) &&
                              district.map((item) => (
                                <MenuItem key={item.id} value={item?.id}>
                                  {item.name}
                                </MenuItem>
                              ))}
                          </Select>
                        </FormControl>
                      </>
                    );
                  }, [district, districtSelect])}
                </Grid>
                <Grid item xs={12} sm={6}>
                  {useMemo(() => {
                    return (
                      <>
                        <FormControl
                          sx={{ mt: 1 }}
                          size="small"
                          className="w-full"
                        >
                          <InputLabel id="demo-select-small-label-ward">
                            
                            <span>
                            Xã/Phường<span style={{ color: 'red' }}>*</span>
                            </span>
                          </InputLabel>
                          <Select
                            labelId="demo-select-small-label-ward"
                            id="demo-select-small"
                            value={wardSelect}

                            label={
                              <span>
                                Xã/Phường<span style={{ color: 'red' }}>*</span>
                              </span>
                            }
                            onChange={(e) => setWardSelect(e.target.value)}
                            MenuProps={MenuProps}
                          >

                            {Array.isArray(ward) &&
                              ward.map((item) => (
                                <MenuItem key={item.id} value={item?.id}>
                                  {item.name}
                                </MenuItem>
                              ))}
                          </Select>
                        </FormControl>
                      </>
                    );
                  }, [ward, wardSelect])}
                </Grid>
                <Grid item xs={12} sm={12}>
                  <TextField
                    margin="dense"

                    label={
                      <span>
                        Địa chỉ<span style={{ color: 'red' }}>*</span>
                      </span>
                    }
                    variant="outlined"
                    size="small"
                    onChange={(e) => {

                      setAddressDetail(e.target.value);

                      if (e.target.value.trim() === '') {
                        setErrAddressDetail("Vui lòng nhập địa chỉ");
                      } else {
                        setErrAddressDetail("");
                      }
                    }}
                    value={addressDetail}
                    type="text"
                    className="w-full"
                    placeholder="Nhập địa chỉ cụ thể của bạn"
                    error={!!errAddressDetail}
                    helperText={errAddressDetail}
                  />
                </Grid>
              </Grid>
              <div className="flex justify-between mt-5">
                <p
                  onClick={() => setOpenC(true)}
                  style={{ cursor: "pointer" }}
                  type="submit"
                  className="mt-2 py-2 px-10 border-solid border-2 border-[#b5d69f] text-[#57ad3f] bg-[#dde8da]
                hover:bg-[#f1f6f0] rounded-md font-semibold uppercase mb-4 duration-300"
                // disabled={user?.money < calculateTotalPrice(cartItems)}
                >
                  Thanh toán
                </p>
                <a
                  onClick={handleRechargeMoney}
                  type="submit"
                  className="mt-2 py-2 px-10 border-solid border-2 border-[#b5d69f] text-[#57ad3f] bg-[#dde8da]
                hover:bg-[#f1f6f0] rounded-md font-semibold uppercase mb-4 duration-300"
                >
                  Nạp Tiền
                </a>
              </div>

              <ToastContainer
                position="top-center"
                autoClose={5000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
                theme="light"
              />
            </form>
          </Grid>
        </Grid>
      </Container>
      <CustomizedDialogs
        isModalOpen={isModalOpen}
        closeModal={closeDepositDialog}
        title={"Nạp tiền"}
        label={"Số tiền nạp"}
        balances={user?.money ? user.money.toLocaleString() + " đ" : 0}
        id={user?.id}
      />
      <Modal
        title="Xác nhận thanh toán đơn hàng"
        open={openC}
        footer={null}
        okText="Xác nhận"
        cancelText="Quay lại"
        onCancel={() => setOpenC(false)}
      >

        <div className="flex justify-end mt-5">

          <Button
            className="mt-2  bg-[#1975D2] text-white "
            onClick={handlePost}
          >
            Xác nhận
          </Button>
          <Button className="mt-2  bg-[#1975D2] text-white " onClick={() => setOpenC(false)}>
            Huỷ
          </Button>
        </div>
      </Modal>
    </div>
  );
};

export default CheckoutProduct;
