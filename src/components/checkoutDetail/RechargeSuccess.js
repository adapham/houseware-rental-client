import React from 'react'
import { useNavigate } from 'react-router-dom';

function RechargeSuccess() {
    const navigate = useNavigate();
  return (
    <div className="flex items-center justify-center h-96" style={{marginBottom:'160px',marginTop:'160px'}}>
    <div className="p-1 rounded shadow-lg bg-gradient-to-r from-green-500 via-yellow-500 to-blue-500">
        <div className="flex flex-col items-center p-6 space-y-2 bg-white">
            <svg
                xmlns="http://www.w3.org/2000/svg"
                className="text-green-600 w-28 h-28"
                fill="none"
                viewBox="0 0 24 24"
                stroke="currentColor"
                strokeWidth={1}
            >
                <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z"
                />
            </svg>
            <h1 className="text-4xl font-bold text-transparent bg-clip-text bg-gradient-to-r from-blue-500 to-green-500">
                Nạp tiền thành công
            </h1>
            <p>
                Cảm ơn bạn đã nạp tiền thành công! Hãy kiểm tra tài khoản của bạn để sử dụng dịch vụ của chúng tôi.
            </p>
            <button className="inline-flex items-center px-4 py-2 text-white bg-green-600 border border-green-600 rounded rounded-full hover:bg-green-700 focus:outline-none focus:ring">
                <svg
                    xmlns="http://www.w3.org/2000/svg"
                    className="w-3 h-3 mr-2"
                    fill="none"
                    viewBox="0 0 24 24"
                    stroke="currentColor"
                    strokeWidth={2}
                >
                    <path
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        d="M7 16l-4-4m0 0l4-4m-4 4h18"
                    />
                </svg>
                <span className="text-sm font-medium" onClick={()=> navigate('/profile')}>Quay lại</span>
            </button>
        </div>
    </div>
</div>

  )
}

export default RechargeSuccess