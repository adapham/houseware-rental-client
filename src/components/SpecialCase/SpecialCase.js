import React, { useCallback, useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { RiShoppingCart2Fill } from "react-icons/ri";
import { MdSwitchAccount } from "react-icons/md";
import { useSelector } from "react-redux";
import { getProductsInCard } from "../../redux/apiRequestProduct";

const SpecialCase = () => {
  const user = useSelector((state) => state?.auth?.loadUser?.user);
  const [quantity, setQuantity] = useState(0);
  //product từ redux store
  const products = useSelector((state) => state.orebiReducer?.products);
  
  //product từ DB
  const [productDb, setProductDb] = useState([]);
  
  // Lấy giá trị từ DB ra
  const getProductInCard = async (id) => {
    try {
      const response = await getProductsInCard(id);
      if (response) {
        setProductDb(response);
      }
    } catch (error) {
      console.error("Error fetching products:", error);
    }
  };
  
  // Reload khi product thay đổi
  useEffect(() => {
    if (user?.id) {
      getProductInCard(user?.id);
    } else {
      setProductDb(products);
    }
  }, [user?.id, products]);
  useEffect(() => {
    setQuantity(productDb.length);
    //setQuantity(productDb.reduce((acc, p) => acc + p.quantity, 0));
  }, [productDb]);

  return (
    <div className="fixed top-52 right-2 z-20 hidden md:flex flex-col gap-2">
      <Link to="/profile">
        <div className="bg-white w-16 h-[70px] rounded-md flex flex-col gap-1 text-[#33475b] justify-center items-center shadow-testShadow overflow-x-hidden group cursor-pointer">
          <div className="flex justify-center items-center">
            <MdSwitchAccount className="text-2xl -translate-x-12 group-hover:translate-x-3 transition-transform duration-200" />

            <MdSwitchAccount className="text-2xl -translate-x-3 group-hover:translate-x-12 transition-transform duration-200" />
          </div>
          <p className="text-xs font-semibold font-titleFont">Hồ sơ</p>
        </div>
      </Link>
      <Link to="/cart">
        <div className="bg-white w-16 h-[70px] rounded-md flex flex-col gap-1 text-[#33475b] justify-center items-center shadow-testShadow overflow-x-hidden group cursor-pointer relative">
          <div className="flex justify-center items-center">
            <RiShoppingCart2Fill className="text-2xl -translate-x-12 group-hover:translate-x-3 transition-transform duration-200" />

            <RiShoppingCart2Fill className="text-2xl -translate-x-3 group-hover:translate-x-12 transition-transform duration-200" />
          </div>
          <p className="text-xs font-semibold font-titleFont">Giỏ hàng</p>
          {productDb ? (
            <p className="absolute top-1 right-2 bg-primeColor text-white text-xs w-4 h-4 rounded-full flex items-center justify-center font-semibold">
              {quantity}
            </p>
          ) : (
            <p>Loading...</p>
          )}
        </div>
      </Link>
    </div>
  );
};

export default SpecialCase;
