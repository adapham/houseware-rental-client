import React from "react";
import { Link } from "react-router-dom";
import { Image } from 'antd';
import {
  bannerImgOne,
  bannerImgTwo,
  bannerImgThree,
} from "../../assets/images";
import { Carousel } from 'antd';


const Banner = () => {
  
  return (
    <div className="w-ful bg-white">
      <Carousel autoplay>
        <div>
          <Link to="/shop">
            <Image preview={false} height={300} width='100%' className="w-full  object-cover " src={bannerImgOne}  />
          </Link>
        </div>
        <div>
          <Link to="/shop">
            <Image preview={false} height={300} width='100%'   className="w-full  object-cover " src={bannerImgTwo} />
          </Link>
        </div>
        <div>
          <Link to="/shop">
            <Image preview={false} height={300} width='100%'   className="w-full  object-cover " src={bannerImgThree} /> 
          </Link>
        </div>
      
      </Carousel>
    </div>
  );
};

export default Banner;
