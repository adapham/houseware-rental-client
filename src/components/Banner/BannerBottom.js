import React from "react";

import {
  featureOne,
  featureThree,
  featureFour,
  featureFive,
  featureSix,
} from "../../assets/images";

const BannerBottom = () => {
  return (
    <>
      <section className="featured section-padding position-relative mb-10">
        <div className="container">
          <div className="grid grid-cols-5 gap-5">
            <div className="mx-2 py-2 border-2 border-[#dde8da] rounded-md">
              <div className="banner-features wow fadeIn animated hover-up grid justify-items-center">
                <img src={featureOne} alt="" />
                <h4 className="bg-2 text-xl text-center font-semibold mt-2">Đặt hàng trực tuyến</h4>
              </div>
            </div>
            <div className="mx-2 py-2 border-2 border-[#dde8da] rounded-md">
              <div className="banner-features wow fadeIn animated hover-up grid justify-items-center">
                <img src={featureThree} alt="" />
                <h4 className="bg-3 text-xl font-semibold mt-2">Tiết kiệm tiền</h4>
              </div>
            </div>
            <div className="mx-2 py-2 border-2 border-[#dde8da] rounded-md">
              <div className="banner-features wow fadeIn animated hover-up grid justify-items-center">
                <img src={featureFour} alt="" />
                <h4 className="bg-4 text-xl font-semibold mt-2">Khuyến mãi</h4>
              </div>
            </div>
            <div className="mx-2 py-2 border-2 border-[#dde8da] rounded-md">
              <div className="banner-features wow fadeIn animated hover-up grid justify-items-center">
                <img src={featureFive} alt="" />
                <h4 className="bg-5 text-xl font-semibold mt-2">Vui vẻ mua sắm</h4>
              </div>
            </div>
            <div className="mx-2 py-2 border-2 border-[#dde8da] rounded-md">
              <div className="banner-features wow fadeIn animated hover-up grid justify-items-center">
                <img src={featureSix} alt="" />
                <h4 className="bg-6 text-xl font-semibold mt-2">Hỗ trợ 24/7</h4>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

export default BannerBottom;
