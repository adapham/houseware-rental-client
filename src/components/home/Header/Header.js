import { motion } from "framer-motion";
import React, { useCallback, useEffect, useState, useRef } from "react";
import { FaCaretDown, FaSearch, FaUser } from "react-icons/fa";
import { FiUser } from "react-icons/fi";
import {
  HiLogin,
  HiLogout,
  HiMenuAlt2,
  HiOutlineMenuAlt4,
} from "react-icons/hi";
import { MdClose } from "react-icons/md";
import { useDispatch, useSelector } from "react-redux";
import { Link, NavLink, useLocation, useNavigate } from "react-router-dom";
import { logoLight } from "../../../assets/images";
import { navBarList, paginationItems } from "../../../constants";
import { loadUserSuccess } from "../../../redux/Slide/authSlide";
import { updateFilter, updatePageble } from "../../../redux/Slide/productSlice";
import {
  getAllCateory,
  getAllProduct,
  getNotification,
  getUpdateIsRead,
} from "../../../redux/apiRequestProduct";
import Flex from "../../designLayouts/Flex";
import Image from "../../designLayouts/Image";
import { Badge, Drawer, List, Space, Typography } from "antd";
import { BellFilled } from "@ant-design/icons";
import Box from '@mui/material/Box';

import MailIcon from '@mui/icons-material/Mail';
import MarkChatUnreadOutlinedIcon from '@mui/icons-material/MarkChatUnreadOutlined';

const Header = () => {
  const user = useSelector((state) => state?.auth?.loadUser?.user);
  const [showUser, setShowUser] = useState(false);
  const [showMenu, setShowMenu] = useState(true);
  const [sidenav, setSidenav] = useState(false);
  const [category, setCategory] = useState(false);

  const location = useLocation();
  const [searchQuery, setSearchQuery] = useState("");
  const [filteredProducts, setFilteredProducts] = useState([]);
  const produtStore = useSelector((state) => state.product);
  const { filters } = produtStore;
  const [show, setShow] = useState(false);
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const [productsSearch, setProductsSearch] = useState();
  const [notificationsOpen, setNotificationsOpen] = useState(false);
  const [orders, setOrders] = useState([]);
  const searchContainerRef = useRef(null);
  const [reload, setReload] = useState(false);
  const getAllCategories = async () => {
    const response = await getAllCateory();
    if (response) {
      setCategory(response);
    }
  };

  const getProducts = async () => {

    const response = await getAllProduct(filters);
    if (response) {

      setProductsSearch(response.content);
    }
  };


  const handleFilter = (id) => {
    dispatch(updateFilter({ categoryId: id }));
    dispatch(updatePageble({ pageIndex: 1, pageSize: 12 }));
    navigate("/shop", {
      state: {
        id: id,
      },
    });
  };
  const handleSearch = (e) => {
    const value = e.target.value;

    dispatch(updateFilter({ keyword: value }));
    setSearchQuery(value);
  };

  function handlerLogout() {
    localStorage.removeItem("access-token");
    localStorage.removeItem("user");
    navigate("/");
    dispatch(loadUserSuccess(null));
  }

  useEffect(() => {
    let ResponsiveMenu = () => {
      if (window.innerWidth < 667) {
        setShowMenu(false);
      } else {
        setShowMenu(true);
      }
    };
    ResponsiveMenu();
    window.addEventListener("resize", ResponsiveMenu);
  }, []);

  useEffect(() => {
    getProducts();
  }, [filters]);

  useEffect(() => {
    getAllCategories();
  }, []);

  useEffect(() => {
    const filtered = paginationItems.filter((item) =>
      item.productName.toLowerCase().includes(searchQuery.toLowerCase())
    );
    setFilteredProducts(filtered);
  }, [searchQuery]);

  const divRef = useRef(null);
  const divRef2 = useRef(null);
  useEffect(() => {
    // Function to handle clicks outside the div
    const handleClickOutside = (event) => {
      if (divRef.current && !divRef.current.contains(event.target)) {
        setShow(false);
      }
    };
    // Add event listener when the component mounts
    document.addEventListener("mousedown", handleClickOutside);
    // Remove event listener when the component unmounts
    return () => {
      document.removeEventListener("mousedown", handleClickOutside);
    };
  }, [divRef]);
  const handleClickOutside2 = (event) => {
    if (
      divRef2.current &&
      !divRef2.current.contains(event.target)
    ) {
      // Clicked outside the search container, close the search result
      setShowUser(false);
    }
  };

  const getAllNotification = async () => {
    const response = await getNotification(user?.id);
    if (response) {
      setOrders(response);
    }
  };
  useEffect(() => {
    if (user?.id) {
      getAllNotification();
    }
  }, [reload]);
  const handleClickOutside = (event) => {
    if (
      searchContainerRef.current &&
      !searchContainerRef.current.contains(event.target)
    ) {
      // Clicked outside the search container, close the search result
      setSearchQuery("");
    }
  };
  useEffect(() => {
    // Add click event listener when the component mounts
    document.addEventListener("click", handleClickOutside);
    // Remove click event listener when the component unmounts
    return () => {
      document.removeEventListener("click", handleClickOutside);
    };
  }, []);
  useEffect(() => {
    // Add click event listener when the component mounts
    document.addEventListener("click", handleClickOutside2);
    // Remove click event listener when the component unmounts
    return () => {
      document.removeEventListener("click", handleClickOutside2);
    };
  }, []);
  const updateIsRead = async (id) => {
    const response = await getUpdateIsRead(id);
    if (response) {
      setReload(!reload);
    }
  }
  const handleItemClick = (id, isRead) => {
    if (!isRead) {
      updateIsRead(id);
    }

  }
  const handleView = (item) => {
    if (item?.notificationType == "RENTED") {
      navigate(`/store/${item.userToId}`, { state: "ALL" });
    } else {
      navigate('/profile', { state: "ALL" });
    }
    setNotificationsOpen(false);
    updateIsRead(item?.id);
  }
  console.log(orders, "hoang");
  return (
    <div className="w-full h-20 bg-[#dde8da] sticky top-0 z-50 border-b-[1px] border-b-gray-200">
      <nav className="h-full px-4 max-w-container mx-auto relative">
        <Flex className="flex items-center justify-between h-full">
          <Link to="/">
            <div>
              <Image className="w-20 object-cover" imgSrc={logoLight} />
            </div>
          </Link>
          <div
            onClick={() => setShow(!show)}
            ref={divRef}
            className="flex h-14 cursor-pointer items-center gap-2 text-primeColor "
          >
            <HiOutlineMenuAlt4 className="w-5 h-5" />
            <p className="text-base text-center text-[#767676] font-normal hover:font-bold hover:text-[#262626] w-40">
              Danh mục sản phẩm{" "}
            </p>

            {show && (
              <motion.ul
                initial={{ y: 30, opacity: 0 }}
                animate={{ y: 0, opacity: 1 }}
                transition={{ duration: 0.5 }}
                className="absolute top-20 z-50 bg-[#dde8da] w-auto h-auto p-8 pb-8"
              >
                {Array.isArray(category) ? (
                  category.map(({ id, categoryName }) => (
                    <li
                      key={id}
                      onClick={() => handleFilter(id)}
                      className="text-gray-900 px-4 py-1 border-b-[1px] border-b-gray-400 hover:border-b-[#57ad3f] hover:text-[#57ad3f] duration-300 cursor-pointer"
                    >
                      {categoryName}
                    </li>
                  ))
                ) : (
                  <p>Không có thể loại nào</p>
                )}
              </motion.ul>
            )}
          </div>
          <div className="relative w-full lg:w-[350px] h-[40px] border-2 border-gray-300 text-base text-primeColor bg-white flex items-center gap-2 justify-between px-6 rounded-3xl">
            <input
              className="flex-1 h-full outline-none placeholder:text-[#C4C4C4] placeholder:text-[14px]"
              type="text"
              onChange={handleSearch}
              value={searchQuery}
              placeholder="Bạn cần thuê sản phẩm gì?"
            />

            <FaSearch className="w-5 h-5" />
            {searchQuery && (
              <div
                ref={searchContainerRef}
                style={{ marginTop: "-6px" }}
                className={`w-full mx-auto h-96 bg-[#dde8da] top-16 absolute left-0 z-50 overflow-y-scroll shadow-2xl scrollbar-hide cursor-pointer`}
              >
                {searchQuery && productsSearch?.length > 0 ? (
                  productsSearch.map((item) => (
                    <div
                      onClick={() =>
                        navigate(
                          `/product/${item?.title
                            .toLowerCase()
                            .split(" ")
                            .join("")}`,
                          {
                            state: {
                              item: item.id,
                            },
                          }
                        ) & setSearchQuery("")
                      }
                      key={item._id}
                      className="max-w-[600px] h-28 bg-[#dde8da] mb-3 flex items-center gap-3 m-4"
                    >
                      <img
                        className="w-24"
                        src={item.imageTextUri[0]}
                        alt="productImg"
                      />
                      <div className="flex flex-col gap-1">
                        <p className="font-semibold text-lg">{item.title}</p>
                        <p className="text-xs overflow-hidden">
                          {item.categoryName}
                        </p>
                        <p className="text-sm">
                          Price:{" "}
                          <span className="text-primeColor font-semibold">
                            ${item.price}
                          </span>
                        </p>
                      </div>
                    </div>
                  ))
                ) : (
                  <p>Không có sản phẩm nào.</p>
                )}
              </div>
            )}
          </div>
          <div>
            {showMenu && (
              <motion.ul
                initial={{ y: 30, opacity: 0 }}
                animate={{ y: 0, opacity: 1 }}
                transition={{ duration: 0.5 }}
                className="flex items-center w-auto z-50 p-0 gap-2"
              >
                <>
                  {navBarList.map(({ _id, title, link }) => (
                    <NavLink
                      key={_id}
                      className="flex font-normal hover:font-bold h-6 justify-center items-center px-12 text-base text-center text-[#767676] hover:underline underline-offset-[4px] decoration-[1px] hover:text-[#262626] md:border-r-[2px] border-r-gray-300 hoverEffect last:border-r-0"
                      to={link}
                      state={{ data: location.pathname.split("/")[1] }}
                    >
                      <li>{title}</li>
                    </NavLink>
                  ))}
                </>
              </motion.ul>
            )}
            <HiMenuAlt2
              onClick={() => setSidenav(!sidenav)}
              className="inline-block md:hidden cursor-pointer w-8 h-6 absolute top-6 right-4"
            />
          </div>
          {user?.id && (

            <>
              <Drawer
                title="Thông báo"
                open={notificationsOpen}
                onClose={() => {
                  setNotificationsOpen(false);
                }}
                maskClosable
              >
                <List
                  dataSource={orders}
                  renderItem={(item) => {
                    const isUnread = !item.isRead;
                    return (
                      <List.Item
                        className={`${isUnread ? 'bg-green-100' : 'bg-white'
                          } mb-4 p-4 rounded-md shadow-md hover:shadow-lg transition duration-300`}
                      >
                        <div className="flex justify-between items-center">
                          <div className={`ml-4 ${!isUnread ? '' : 'cursor-pointer '
                            }`} onClick={() => handleView(item)}>
                            <Typography.Text className="text-sm text-gray-500">
                              {item?.createdTime}
                            </Typography.Text>
                            <br />
                            <Typography.Text className="font-semibold text-base">
                              {item?.content}
                            </Typography.Text>

                          </div>
                          <button
                            onClick={() => handleItemClick(item.id, item.isRead)}

                            className={`${isUnread ? 'bg-green-500' : 'bg-gray-300'
                              } text-white px-3 py-1 rounded-md  transition duration-300 ml-2
                              ${!isUnread ? '' : 'hover:bg-green-600'
                              }`}
                          >
                            Đánh dâu đã đọc
                          </button>
                        </div>
                      </List.Item>
                    );
                  }}
                ></List>
              </Drawer>
            </>
          )
          }


          {Array.isArray(orders) && user?.id && (
            <>
              <Box sx={{ color: 'action.active', cursor: 'pointer' }} onClick={() => {
                navigate('/profile', { state: 1 });
              }}>
                <Badge color="secondary" variant="dot">
                  <MarkChatUnreadOutlinedIcon />
                </Badge>
              </Box>

              <Space>
                <Badge count={orders.filter((item) => item.isRead === false).length}>
                  <BellFilled
                    style={{ fontSize: 24 }}
                    onClick={() => {
                      setNotificationsOpen(true);
                      setReload(!reload);
                    }}
                  />
                </Badge>
              </Space>
            </>
          )}





          <div className="flex gap-4 mt-2 lg:mt-0 items-center pr-6 cursor-pointer relative">
            <div onClick={() => setShowUser(!showUser)} ref={divRef2} className="flex">
              <FaUser />
              <FaCaretDown />
            </div>
            {showUser && (
              <motion.ul
                initial={{ y: 30, opacity: 0 }}
                animate={{ y: 0, opacity: 1 }}
                transition={{ duration: 0.5 }}
                className="absolute top-12 right-0 z-50 bg-[#dde8da] w-48 h-auto p-5 pb-6"
              >
                {user ? (
                  <>
                    <Link to="/profile" >
                      <li className="text-gray-900 hover:border-b-[#57ad3f] hover:text-[#57ad3f] px-4 py-1 border-b-[1px] border-b-gray-400 hover:border-b-white hover:text-white duration-300 cursor-pointer">
                        <div className="flex items-center justify-start">
                          <FiUser className="mr" />
                          <span>Tài khoản</span>
                        </div>
                      </li>
                    </Link>
                    <li
                      onClick={handlerLogout}
                      className="text-gray-900 hover:border-b-[#57ad3f] hover:text-[#57ad3f] px-4 py-1 border-b-[1px] border-b-gray-400  hover:border-b-white hover:text-white duration-300 cursor-pointer"
                    >
                      <div className="flex items-center justify-start">
                        <HiLogout className="mr-2" />
                        <span>Thoát</span>
                      </div>
                    </li>
                  </>
                ) : (
                  <>
                    <Link to="/signin">
                      <li className="text-gray-900 hover:border-b-[#57ad3f] hover:text-[#57ad3f] px-4 py-1 border-b-[1px] border-b-gray-400 hover:border-b-white hover:text-white duration-300 cursor-pointer">
                        <div className="flex items-center justify-start">
                          <HiLogin className="mr-2" />
                          <span>Đăng nhập</span>
                        </div>
                      </li>
                    </Link>
                    <Link onClick={() => setShowUser(false)} to="/signup">
                      <li className="text-gray-900 hover:border-b-[#57ad3f] hover:text-[#57ad3f] px-4 py-1 border-b-[1px] border-b-gray-400 hover:border-b-white hover:text-white duration-300 cursor-pointer">
                        <div className="flex items-center justify-start">
                          <FiUser className="mr-2" />
                          <span>Đăng ký</span>
                        </div>
                      </li>
                    </Link>
                  </>
                )}
              </motion.ul>
            )}
          </div>
        </Flex>
      </nav>
    </div>
  );
};

export default Header;
