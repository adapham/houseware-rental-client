import React, { useEffect, useState } from "react";
import Heading from "../Products/Heading";
import Product from "../Products/Product";
import {
  bestSellerOne,

} from "../../../assets/images/index";
import { getTopSellers } from "../../../redux/apiRequestProduct";

const BestSellers = () => {
  const [topSeller, setTopSeller] = useState([]);
  useEffect(() => {
    getTopSeller();
  }, [])
  const getTopSeller = async () => {
    const response = await getTopSellers();
    if (response) {

      setTopSeller(response);
    }
  }
  return (
    <div className="w-full pb-20">
      <Heading heading="Sản phẩm thuê nhiều" />
      <div className="w-full grid grid-cols-1 md:grid-cols-2 lgl:grid-cols-3 xl:grid-cols-4 gap-10">
        {topSeller?.map((item) => (
          <div key={item.id}>
            <Product
              _id={item?.id}
              img={item?.imageTextUri[0]}
              productName={item?.title}
              price={item?.price}
              createDate={item?.createdTime}
              des={item?.description}
              categoryName={item?.categoryName}
              depositPrice={item?.depositPrice}
              quantity={item?.quantity}
              status={item?.status}
              storeId={item?.storeId}
              storeName={item?.storeName}
            />
          </div>

        ))}
      </div>
    </div>
  );
};

export default BestSellers;
