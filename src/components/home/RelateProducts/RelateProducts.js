import React, { useEffect, useState } from "react";
import Heading from "../Products/Heading";
import Product from "../Products/Product";
import SampleNextArrow from "../NewArrivals/SampleNextArrow";
import SamplePrevArrow from "../NewArrivals/SamplePrevArrow";
import { getRelateProducts } from "../../../redux/apiRequestProduct";
import Slider from "react-slick";

const RelateProducts = ({categoryId}) => {

  const [relateProduct, setRelateProduct] = useState([]);
  const settings = {
    infinite: true,
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 1,
    nextArrow: <SampleNextArrow />,
    prevArrow: <SamplePrevArrow />,
    responsive: [
      {
        breakpoint: 1025,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
          infinite: true,
        },
      },
      {
        breakpoint: 769,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          infinite: true,
        },
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          infinite: true,
        },
      },
    ],
  };
  useEffect(() => {
    if(categoryId){
      getRelateProduct(categoryId);
    }
  }, [categoryId])
  const getRelateProduct = async (id) => {
    const response = await getRelateProducts(id);

    if (response) {
      setRelateProduct(response);
    }
  }

  return (
    <div className="w-full pb-20">
      <Heading heading="Sản phẩm tương tự" />
      <Slider {...settings}>
        {relateProduct && 
          relateProduct?.map((item,index) => (
            <div className="px-2" key={item?.id}>
            <Product
              _id={item?.id}
              img={item?.imageTextUri[0]}
              productName={item?.title}
              price={item?.price}
              createDate={item?.createdTime}
              des={item?.description}
              categoryName={item?.categoryName}
              depositPrice={item?.depositPrice}
              quantity={item?.quantity}
              status={item?.status}
              storeId={item?.storeId}
              storeName={item?.storeName}
            />
          </div>
          ))
        }
      </Slider>
    </div>
  );
};

export default RelateProducts;
