import React, { useEffect, useState } from "react";
import Slider from "react-slick";
import Heading from "../Products/Heading";
import Product from "../Products/Product";
import {
  newArrOne,
} from "../../../assets/images/index";
import SampleNextArrow from "./SampleNextArrow";
import SamplePrevArrow from "./SamplePrevArrow";
import { isTimestampWithinOneWeek } from "../../../utils/formatConfig";
import { getNewArrivals } from "../../../redux/apiRequestProduct";

const NewArrivals = () => {
  const [newArrival, setNewArrival] = useState([]);
  const settings = {
    infinite: true,
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 1,
    nextArrow: <SampleNextArrow />,
    prevArrow: <SamplePrevArrow />,
    responsive: [
      {
        breakpoint: 1025,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
          infinite: true,
        },
      },
      {
        breakpoint: 769,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          infinite: true,
        },
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          infinite: true,
        },
      },
    ],
  };
  useEffect(()=>{
    getNewArrival();
  },[])
  const getNewArrival = async () => {
    const response = await getNewArrivals();
    if (response) {
      setNewArrival(response);
    }
  }

  return (
    <div className="w-full pb-16">
      <Heading heading="Sản phẩm mới" />
      <Slider {...settings}>

        {
          newArrival?.map((item) => (
            <div className="px-2" key={item?.id}>
              <Product
                _id={item?.id}
                img={item?.imageTextUri[0]}
                productName={item?.title}
                price={item?.price}
                createDate={item?.createdTime}
                des={item?.description}
                categoryName={item?.categoryName}
                depositPrice={item?.depositPrice}
                quantity={item?.quantity}
                status={item?.status}
                storeId={item?.storeId}
                storeName={item?.storeName}
              />
            </div>
          ))
        }
      </Slider>
    </div>
  );
};

export default NewArrivals;
