import React from "react";

import { FaShoppingCart } from "react-icons/fa";
import { MdOutlineLabelImportant } from "react-icons/md";
import Image from "../../designLayouts/Image";
import Badge from "./Badge";
import { useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { addToCart } from "../../../redux/Slide/orebiSlice";
import {
  convertMoney,
  isTimestampWithinOneWeek,
} from "../../../utils/formatConfig";
import {  addProductToCardHome } from "../../../redux/apiRequestProduct";
import {  toast } from "react-toastify";
import { FaStar, FaHeart, FaCheckCircle, FaRegGem } from "react-icons/fa";


const Product = (props) => {
  const dispatch = useDispatch();
  const _id = props._id;
  const idString = (_id) => {
    return String(_id).toLowerCase().split(" ").join("");
  };
  const rootId = idString(_id);
  const user = useSelector((state) => state?.auth?.loadUser?.user);
  const navigate = useNavigate();
  const productItem = props._id;

  const handleProductDetails = () => {
    navigate(`/product/${rootId}`, {
      state: {
        item: productItem,
      },
    });
  };
  
  const handleAddToCard = () => {

    if (user?.id) {
      if (user?.storeId !== props?.storeId) {
        addItemsToCart({
          title: props?.productName,
          imageText: props?.img,
          price: props?.price,
          depositPrice: props.depositPrice,
          rentalPeriod: 1,
          productId: props?._id,
          userId: user?.id,
          quantity: 1,
          storeId: props?.storeId,
        });
      } else {
        toast.error("Chủ shop không thể mua sản phẩm của shop mình", {
          position: "top-right",
          autoClose: 2000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
          theme: "light",
        });
      }
    } else {
      dispatch(
        addToCart({
          productId: props?._id,
          title: props?.productName,
          quantity: 1,
          period: 1,
          imageText: props?.img,
          rentalPeriod: 1,
          price: props?.price,
          depositPrice: props?.depositPrice,
          storeId: props?.storeId,
        })
      );
      toast.success("Thêm sản phẩm thành công!", {
        position: "top-right",
        autoClose: 2000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "light",
      });
    }

  };
  const addItemsToCart = async (product) => {
    const response = await addProductToCardHome(product);
    
    if (response == "ok") {
      toast.success("Thêm sản phẩm thành công!", {
        position: "top-right",
        autoClose: 2000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "light",
      });
      dispatch(
        addToCart({
          productId: props?._id,
          title: props?.productName,
          quantity: 1,
          period: 1,
          imageText: props?.img,
          rentalPeriod: 1,
          price: props?.price,
          depositPrice: props?.depositPrice,
          storeId: props?.storeId,
        })
      );
    }else if(response.status ==400){
      toast.warn("Sản phẩm hết hàng", {
        position: "top-right",
        autoClose: 2000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "light",
      });
    }
  };

  const getStatusLabel = () => {
    switch (props?.status) {
      case "MINT":
        return {
          label: "Mới",
          banner: "Mới",
          icon: <FaStar className="text-yellow-500" />,
        };
      case "GOOD":
        return {
          label: "Tốt",
          banner: "Tốt",
          icon: <FaHeart className="text-red-500" />,
        };
      case "VERY_GOOD":
        return {
          label: "Rất tốt",
          banner: "Rất tốt",
          icon: <FaCheckCircle className="text-green-500" />,
        };
      case "NEAR_MINT":
        return {
          label: "Như mới",
          banner: "Như mới",
          icon: <FaRegGem className="text-blue-500" />,
        };
      default:
        return {
          label: "",
          banner: "",
          icon: <></>,
        };
    }
  };

  const statusLabel = getStatusLabel();

  return (
    <div className="w-full relative group border rounded-lg shadow shadow-[#dde8da] border-[#dde8da]">
      {props.quantity <= 0 && (
        <div className="absolute top-0 left-0 right-0  bg-red-500 text-white text-center py-2  ">
          Hết hàng
        </div>
      )}
      <div className="max-w-80 max-h-80 relative overflow-y-hidden ">
        <div className="w-full h-[330px] flex items-center justify-center">
          <div className="max-w-full max-h-full">
            <Image className="w-full h-full object-cover" imgSrc={props.img} />
          </div>
        </div>
        <div className="absolute top-6 left-8">
          {isTimestampWithinOneWeek(props.createDate) && <Badge text="New" />}
        </div>
        {props.quantity > 0 ? (
          <div className="w-full h-32 absolute bg-white -bottom-[130px] group-hover:bottom-0 duration-700">
            <ul className="w-full h-full flex flex-col items-end justify-center gap-2 font-titleFont px-2 border-l border-r">
              <li
                onClick={handleAddToCard}
                className="text-[#767676] hover:text-primeColor text-base font-medium border-b-[1px] border-b-gray-200 hover:border-b-primeColor flex items-center justify-end gap-2 hover:cursor-pointer pb-1 duration-300 w-full"
              >
                Thuê ngay
                <span>
                  <FaShoppingCart />
                </span>
              </li>

              <li
                onClick={handleProductDetails}
                className="text-[#767676] hover:text-primeColor text-base font-medium border-b-[1px] border-b-gray-200 hover:border-b-primeColor flex items-center justify-end gap-2 hover:cursor-pointer pb-1 duration-300 w-full"
              >
                Xem chi tiết
                <span className="text-lg">
                  <MdOutlineLabelImportant />
                </span>
              </li>
            </ul>
          </div>
        ) : (
          <div className="w-full h-32 absolute bg-white -bottom-[130px] group-hover:bottom-0 duration-700">
            <ul className="w-full h-full flex flex-col items-end justify-center gap-2 font-titleFont px-2 border-l border-r">
              <li className="text-red-500 hover:text-primeColor text-base font-medium border-b-[1px] border-b-gray-200 hover:border-b-primeColor flex items-center justify-end gap-2 hover:cursor-pointer pb-1 duration-300 w-full">
                Hết hàng
                <span>
                  <FaShoppingCart />
                </span>
              </li>

              <li
                onClick={handleProductDetails}
                className="text-[#767676] hover:text-primeColor text-base font-medium border-b-[1px] border-b-gray-200 hover:border-b-primeColor flex items-center justify-end gap-2 hover:cursor-pointer pb-1 duration-300 w-full"
              >
                Xem chi tiết
                <span className="text-lg">
                  <MdOutlineLabelImportant />
                </span>
              </li>
            </ul>
          </div>
        )}
      </div>

      <div className="max-w-80 py-6 flex flex-col gap-1 border-[1px] border-t-0 px-4 ">

        <div className="flex justify-between">
          <div>
            <span className="font-bold font-semibold text-primeColor text-lg overflow-hidden whitespace-nowrap">Thể loại:</span>
            <span className=" text-lg overflow-hidden whitespace-nowrap" > {props?.categoryName}</span>
          </div>

        
            <div className="status-banner bg-[#dde8da] text-[#57ad3f] px-5 py-1 rounded-full inline-block">
              {statusLabel.banner}
            </div>
            
        </div>
        <div>
          <div className="font-titleFont flex justify-start">
            <p className="text-lg text-primeColor font-semibold   mr-1">
              Tên:
            </p>
            <p className=" text-lg truncate" > {props?.productName}</p>
          </div>
          <span className="text-left font-bold font-semibold text-primeColor text-lg ">
            Giá: 
          </span>
          <span className=" text-lg truncate" > {convertMoney(props?.price)} VND/tháng</span>
        </div>
      </div>
    </div>
  );
};

export default Product;
