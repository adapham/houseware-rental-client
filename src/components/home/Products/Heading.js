import React from "react";

const Heading = ({ heading }) => {
  return (
    <div className="pb-6">
      <h3 style={{
          alignItems: 'center',
          display: 'flex',
          flexFlow: 'row wrap',
          justifyContent: 'space-between',
          position: 'relative',
          width: '100%'
        }}>
        <b className="bg-gray-300 text-opacity-10 block flex-1 h-0.5" />
        <span className="border-2 px-4 py-1 border-gray-300 rounded-md border-solid text-3xl font-semibold">
          {heading}
        </span>
        <b className="bg-gray-300 text-opacity-10 block flex-1 h-0.5" />
    </h3>
    </div>
  );
};

export default Heading;
