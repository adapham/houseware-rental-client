import React, { useState } from "react";
import { motion } from "framer-motion";
import { FaFacebook, FaYoutube, FaLinkedin } from "react-icons/fa";
import FooterListTitle from "./FooterListTitle";
import { paymentCard } from "../../../assets/images";
import Image from "../../designLayouts/Image";
import { Link } from "react-router-dom";

const Footer = () => {
  const [emailInfo, setEmailInfo] = useState("");
  const [subscription, setSubscription] = useState(false);
  const [errMsg, setErrMsg] = useState("");

  const emailValidation = () => {
    return String(emailInfo)
      .toLocaleLowerCase()
      .match(/^\w+([-]?\w+)*@\w+([-]?\w+)*(\.\w{2,3})+$/);
  };

  const handleSubscription = () => {
    if (emailInfo === "") {
      setErrMsg("Please provide an Email !");
    } else if (!emailValidation(emailInfo)) {
      setErrMsg("Please give a valid Email!");
    } else {
      setSubscription(true);
      setErrMsg("");
      setEmailInfo("");
    }
  };
  return (
    <div className="w-full bg-[#dde8da] py-10 sticky top-full">
      <div className="max-w-container mx-auto grid grid-cols-1 md:grid-cols-2  xl:grid-cols-6 px-4 gap-10">
        <div className="col-span-2">
          <FooterListTitle title="Về chúng tôi" />
          <div className="flex flex-col gap-6">
            <p className="text-base w-full xl:w-[80%]">
            Houseware Rental là nền tảng online giúp kết nối Chủ tiệm và Khách thuê trong việc thuê vật dụng. Houseware Rental ra đời với mong muốn hướng đến một lối sống tối giản, tiết kiệm và tiêu dùng bền vững.
            </p>
            
          </div>
        </div>
        <div>
          <FooterListTitle title="Giới thiệu" />
          <ul className="flex flex-col gap-2">
              <Link to="/guideLessor">
                <li className="font-titleFont text-base text-lightText hover:text-black hover:underline decoration-[1px] decoration-gray-500 underline-offset-2 cursor-pointer duration-300">
                  Hướng dẫn tính năng cho Chủ Tiệm
                </li>
              </Link>
              <Link to="/policy">
                <li className="font-titleFont text-base text-lightText hover:text-black hover:underline decoration-[1px] decoration-gray-500 underline-offset-2 cursor-pointer duration-300">
                  Chính sách, quy định dành cho Chủ Tiệm
                </li>
              </Link>
              <Link to="/faq">
                <li className="font-titleFont text-base text-lightText hover:text-black hover:underline decoration-[1px] decoration-gray-500 underline-offset-2 cursor-pointer duration-300">
                  FAQ - câu hỏi thường gặp
                </li>
              </Link>
          </ul>
        </div>
        <div>
          <FooterListTitle title="Địa chỉ" />
            <ul className="flex flex-col gap-2">
              <li className="font-titleFont text-base text-lightText hover:text-black hover:underline decoration-[1px] decoration-gray-500 underline-offset-2 cursor-pointer duration-300">
              Số nhà 10, Thôn 8, Xã Thạch Hòa, Huyện Thạch Thất, Hà Nội, Việt Nam
              </li>
            </ul>
        </div>
        <div className="col-span-2 flex flex-col items-center w-full px-4">
          <div className="w-full">

          <FooterListTitle title="Theo dõi chúng tôi trên" />
            <ul className="flex items-center gap-2 mt-5">
                <a
                  href="https://www.youtube.com/channel/UCvcIk44VIqHJLnbhstUfRhA"
                  target="_blank"
                  rel="noreferrer"
                >
                  <li className="w-7 h-7 bg-primeColor text-gray-100 hover:text-white cursor-pointer text-lg rounded-full flex justify-center items-center hover:bg-black duration-300">
                    <FaYoutube />
                  </li>
                </a>
                
                <a
                  href="https://www.facebook.com/oomhoang/"
                  target="_blank"
                  rel="noreferrer"
                >
                  <li className="w-7 h-7 bg-primeColor text-gray-100 hover:text-white cursor-pointer text-lg rounded-full flex justify-center items-center hover:bg-black duration-300">
                    <FaFacebook />
                  </li>
                </a>
                <a
                  href="https://www.linkedin.com/in/ho%C3%A0ng-nguy%E1%BB%85n-35a187217/"
                  target="_blank"
                  rel="noreferrer"
                >
                  <li className="w-7 h-7 bg-primeColor text-gray-100 hover:text-white cursor-pointer text-lg rounded-full flex justify-center items-center hover:bg-black duration-300">
                    <FaLinkedin />
                  </li>
                </a>
              </ul>

              <Image
                className={`mx-auto ${
                  subscription ? "mt-2" : "mt-6"
                }`}
                imgSrc={paymentCard}
              />
          </div>
        </div>
      </div>
      
    </div>
  );
};

export default Footer;
