import React, { useEffect, useState } from 'react'
import SearchInfor from './SearchInfor'
import HistoryRentalOrder from './HistoryRentalOrder';
import { useSelector } from 'react-redux';
import { getHistoryOrdersLessee } from '../../redux/apiRequestProduct';
import RentalOrderManagement from './RentalOrderManagement';
import { Grid } from 'antd';

function HistoryRented({view,setView,setLoadChat,loadChat}) {
    const user = useSelector((state) => state?.auth?.loadUser?.user);
    const [fromDate, setFromDate] = useState();
  const [toDate, setToDate] = useState();
  const [reload, setReload] = useState(false);
  const [idOrder,setIdOrder]=useState();
  const [data,setData]=useState([]);
  const [loading, setLoading] = useState(true);
  useEffect(() => {
    const param = {
      dateFrom: fromDate,
      dateTo: toDate,
    };
    if (fromDate === null || toDate === null) {
      param = {};
    }
    getOrderLessors(param);
  }, [ reload]);
  const getOrderLessors = async (infor) => {
    if (user?.id) {
      try {
        setLoading(true); // Set loading to true when making the request
        const response = await getHistoryOrdersLessee(user?.id, infor);

        if (response && response.content) {
          const dataOrder = response.content;
          setData(dataOrder);
        }
      } catch (err) {
        console.log(err);
      } finally {
        setLoading(false); // Set loading to false when the request is complete
      }
    }
  };

  return (
    <>
    
      {view ? ( 
         <RentalOrderManagement typeS={"qldh"}idOrder={idOrder}setIdOrder={setIdOrder} />
      ):(
        <>
        <SearchInfor
        setReload={setReload}
        reload={reload}
        setFromDate={setFromDate}
        setToDate={setToDate}
      />
          <HistoryRentalOrder orderList={data} setReload={setReload} reload={reload} setView={setView} setLoadChat={setLoadChat} loadChat={loadChat} setIdOrder={setIdOrder} loading={loading}/>
          </>
      )}
          

    </>
  )
}

export default HistoryRented