import React from 'react';
import { ArrowDownOutlined, ArrowUpOutlined } from '@ant-design/icons';
import { Card, Col, Row, Statistic } from 'antd';
import { FcPaid } from "react-icons/fc";
import { FcConferenceCall } from "react-icons/fc";
import { FcCurrencyExchange } from "react-icons/fc";
import { FcComboChart } from "react-icons/fc";
import PieChart from '../Store/PieChart';
import AirChart from '../Store/AirChart';
import AreaSpline from '../Store/AreaSpline';
import { FcDebt } from "react-icons/fc";
import { FcShop } from "react-icons/fc";
import DonutChart from '../Store/DonutChart';
import ColumnChart from '../Store/ColumnChart';

const StatisticStore = ({ staticData,
  staticDataTotalOrder
  , staticDataIncome,
  staticTotalProductRented
  , setPage
  , setTypeIncome,
  setTypeOrder,
  statisUserRented,
  setTypeUserRented,
  setSelectKey
 }) => (
  <div >
    <Row gutter={1} justify="space-around">
      <Col span={1 / 6}>
        <Card bordered={false} >
          <Statistic
            title={
              <>
                < FcCurrencyExchange className='w-10 h-10' />
                <span className='text-xg font-extrabold'>
                  Tổng doanh thu
                </span>
              </>
            }
            value={staticData?.totalRevenue}
            precision={0}
            valueStyle={{
              color: '#3f51b5',
            }}
            // prefix={<ArrowUpOutlined />}
            suffix="đ"
          />
        </Card>
      </Col>
      <Col span={1 / 6}>
        <Card bordered={false} >
          <Statistic
            title={
              <>
                < FcDebt className='w-10 h-10' />
                <span className='text-xg font-extrabold'>
                  Tổng tiền nhận lại
                </span>
              </>
            }
            value={staticData?.totalReceivedBack}
            precision={0}
            valueStyle={{
              color: '#607d8b',
            }}
            // prefix={<ArrowDownOutlined />}
            suffix="đ"
          />
        </Card>
      </Col>
      <Col span={1 / 6}>
        <Card bordered={false} className='cursor-pointer' onClick={() => {setPage(4);setSelectKey("4");}} >
          <Statistic
            title={
              <>
                < FcPaid className='w-10 h-10' />
                <span className='text-xg font-extrabold'>
                  Sản phẩm đang thuê
                </span>
              </>
            }
            value={(staticData?.totalOrderRented)}
            precision={0}
            valueStyle={{
              color: '#3f8600',
            }}
            // prefix={<ArrowUpOutlined />}
            suffix="Đang thuê"
          />
        </Card>
      </Col>
      <Col span={1 / 6}>
        <Card bordered={false} className='cursor-pointer' onClick={() => {setPage(4);setSelectKey("4");}}>
          <Statistic
            title={
              <>
                < FcShop className='w-10 h-10' />
                <span className='text-xg font-extrabold'>
                  Sản phẩm đã thuê
                </span>
              </>
            }
            value={staticData?.totalOrderDone}
            precision={0}
            valueStyle={{
              color: '#ffc107',
            }}
            // prefix={<ArrowDownOutlined />}
            suffix="Sản phẩm"
          />
        </Card>
      </Col>
      <Col span={1 / 6}>
        <Card bordered={false} className='cursor-pointer' onClick={() => {setPage(2);setSelectKey("2");}}>
          <Statistic
            title={
              <>
                < FcComboChart className='w-10 h-10' />
                <span className='text-xg font-extrabold'>
                  Tổng sản phẩm
                </span>
              </>
            }
            value={staticData?.totalProduct}
            precision={0}
            valueStyle={{
              color: '#00bcd4',
            }}
            // prefix={<ArrowUpOutlined />}
            suffix="Sản phẩm"
          />
        </Card>
      </Col>
      <Col span={1 / 6}>
        <Card bordered={false} >
          <Statistic
            title={
              <>
                < FcConferenceCall className='w-10 h-10' />
                <span className='text-xg font-extrabold'>
                  Tổng người thuê
                </span>
              </>
            }
            value={staticData?.totalTenant}
            precision={0}
            valueStyle={{
              color: '#ffb74d',
            }}
            // prefix={<ArrowDownOutlined />}
            suffix="Người thuê"
          />
        </Card>
      </Col>
    </Row>
    <Row className='mt-10' gutter={16}>
      <Col span={12}>
        <PieChart data={staticData?.totalProductCategory} />
      </Col>

      <Col span={12}>
        <DonutChart staticTotalProductRented={staticTotalProductRented} />
      </Col>
    </Row>
    <Row>
      <Col span={12}>
        <AreaSpline staticDataTotalOrder={staticDataTotalOrder} setTypeOrder={setTypeOrder}  />
      </Col>
      <Col span={12}>
        <AirChart staticDataIncome={staticDataIncome} setTypeIncome={setTypeIncome}  />
      </Col>
    </Row>
    <Row>
      <Col span={24}>
        <ColumnChart statisUserRented={statisUserRented} setTypeUserRented={setTypeUserRented}   />
      </Col>

    </Row>


  </div>

);
export default StatisticStore;