import React, {useEffect, useState} from 'react'
import {FcOk} from "react-icons/fc";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import Select from "@mui/material/Select";
import TextField from "@mui/material/TextField";
import {toast} from "react-toastify";
import httpRequest from "../../redux/Req/apiConfig";
import FormControl from "@mui/material/FormControl";
import {getCategories} from "../../redux/apiRequestProduct";
import {FormHelperText} from "@mui/material";

const AddProduct = ({isModalOpen, closeModalAddProduct, user, reload, setReload}) => {
    const [category, setCategory] = useState();
    const [uploadedFiles, setUploadedFiles] = useState([])
    const [uploadedFilesSub, setUploadedFilesSub] = useState([])
    const [formData, setFormData] = useState(new FormData());
    const [fileLimit, setFileLimit] = useState(false);
    const [error, setError] = useState("");
    const [errorD, setErrorD] = useState("");
    const MAX_COUNT = 3;

    const handleChange = (e) => {
        const {name, value} = e.target;
        if ((name === "price") && parseInt(value, 10) < 10000) {
            // Set the error message
            setError("Giá thuê không nhỏ hơn 10.000 VND");
            }else{
                setError("");
            }

            if ((name === "depositPrice") && parseInt(value, 10) < 10000) {
                // Set the error message
                setErrorD("Giá cọc không nhỏ hơn 10.000 VND");
                }else{
                    setErrorD("");
                }
        setProductDetaill((prevProductDetail) => ({
            ...prevProductDetail,
            [name]: value,
        }));
       
        formData.set(name, value)
    };

      
      
      

    const [productDetaill, setProductDetaill] = useState({
        categoryId: '',
        description: '',
        title: '',
        price: 0,
        quantity: 1,
        regulations: '',
        dimensions: '',
        capacity: '',
        wattage: '',
        weight: '',
        model: '',
        breadth: '',
        depositPrice: 0,
        status: '',
        length: ''

    });

    const handleUploadFiles = files => {
        const uploaded = [...uploadedFiles];
        let limitExceeded = false;
        files.some((file) => {
            if (uploaded.findIndex((f) => f.name === file.name) === -1) {

                if (uploaded.length === MAX_COUNT) {
                    setFileLimit(true);
                }
                if (uploaded.length > MAX_COUNT) {
                    alert(`You can only add a maximum of ${MAX_COUNT} files`);
                    setFileLimit(false);
                    limitExceeded = true;
                    return true;
                }
                uploaded.push(file);
            }
        })
        if (!limitExceeded) setUploadedFiles(uploaded)

    }

    const handleRemoveFile = (fileName) => {
        const updatedFiles = [...uploadedFiles];
        const fileIndex = updatedFiles.findIndex((file) => file.name === fileName);
        if (fileIndex !== -1) {
            updatedFiles.splice(fileIndex, 1);
            setUploadedFilesSub(updatedFiles);
            setFileLimit(false);
        }
    }

    useEffect(() => {
        setUploadedFiles(uploadedFilesSub);

    }, [uploadedFilesSub]);


    const handleFileEvent = (e) => {
        const chosenFiles = Array.prototype.slice.call(e.target.files)
        handleUploadFiles(chosenFiles);
    }

    const handleShowFiles = (file) => {
        return URL.createObjectURL(file);
    };

    const handleSubmit = (e) => {
        e.preventDefault();
        uploadedFiles.map((file) => {
            formData.append('image', file)
        })
        if (productDetaill?.description) {
            const des = productDetaill.description.split("\n").join("<br/>");

            formData.set('description', des.trim())
        }
        if (productDetaill?.regulations) {
            const req = productDetaill.regulations.split("\n").join("<br/>");
            console.log("des",req)
            formData.set('regulations', req.trim())
        }
        formData.set('status', productDetaill.status)
        formData.set('brand', productDetaill.model.trim())
        formData.set('depositPrice', productDetaill.depositPrice)

        formData.set('dimensions', productDetaill.dimensions.trim())
        formData.set('length', productDetaill.length.trim())
        formData.set('price', productDetaill.price)
        formData.set('quantity', productDetaill.quantity)

        formData.set('title', productDetaill.title.trim())
        formData.set('wattage', productDetaill.wattage.trim())
        formData.set('weight', productDetaill.weight.trim())
        formData.set('capacity', productDetaill.capacity.trim())
        formData.set('categoryId', productDetaill.categoryId)
        formData.set("userId", user?.id)
        const url = 'api/products'
        if( productDetaill.depositPrice >= 10000 && productDetaill.price >= 10000){
            httpRequest.postToken(url, formData)
            .then(response => {
                if (response.status && response.status != 200) {
                    const errorMessage = response?.data?.message || "Thêm sản phẩm thất bại do dữ liệu không hợp lệ!";
                    toast(errorMessage);
                  
                } else {
                    setUploadedFilesSub([])
                    formData.delete('image')
                    closeModalAddProduct();
                    setReload(!reload)
                    toast("Thêm sản phẩm thành công");
                }
            })
            .catch(error => {
                // Xử lý lỗi trong trường hợp không thể gọi hàm `httpRequest.putToken`
            });
        }else{
            toast.error("Giá thuê và giá cọc bắt buộc >= 10000", {
                position: "top-right",
                autoClose: 800,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
                theme: "light",
              });
        }
        
    };

    useEffect(() => {
        getCategorie();
    }, []);

    const getCategorie = async () => {
        const response = await getCategories();
        if (response) {
            setCategory(response);
        }
    };

    const ITEM_HEIGHT = 48;
    const ITEM_PADDING_TOP = 8;
    const MenuProps = {
        PaperProps: {
            style: {
                maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
                width: 250,
            },
        },
    };

    return (
        <center>
            <div
                id="createProductModal"
                tabindex="-1"
                aria-hidden="true"
                class={`${isModalOpen ? "" : "hidden"
                } overflow-y-auto overflow-x-hidden fixed top-0 right-0 left-0 z-50 justify-center items-center w-full md:inset-0 h-[calc(100%-1rem)] md:h-full`}
            >
                <div class="relative p-4 w-full max-w-5xl h-full md:h-auto">
                    <div class="relative p-4 bg-white rounded-lg shadow light:bg-gray-800 sm:p-5">
                        <div
                            class="flex justify-between items-center pb-4 mb-4 rounded-t border-b sm:mb-5 light:border-gray-600">
                            <h3 class="text-lg font-semibold text-gray-900 light:text-white">
                                Thêm sản phẩm mới
                            </h3>
                            <button
                                type="button"
                                class="text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm p-1.5 ml-auto inline-flex items-center light:hover:bg-gray-600 light:hover:text-white"
                                data-modal-toggle="createProductModal"
                                onClick={closeModalAddProduct}
                            >
                                <svg
                                    aria-hidden="true"
                                    class="w-5 h-5"
                                    fill="currentColor"
                                    viewbox="0 0 20 20"
                                    xmlns="http://www.w3.org/2000/svg"
                                >
                                    <path
                                        fill-rule="evenodd"
                                        d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                                        clip-rule="evenodd"
                                    />
                                </svg>
                                <span class="sr-only">Close modal</span>
                            </button>
                        </div>

                        <form onSubmit={handleSubmit}>
                            {/* Thông tin sản phẩm */}
                            <div className='flex justify-between'>
                            <fieldset className="border-2 rounded-lg border-[#dde8da] p-3" style={{width:'68%'}}>
                                <legend className="text-lg text-center">Thông tin sản phẩm</legend>
                                <div class="grid gap-4 sm:col-span-2 md:gap-6 sm:grid-cols-2">
                                    <div>
                                        <TextField
                                            margin="normal"
                                            label="Tiêu đề"
                                            variant="outlined"
                                            size="small"
                                            type="text"
                                            className="w-full"
                                            placeholder="Nhập tại đây"
                                            name="title"
                                            id="name"
                                            required={true}
                                            onChange={handleChange}
                                            InputLabelProps={{
                                                shrink: true,
                                            }}
                                            inputProps={{maxLength: 100}}
                                        />
                                    </div>
                                    <div>
                                        <FormControl required sx={{mt: 2}} className="w-full" size="small">
                                            <InputLabel id="demo-select-small-label-category">Thể loại</InputLabel>
                                            <Select
                                                label="Thể loại"
                                                labelId="demo-select-small-label-category"
                                                id="categoryId"
                                                name='categoryId'
                                                onChange={handleChange}
                                                MenuProps={MenuProps}

                                            >

                                                {Array.isArray(category) ? (
                                                    category.map((item) => (
                                                        <MenuItem key={item.id} value={item?.id}>
                                                            {item.categoryName}
                                                        </MenuItem>
                                                    ))
                                                ) : (
                                                    <MenuItem value={-1}></MenuItem>
                                                )}
                                            </Select>
                                        </FormControl>
                                    </div>
                                    <div>
                                        <FormControl required sx={{mt: 2}} className="w-full" size="small">
                                            <InputLabel id="demo-select-small-label-category">Trạng thái</InputLabel>
                                            <Select
                                                label="Thể loại"
                                                labelId="demo-select-small-label-category"
                                                id="status"
                                                name='status'
                                                onChange={handleChange}
                                                MenuProps={MenuProps}

                                            >
                                                <MenuItem value="MINT">
                                                    Mới
                                                </MenuItem>
                                                <MenuItem value="GOOD">
                                                    Tốt
                                                </MenuItem>
                                                <MenuItem value="VERY_GOOD">
                                                    Rất tốt
                                                </MenuItem>
                                                <MenuItem value="NEAR_MINT">
                                                    Như mới
                                                </MenuItem>

                                            </Select>
                                        </FormControl>
                                    </div>
                                    <div>
                                        <TextField
                                            margin="normal"
                                            label="Nhãn hiệu"
                                            variant="outlined"
                                            size="small"
                                            type="text"
                                            className="w-full"
                                            placeholder="Nhập tại đây"
                                            name="model"
                                            id="model"
                                            required={true}
                                            onChange={handleChange}
                                            InputLabelProps={{
                                                shrink: true,
                                            }}
                                            inputProps={{maxLength: 100}}
                                        />
                                    </div>
                                    <div>
                                        <TextField
                                            margin="normal"
                                            variant="outlined"
                                            size="small"
                                            className="w-full"
                                            placeholder="Nhập tại đây"
                                            label="Giá thuê"
                                            type="number"
                                            name="price"
                                            id="price"
                                            required={true}
                                            defaultValue={0}                                           
                                            onChange={handleChange}
                                            InputLabelProps={{
                                                shrink: true,
                                            }}
                                            inputProps={{maxLength: 10,min: 0,
                               
                                                type: "number",}}
                                        />
                                         {error && <div style={{ color: "red" }}>{error}</div>}
                                    </div>
                                    <div>
                                        <TextField
                                            margin="normal"
                                            variant="outlined"
                                            size="small"
                                            className="w-full"
                                            placeholder="Nhập tại đây"
                                            label="Giá cọc"
                                            type="number"
                                            min={0}
                                            name="depositPrice"
                                            id="depositPrice"
                                            required={true}
                                            defaultValue={0}
                                            onChange={handleChange}
                                            InputLabelProps={{
                                                shrink: true,
                                            }}
                                            inputProps={{maxLength: 10,min: 0,
              
                                                type: "number",}}
                                        />
                                        {errorD && <div style={{ color: "red" }}>{errorD}</div>}
                                    </div>
                                    <div>
                                        <TextField
                                            margin="normal"
                                            variant="outlined"
                                            size="small"
                                            className="w-full"
                                            placeholder="Nhập tại đây"
                                            label="Số lượng"
                                            type="number"
                                            min={1}
                                            name="quantity"
                                            id="quantity"
                                            required={true}
                                            defaultValue={1}
                                            value={productDetaill?.quantity}
                                            onChange={(e)=>{
                                                const inputValue = e.target.value;
                                                const parsedValue = parseInt(inputValue);
                                                const updatedValue = isNaN(parsedValue) || parsedValue <= 1 ? 1 : parsedValue;
                                                setProductDetaill({...productDetaill,quantity:updatedValue});
                                            }}
                                            InputLabelProps={{
                                                shrink: true,
                                            }}
                                            inputProps={{maxLength: 10,min: 1,
                                           
                                                type: "number",}}
                                        />
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset className="border-2 rounded-lg border-[#dde8da]  p-3" style={{width:'30%'}}>
                                <legend className="text-lg text-center">Thông số kỹ thuật</legend>
                                <div class="grid gap-4 sm:col-span-2 md:gap-6 sm:grid-cols-1">
                                    <div>
                                        <TextField
                                            margin="normal"
                                            variant="outlined"
                                            size="small"
                                            className="w-full"
                                            placeholder="Nhập tại đây"
                                            label="Trọng lượng (kg)"
                                            type="text"
                                            name="weight"
                                            id="weight"
                                            required={true}
                                            onChange={handleChange}
                                            InputLabelProps={{
                                                shrink: true,
                                            }}
                                            inputProps={{maxLength: 100}}
                                        />
                                    </div>
                                    <div>
                                        <TextField
                                            margin="normal"
                                            variant="outlined"
                                            size="small"
                                            className="w-full"
                                            placeholder="Nhập tại đây"
                                            label="Kích thước"
                                            type="text"
                                            name="dimensions"
                                            id="dimensions"
                                            required={true}
                                            onChange={handleChange}
                                            InputLabelProps={{
                                                shrink: true,
                                            }}
                                            inputProps={{maxLength: 100}}
                                        />
                                    </div>
                                    <div>
                                        <TextField
                                            margin="normal"
                                            variant="outlined"
                                            size="small"
                                            className="w-full"
                                            placeholder="Nhập tại đây"
                                            label="Dung tích"
                                            type="text"
                                            name="capacity"
                                            id="capacity"
                                            required={true}
                                            onChange={handleChange}
                                            InputLabelProps={{
                                                shrink: true,
                                            }}
                                        />
                                    </div>
                                    <div>
                                        <TextField
                                            margin="normal"
                                            variant="outlined"
                                            size="small"
                                            className="w-full"
                                            placeholder="Nhập tại đây"
                                            label="Công suất"
                                            type="text"
                                            name="wattage"
                                            id="wattage"
                                            required={true}
                                            onChange={handleChange}
                                            InputLabelProps={{
                                                shrink: true,
                                            }}
                                        />
                                    </div>
                                </div>
                            </fieldset>

                            </div>
                            {/* Thông số kỹ thuật */}
                            <div className='flex justify-between'>

                            <div class="sm:col-span-2 mt-5 rounded-lg border-[#dde8da] " style={{width:'49%'}}>
                                <TextField
                                    id="description"
                                    name='description'
                                    label="Mô tả sản phẩm"
                                    placeholder="Nhập mô tả sản phẩm tại đây"
                                    multiline
                                    rows={3}
                                    className='w-full'
                                    onChange={handleChange}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    inputProps={{maxLength: 400}}
                                />
                            </div>
                            <div class="sm:col-span-2 mt-5 rounded-lg border-[#dde8da] " style={{width:'49%'}}>
                                <TextField
                                    id="regulations"
                                    name='regulations'
                                    label="Quy định cho thuê"
                                    placeholder="Nhập quy định cho thuê tại đây"
                                    multiline
                                    rows={3}
                                    className='w-full'
                                    onChange={handleChange}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    inputProps={{maxLength: 400}}
                                />
                            </div>
                            </div>
                            <div class="m-4">
                                <span
                                    class="block mb-2 text-sm font-medium text-gray-900 light:text-white">Ảnh sản phẩm </span>
                                <div className="grid grid-cols-4 gap-4 mb-4">
                                    {uploadedFiles?.map((file) => (
                                        <>
                                            <div key={file.name}
                                                 className="relative p-2 bg-gray-100 rounded-lg sm:w-36 sm:h-36">
                                                <img src={handleShowFiles(file)} alt={file.name}
                                                     className="w-full h-full object-cover"/>
                                                <a onClick={(e) => handleRemoveFile(file.name)}>
                                                    <svg
                                                        aria-hidden="true"
                                                        className="w-5 h-5"
                                                        fill="currentColor"
                                                        viewBox="0 0 20 20"
                                                        xmlns="http://www.w3.org/2000/svg"
                                                    >
                                                        <path
                                                            fillRule="evenodd"
                                                            d="M9 2a1 1 0 00-.894.553L7.382 4H4a1 1 0 000 2v10a2 2 0 002 2h8a2 2 0 002-2V6a1 1 0 100-2h-3.382l-.724-1.447A1 1 0 0011 2H9zM7 8a1 1 0 012 0v6a1 1 0 11-2 0V8zm5-1a1 1 0 00-1 1v6a1 1 0 102 0V8a1 1 0 00-1-1z"
                                                            clipRule="evenodd"
                                                        />
                                                    </svg>
                                                </a>
                                            </div>
                                        </>
                                    ))}

                                </div>
                            </div>
                            <div class="mb-4">
                                <div className="App">
                                    <input id='fileUpload' type='file' multiple
                                           accept='image/png'
                                           onChange={handleFileEvent}
                                           disabled={fileLimit}
                                    />
                                    <label htmlFor='fileUpload'>
                                        <a className={`btn btn-primary ${!fileLimit ? '' : 'disabled'} `}></a>
                                    </label>
                                    <div className="uploaded-files-list">
                                    </div>
                                </div>
                            </div>
                            <div class="flex justify-center">
                                <button
                                    data-modal-toggle="createProductModal"
                                    type="submit"
                                    class="w-full bg-[#A3CB38] justify-center sm:w-auto text-gray-500 inline-flex items-center bg-white hover:bg-gray-100 focus:ring-4 focus:outline-none focus:ring-primary-300 rounded-lg border border-gray-200 text-sm font-medium px-5 py-2.5 hover:text-gray-900 focus:z-10 light:bg-gray-700 light:text-gray-300 light:border-gray-500 light:hover:text-white light:hover:bg-gray-600 light:focus:ring-gray-600"
                                >
                                    <FcOk/>
                                    Thêm sản phẩm mới
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </center>
    )
}

export default AddProduct