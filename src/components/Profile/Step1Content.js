import React from 'react';
import {Carousel} from 'antd';
import image1 from '../../assets/images/register1.jpg'
import image2 from '../../assets/images/register2.png'
import image3 from '../../assets/images/register3.png'
import image4 from '../../assets/images/register4.png'
import image5 from '../../assets/images/register5.png'
import image6 from '../../assets/images/register6.png'
const contentStyle = {
    height: '540px',
    color: '#fff',
    lineHeight: '150px',
    textAlign: 'center',
    background: '#364d79',
};

function Step1Content() {
    return (
        <div className="mt-10">
            <Carousel play>
                <div>
                    <h3 style={contentStyle}>
                    <img alt="example" style={{ objectFit: 'contain', height: '520px', width: '100%' }} src={image1} />
                    </h3>
                </div>
                <div>
                    <h3 style={contentStyle}>
                    <img alt="example" style={{ objectFit: 'contain', height: '520px', width: '100%' }} src={image2} />
                    </h3>
                </div>
                <div>
                    <h3 style={contentStyle}>
                    <img alt="example" style={{ objectFit: 'contain', height: '520px', width: '100%' }} src={image3} />
                    </h3>
                </div>
                <div>
                    <h3 style={contentStyle}>
                    <img alt="example" style={{ objectFit: 'contain', height: '520px', width: '100%' }} src={image4} />
                    </h3>
                </div>
                <div>
                    <h3 style={contentStyle}>
                    <img alt="example" style={{ objectFit: 'contain', height: '520px', width: '100%' }} src={image5} />
                    </h3>
                </div>
                <div>
                    <h3 style={contentStyle}>
                    <img alt="example" style={{ objectFit: 'contain', height: '520px', width: '100%' }} src={image6} />
                    </h3>
                </div>
               
            </Carousel>
        </div>
    );
}

export default Step1Content;