import React, { useEffect, useState } from "react";
import TableContainer from "@mui/material/TableContainer";
import Paper from "@mui/material/Paper";
import Box from "@mui/material/Box";
import Tab from "@mui/material/Tab";
import TabContext from "@mui/lab/TabContext";
import TabList from "@mui/lab/TabList";
import TabPanel from "@mui/lab/TabPanel";
import HistoryProduct from "./HistoryProduct";
import SearchInfor from "./SearchInfor";
import { getHistoryTransaction } from "../../redux/apiRequestProduct";
import { useSelector } from "react-redux";

const ManageTransHistory = () => {
  const user = useSelector((state) => state?.auth?.loadUser?.user);
  const [value, setValue] = useState("1");
  const [fromDate, setFromDate] = useState();
  const [toDate, setToDate] = useState();
  const [reload, setReload] = useState(false);
  const [type, setType] = useState("RECHARGE");
  const [transactionData, setTransactionData] = useState([]);
  useEffect(() => {
    const param = {
      fromDate: fromDate,
      toDate: toDate,
      type: type,
    };

    transactionHistory(param);
  }, [type, reload]);
  const transactionHistory = async (infor) => {
    if (user?.id) {
      const response = await getHistoryTransaction(user?.id, infor);
      if (response) {
        setTransactionData(response.content);
      }
    }
  };

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <div>
      <SearchInfor
        setReload={setReload}
        reload={reload}
        setFromDate={setFromDate}
        setToDate={setToDate}
      />
      <Box sx={{ width: "100%", typography: "body1" }}>
        <TabContext value={value}>
          <Box sx={{ borderBottom: 1, borderColor: "divider" }}>
            <TabList
              onChange={handleChange}
              aria-label="lab API tabs example"
              className="bg-[#f5f5f5]"
            >
              <Tab
                sx={{ width: "20%", flexShrink: 0, fontSize: "15px" }}
                label="NẠP TIỀN"
                value="1"
                onClick={() => setType("RECHARGE")}
              />
              <Tab
                sx={{ width: "20%", flexShrink: 0, fontSize: "15px" }}
                label="RÚT TIỀN"
                value="2"
                onClick={() => setType("WITHDRAW_MONEY")}
              />
              <Tab
                sx={{ width: "20%", flexShrink: 0, fontSize: "15px" }}
                label="THANH TOÁN"
                value="3"
                onClick={() => setType("PAYMENT")}
              />
              { user?.roleId == 2 &&
                <Tab
                sx={{ width: "20%", flexShrink: 0, fontSize: "15px" }}
                label="CHUYỂN TIỀN"
                value="4"
                onClick={() => setType("TRANSFER_MONEY")}
              />
              }
               
            </TabList>
          </Box>
          <TableContainer component={Paper}>
            <TabPanel value="1">
              <HistoryProduct transactionData={transactionData} check={1} type={type} toDate={toDate} fromDate ={fromDate} />
            </TabPanel>
          </TableContainer>

          <TableContainer component={Paper}>
            <TabPanel value="2">
              <HistoryProduct transactionData={transactionData}check={2} type={type} toDate={toDate} fromDate ={fromDate} />
            </TabPanel>
          </TableContainer>

          <TableContainer component={Paper}>
            <TabPanel value="3">
              <HistoryProduct transactionData={transactionData} check={3} type={type} toDate={toDate} fromDate ={fromDate}/>
            </TabPanel>
          </TableContainer>
          <TableContainer component={Paper}>
            <TabPanel value="4">
              <HistoryProduct transactionData={transactionData} check={4} type={type} toDate={toDate} fromDate ={fromDate}/>
            </TabPanel>
          </TableContainer>
        </TabContext>
      </Box>
    </div>
  );
};

export default ManageTransHistory;
