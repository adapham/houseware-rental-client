import * as React from "react";

import { convertMoney } from "../../utils/formatConfig";
import { BsEyeFill } from "react-icons/bs";

import { Button, Modal, Table, Tooltip } from "antd";
import IconButton from "@mui/material/IconButton";
import dayjs from "dayjs";
import RentalDetailOrder from "./RentalDetailOrder";
import {
  FcCheckmark,
  FcClock,
  FcCancel,
  FcInfo,
  FcApproval,
} from "react-icons/fc";
import { useState } from "react";
import {
  exportOrderByUser,
  getOrderApprove,
  getOrderConfirm,
  getOrderDelete,
  getOrderLessor,
  getOrderSuccess,
} from "../../redux/apiRequestProduct";
import { FcDocument } from "react-icons/fc";

import { useSelector } from "react-redux";
import { FcApprove } from "react-icons/fc";
import TextArea from "antd/es/input/TextArea";
import { toast } from "react-toastify";
export default function RentalOrderTableStore({
  orderList,
  status,
  setReload,
  reload,
  type
}) {
  const [page, setPage] = useState(0);
  const [userIdChoose, setUserIdChoose] = useState()
  const [rowsPerPage, setRowsPerPage] = useState(6);
  const [openD, setOpenD] = useState(false);
  const [openC, setOpenC] = useState(false);
  const [descriptionR, setDescriptionR] = useState();
  const [selectedId, setSelectedId] = useState();
  const [idDelete, setIdDelete] = useState();
  const [checkStatus, setCheckStatus] = useState();
  const [dataDetail, setDataDetail] = useState([]);
  const user = useSelector((state) => state?.auth?.loadUser?.user);
  const showModalD = () => {
    setOpenD(true);
  };
  const getDelete = async (id, content) => {
    const response = await getOrderDelete(id, content);
    if (response) {
      onClose();
      setReload(!reload)
      toast.success("Huỷ sản đơn hàng thành công", {
        position: "top-right",
        autoClose: 2000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "light",
      });
    }
  };
  const hideModalD = (status) => {
    setOpenD(false);
    if (status) {
      const value = descriptionR;
      getDelete(idDelete, value);
    }
    setReload(!reload);
  };
  const hideModalC = (status, id) => {
    setOpenC(false);

    if (status) {
      if (checkStatus === "NEW") {
        getApprove(id);
      } else if (checkStatus === "PENDING") {
        getSuccess(id);
      }
      setOpen(false);
    }
  };
  const [open, setOpen] = useState(false);
  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };
  const onClose = () => {
    setOpen(false);
  };
  const handleGetDetail = (data, id) => {

    setUserIdChoose(data?.orderManageDetailResps?.[0].userId);
    setSelectedId(id);
    setDataDetail(data?.orderManageDetailResps);
    setCheckStatus(data?.status);
    setOpen(true);
  };
  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };
  const getExport = async () => {
    const response = await exportOrderByUser(user?.id);
    if (response.status == 200) {
      toast.success("Xuất báo cáo thành công", {
        position: "top-right",
        autoClose: 800,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "light",
      });
    } else {
      toast.error("Xuất báo cáo thất bại", {
        position: "top-right",
        autoClose: 800,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "light",
      });
    }
  };
  const handleExport = () => {
    if (user?.id) {
      getExport();
    }
  };
  const convertStatus = (value) => {
    let icon, color;

    switch (value) {
      case "NEW":
        icon = <FcInfo />;
        color = "text-yellow-500";
        return (
          <span className={`font-bold ${color} flex items-center`}>
            {icon} Chờ xác nhận
          </span>
        );

      case "PENDING":
        icon = <FcClock />;
        color = "text-blue-500";
        return (
          <span className={`font-bold ${color} flex items-center`}>
            {icon} Đang giao
          </span>
        );
      case "CONFIRM":
        icon = <FcApprove />;
        color = "text-pink-500";
        return (
          <span className={`font-bold ${color} flex items-center`}>
            {icon} Chờ xác nhận
          </span>
        );
      case "RENTED":
        icon = <FcCheckmark />;
        color = "text-green-500";
        return (
          <span className={`font-bold ${color} flex items-center`}>
            {icon} Hoàn tất
          </span>
        );

      case "CANCEL":
        icon = <FcCancel />;
        color = "text-red-500";
        return (
          <span className={`font-bold ${color} flex items-center`}>
            {icon} Đã huỷ
          </span>
        );

      case "DONE":
        icon = <FcApproval />;
        color = "text-purple-500";
        return (
          <span className={`font-bold ${color} flex items-center`}>
            {icon} Đã hoàn thành
          </span>
        );

      default:
        return value;
    }
  };

  const getApprove = async (id) => {
    const response = await getOrderApprove(id);
    if (response) {
      setReload(!reload);
      toast.success("Xác nhận đơn hàng", {
        position: "top-right",
        autoClose: 2000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "light",
      });
    }
  };

  const getSuccess = async (id) => {
    const response = await getOrderConfirm(id, user?.id);
    if (response) {
      setReload(!reload);
      toast.success("Đơn hàng được thuê thành công", {
        position: "top-right",
        autoClose: 2000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "light",
      });
    }
  };
  const handleApprove = () => {

    setOpenC(true)

    //setOpenD(false);
  };
  const handleDelete = (id) => {
    setOpen(false);
    setIdDelete(id);
    showModalD(id);
    setReload(!reload)
  };

  const columns = [
    {
      title: "Mã đơn hàng",
      dataIndex: "code",
      key: "code",
      minWidth: 100,
    },
    {
      title: "Người thuê",
      dataIndex: "lessee",
      key: "lessee",
      minWidth: 100,
    },
    {
      title: "Ngày tạo đơn",
      dataIndex: "createdTime",
      key: "createdTime",
      minWidth: 100,
      align: "center",
      style: {
        fontWeight: "bold",
      },
      sorter: (a, b) => dayjs(a.createdTime).unix() - dayjs(b.createdTime).unix(),
      render: (value) => dayjs(value).format("DD-MM-YYYY"),
    },
    {
      title: "Tổng sản phẩm",
      dataIndex: "orderManageDetailResps",
      key: "orderManageDetailResps",
      minWidth: 100,
      align: "center",
      style: {
        fontWeight: "bold",
      },
      render: (value) => value?.length,
    },
    {
      title: "Tổng số tiền",
      dataIndex: "totalprice",
      key: "totalprice",
      minWidth: 100,
      align: "center",
      style: {
        fontWeight: "bold",
      },
      sorter: (a, b) => a.totalprice - b.totalprice,
      render: (value) => <span style={{ fontWeight: "bold" }}>{convertMoney(value)} VND</span>,
    },
    {
      title: "Trạng thái",
      dataIndex: "status",
      key: "status",
      minWidth: 100,
      align: "center",
      render: (value) => convertStatus(value),
    },
    {
      title: type == "ALL" && (
        <>
          Xuất báo cáo
          <Tooltip title="Xuất Báo cáo" arrow onClick={handleExport}>
            <IconButton>
              <FcDocument style={{ fontSize: "30px" }} />
            </IconButton>
          </Tooltip>
        </>
      ),
      dataIndex: "action",
      key: "action",
      minWidth: 170,
      align: "center",
      render: (value, rows) => (
        <>
          <Tooltip title="Xem chi tiết" arrow>
            <IconButton>
              <BsEyeFill
                style={{ fontSize: "20px" }}
                onClick={() => handleGetDetail(rows, rows.id)}
              />
            </IconButton>
          </Tooltip>
        </>
      ),
    }
    
  ];





  return (
    <>


      <Table
        columns={columns}
        dataSource={orderList}
        pagination={{
          defaultPageSize: 8,
          showSizeChanger: true,
          pageSizeOptions: ["8", "10", "20", "30"],
        }} />

      <Modal
        title="Chi tiết sản phẩm"
        centered
        open={open}
        footer={null}
        width={1200}
        height={800}
        onCancel={onClose}
      >
        {dataDetail && (
          <RentalDetailOrder dataDetail={dataDetail} status={status} userIdChoose={userIdChoose} />
        )}
        <div className="flex justify-end mt-5">
          {(checkStatus === "NEW" || checkStatus === "PENDING") && (
            <Button
              className="mt-2  bg-[#1975D2] text-white "
              onClick={() => handleApprove()}
              style={{ marginRight: "60px" }}

            >
              {checkStatus === "NEW" && "Xác nhận đơn hàng"}
              {checkStatus === "PENDING" && "Xác nhận đã giao hàng"}
            </Button>
          )}
          {(checkStatus == "NEW" || checkStatus == "PENDING") && (checkStatus !== "CONFIRM") && (
            <Button
              className="mt-2  bg-[#1975D2] text-white "
              onClick={() => handleDelete(selectedId)}
            >
              Huỷ đơn
            </Button>
          )}

        </div>
      </Modal>
      <Modal
        title="Huỷ đơn hàng"
        open={openD}
        footer={null}
        okText="Xác nhận"
        cancelText="Quay lại"
        onCancel={() => hideModalD(false)}
      >
        <p>Bạn có chắc muốn huỷ order không?</p>
        <TextArea
          onChange={(e) => setDescriptionR(e.target.value)}
          placeholder="Điền lý do ở dây"
          value={descriptionR}
        ></TextArea>
        <div className="flex justify-end mt-5">
          {descriptionR?.length > 0 ? (
            <Button
              className="mt-2  bg-[#1975D2] text-white "
              onClick={() => hideModalD(true)}
            >
              Có
            </Button>
          ) : (
            <Button className="mt-2  bg-[#1975D2] text-white " type="primary" disabled >
              Có
            </Button>
          )}
          <Button className="mt-2  bg-[#1975D2] text-white " onClick={() => hideModalD(false)}>
            Không
          </Button>
        </div>
      </Modal>
      <Modal
        title={checkStatus === "PENDING" ? "Xác nhận đã giao hàng thành công" : "Bạn có muốn xác nhận đơn hàng này không ?"} open={openC}
        footer={null}

        cancelText="Quay lại"
        onCancel={() => hideModalC(false, selectedId)}
      >

        <div className="flex justify-end mt-5">

          <Button
            className="mt-2  bg-[#1975D2] text-white "
            onClick={() => hideModalC(true, selectedId)}
          >
            Có
          </Button>
          <Button className="mt-2  bg-[#1975D2] text-white " onClick={() => hideModalC(false)}>
            Không
          </Button>
        </div>
      </Modal>
    </>
  );
}
