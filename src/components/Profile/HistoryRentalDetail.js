import * as React from "react";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import { convertMoney, convertToDate } from "../../utils/formatConfig";
import { useNavigate } from "react-router-dom";
import { addProductToCard, getConfirmReturn } from "../../redux/apiRequestProduct";
import { addToCart } from "../../redux/Slide/orebiSlice";

import { useDispatch, useSelector } from "react-redux";
import { List, ListItem } from "@mui/material";
import { BsFacebook, BsFillTelephoneFill } from "react-icons/bs";
import Accordion from "@mui/material/Accordion";
import AccordionDetails from "@mui/material/AccordionDetails";
import AccordionSummary from "@mui/material/AccordionSummary";
import Typography from "@mui/material/Typography";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import { Button, Col, Modal, Row } from "antd";
import { toast } from "react-toastify";
import { useState } from "react";

function monneyFormat(num) {
  return `${convertMoney(parseFloat(num))}đ`;
}

function priceRow(price, depositePrice) {
  return price + depositePrice;
}

function createRow(product, price, depositePrice, quantity, rentalDate, total) {
  //const total = priceRow(price, depositePrice);
  return { product, price, depositePrice, quantity, rentalDate, total };
}

function subtotal(items) {
  return items
    .map(({ total }) => Number(total)) // Convert total to a number
    .reduce((sum, i) => sum + i, 0);
}

function HistoryRentalDetail({ dataDetail, setView, setLoadChat, loadChat,
   orderDetailId, setReload, reload ,chooseStatus,setOpen}) {
  const [openC, setOpenC] = useState(false);
  const dispatch = useDispatch();
  const user = useSelector((state) => state?.auth?.loadUser?.user);
  const navigate = useNavigate();
  const addItemsToCart = async (product) => {
    const response = await addProductToCard(product);
    if (response) {
      toast.success("Thêm sản phẩm thành công!", {
        position: "top-right",
        autoClose: 2000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "light",
      });
      dispatch(
        addToCart({
          productId: dataDetail?.productId,
          title: dataDetail?.productName,
          quantity: dataDetail?.quantity,
          //period: dataDetail?.rentalPeriod,
          imageText: dataDetail?.imageText[0],
          rentalPeriod: dataDetail?.rentalPeriod,
          price: dataDetail?.priceProduct,
          depositPrice: dataDetail?.depositPrice,
          storeId: dataDetail?.storeId,
        })
      );
    }
  };
  const handleAddToCard = () => {
    if (user?.storeId !== dataDetail?.storeId) {
      if (user?.id) {
        addItemsToCart({
          productId: dataDetail?.productId,
          title: dataDetail?.productName,
          quantity: dataDetail?.quantity,
          //period: dataDetail?.rentalPeriod,
          imageText: dataDetail?.imageText[0],
          rentalPeriod: dataDetail?.rentalPeriod,
          price: dataDetail?.priceProduct,
          depositPrice: dataDetail?.depositPrice,
          storeId: dataDetail?.storeId,
          userId: user?.id,
        });
      } else {
        dispatch(
          addToCart({
            productId: dataDetail?.productId,
            title: dataDetail?.productName,
            quantity: dataDetail?.quantity,
            //period: dataDetail?.rentalPeriod,
            imageText: dataDetail?.imageText[0],
            rentalPeriod: dataDetail?.rentalPeriod,
            price: dataDetail?.priceProduct,
            depositPrice: dataDetail?.depositPrice,
            storeId: dataDetail?.storeId,
          })
        );
      }
    } else {
      toast.error("Chủ shop không thể mua sản phẩm của shop mình", {
        position: "top-right",
        autoClose: 800,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "light",
      });
    }
    navigate("/cart");
  };
  const rows = [
    createRow(
      <div class="flex items-center mr-3">
        <img
          src={dataDetail.imageText[0]}
          alt="Image"
          class="h-8 w-auto mr-3"
        />
        {dataDetail.productName}
      </div>,
      `${dataDetail.priceProduct}`,
      `${dataDetail.depositPrice}`,
      `${dataDetail.quantity}`,
      `${`${dataDetail.rentalPeriod} tháng`}`,
      `${dataDetail.price}`
    ),
  ];
  const [expanded, setExpanded] = React.useState(false);
  const handleChange = (panel) => (event, isExpanded) => {
    setExpanded(isExpanded ? panel : false);
  };
  const [expanded2, setExpanded2] = React.useState(false);
  const handleChange2 = (panel) => (event, isExpanded) => {
    setExpanded2(isExpanded ? panel : false);
  };
  const invoiceSubtotal = subtotal(rows);
  const returnOrder = async (id) => {
    const response = await getConfirmReturn(id);
    if (response) {
      setReload(!reload);
      toast.success("Trả hàng thành công", {
        position: "top-right",
        autoClose: 800,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "light",
      });
    }
  }
  const hideModalC = () => {
    setOpenC(false)
    setOpen(false)
    returnOrder(orderDetailId);
  }


  return (
    <>
      <TableContainer className="p-5" component={Paper}>
        <Table sx={{ minWidth: 900 }} aria-label="spanning table">
          <TableHead>
            <TableRow>
              <TableCell align="center">Tên sản phẩm</TableCell>
              <TableCell align="center">Giá thuê</TableCell>
              <TableCell align="center">Giá cọc</TableCell>
              <TableCell align="center">Số lượng</TableCell>
              <TableCell align="center">Thời gian thuê</TableCell>
              <TableCell align="center">Tiền thuê</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {rows.map((row) => (
              <TableRow key={row.product}>
                <TableCell>{row.product}</TableCell>
                <TableCell align="center">{monneyFormat(row.price)}</TableCell>
                <TableCell align="center">
                  {monneyFormat(row.depositePrice)}
                </TableCell>
                <TableCell align="center">{row.quantity}</TableCell>
                <TableCell align="center">{row.rentalDate}</TableCell>
                <TableCell align="center">{monneyFormat(row.total)}</TableCell>
              </TableRow>
            ))}

            <TableRow>
              <TableCell ></TableCell>
              <TableCell ></TableCell>
              <TableCell ></TableCell>
              <TableCell></TableCell>
              <TableCell size="large" align="right" colSpan={1}>
                Tổng tiền đi thuê:
              </TableCell>
              <TableCell align="center">{monneyFormat(invoiceSubtotal)}</TableCell>
            </TableRow>
          </TableBody>
        </Table>
        <Row gutter={16} className="mt-3">
          <Col span={12}>
            <Accordion
              expanded={expanded === "panel1"}
              onChange={handleChange("panel1")}
            >
              <AccordionSummary
                expandIcon={<ExpandMoreIcon />}
                aria-controls="panel1bh-content"
                id="panel1bh-header"
                className="flex justify-between w-full"
              >
                <Typography
                  sx={{ width: "70%", flexShrink: 0, fontSize: "18px", paddingLeft: "20px" }}

                >
                  Thông tin liên hệ
                </Typography>
                <Typography sx={{ color: "text.secondary" }}>
                  xem chi tiết
                </Typography>
              </AccordionSummary>
              <AccordionDetails>
                <Typography>
                  <List>
                    <ListItem disablePadding>
                      <div className="flex justify-between w-full">
                        <div>Địa chỉ:<span className="font-bold"> {dataDetail?.address}</span></div>
                        <div>
                          <div className="flex justify-center items-center">

                            <div className="m-1 ml-3 flex justify-center items-center">
                              <BsFillTelephoneFill style={{ fontSize: "24px" }} />
                              <div className="ml-2">{dataDetail?.phone}</div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </ListItem>
                  </List>
                </Typography>
              </AccordionDetails>
            </Accordion>
          </Col>
          <Col span={12}>
            <Accordion
              expanded={expanded2 === "panel1"}
              onChange={handleChange2("panel1")}
            >
              <AccordionSummary
                expandIcon={<ExpandMoreIcon />}
                aria-controls="panel1bh-content"
                id="panel1bh-header"
                className="flex justify-between w-full"
              >
                <Typography
                  sx={{
                    width: "70%",
                    flexShrink: 0,
                    fontSize: "18px",
                    paddingLeft: "20px",
                  }}
                >
                  Thông tin đơn hàng
                </Typography>
                <Typography sx={{ color: "text.secondary" }}>
                  xem chi tiết
                </Typography>
              </AccordionSummary>
              <AccordionDetails>
                <Typography>
                  <List>
                    <ListItem disablePadding>
                      <div className="w-full">
                        <div>
                          Địa chỉ: <span className="font-bold">
                            {dataDetail?.addressShip}
                          </span>
                        </div>
                        <div>
                          Email: <span className="font-bold">
                            {dataDetail?.email}
                          </span>
                        </div>
                        <div>
                          Họ và Tên: <span className="font-bold">
                            {dataDetail?.fullName}
                          </span>
                        </div>
                        <div>
                          Số điện thoại: <span className="font-bold">
                            {dataDetail?.phone}
                          </span>
                        </div>

                      </div>
                    </ListItem>
                  </List>
                </Typography>
              </AccordionDetails>
            </Accordion>
          </Col>

        </Row>
        <div className="flex justify-end my-4">
          {chooseStatus =="RENTED" && (

          <div className="mx-5 border">
            <Button className="bg-[#1975D2] text-white" onClick={() => setOpenC(true)}>
              Trả hàng
            </Button>
          </div>
          )}
          <div className="mx-5 border">
            <Button className="  bg-[#1975D2] text-white "
              onClick={() => {
                navigate('/profile', { state: dataDetail?.userId });
                setLoadChat(!loadChat);
              }}
            >Liên hệ </Button>
          </div>
          <div className="mx-5 border">
            <Button className="bg-[#1975D2] text-white" onClick={handleAddToCard}>
              Thuê lại
            </Button>
          </div>
          <div className="mx-5 border">
            <Button
              className="bg-[#1975D2] text-white"
              onClick={() => navigate(`/product/${dataDetail?.productId}`, { state: dataDetail?.productId })}
            
            >
              Xem sản phẩm
            </Button>
          </div>
          <div className="mx-5 border">
            <Button className="bg-[#1975D2] text-white" onClick={() => setView(true)}>
              Xem đơn hàng
            </Button>
          </div>
        </div>
      </TableContainer>
      <Modal
        title="Xác nhận trả hàng"
        open={openC}
        footer={null}
        okText="Xác nhận"
        cancelText="Quay lại"
        onCancel={() => setOpenC(false)}
      >
        <p>Quý khách nên kiểm tra sản phẩm cẩn thận và chụp hình/quay phim lại trước khi trả hàng để hạn chế rủi ro.</p>
        <div className="flex justify-end mt-5">

          <Button
            className="mt-2  bg-[#1975D2] text-white "
            onClick={() => hideModalC()}
          >
            Có
          </Button>
          <Button className="mt-2  bg-[#1975D2] text-white " onClick={() => setOpenC(false)}>
            Không
          </Button>
        </div>
      </Modal>
    </>
  );
}

export default HistoryRentalDetail;
