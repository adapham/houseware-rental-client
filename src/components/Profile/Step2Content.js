import React, { useEffect, useMemo, useRef, useState } from "react";
import TextField from "@mui/material/TextField";
import { Container, Grid } from "@material-ui/core";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import Select from "@mui/material/Select";
import FormControl from "@mui/material/FormControl";
import {
  getDistrictByProvince,
  getProvince,
  getRequest,
  getUser,
  getWardByDistrict,
} from "../../redux/apiRequestProduct";
import { regexPhoneNumber } from "../../constants/constantsParam";


import UploadImageStore from "../Store/UploadImageStore";

import { useDispatch, useSelector } from "react-redux";
import { toast } from "react-toastify";


function Step2Content({ handleNext, finish, setFinish, status, setStatus }) {

  const [ward, setWard] = useState();
  const [district, setDistrict] = useState();
  const [province, setProvince] = useState();
  const [storeName, setStoreName] = useState("");
  const [phoneNumber, setPhoneNumber] = useState("");
  const [wardSelect, setWardSelect] = useState("");
  const [districtSelect, setDistrictSelect] = useState("");
  const [provinceSelect, setProvinceSelect] = useState("");
  const [userD, setUserD] = useState([]);
  const [errStoreName, setErrStoreName] = useState("");
  const [errPhoneNumber, setErrPhoneNumber] = useState("");
  const [addressDetail, setAddressDetail] = useState("");
  const [errAddressDetail, setErrAddressDetail] = useState("");
  const [descriptionDetail, setDescriptionDetail] = useState("");
  const [errDescriptionDetail, setErrDescriptionDetail] = useState("");


  const [fileList, setFileList] = useState([]);
  const user = useSelector((state) => state?.auth?.loadUser?.user);
  var formData = new FormData();
  const getUpdateUser = async (userId) => {
    const response = await getUser(userId);
    if (response) {
      setUserD(response);
      setAddressDetail(response.address);
    }
  }
  const getProvinces = async () => {
    const response = await getProvince();
    if (response) setProvince(response);
  };

  const getDistrict = async (provinceId) => {
    const response = await getDistrictByProvince(provinceId);
    if (response) {
      setDistrict(response);
    }
  };
  const getWard = async (districtId) => {
    const response = await getWardByDistrict(districtId);
    if (response) {
      setWard(response);
    }
  };
  useEffect(() => {
    getProvinces();
    getUpdateUser(user?.id)
  }, []);
  useEffect(() => {
    if (userD?.province) {
      var idProvince = province?.filter(
        (item) => item.name == userD?.province
      )[0]?.id;
      setProvinceSelect(idProvince);
    }
  }, [province, userD]);
  useEffect(() => {
    if (provinceSelect) {
      getDistrict(provinceSelect);
    }
  }, [provinceSelect]);
  useEffect(() => {
    var idDistrict = district?.filter((item) => item.name == userD?.district)[0]
      ?.id;
    if (userD?.district) {
      setDistrictSelect(idDistrict);
    }
  }, [district]);

  const ITEM_HEIGHT = 48;
  const ITEM_PADDING_TOP = 8;
  const MenuProps = {
    PaperProps: {
      style: {
        maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
        width: 250,
      },
    },
  };
  useEffect(() => {
    if (districtSelect) {
      getWard(districtSelect);
    }
  }, [districtSelect]);
  useEffect(() => {
    var idWar = ward?.filter((item) => item.name == userD?.ward)[0]?.id;
    if (userD?.ward) {
      setWardSelect(idWar);
    }
  }, [ward]);

  const getSentRequestLessor = async (data) => {

    const response = await getRequest(data);
    
    setFinish(false);
   
    if (response.status == "PROCESSING") {

      handleNext(true)
      toast.success("Đăng ký cửa hàng thành công", {
        position: "top-right",
        autoClose: 2000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "light",
      });

      setStatus(response.status);
    } else {

      toast.error("Đăng ký cửa hàng thất bại", {
        position: "top-right",
        autoClose: 2000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "light",
      });
    }
  };
  useEffect(() => {
    if (finish) {
      hendleSubmit();
    }
  }, [finish]);

  const hendleSubmit = async () => {
     
    if (!storeName) {
      setErrStoreName("Nhập tên của bạn");
    }
    if (!addressDetail) {
      setErrAddressDetail("Nhập địa chỉ của bạn");
    }
    if (!phoneNumber) {
      const regexMatch = regexPhoneNumber.test(phoneNumber);
      if (!regexMatch) {
        setErrPhoneNumber("Vui lòng nhập số điện thoại hợp lệ");
      }
    }
    if (phoneNumber && addressDetail && storeName && descriptionDetail
      && provinceSelect && districtSelect && wardSelect
      && storeName.trim() != "" && addressDetail.trim() != "" 
      && descriptionDetail.trim() != "" && errPhoneNumber.trim() == "") {
      formData.set("type", "ROLE_UP");
      formData.set("title", storeName.trim());
      formData.set("phone", phoneNumber.trim());
      formData.set(
        "ward",
        ward?.filter((item) => item.id == wardSelect)[0]?.name
      );
      formData.set(
        "district",
        district?.filter((item) => item.id == districtSelect)[0]?.name
      );
      formData.set(
        "province",
        province?.filter((item) => item.id == provinceSelect)[0]?.name
      );
      formData.set("address", addressDetail.trim());
      formData.set("userId", userD?.id);
      formData.set("description", descriptionDetail.trim());
      console.log(fileList?.[0].originFileObj);
      formData.set("image", fileList?.[0].originFileObj);
      getSentRequestLessor(formData);


    } else {

      toast.warning("Vui lòng nhập đầy đủ thông tin", {
        position: "top-right",
        autoClose: 2000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "light",
      });

    }
    setErrPhoneNumber("");
  };

  return (
    <div className="maincontainer mt-5">
      <Container>
        <Grid container className="flex justify-between mb-10">
          <Grid item md={12} className="order-md-1 mx-8 px-8 py-4">
            <fieldset className="border-2 rounded-lg border-[#dde8da] p-3">
              <legend className="text-lg text-center">
                Thông tin cửa hàng
              </legend>
              <form className="needs-validation mt-2">
                <Grid container spacing={2}>
                  <Grid item xs={12} sm={6}>
                    <TextField
                      margin="normal"

                      label={
                        <span>
                          Tên cửa hàng<span style={{ color: 'red' }}>*</span>
                        </span>
                      }
                      variant="outlined"
                      size="small"
                      type="text"
                      className="w-full"
                      placeholder="Nhập tại đây"
                      InputLabelProps={{
                        shrink: true,
                      }}

                      onChange={(e) => {
                        const storeN = e.target.value;
                        setStoreName(storeN);

                        if (storeN.trim() === '') {
                          setErrStoreName("Vui lòng nhập tên cửa hàng");
                        } else {
                          setErrStoreName("");
                        }
                      }}
                      value={storeName}
                      error={!!errStoreName}
                      helperText={errStoreName}
                    />
                  </Grid>
                  <Grid item xs={12} sm={6}>
                    <TextField
                      margin="normal"

                      label={
                        <span>
                          Số điện thoại cửa hàng<span style={{ color: 'red' }}>*</span>
                        </span>
                      }
                      variant="outlined"
                      size="small"
                      type="text"
                      className="w-full"
                      placeholder="Nhập tại đây"
                      InputLabelProps={{
                        shrink: true,
                      }}
                      onChange={(e) => {
                        const trimmedPhoneNumber = e.target.value.trim();
                        setPhoneNumber(trimmedPhoneNumber);

                        if (trimmedPhoneNumber === '') {
                          setErrPhoneNumber("Vui lòng nhập số điện thoại");
                        } else {
                          const regexMatch = regexPhoneNumber.test(trimmedPhoneNumber);
                          if (!regexMatch) {
                            setErrPhoneNumber("Vui lòng nhập số điện thoại hợp lệ");
                          } else {
                            setErrPhoneNumber("");
                          }
                        }
                      }}
                      value={phoneNumber}
                      error={!!errPhoneNumber}
                      helperText={errPhoneNumber}
                    />
                  </Grid>

                  <Grid item xs={12} sm={6}>
                    {useMemo(() => {
                      return (
                        <>
                          <FormControl
                            sx={{ mt: 1 }}
                            size="small"
                            className="w-full"
                          >
                            <InputLabel id="demo-select-small-label-province">

                              <span>
                                Tỉnh/Thành phố<span style={{ color: 'red' }}>*</span>
                              </span>
                            </InputLabel>
                            <Select
                              labelId="demo-select-small-label-province"
                              id="demo-select-small"
                              value={provinceSelect}
                              label="Tỉnh/Thành phố"
                              onChange={(e) =>
                                setProvinceSelect(e.target.value)
                              }
                              MenuProps={MenuProps}
                            >
                              {Array.isArray(province) &&
                                province.map((item) => (
                                  <MenuItem key={item.id} value={item?.id}>
                                    {item.name}
                                  </MenuItem>
                                ))}
                            </Select>
                          </FormControl>
                        </>
                      );
                    }, [province, provinceSelect])}
                  </Grid>
                  <Grid item xs={12} sm={6}>
                    {useMemo(() => {
                      return (
                        <>
                          <FormControl
                            sx={{ mt: 1 }}
                            size="small"
                            className="w-full"
                          >
                            <InputLabel id="demo-select-small-label-district">

                              <span>
                                Quận/Huyện<span style={{ color: 'red' }}>*</span>
                              </span>
                            </InputLabel>
                            <Select
                              labelId="demo-select-small-label-district"
                              id="demo-select-small"
                              value={districtSelect}
                              label="Quận/Huyện"
                              onChange={(e) =>
                                setDistrictSelect(e.target.value)
                              }
                              MenuProps={MenuProps}
                            >
                              {/* <MenuItem value={-1}>
                                <em>Chọn Quận/Huyện</em>
                              </MenuItem> */}
                              {Array.isArray(district) &&
                                district.map((item) => (
                                  <MenuItem key={item.id} value={item?.id}>
                                    {item.name}
                                  </MenuItem>
                                ))}
                            </Select>
                          </FormControl>
                        </>
                      );
                    }, [district, districtSelect])}
                  </Grid>
                  <Grid item xs={12} sm={6}>
                    {useMemo(() => {
                      return (
                        <>
                          <FormControl
                            sx={{ mt: 2 }}
                            size="small"
                            className="w-full"
                          >
                            <InputLabel id="demo-select-small-label-ward">

                              <span>
                                Xã/Phường<span style={{ color: 'red' }}>*</span>
                              </span>
                            </InputLabel>
                            <Select
                              labelId="demo-select-small-label-ward"
                              id="demo-select-small"
                              value={wardSelect}
                              label="Xã/Phường"
                              onChange={(e) => setWardSelect(e.target.value)}
                              MenuProps={MenuProps}
                            >
                              {/* <MenuItem value={-1}>
                                <em>Chọn Xã/Phường</em>
                              </MenuItem> */}
                              {Array.isArray(ward) &&
                                ward.map((item) => (
                                  <MenuItem key={item.id} value={item?.id}>
                                    {item.name}
                                  </MenuItem>
                                ))}
                            </Select>
                          </FormControl>
                        </>
                      );
                    }, [ward, wardSelect])}
                  </Grid>
                  <Grid item xs={12} sm={6}>
                    <TextField
                      margin="normal"
                      label={
                        <span>
                          Địa chỉ<span style={{ color: 'red' }}>*</span>
                        </span>
                      }
                      variant="outlined"
                      size="small"
                      type="text"
                      className="w-full"
                      placeholder="Nhập tại đây"
                      InputLabelProps={{
                        shrink: true,
                      }}
                      onChange={(e) => {

                        setAddressDetail(e.target.value);

                        if (e.target.value.trim() === '') {
                          setErrAddressDetail("Vui lòng nhập địa chỉ");
                        } else {
                          setErrAddressDetail("");
                        }
                      }}
                      value={addressDetail}
                      error={!!errAddressDetail}
                      helperText={errAddressDetail}
                    />
                  </Grid>
                  <Grid item xs={12} sm={12}>
                    <TextField

                      label={
                        <span>
                          Mô tả cửa hàng<span style={{ color: 'red' }}>*</span>
                        </span>
                      }
                      placeholder="Nhập mô tả cửa hàng tại đây"
                      multiline
                      rows={2}
                      className="w-full"
                      InputLabelProps={{
                        shrink: true,
                      }}
                      onChange={(e) => {
                        const trimmedDescription = e.target.value;
                        setDescriptionDetail(trimmedDescription);

                        if (trimmedDescription.trim() === '') {
                          setErrDescriptionDetail("Vui lòng nhập mô tả");
                        } else {
                          // Additional validation logic if needed

                          setErrDescriptionDetail("");
                        }
                      }}
                      value={descriptionDetail}
                      error={!!errDescriptionDetail}
                      helperText={errDescriptionDetail}
                    />
                  </Grid>
                </Grid>
              </form>

              <div class="m-4">
                <span class="block mb-2 text-sm font-medium text-gray-900 light:text-white">
                  Ảnh của hàng
                </span>
                <UploadImageStore
                  fileList={fileList}
                  setFileList={setFileList}
                />
              </div>
            </fieldset>
          </Grid>
        </Grid>
      </Container>
    </div>
  );
}

export default Step2Content;
