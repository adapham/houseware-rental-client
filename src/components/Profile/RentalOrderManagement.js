import React, { useEffect, useState } from "react";
import SearchInfor from "./SearchInfor";
import RentalOrderTable from "./RentalOrderTable";
import Box from "@mui/material/Box";
import Tab from "@mui/material/Tab";
import TabContext from "@mui/lab/TabContext";
import TabList from "@mui/lab/TabList";
import TabPanel from "@mui/lab/TabPanel";

import TableContainer from "@mui/material/TableContainer";

import Paper from "@mui/material/Paper";
import { getOrderLessee, getOrderLessor, getOrderStatusLessee } from "../../redux/apiRequestProduct";
import { useSelector } from "react-redux";
import { Await } from "react-router-dom";

const RentalOrderManagement = ({ typeS, setLoadChat, loadChat, idOrder,setIdOrder ,typeChoose,typeNotify,reloadType}) => {

  const [value, setValue] = React.useState("2");
  const user = useSelector((state) => state?.auth?.loadUser?.user);
  const handleChange = (event, newValue) => {
    setValue(newValue);
  };
  const [fromDate, setFromDate] = useState();
  const [staticLessee, setStaticLessee] = useState();
  const [toDate, setToDate] = useState();
  const [type, setType] = useState("NEW");
  const [reload, setReload] = useState(false);
  const [orderNew, setOrderNew] = useState([]);
  const [orderPending, setOrderPending] = useState([]);
  const [orderALL, setOrderALL] = useState([]);
  const [orderDone, setOrderDone] = useState([]);
  const [orderCancel, setOrderCancel] = useState([]);
  const [orderConfirm, setOrderConfirm] = useState([]);
  const [dataTemp, setDataTemp] = useState();
  useEffect(() => {

    const param = {
      dateFrom: fromDate,
      dateTo: toDate,
    };

    if (fromDate === null || toDate === null) {
      param = {};
    }
    getStaticLessee(user?.id);
    getOrderLessors(param);
  }, [type, reload]);
  const getOrderLessors = async (infor) => {
    if (user?.id) {
      var response = null;
      if (typeS == "qldh") {
        response = await getOrderLessee(user?.id, infor);
      } else {
        response = await getOrderLessor(user?.id, infor);
      }

      if (response) {
        if (response.content) {
          setOrderALL(response.content)
          setOrderNew(response.content.filter((r) => r.status === "NEW"));
          setOrderPending(response.content.filter((r) => r.status === "PENDING"));
          setOrderDone(response.content.filter((r) => r.status === "RENTED"));
          setOrderCancel(response.content.filter((r) => r.status === "CANCEL"));
          setOrderConfirm(response.content.filter((r) => r.status === "CONFIRM"));
          setDataTemp(orderALL.filter((item) => item.id == idOrder));
        }

      }
    }
  };
  useEffect(() => {
    if (idOrder) {
      setDataTemp(orderALL.filter((item) => item.id == idOrder));

    }
  }, [orderALL])
  
  useEffect(() => {
   
    if(idOrder ){
      switch (dataTemp?.[0]?.status ) {
        case "NEW":
          setValue("2");
          break;
        case "PENDING":
          setValue("3");
          break;
        case "CONFIRM":
          setValue("6");
          break;
        case "RENTED":
          setValue("4");
          break;
        case "CANCEL":
          setValue("5");
          break;
        default:
          setValue("2");
      }
    }
    
  }, [dataTemp])
  useEffect(() => {
    if(typeChoose){
     
      switch ( typeChoose) {
        case "NEW":
          setValue("2");
          setType("NEW")
          break;
        case "PENDING":
          setValue("3");
          setType("PENDING")
          break;
        case "CONFIRM":
          setValue("6");
          setType("CONFIRM")
          break;
        case "RENTED":
          setValue("4");
          setType("RENTED")
          break;
        case "CANCEL":
          setValue("5");
          setType("CANCEL")
          break;
        default:
          setValue("2");
          
      }
    }
    
  }, [])
  useEffect(() => {
    if(typeNotify){
     
      switch ( typeNotify) {
        case "ALL":
          setValue("1");
          setType("ALL")
          break;
        case "NEW":
          setValue("2");
          setType("NEW")
          break;
        case "PENDING":
          setValue("3");
          setType("PENDING")
          break;
        case "CONFIRM":
          setValue("6");
          setType("CONFIRM")
          break;
        case "RENTED":
          setValue("4");
          setType("RENTED")
          break;
        case "CANCEL":
          setValue("5");
          setType("CANCEL")
          break;
        default:
          setValue("2");
          
      }
    }
    
  }, [reloadType])
  const getStaticLessee = async (userId) => {
    const response = await getOrderStatusLessee(userId);
    if (response) {

      setStaticLessee(response?.mapsData);
    }
  };

  return (

    <div >
      <Box sx={{ width: "100%", typography: "body1" }}>
        <TabContext value={value}>
          <Box sx={{ borderBottom: 1, borderColor: "divider" }}>
            <TabList
              onChange={handleChange}
              aria-label="lab API tabs example"
              className="bg-[#f5f5f5]"
            >
              <Tab
                sx={{ width: "10%", flexShrink: 0, fontSize: "15px" }}
                label="TẤT CẢ"
                value="1"
                onClick={() => {setType("ALL");setIdOrder(null);}}
              />
              <Tab
                sx={{ width: "16%", flexShrink: 0, fontSize: "15px" }}
                label={`XÁC NHẬN (${staticLessee?.NEW})`}
                value="2"
                onClick={() => {setType("NEW");setIdOrder(null);}}
              />
              <Tab
                sx={{ width: "19%", flexShrink: 0, fontSize: "15px" }}
                label={`ĐANG GIAO (${staticLessee?.PENDING})`}
                value="3"
                onClick={() => {setType("PENDING");setIdOrder(null);}}
              />
              <Tab
                sx={{ width: "24%", flexShrink: 0, fontSize: "15px" }}
                label={`CHỜ XÁC NHẬN (${staticLessee?.CONFIRM})`}
                value="6"
                onClick={() => {setType("CONFIRM");setIdOrder(null);}}
              />
              <Tab
                sx={{ width: "16%", flexShrink: 0, fontSize: "15px" }}
                label={`HOÀN TẤT (${staticLessee?.RENTED})`}
                value="4"
                onClick={() => {setType("RENTED");setIdOrder(null);}}
              />
              <Tab
                sx={{ width: "15%", flexShrink: 0, fontSize: "15px" }}
                label={`ĐÃ HUỶ (${staticLessee?.CANCEL})`}
                value="5"
                onClick={() => {setType("CANCEL");setIdOrder(null);}}
              />


            </TabList>
          </Box>
          
              <TableContainer component={Paper}>
                <TabPanel value="1">
                  <SearchInfor setReload={setReload} reload={reload} setFromDate={setFromDate} setToDate={setToDate} />
                  <RentalOrderTable orderList={orderALL} type={type} setReload={setReload}
                    reload={reload} setLoadChat={setLoadChat} loadChat={loadChat} dataTemp={dataTemp} idOrder={idOrder}/>
                </TabPanel>
              </TableContainer>

              <TableContainer component={Paper}>
                <TabPanel value="2">
                  <SearchInfor setReload={setReload} reload={reload} setFromDate={setFromDate} setToDate={setToDate} />
                  <RentalOrderTable orderList={orderNew} type={type} setReload={setReload}
                    reload={reload} setLoadChat={setLoadChat} loadChat={loadChat} dataTemp={dataTemp}idOrder={idOrder} />
                </TabPanel>
              </TableContainer>

              <TableContainer component={Paper}>
                <TabPanel value="3">
                  <SearchInfor setReload={setReload} reload={reload} setFromDate={setFromDate} setToDate={setToDate} />
                  <RentalOrderTable orderList={orderPending} type={type} setReload={setReload}
                    reload={reload} setLoadChat={setLoadChat} loadChat={loadChat} dataTemp={dataTemp}idOrder={idOrder}/>
                </TabPanel>
              </TableContainer>

              <TableContainer component={Paper} >
                <TabPanel value="4">
                  <SearchInfor setReload={setReload} reload={reload} setFromDate={setFromDate} setToDate={setToDate} />
                  <RentalOrderTable orderList={orderDone} type={type} setReload={setReload}
                    reload={reload} setLoadChat={setLoadChat} loadChat={loadChat} dataTemp={dataTemp}idOrder={idOrder}/>
                </TabPanel>
              </TableContainer>

              <TableContainer component={Paper}>
                <TabPanel value="5">
                  <SearchInfor setReload={setReload} reload={reload} setFromDate={setFromDate} setToDate={setToDate} />
                  <RentalOrderTable orderList={orderCancel} type={type} setReload={setReload}
                    reload={reload} setLoadChat={setLoadChat} loadChat={loadChat} dataTemp={dataTemp}idOrder={idOrder}/>
                </TabPanel>
              </TableContainer>
              <TableContainer component={Paper}>
                <TabPanel value="6">
                  <SearchInfor setReload={setReload} reload={reload} setFromDate={setFromDate} setToDate={setToDate} />
                  <RentalOrderTable orderList={orderConfirm} type={type} setReload={setReload}
                    reload={reload} setLoadChat={setLoadChat} loadChat={loadChat} dataTemp={dataTemp}idOrder={idOrder} />
                </TabPanel>
              </TableContainer>
  

        </TabContext>
      </Box>
    </div>
  );
};

export default RentalOrderManagement;
