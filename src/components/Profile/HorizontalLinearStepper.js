import * as React from "react";
import Box from "@mui/material/Box";
import Stepper from "@mui/material/Stepper";
import Step from "@mui/material/Step";
import StepLabel from "@mui/material/StepLabel";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import Step1Content from "../../components/Profile/Step1Content";
import Step2Content from "../../components/Profile/Step2Content";
import Step3Content from "../../components/Profile/Step3Content";
import { getStatusRequest } from "../../redux/apiRequestProduct";
import { useSelector } from "react-redux";

const steps = [
  "Hướng dẫn chủ tiệm",
  "Chính sách & điều khoản",
  "Thiếp lập thông tin cửa hàng",
];

export default function HorizontalLinearStepper({ checkRequest }) {
  const [activeStep, setActiveStep] = React.useState(0);
  const [skipped, setSkipped] = React.useState(new Set());
  const [finish, setFinish] = React.useState(false);
  const [checked, setChecked] = React.useState(false);
  const [status, setStatus] = React.useState("");
  const user = useSelector((state) => state?.auth?.loadUser?.user);
  const isStepOptional = (step) => {
    return step === 1;
  };

  const isStepSkipped = (step) => {
    return skipped.has(step);
  };

  const handleNext = (status) => {
    let newSkipped = skipped;
    if (isStepSkipped(activeStep)) {
      newSkipped = new Set(newSkipped.values());
      newSkipped.delete(activeStep);
    }

    setActiveStep((prevActiveStep) => prevActiveStep + 1);
    setSkipped(newSkipped);
  };

  const handleBack = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
  };

  const handleSkip = () => {
    if (!isStepOptional(activeStep)) {
      // You probably want to guard against something like this,
      // it should never occur unless someone's actively trying to break something.
      throw new Error("You can't skip a step that isn't optional.");
    }

    setActiveStep((prevActiveStep) => prevActiveStep + 1);
    setSkipped((prevSkipped) => {
      const newSkipped = new Set(prevSkipped.values());
      newSkipped.add(activeStep);
      return newSkipped;
    });
  };

  const handleReset = () => {
    setActiveStep(0);
  };
  React.useEffect(() => {
    getCheckRequestLessor(user?.id);
  }, []);
  const getCheckRequestLessor = async (userId) => {
    const response = await getStatusRequest(userId);
    if (response) {
      setStatus(response);
    }
  };

  return (
    <>
      {checkRequest == "PROCESSING" ? (
        <React.Fragment>
          
          <Typography sx={{ mt: 2, mb: 1 }} className="text-green">
            Yêu cầu sẽ được hệ thống phê duyệt.
          </Typography>
          <Box sx={{ display: "flex", flexDirection: "row", pt: 2 }}>
            <Box sx={{ flex: "1 1 auto" }} />
            {/* <Button onClick={handleReset}>Reset</Button> */}
          </Box>
        </React.Fragment>
      ) : (
        <Box sx={{ width: "100%" }}>
          <Stepper activeStep={activeStep}>
            {steps.map((label, index) => {
              const stepProps = {};
              const labelProps = {};
              if (isStepSkipped(index)) {
                stepProps.completed = false;
              }
              return (
                <Step key={label} {...stepProps}>
                  <StepLabel {...labelProps}>{label}</StepLabel>
                </Step>
              );
            })}
          </Stepper>
          {activeStep === steps.length ? (
            <React.Fragment>
              <Typography sx={{ mt: 2, mb: 1 }}>
                Yêu cầu sẽ được hệ thống phê duyệt.
              </Typography>
              <Box sx={{ display: "flex", flexDirection: "row", pt: 2 }}>
                <Box sx={{ flex: "1 1 auto" }} />
                {/* <Button onClick={handleReset}>Reset</Button> */}
              </Box>
            </React.Fragment>
          ) : (
            <React.Fragment>
              {/* <Typography sx={{ mt: 2, mb: 1 }}>
            Step {activeStep + 1}
          </Typography> */}

              {/* Custom content for each step */}
              {activeStep === 0 && <Step1Content />}
              {activeStep === 1 && <Step3Content setChecked={setChecked} />}
              {activeStep === 2 && (
                <Step2Content
                  handleNext={handleNext}
                  finish={finish}
                  setFinish={setFinish}
                  status={status}
                  setStatus={setStatus}
                />
              )}

              <Box sx={{ display: "flex", flexDirection: "row", pt: 2 }}>
                <Button
                  color="inherit"
                  disabled={activeStep === 0}
                  onClick={handleBack}
                  sx={{ mr: 1 }}
                >
                  Back
                </Button>
                <Box sx={{ flex: "1 1 auto" }} />
                <Button
                  disabled={activeStep === 1 && !checked}
                  onClick={() => {
                    if (activeStep === steps.length - 1) {
                      setFinish(true);
                      //handleNext(activeStep === steps.length - 1);
                    } else {
                      handleNext(activeStep === steps.length - 1);
                    }
                  }}
                >
                  {activeStep === steps.length - 1 ? "Hoàn thành" : "Tiết tục"}
                </Button>
              </Box>
            </React.Fragment>
          )}
        </Box>
      )}
    </>
  );
}
