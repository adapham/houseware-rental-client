import React, { useEffect, useState } from "react";
import SeachInforStore from "./SeachInforStore";
import RentalOrderTableStore from "./RentalOrderTableStore";
import Box from "@mui/material/Box";
import Tab from "@mui/material/Tab";
import TabContext from "@mui/lab/TabContext";
import TabList from "@mui/lab/TabList";
import TabPanel from "@mui/lab/TabPanel";

import TableContainer from "@mui/material/TableContainer";

import Paper from "@mui/material/Paper";
import { getOrderLessor, getOrderStatusLessor } from "../../redux/apiRequestProduct";
import { useSelector } from "react-redux";

const RentalOrderManagerStoreNew = ({ typeNotify,location }) => {
  const [value, setValue] = React.useState("2");
  const user = useSelector((state) => state?.auth?.loadUser?.user);
  const handleChange = (event, newValue) => {
    setValue(newValue);
  };
  const [fromDate, setFromDate] = useState();
  const [staticLessor, setStaticLessor] = useState();
  const [toDate, setToDate] = useState();
  const [type, setType] = useState("NEW");
  const [reload, setReload] = useState(false);
  const [orderNew, setOrderNew] = useState([]);
  const [orderPending, setOrderPending] = useState([]);
  const [orderALL, setOrderALL] = useState([]);
  const [orderDone, setOrderDone] = useState([]);
  const [orderCancel, setOrderCancel] = useState([]);
  const [orderConfirm, setOrderConfirm] = useState([]);

  const [status, setStatus] = useState([]);
  
  useEffect(() => {
    if(typeNotify){
     
      switch ( typeNotify) {
        case "ALL":
          setValue("1");
          setType("ALL")
          break;
        case "NEW":
          setValue("2");
          setType("NEW")
          break;
        case "PENDING":
          setValue("3");
          setType("PENDING")
          break;
        case "CONFIRM":
          setValue("6");
          setType("CONFIRM")
          break;
        case "RENTED":
          setValue("4");
          setType("RENTED")
          break;
        case "CANCEL":
          setValue("5");
          setType("CANCEL")
          break;
        default:
          setValue("2");
          
      }
    }
    
  }, [location])
  useEffect(() => {
    getStaticLessor(user?.id)
    const param = {
      dateFrom: fromDate,
      dateTo: toDate,
    };

    if (fromDate === null || toDate === null) {
      param = {};
    }
    getOrderLessors(param, status);

  }, [reload, status, type]);

  const getOrderLessors = async (infor, status) => {
    if (user?.id) {
      var response = null;
      response = await getOrderLessor(user?.id, infor, status);
      if (response) {
        if (response.content) {
          setOrderALL(response.content)
          setOrderNew(response.content.filter((r) => r.status === "NEW"));
          setOrderPending(response.content.filter((r) => r.status === "PENDING"));
          setOrderDone(response.content.filter((r) => r.status === "RENTED"));
          setOrderCancel(response.content.filter((r) => r.status === "CANCEL"));
          setOrderConfirm(response.content.filter((r) => r.status === "CONFIRM"));
        }

      }
    }
  };


  const getStaticLessor = async (userId) => {
    const response = await getOrderStatusLessor(userId);
    if (response) {

      setStaticLessor(response?.mapsData);
    }
  };
  return (

    <div >
      <Box sx={{ width: "100%", typography: "body1" }}>
        <TabContext value={value}>
          <Box sx={{ borderBottom: 1, borderColor: "divider" }}>
            <TabList
              onChange={handleChange}
              aria-label="lab API tabs example"
              className="bg-[#f5f5f5]"
            >
              <Tab
                sx={{ width: "10%", flexShrink: 0, fontSize: "15px" }}
                label="TẤT CẢ"
                value="1"
                onClick={() => setType("ALL")}
              />
              <Tab
                sx={{ width: "16%", flexShrink: 0, fontSize: "15px" }}
                label={`XÁC NHẬN (${staticLessor?.NEW})`}
                value="2"
                onClick={() => setType("NEW")}
              />
              <Tab
                sx={{ width: "19%", flexShrink: 0, fontSize: "15px" }}
                label={`ĐANG GIAO (${staticLessor?.PENDING})`}
                value="3"
                onClick={() => setType("PENDING")}
              />
              <Tab
                sx={{ width: "24%", flexShrink: 0, fontSize: "15px" }}
                label={`CHỜ XÁC NHẬN (${staticLessor?.CONFIRM})`}
                value="6"
                onClick={() => setType("CONFIRM")}
              />
              <Tab
                sx={{ width: "16%", flexShrink: 0, fontSize: "15px" }}
                label={`HOÀN TẤT (${staticLessor?.RENTED})`}
                value="4"
                onClick={() => setType("RENTED")}
              />
              <Tab
                sx={{ width: "15%", flexShrink: 0, fontSize: "15px" }}
                label={`ĐÃ HUỶ (${staticLessor?.CANCEL})`}
                value="5"
                onClick={() => setType("CANCEL")}
              />


            </TabList>
          </Box>
          <TableContainer component={Paper}>
            <TabPanel value="1">
              <SeachInforStore setReload={setReload} reload={reload} setFromDate={setFromDate} setToDate={setToDate} />
              <RentalOrderTableStore orderList={orderALL} type={type} setReload={setReload} reload={reload} />
            </TabPanel>
          </TableContainer>

          <TableContainer component={Paper}>
            <TabPanel value="2">
              <SeachInforStore setReload={setReload} reload={reload} setFromDate={setFromDate} setToDate={setToDate} />
              <RentalOrderTableStore orderList={orderNew} type={type} setReload={setReload} reload={reload} />
            </TabPanel>
          </TableContainer>

          <TableContainer component={Paper}>
            <TabPanel value="3">
              <SeachInforStore setReload={setReload} reload={reload} setFromDate={setFromDate} setToDate={setToDate} />
              <RentalOrderTableStore orderList={orderPending} type={type} setReload={setReload} reload={reload} />
            </TabPanel>
          </TableContainer>

          <TableContainer component={Paper} >
            <TabPanel value="4">
              <SeachInforStore setReload={setReload} reload={reload} setFromDate={setFromDate} setToDate={setToDate} />
              <RentalOrderTableStore orderList={orderDone} type={type} setReload={setReload} reload={reload} />
            </TabPanel>
          </TableContainer>

          <TableContainer component={Paper}>
            <TabPanel value="5">
              <SeachInforStore setReload={setReload} reload={reload} setFromDate={setFromDate} setToDate={setToDate} />
              <RentalOrderTableStore orderList={orderCancel} type={type} setReload={setReload} reload={reload} />
            </TabPanel>
          </TableContainer>
          <TableContainer component={Paper}>
            <TabPanel value="6">
              <SeachInforStore setReload={setReload} reload={reload} setFromDate={setFromDate} setToDate={setToDate} />
              <RentalOrderTableStore orderList={orderConfirm} type={type} setReload={setReload} reload={reload} />
            </TabPanel>
          </TableContainer>
        </TabContext>
      </Box>
    </div>
  );
};

export default RentalOrderManagerStoreNew;
