import React, {useEffect, useState} from "react";
import {FcOk} from "react-icons/fc";
import TextField from "@mui/material/TextField";
import FormControl from "@mui/material/FormControl";
import InputLabel from "@mui/material/InputLabel";
import Select from "@mui/material/Select";
import MenuItem from "@mui/material/MenuItem";
import {getCategories,} from "../../redux/apiRequestProduct";
import {toast} from "react-toastify";
import httpRequest from "../../redux/Req/apiConfig";

const ViewDetailProd = ({
                            isModalOpenView,
                            closeModalViewProduct,
                            user,
                            setReload,
                            productDetail,
                        }) => {
    const [category, setCategory] = useState([]);
    const [categorySelect, setCategorySelect] = useState(0);
    const [err, setErr] = useState("");
    const [uploadedFiles, setUploadedFiles] = useState([]);
    const [uploadedFilesSub, setUploadedFilesSub] = useState([]);
    const [formData, setFormData] = useState(new FormData());
    const [fileLimit, setFileLimit] = useState(false);
    const [filesShow, setfilesShow] = useState([]);
    const [imageLinks, setImageLinks] = useState();
    const [prod, setProd] = useState();
  

    const [productDetaill, setProductDetaill] = useState({
        categoryId: '',
        description: '',
        title: '',
        price: '',
        quantity: '',
        regulations: '',
        dimensions: '',
        capacity: '',
        wattage: '',
        weight: '',
        model: '',
        breadth: '',
        depositPrice: '',
        length: ''

        // description: productDetail?.description,
        // title: productDetail?.title,
        // price: productDetail?.price,
        // quantity: productDetail?.quantity,
        // regulations: productDetail?.regulations,
        // model: productDetail?.model,
        // dimensions: productDetail?.dimensions,
        // capacity: productDetail?.capacity,
        // wattage: productDetail?.wattage,
        // weight: productDetail?.weight,
        // brand:productDetail?.brand,
        // breadth:productDetail?.breadth,
        // depositPrice: productDetail?.depositPrice,
        // length:productDetail?.length
    });

    let MAX_COUNT = 3;

    const handleChange1 = (event) => {
        setCategorySelect(event.target.value);
    };
    const handleChange = (e) => {
        const {name, value} = e.target;
        setProductDetaill((prevProductDetail) => ({
            ...prevProductDetail,
            [name]: value,
        }));
        formData.set(name, value)
    };

    useEffect(() => {
        if (productDetail?.description) {
            const des = productDetail?.description.split("<br/>").join("\n");
            productDetaill.description = des;
        }
        if (productDetail?.regulations) {
            const reg = productDetail?.regulations.split("<br/>").join("\n");

            productDetaill.regulations = reg;
        }
        productDetaill.title = productDetail?.title;
        productDetaill.price = productDetail?.price;
        productDetaill.quantity = productDetail?.quantity;
        productDetaill.dimensions = productDetail?.dimensions;
        productDetaill.capacity = productDetail?.capacity;
        productDetaill.wattage = productDetail?.wattage;
        productDetaill.weight = productDetail?.weight;
        productDetaill.brand = productDetail?.brand ?? null;
        productDetaill.breadth = productDetail?.breadth ?? null;
        productDetaill.depositPrice = productDetail?.depositPrice
        productDetaill.model = productDetail?.model ?? null
        productDetaill.categoryId = productDetail?.category?.id ?? null
        productDetaill.status = productDetail?.status ?? null

        console.log("productDetaill", productDetaill)
    }, [productDetail]);

    useEffect(() => {
        setImageLinks(productDetail?.imageTextUri)
    }, [productDetail]);

    useEffect(() => {
        if (uploadedFiles.length == 0) {
            MAX_COUNT = MAX_COUNT - imageLinks?.length;
        } else {
            MAX_COUNT = MAX_COUNT - (imageLinks?.length + uploadedFiles?.length) + 2;
        }
    }, [imageLinks, uploadedFiles]);


    const handleUploadFiles = (files) => {
        const uploaded = [...uploadedFiles];
        let limitExceeded = false;
        files.some((file) => {
            if (uploaded.findIndex((f) => f.name === file.name) === -1) {
                if (uploaded.length === MAX_COUNT) {
                    setFileLimit(true);
                }
                if (uploaded.length > MAX_COUNT) {
                    toast("You can only add a maximum of 4 files")
                    setFileLimit(false);
                    limitExceeded = true;
                    return true;
                }
                uploaded.push(file);
            }
        });
        if (!limitExceeded) setUploadedFiles(uploaded);
    };

    const handleRemoveFile = (fileName) => {
        const updatedFiles = [...uploadedFiles];
        const fileIndex = updatedFiles.findIndex((file) => file.name === fileName);
        if (fileIndex !== -1) {
            updatedFiles.splice(fileIndex, 1);
            setUploadedFilesSub(updatedFiles);
            setFileLimit(false);
        }
    };

    const handleRemoveFileLink = (fileName) => {
        imageLinks?.map((value) => {
            if (fileName == value) {
                var newImages = imageLinks.filter(item => item !== fileName)
                setFileLimit(false);
                return setImageLinks(newImages);
            }
        });
    };

    useEffect(() => {
        setUploadedFiles(uploadedFilesSub);
    }, [uploadedFilesSub]);

    const handleFileEvent = (e) => {
        const chosenFiles = Array.prototype.slice.call(e.target.files);
        handleUploadFiles(chosenFiles);
        e.target.value = null;
    };

    const handleShowFiles = (file) => {
        return file;
    };
    const handleShowFilesURL = (file) => {
        return URL.createObjectURL(file);
    };

    const handleSubmit = (e) => {
        e.preventDefault();
        uploadedFiles.map((file) => {
            formData.append('image', file)
        })
        if (productDetaill?.description) {
            const des = productDetaill.description.split("\r\n").join("<br/>");
            formData.set('description', des)
        }
        if (productDetaill?.regulations) {
            const req = productDetaill.regulations.split("\r\n").join("<br/>");
            formData.set('regulations', req)
        }

        formData.set('imageTextUri', imageLinks)
        formData.set('status', productDetaill.status)
        formData.set('brand', productDetaill.brand)
        formData.set('depositPrice', productDetaill.depositPrice)
        formData.set('dimensions', productDetaill.dimensions)
        formData.set('length', productDetaill.length)
        formData.set('price', productDetaill.price)
        formData.set('quantity', productDetaill.quantity)
        formData.set('regulations', productDetaill.regulations)
        formData.set('title', productDetaill.title)
        formData.set('wattage', productDetaill.wattage)
        formData.set('categoryId', productDetaill.categoryId)
        const url = 'api/products/' + productDetail?.id
        httpRequest.putToken(url, formData)
            .then(response => {
                if (response.status && response.status != 200) {
                    const errorMessage = response?.data?.message || "Chỉnh sửa sản phẩm thất bại";
                    toast(errorMessage);
                    setErr(errorMessage)
                } else {
                    setUploadedFilesSub([])
                    setImageLinks([])
                    formData.delete('image')
                    closeModalViewProduct()
                    setReload(true)
                    toast("Chỉnh sửa sản phẩm thành công");
                }
            })
            .catch(error => {
                // Xử lý lỗi trong trường hợp không thể gọi hàm `httpRequest.putToken`
            });

    };

    useEffect(() => {
        getCategorie()
    }, []);

    useEffect(() => {
        console.log("productDetail", productDetail)
        if (productDetail?.category?.id) {
            console.log("productDetail?.category?.id", productDetail?.category?.id)

            const value = category?.filter((item) => item.id == productDetail?.category?.id)?.[0]?.id;
            setCategorySelect(value);
        }
    }, [productDetail]);

    const getCategorie = async () => {
        const response = await getCategories();
        if (response) {
            setCategory(response);
            console.log("category", category)
        }

    };

    const ITEM_HEIGHT = 48;
    const ITEM_PADDING_TOP = 8;
    const MenuProps = {
        PaperProps: {
            style: {
                maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
                width: 250,
            },
        },
    };

    return (
        <center>
            <div
                id="createProductModal"
                tabIndex="-1"
                aria-hidden="true"
                class={`${isModalOpenView ? "" : "hidden"
                } overflow-y-auto overflow-x-hidden fixed top-0 right-0 left-0 z-50 justify-center items-center w-full md:inset-0 h-[calc(100%-1rem)] md:h-full`}
            >
                <div class="relative p-4 w-full max-w-5xl h-full md:h-auto">
                    <div class="relative p-4 bg-white rounded-lg shadow light:bg-gray-800 sm:p-5">
                        <div
                            class="flex justify-between items-center pb-4 mb-4 rounded-t border-b sm:mb-5 light:border-gray-600">
                            <button
                                type="button"
                                className="text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm p-1.5 ml-auto inline-flex items-center light:hover:bg-gray-600 light:hover:text-white"
                                data-modal-toggle="createProductModal"
                                onClick={closeModalViewProduct}
                            >
                                <svg
                                    aria-hidden="true"
                                    className="w-5 h-5"
                                    fill="currentColor"
                                    viewBox="0 0 20 20"
                                    xmlns="http://www.w3.org/2000/svg"
                                >
                                    <path
                                        fill-rule="evenodd"
                                        d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                                        clip-rule="evenodd"
                                    />
                                </svg>
                                <span class="sr-only">Close modal</span>
                            </button>
                        </div>

                        <form onSubmit={handleSubmit}>
                            {/* Thông tin sản phẩm */}
                            <div className="flex justify-between">
                            <fieldset className="border-2 rounded-lg border-[#dde8da] p-3" style={{width:'78%'}}>
                                <legend className="text-lg text-center">
                                    Chi tiết thông tin sản phẩm
                                </legend>
                                <div class="grid gap-4 sm:grid-cols-2">
                                    <div>
                                        <TextField
                                            margin="normal"
                                            label="Tiêu đề"
                                            variant="outlined"
                                            size="small"
                                            type="text"
                                            className="w-full"
                                            placeholder={productDetaill?.title ? '' : "Trống"}
                                            name="title"
                                            id="name"
                                            readOnly={true}
                                            value={productDetaill?.title}
                                            readonly={true}
                                            InputLabelProps={{
                                                shrink: true,
                                            }}
                                        />
                                    </div>
                                    <div>
                                        <FormControl sx={{mt: 2}} className="w-full" size="small">
                                            <InputLabel id="demo-select-small-label-category">
                                                Thể loại
                                            </InputLabel>
                                            <Select
                                                label="Thể loại"
                                                labelId="demo-select-small-label-category"
                                                id="category"
                                                name="categoryId"
                                                readonly={true}
                                                readOnly={true}
                                                MenuProps={MenuProps}
                                                value={productDetaill?.categoryId}
                                            >

                                                {category?.map((item) => (

                                                    <MenuItem key={item.id} value={item?.id}
                                                    >
                                                        {item.categoryName}
                                                    </MenuItem>
                                                ))}
                                            </Select>
                                        </FormControl>
                                    </div>
                                    <div>
                                        <FormControl sx={{mt: 2}} className="w-full" size="small">
                                            <InputLabel id="demo-select-small-label-category">Trạng thái</InputLabel>
                                            <Select
                                                label="TRạng thái"
                                                labelId="demo-select-small-label-category"
                                                id="status"
                                                name='status'
                                                readonly={true}
                                                readOnly={true}
                                                onChange={handleChange}
                                                MenuProps={MenuProps}
                                                value={productDetaill?.status ?? -1}
                                            >
                                                <MenuItem value="MINT">
                                                    Mới
                                                </MenuItem>
                                                <MenuItem value="GOOD">
                                                    Tốt
                                                </MenuItem>
                                                <MenuItem value="VERY_GOOD">
                                                    Rất tốt
                                                </MenuItem>
                                                <MenuItem value="NEAR_MINT">
                                                    Như mới
                                                </MenuItem>

                                            </Select>
                                        </FormControl>
                                    </div>
                                    <div>
                                        <TextField
                                            margin="normal"
                                            label="Nhãn hiệu"
                                            variant="outlined"
                                            size="small"
                                            type="text"
                                            className="w-full"
                                            placeholder="Nhập tại đây"
                                            name="model"
                                            id="model"
                                            inputProps={{maxLength: 100}}
                                            value={productDetaill?.model}

                                            onChange={handleChange}
                                            InputLabelProps={{
                                                shrink: true,
                                            }}
                                        />
                                    </div>
                                    <div>
                                        <TextField
                                            margin="normal"
                                            variant="outlined"
                                            size="small"
                                            className="w-full"
                                            placeholder="Nhập tại đây"
                                            label="Giá thuê"
                                            value={productDetaill?.price}
                                            type="number"
                                            min={0}
                                            name="price"
                                            id="price"
                                            required=""
                                            defaultValue={0}
                                            readonly={true}
                                            readOnly={true}
                                            InputLabelProps={{
                                                shrink: true,
                                            }}
                                        />
                                    </div>
                                    <div>
                                        <TextField
                                            margin="normal"
                                            variant="outlined"
                                            size="small"
                                            label="Giá cọc"
                                            className="w-full"
                                            placeholder="Nhập tại đây"
                                            value={productDetaill?.depositPrice}
                                            type="number"
                                            min={0}
                                            name="depositPrice"
                                            id="depositPrice"
                                            required=""
                                            readonly={true}
                                            readOnly={true}
                                            InputLabelProps={{
                                                shrink: true,
                                            }}
                                        />
                                    </div>
                                    <div>
                                        <TextField
                                            margin="normal"
                                            variant="outlined"
                                            size="small"
                                            className="w-full"
                                            placeholder={productDetaill?.quantity ? '' : "Trống"}
                                            label="Số lượng"
                                            type="number"
                                            min={0}
                                            name="quantity"
                                            value={productDetaill?.quantity}
                                            id="quantity"
                                            required=""
                                            defaultValue={0}
                                            readonly={true}
                                            readOnly={true}
                                            InputLabelProps={{
                                                shrink: true,
                                            }}
                                        />
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset className="border-2 rounded-lg border-[#dde8da] mt-5 p-3" style={{width:'20%'}}>
                                <legend className="text-lg text-center">
                                    Thông số kỹ thuật
                                </legend>
                                <div class="grid gap-4 sm:col-span-2 md:gap-6 sm:grid-cols-1">
                                    <div>
                                        <TextField
                                            margin="normal"
                                            variant="outlined"
                                            size="small"
                                            className="w-full"
                                            placeholder={productDetaill?.weight ? '' : "Trống"}
                                            label="Trọng lượng (kg)"
                                            value={productDetaill?.weight}
                                            type="text"
                                            name="weight"
                                            id="weight"
                                            required=""
                                            readonly={true}
                                            readOnly={true}
                                            InputLabelProps={{
                                                shrink: true,
                                            }}
                                        />
                                    </div>
                                    <div>
                                        <TextField
                                            margin="normal"
                                            variant="outlined"
                                            size="small"
                                            className="w-full"
                                            placeholder={productDetaill?.dimensions ? '' : "Trống"}
                                            value={productDetaill?.dimensions}
                                            label="Kích thước"
                                            type="text"
                                            name="dimensions"
                                            id="dimensions"
                                            required=""
                                            readonly={true}
                                            readOnly={true}
                                            InputLabelProps={{
                                                shrink: true,
                                            }}
                                        />
                                    </div>
                                    <div>
                                        <TextField
                                            margin="normal"
                                            variant="outlined"
                                            size="small"
                                            className="w-full"
                                            placeholder={productDetaill?.capacity ? '' : "Trống"}
                                            value={productDetaill?.capacity}
                                            label="Dung tích"
                                            type="text"
                                            name="capacity"
                                            id="capacity"
                                            required=""
                                            readonly={true}
                                            readOnly={true}
                                            InputLabelProps={{
                                                shrink: true,
                                            }}
                                        />
                                    </div>
                                    <div>
                                        <TextField
                                            margin="normal"
                                            variant="outlined"
                                            size="small"
                                            className="w-full"
                                            value={productDetaill?.wattage}
                                            placeholder={productDetaill?.wattage ? '' : "Trống"}
                                            label="Công suất"
                                            type="text"
                                            name="wattage"
                                            id="wattage"
                                            required=""
                                            readonly={true}
                                            readOnly={true}
                                            InputLabelProps={{
                                                shrink: true,
                                            }}
                                        />
                                    </div>
                                </div>
                            </fieldset>
                                </div>
                            {/* Thông số kỹ thuật */}
                            <div className="flex justify-between">
                            <div class="sm:col-span-2 mt-5 "style={{width:'49%'}}>
                                <TextField
                                    id="description"
                                    name="description"
                                    label="Mô tả sản phẩm"
                                    value={productDetaill?.description}
                                    placeholder={productDetaill?.description ? '' : "Trống"}
                                    multiline
                                    rows={3}
                                    className="w-full"
                                    readonly={true}
                                    readOnly={true}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                />
                            </div>
                            <div class="sm:col-span-2 mt-5" style={{width:'49%'}}>
                                <TextField
                                    id="regulations"
                                    name="regulations"
                                    label="Quy định cho thuê"
                                    placeholder={productDetaill?.regulations ? '' : "Trống"}
                                    multiline
                                    rows={3}
                                    value={productDetaill.regulations}
                                    className="w-full"
                                    readonly={true}
                                    readOnly={true}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                />
                            </div>
                            </div>
                            <div class="m-4">
                <span class="block mb-2 text-sm font-medium text-gray-900 light:text-white">
                  Ảnh sản phẩm{" "}
                </span>
                                <div className="grid grid-cols-4 gap-4 mb-4">
                                    {imageLinks?.map((file) => (
                                        <>
                                            <div
                                                key={file.name}
                                                className="relative p-2 bg-gray-100 rounded-lg sm:w-36 sm:h-36"
                                            >
                                                <img
                                                    src={handleShowFiles(file)}
                                                    alt={file}
                                                    className="w-full h-full object-cover"
                                                />
                                            </div>
                                        </>
                                    ))}

                                    {uploadedFiles?.map((file) => (
                                        <>
                                            <div key={file.name}
                                                 className="relative p-2 bg-gray-100 rounded-lg sm:w-36 sm:h-36">
                                                <img src={handleShowFilesURL(file)} alt={file.name}
                                                     className="w-full h-full object-cover"/>
                                                <a onClick={(e) => handleRemoveFile(file.name)}>
                                                    <svg
                                                        aria-hidden="true"
                                                        className="w-5 h-5"
                                                        fill="currentColor"
                                                        viewBox="0 0 20 20"
                                                        xmlns="http://www.w3.org/2000/svg"
                                                    >
                                                        <path
                                                            fillRule="evenodd"
                                                            d="M9 2a1 1 0 00-.894.553L7.382 4H4a1 1 0 000 2v10a2 2 0 002 2h8a2 2 0 002-2V6a1 1 0 100-2h-3.382l-.724-1.447A1 1 0 0011 2H9zM7 8a1 1 0 012 0v6a1 1 0 11-2 0V8zm5-1a1 1 0 00-1 1v6a1 1 0 102 0V8a1 1 0 00-1-1z"
                                                            clipRule="evenodd"
                                                        />
                                                    </svg>
                                                </a>
                                            </div>
                                        </>
                                    ))}
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </center>

    );
};

export default ViewDetailProd;
