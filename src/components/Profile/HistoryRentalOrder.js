import * as React from "react";

import { convertMoney, convertToDate } from "../../utils/formatConfig";
import { BsEyeFill } from "react-icons/bs";
import { GiConfirmed, GiCancel } from "react-icons/gi";
import { Avatar, Button, Col, Modal, Table, Tooltip } from "antd";
import IconButton from "@mui/material/IconButton";
import { FcUndo } from "react-icons/fc";
import LoadingScreen from "../../pages/examples/loading-screen";
import { useState } from "react";
import Rating from "@mui/material/Rating";
import Typography from "@mui/material/Typography";
import {
  getCommentReview,
  getOrderDelete,
  getReport,
} from "../../redux/apiRequestProduct";

import { FcCheckmark, FcClock, FcCancel, FcInfo, FcApproval } from 'react-icons/fc';
import HistoryRentalDetail from "./HistoryRentalDetail";
import { FcOnlineSupport } from "react-icons/fc";
import { Radio, Space } from 'antd';
import TextArea from "antd/es/input/TextArea";
import { useSelector } from "react-redux";
import { FcComments } from "react-icons/fc";
import { toast } from "react-toastify";
function HistoryRentalOrder({ orderList, setReload, reload, setView,setLoadChat,loadChat,setIdOrder,loading}) {
  const [valueOpRe, setValueOpRe] = useState();


  const [openD, setOpenD] = useState(false);
  const [descriptionC, setDescriptionC] = React.useState();
  const [openR, setOpenR] = useState(false);
  const [openC, setOpenC] = useState(false);
  const [descriptionR, setDescriptionR] = useState("");
  const [idDelete, setIdDelete] = useState();
  const [description, setDescription] = useState("");
  const user = useSelector((state) => state?.auth?.loadUser?.user);
  const [productIdCh,setProductIdCh]=useState();
  const [valueRate, setValueRate] = React.useState(5);
  const [orderDetailId,setOrderDetailId]=useState();
  const [chooseStatus,setChooseStatus]=useState();

  const convertStatus = (value) => {
    let icon, color;

    switch (value) {
      case "NEW":
        icon = <FcInfo />;
        color = "text-yellow-500";
        return (
          <span className={`font-bold ${color} flex items-center`}>
            {icon}  Chờ xác nhận
          </span>
        );

      case "PENDING":
        icon = <FcClock />;
        color = "text-blue-500";
        return (
          <span className={`font-bold ${color} flex items-center`}>
            {icon} Đang giao
          </span>
        );

      case "RENTED":
        icon = <FcCheckmark />;
        color = "text-green-500";
        return (
          <span className={`font-bold ${color} flex items-center`}>
            {icon} Đang thuê
          </span>
        );

      case "CANCEL":
        icon = <FcCancel />;
        color = "text-red-500";
        return (
          <span className={`font-bold ${color} flex items-center`}>
            {icon} Đã huỷ
          </span>
        );

      case "DONE":
        icon = <FcApproval />;
        color = "text-purple-500";
        return (
          <span className={`font-bold ${color} flex items-center`}>
            {icon} Đã hoàn thành
          </span>
        );
        case "RETURN":
        icon = <FcUndo />;
        color = "text-blue-500";
        return (
          <span className={`font-bold ${color} flex items-center`}>
            {icon} Chờ trả hàng
          </span>
        );

      default:
        return value;
    }
  };
  const convertStatusOrder = {
    NEW: 1,
    PENDING: 2,
    RENTED: 3,
    CANCEL: 4,
    DONE: 5,
    RETURN: 6,
  };
  const hideModalR = (status) => {

    const value = valueOpRe == 1 ? description : valueOpRe;
    
    if(status){
      getReportRequest(orderList[0]?.orderId, value);
    }
    setOpenR(false)
    setDescription(null);
    setValueOpRe(null)
  };
  const getReportRequest = async (id, content) => {
    const responnse = await getReport(id, user?.id, content);
    if (responnse) {
      setOpenR(false);
      toast.success("Báo cáo thành công!", {
        position: "top-right",
        autoClose: 2000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "light",
      });
    }
  }

  const hideModalD = (status) => {
    setOpenD(false);
    if (status) {
      const value = descriptionR;
      getDelete(idDelete, value);
      setDescriptionR("")
    }
    
  };

  const [open, setOpen] = useState(false);

  const onClose = () => {
    setOpen(false);
  };
  const handleGetDetail = (data) => {
    setChooseStatus(data?.status)
    setOrderDetailId(data.orderId);
    setIdOrder(data.id)
    setDataDetail(data);
    setOpen(true);
  };
  const handleReport = () => {
    setOpenR(true);
  }

  const onChange = (e) => {
    setValueOpRe(e.target.value);
  };


  const getDelete = async (id, content) => {
    const response = await getOrderDelete(id, content);
    if (response) {
      setReload(!reload);
      toast.success("Huỷ sản đơn hàng thành công", {
        position: "top-right",
        autoClose: 2000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "light",
      });
    }
  };

 
  const [dataDetail, setDataDetail] = useState([]);
  const columns = [
    { 
      title: "STT", 
      dataIndex: "code", 
      key: "code", 
      width: '5%' 
    },
    { 
      title: "Tên sản phẩm", 
      dataIndex: "productName", 
      key: "productName", 
      width: '30%' ,
      render: (value,record) => (
        <div class="flex items-center mr-3" key={record.id}>
          <img
            src={record.imageText[0]}
            alt="Image"
            class="h-8 w-auto mr-3"
          />
          {record.productName}
        </div>
      ),
    },
    {
      title: "Số ngày còn lại",
      dataIndex: "timeDay",
      key: "timeDay",
      width: '18%',
      sorter: (a, b) => a.timeDay - b.timeDay,
      align: "center",
      render: (value) => (
        <span style={{ fontWeight: "bold" }}>{value}</span>
      ),
    },
    {
      title: "Thời gian thuê",
      dataIndex: "rentTime",
      key: "rentTime",
      width: '15%',
      align: "center",
      render: (value, record) => {
        const createdTime = convertToDate(record.createdTime);
        const expiredDate = convertToDate(record.expiredDate);
    
        if (createdTime && expiredDate) {
          const formattedCreatedTime = createdTime.toLocaleString();
          const formattedExpiredDate = expiredDate.toLocaleString();
    
          return (
            <span style={{ fontWeight: "bold" }}>
              {`${formattedCreatedTime} đến ${formattedExpiredDate}`}
            </span>
          );
        } else {
          return <span style={{ fontWeight: "bold" }}>-</span>;
        }
      },
    },
    
    
    {
      title: "Tổng số tiền",
      dataIndex: "price",
      key: "price",
      width: '15%',
      align: "center",
      sorter: (a, b) => a.price - b.price,
      render: (value) => (
        <span style={{ fontWeight: "bold" }}>{convertMoney(value)} VND</span>
      ),
    },
    {
      title: "Trạng thái",
      dataIndex: "status",
      key: "status",
      width: '22%',
      sorter: (a, b) => convertStatusOrder[a.status] - convertStatusOrder[b.status],
      align: "center",
      render: (value) => (
        <span style={{ fontWeight: "bold" }}>{convertStatus(value)}</span>
      ),
    },
    {
      title: "Thao tác",
      dataIndex: "action",
      key: "action",
      width: '5%',
      align: "center",
      render: (value,rows) => (
        <div className="flex justify-center">
          <div className="mr-2">
            <Tooltip title="Bình luận" arrow >
              <IconButton >
                <FcComments
                  style={{ fontSize: "20px" }}

                  onClick={() => {setOpenC(true); setProductIdCh(rows?.productId)}}
                />
              </IconButton>
            </Tooltip>
          </div>
          {/* {rows?.status !="DONE" && (

          <div className="">
            <Tooltip title="Báo cáo" arrow >
              <IconButton >
                <FcOnlineSupport
                  style={{ fontSize: "20px" }}
                  onClick={() => handleReport(rows)}
                />
              </IconButton>
            </Tooltip>
          </div>
          ) } */}
          <div className="ml-2">
            <Tooltip title="Xem chi tiết" arrow >
              <IconButton >
                <BsEyeFill
                  style={{ fontSize: "20px" }}
                  onClick={() => handleGetDetail(rows)}
                />
              </IconButton>
            </Tooltip>
          </div>
        </div>
      ),
    },
  ];
  

  
  const handleComment = () => {
    const params = {
      productId: productIdCh,
      userId: user?.id,
      content: descriptionC,
      rating: valueRate,
    };
    reviewComment(params);
    
  }
  const reviewComment = async (infor) => {
    const response = await getCommentReview(infor);
    if (response) {
      toast.success("Đánh giá sản phẩm thành công!", {
        position: "top-right",
        autoClose: 800,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "light",
      });
      setDescriptionC("");
      setOpenC(false);
    }
    if(response?.message){
      toast.error(`${response?.message}`, {
        position: "top-right",
        autoClose: 800,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "light",
      });
    }
  };
  
  if (loading) {
    return <LoadingScreen />;
  }

  return (
    <>
      <Table
          columns={columns}
          dataSource={orderList}
          pagination={{
            defaultPageSize: 8,
            showSizeChanger: true,
            pageSizeOptions: ["8","10", "20", "30"],
          }}/>

      <Modal
        title="Chi tiết sản phẩm"
        centered
        open={open}
        footer={null}
        width={1200}
        height={800}
        onCancel={onClose}
      >
        {dataDetail && <HistoryRentalDetail dataDetail={dataDetail} setView={setView} 
        setLoadChat={setLoadChat} loadChat={loadChat} 
        orderDetailId={orderDetailId} setReload={setReload} reload={reload} chooseStatus={chooseStatus} setOpen={setOpen}/>}
      </Modal>
      <Modal
        title="Huỷ đơn hàng"
        open={openD}
        footer={null}
        okText="Xác nhận"
        cancelText="Quay lại"
        onCancel={hideModalD}
      >
        <p>Bạn có chắc muốn huỷ order không?</p>
        <TextArea
          onChange={(e) => setDescriptionR(e.target.value)}
          placeholder="Điền lý do ở dây"
          value={descriptionR}
        ></TextArea>
        <div className="flex justify-end mt-5">
          <Button
            className="bg-[#1975D2] text-white "
            onClick={() => hideModalD(true)}
          >
            Đồng ý
          </Button>
          <Button className="bg-[#1975D2] text-white " onClick={() => hideModalD(false)}>
            Huỷ bỏ
          </Button>
        </div>
      </Modal>
      <Modal
        title="Báo cáo vi phạm"
        open={openR}
        footer={null}
        okText="Xác nhận"
        cancelText="Quay lại"
        onCancel={()=>hideModalR(false)}
      >

        <Radio.Group onChange={onChange} value={valueOpRe}>
          <Space direction="vertical">
            <Radio value={'Sản phẩm nhận không đúng như đơn hàng'}>
              Sản phẩm nhận không đúng như đơn hàng
            </Radio>
            <Radio value={'Không nhận được sản phẩm thuê'}>
              Không nhận được sản phẩm thuê
            </Radio>
            <Radio value={'Sản phẩm nhận bị hư hỏng'}>
              Sản phẩm nhận bị hư hỏng
            </Radio>
            {/* <Radio value={'Chủ tiệm không đồng ý nhận lại sản phẩm'}>
              Chủ tiệm không đồng ý nhận lại sản phẩm
            </Radio> */}
            <Radio value={'Chủ tiệm muốn cho thuê bên ngoài Houseware Rental'}>
              Chủ tiệm muốn cho thuê bên ngoài Houseware Rental
            </Radio>
            {/* <Radio value={'Không thống nhất được phí thuê/đặt cọc'}>
              Không thống nhất được phí thuê/đặt cọc
            </Radio> */}
            <Radio value={1}>
              Lý do khác
              {valueOpRe === 1 ? (
                <TextArea
                  onChange={(e) => setDescription(e.target.value)}
                  placeholder="Điền lý do ở dây"
                  value={description}
                ></TextArea>
              ) : null}
            </Radio>
          </Space>
        </Radio.Group>
        <div className="flex justify-end mt-5">
        {(description ||valueOpRe ) ?(
            <Button
            className="mt-2  bg-[#1975D2] text-white "
             onClick={() => hideModalR(true)}
           >
             Gửi
           </Button>
          ):(
            <Button
           className="mt-2  bg-[#1975D2] text-white "
           disabled
          >
            Gửi
          </Button>
          )}
        </div>
      </Modal>
      <Modal
        title="Đánh giá sản phẩm"
        open={openC}
        footer={null}
        okText="Xác nhận"
        cancelText="Quay lại"
        onCancel={() => setOpenC(false)}
      >
        <Col span={24}>
        
          <div className="flex justify-between">
            <div>
              <Avatar src={<img src={user?.imageUrl} alt="avatar" />} />{user?.name}
            </div>
            <div>
              <div>
                <Typography gutterBottom>Điểm của bạn:</Typography>
              </div>
              <div>
                <Rating
                  name="simple-controlled"
                  value={valueRate}
                  onChange={(event, newValue) => {
                    setValueRate(newValue);
                  }}
                />
              </div>
            </div>
          </div>

          <TextArea
            onChange={(e) => setDescriptionC(e.target.value)}
            value={descriptionC}
            placeholder="Bình luận ở đây"
          ></TextArea>


        </Col>
        <div className="flex justify-end mt-5">
          <Button
            className="mt-2  bg-[#1975D2] text-white flex"
            onClick={handleComment}
          >
            <FcCheckmark /> Xác nhận đánh giá
          </Button>

        </div>
      </Modal>
    </>
  );
}

export default HistoryRentalOrder;
