import React, { useEffect, useState } from "react";
import EditProduct from "./EditProduct";

import { Button, Col, Pagination, Row, Table, Tooltip } from "antd";
import { SearchOutlined } from "@ant-design/icons";
import { BiSolidEdit } from "react-icons/bi";
import { IoAddCircleSharp } from "react-icons/io5";
import { IconButton, Rating, Switch } from "@mui/material";
import AddProduct from "./AddProduct";
import { useSelector } from "react-redux";

import VisibilityIcon from "@mui/icons-material/Visibility";
import {
  exportProductByUser,
  getProductById,
  getProductOfUser,
} from "../../redux/apiRequestProduct";

import ViewDetailProd from "./ViewDetailProd";
import httpRequest from "../../redux/apiConfig";

import { FcApproval, FcCancel, FcClock } from "react-icons/fc";
import { toast } from "react-toastify";
import LoadingScreen from "../../pages/examples/loading-screen";
import { FcDocument } from "react-icons/fc";

const BASE_URL = process.env.REACT_APP_BASE_URL;
const API_URL = `${BASE_URL}/api/`;
const OrderManagement = () => {
  const [productList, setProductList] = useState([]);

  const [isSwitchOn, setIsSwitchOn] = useState();
  const [loading, setLoading] = useState(true);
  const [keySearch, setKeySearch] = useState("");
  const user = useSelector((state) => state?.auth?.loadUser?.user);
  const [productDetail, setProductDetail] = useState([]);
  const [reload, setReload] = useState(false);
  const [idDelete, setIdDelete] = useState(0);

  const [filter, setFilter] = useState({
    keyword: "",
    pageIndex: 1,
    pageSize: 8,
  });
  const [totalPage, setTotalPage] = useState();

  useEffect(() => {
    getListProductOfUser(user?.id, filter);
  }, [filter, productDetail, reload]);

  const getListProductOfUser = async (userId, infor) => {
    try {
      setLoading(true);
      const response = await getProductOfUser(userId, infor);
      if (response && response.content) {
        setProductList(response.content);
        setTotalPage(response.pageable.totalElements);
      }
    } catch (error) {
      console.error(error);
    } finally {
      setLoading(false);
    }
  };

  const getProductsbyId = async (productId) => {
    const response = await getProductById(productId);
    if (response) {
      setProductDetail(response);
    }
  };

  const handleChangeShowHide = (event) => {
    const url =
      API_URL + "products/" + idDelete.toString().trim() + "/visible-hidden";
    console.log("url", url);
    httpRequest.put(url, {}, {}).then((res) => {
      setReload(!reload);
    });
    setReload(!reload);
    toast.success(
      `${isSwitchOn ? "Ẩn sản phẩm thành công" : "Hiện sản phẩm thành công"}`,
      {
        position: "top-right",
        autoClose: 800,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "light",
      }
    );
    closeModalDeleteProduct(true);
  };
  const convertApprove = (value) => {
    let icon, color;
    switch (value) {
      case "WAIT_APPROVE":
        icon = <FcClock />;
        color = "text-yellow-500";
        return (
          <>
            <span className={`font-bold  ${color}  flex items-center`}>
              {icon} Chờ duyệt
            </span>
          </>
        );
      case "APPROVED":
        icon = <FcApproval />;
        color = "text-blue-500";
        return (
          <>
            <span className={`font-bold  ${color} flex items-center`}>
              {icon} Đã duyệt
            </span>
          </>
        );
      case "REFUSED":
        icon = <FcCancel />;
        color = "text-red-500";
        return (
          <>
            <span className={`font-bold  ${color} flex items-center`}>
              {icon} Từ chối
            </span>
          </>
        );
      default:
        return value;
    }
  };
  const convertStatus = (value) => {
    let icon, color;
    switch (value) {
      case "MINT":
        color = "text-green-500";
        return (
          <>
            {" "}
            <span className={`font-bold ${color} flex items-center`}>Mới</span>
          </>
        );

      case "GOOD":
        color = "text-green-500";
        return (
          <>
            {" "}
            <span className={`font-bold ${color} flex items-center`}>Tốt</span>
          </>
        );

      case "VERY_GOOD":
        color = "text-green-500";
        return (
          <>
            {" "}
            <span className={`font-bold ${color} flex items-center`}>
              Rất tốt
            </span>
          </>
        );
      case "NEAR_MINT":
        color = "text-green-500";
        return (
          <>
            {" "}
            <span className={`font-bold ${color} flex items-center`}>
              Như mới
            </span>
          </>
        );
      default:
        return value;
    }
  };
  const convertStatusOrder = {
    MINT: 1,
    GOOD: 2,
    VERY_GOOD: 3,
    NEAR_MINT: 4,
  };
  const customCategoryOrder = [
    "Máy Giặt",
    "Máy sưởi",
    "Quạt",
    "Tivi",
    "Tủ Lạnh",
    "Điều Hòa",
  ];

  const columns = [
    {
      title: "STT",
      dataIndex: "id",
      key: "id",
      minWidth: 20,
      align: "center",
      render: (text, record, index) => index + 1,
    },
    {
      title: "Sản Phẩm",
      dataIndex: "product",
      key: "product",
      minWidth: 80,
      render: (value, record) => (
        <div className="flex items-center mr-3" key={record.id}>
          <img
            src={record?.imageTextUri[0]}
            alt={record.name}
            className="h-9 w-11 mr-3"
            style={{ alignSelf: "flex-start" }}
          />

          <Tooltip title="Xem chi tiết">
            <a
              onClick={() => openModalViewProduct(record?.id)}
              className="centered-link"
            >
              {record.title}
            </a>
          </Tooltip>
        </div>
      ),
    },
    {
      title: "Thể loại",
      dataIndex: "categoryName",
      key: "categoryName",
      sorter: (a, b) =>
        customCategoryOrder.indexOf(a.categoryName) -
        customCategoryOrder.indexOf(b.categoryName),
      minWidth: 50,
      align: "center",
    },
    {
      title: "Đánh giá",
      dataIndex: "rating",
      key: "rating",
      minWidth: 100,
      align: "center",
      render: (value, record) => (
        <Rating
          name="disabled"
          value={record.star}
          readOnly
          sx={{ fontSize: "20px" }}
          key={record.id}
        />
      ),
    },
    {
      title: "Số lượng",
      dataIndex: "quantity",
      key: "quantity",
      minWidth: 40,
      sorter: (a, b) => a.quantity - b.quantity,
      align: "center",
    },
    {
      title: "Tình trạng",
      dataIndex: "status",
      key: "status",
      minWidth: 50,
      sorter: (a, b) =>
        convertStatusOrder[a.status] - convertStatusOrder[b.status],
      render: (value) => convertStatus(value),
      align: "center",
    },
    {
      title: "Trạng thái",
      dataIndex: "approve",
      key: "approve",
      minWidth: 80,
      align: "center",
      render: (value) => convertApprove(value),
    },
    {
      title: "Thao tác",
      dataIndex: "action",
      key: "action",
      minWidth: 100,
      align: "center",
      render: (value, record) => (
        <div className="flex justify-center">
          <Tooltip title="Sửa" arrow>
            <IconButton>
              <BiSolidEdit onClick={() => openModalEditProduct(record?.id)} />
            </IconButton>
          </Tooltip>

          <Tooltip title="Xem chi tiết" arrow>
            <IconButton>
              <VisibilityIcon
                onClick={() => openModalViewProduct(record?.id)}
              />
            </IconButton>
          </Tooltip>

          <Tooltip title="Ẩn/Hiện" arrow>
            <IconButton>
              <Switch
                onClick={() =>
                  openModalDeleteProduct(record?.id, record?.hidden)
                }
                checked={record?.hidden}
                onChange={handleSwitchToggle}
                inputProps={{ "aria-label": "controlled" }}
              />
            </IconButton>
          </Tooltip>
        </div>
      ),
    },
  ];

  const handleSwitchToggle = (e) => {
    
    setIsSwitchOn(e.target.checked);
  };

  const [isModalOpen, setIsModalOpen] = useState(false);
  const openModalAddProduct = () => {
    setIsModalOpen(true);
  };
  const closeModalAddProduct = () => {
    setIsModalOpen(false);
  };
  const [isModalOpenEdit, setIsModalOpenEdit] = useState(false);
  const openModalEditProduct = (id) => {
    getProductsbyId(id);

    setIsModalOpenEdit(true);
  };

  const openModalViewProduct = (id) => {
    getProductsbyId(id);
    setIsModalOpenView(true);
  };
  const closeModalEditProduct = () => {
    setIsModalOpenEdit(false);
  };
  const [isModalOpenDelete, setIsModalOpenDelete] = useState(false);
  const openModalDeleteProduct = (productId, isHidden) => {
    setIdDelete(productId);
    setIsModalOpenDelete(true);
  };
  const closeModalDeleteProduct = () => {
    setIsModalOpenDelete(false);
  };
  const [isModalOpenView, setIsModalOpenView] = useState(false);

  const closeModalViewProduct = () => {
    setIsModalOpenView(false);
  };
  const handlePaginationChange = (page, pageSize) => {
    setFilter({ ...filter, pageIndex: page, pageSize: pageSize });
  };

  if (loading) {
    return <LoadingScreen />;
  }
  const handleSearch = () => {
    setFilter({ ...filter, keyword: keySearch, pageIndex: 1 });

    setKeySearch("");
  };
  const getExport = async () => {
    const response = await exportProductByUser(user?.id, filter.keyword);
    if (response.status == 200) {
      toast.success("Xuất báo cáo thành công", {
        position: "top-right",
        autoClose: 800,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "light",
      });
    } else {
      toast.error("Xuất báo cáo thất bại", {
        position: "top-right",
        autoClose: 800,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "light",
      });
    }
  };
  const handleExport = () => {
    if (user?.id) {
      getExport();
    }
  };

  return (
    <>
      <Row>
        <Col span={24}>
          <div class=" flex justify-between mb-5">
            <div>
              <form class="flex items-center">
                <label for="simple-search" class="sr-only">
                  Search
                </label>
                <div class="relative">
                  <div class="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                    <SearchOutlined />
                  </div>
                  <input
                    type="text"
                    id="simple-search"
                    placeholder="Tìm kiếm sản phẩm"
                    required=""
                    class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-primary-500 focus:border-primary-500 block w-full pl-10 p-2 "
                    onChange={(e) => setKeySearch(e.target.value)}
                  />
                </div>
                <Button className="bg-[#2ecc71] ml-2 " onClick={handleSearch}>
                  Tìm kiếm
                </Button>
              </form>
            </div>

            <div className="flex">
              <div className="mr-2 " style={{ marginTop: -5 }}>
                <Tooltip title="Xuất Báo cáo" arrow onClick={handleExport}>
                  <IconButton>
                    <FcDocument style={{ fontSize: "30px" }} />
                  </IconButton>
                </Tooltip>
              </div>
              <div>
                <button
                  type="button"
                  class="flex items-center justify-center   hover:bg-primary-800  font-medium rounded-lg text-sm px-4 py-2 bg-[#00a8ff]"
                  onClick={openModalAddProduct}
                >
                  <IoAddCircleSharp />
                  Thêm sản phẩm
                </button>
              </div>
            </div>
          </div>

          <Table
            columns={columns}
            dataSource={productList}
            pagination={false}
          />
          <div className="mt-2 flex justify-end">
            <Pagination
              current={filter?.pageIndex}
              total={totalPage}
              pageSize={filter?.pageSize}
              onChange={handlePaginationChange}
            />
          </div>

          <AddProduct
            isModalOpen={isModalOpen}
            closeModalAddProduct={closeModalAddProduct}
            user={user}
            setReload={setReload}
            reload={reload}
          />

          <EditProduct
            isModalOpenEdit={isModalOpenEdit}
            closeModalEditProduct={closeModalEditProduct}
            user={user}
            productDetail={productDetail}
            setReload={setReload}
            reload={reload}
          />
          <ViewDetailProd
            isModalOpenView={isModalOpenView}
            closeModalViewProduct={closeModalViewProduct}
            user={user}
            productDetail={productDetail}
            setReload={setReload}
            reload={reload}
          />

          <center>
            <div
              id="delete-modal"
              tabIndex="-1"
              className={`fixed top-1/2 left-1/2 right-0 z-50 p-4 overflow-x-hidden ${
                isModalOpenDelete ? "" : "hidden"
              } overflow-y-auto md:inset-0 h-[calc(100%-1rem)] max-h-full`}
            >
              <div className="relative w-full h-auto max-w-md max-h-full">
                <div className="relative bg-white rounded-lg shadow light:bg-gray-700">
                  <button
                    type="button"
                    onClick={closeModalDeleteProduct}
                    className="absolute top-3 right-2.5 text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm p-1.5 ml-auto inline-flex items-center light:hover:bg-gray-800 light:hover:text-white"
                  >
                    <svg
                      aria-hidden="true"
                      className="w-5 h-5"
                      fill="currentColor"
                      viewBox="0 0 20 20"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        fillRule="evenodd"
                        d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                        clipRule="evenodd"
                      />
                    </svg>
                    <span className="sr-only">Close modal</span>
                  </button>
                  <div className="p-6 text-center">
                    <svg
                      aria-hidden="true"
                      className="mx-auto mb-4 text-gray-400 w-14 h-14 light:text-gray-200"
                      fill="none"
                      stroke="currentColor"
                      viewBox="0 0 24 24"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        stroke-linecap="round"
                        stroke-linejoin="round"
                        stroke-width="2"
                        d="M12 8v4m0 4h.01M21 12a9 9 0 11-18 0 9 9 0 0118 0z"
                      />
                    </svg>
                    <h3 className="mb-5 text-lg font-normal text-gray-500 light:text-gray-400">
                      Bạn muốn thay đổi trạng thái sản phẩm này?
                    </h3>
                    <button
                      onClick={handleChangeShowHide}
                      type="button"
                      className="text-white bg-red-600 hover:bg-red-800 focus:ring-4 focus:outline-none focus:ring-red-300 light:focus:ring-red-800 font-medium rounded-lg text-sm inline-flex items-center px-5 py-2.5 text-center mr-2"
                    >
                      Xác nhận
                    </button>
                    <button
                      onClick={closeModalDeleteProduct}
                      type="button"
                      className="text-gray-500 bg-white hover:bg-gray-100 focus:ring-4 focus:outline-none focus:ring-gray-200 rounded-lg border border-gray-200 text-sm font-medium px-5 py-2.5 hover:text-gray-900 focus:z-10 light:bg-gray-700 light:text-gray-300 light:border-gray-500 light:hover:text-white light:hover:bg-gray-600 light:focus:ring-gray-600"
                    >
                      Huỷ
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </center>
        </Col>
      </Row>
    </>
  );
};

export default OrderManagement;
