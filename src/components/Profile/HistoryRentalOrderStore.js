import React, { useEffect, useState } from 'react'
import SearchInfor from './SearchInfor'
import HistoryRentalOrder from './HistoryRentalOrder';
import { useSelector } from 'react-redux';
import { getHistoryOrdersLessor } from '../../redux/apiRequestProduct';
import RentalOrderManagement from './RentalOrderManagement';
import { Grid } from 'antd';
import HistoryStoreRentedOrder from './HistoryStoreRentedOrder';

function HistoryRentalOrderStore({ view, setView }) {
  const user = useSelector((state) => state?.auth?.loadUser?.user);
  const [fromDate, setFromDate] = useState();
  const [toDate, setToDate] = useState();
  const [reload, setReload] = useState(false);
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(true);
  useEffect(() => {
    const param = {
      dateFrom: fromDate,
      dateTo: toDate,
    };

    if (fromDate === null || toDate === null) {
      param = {};
    }
    getOrderLessors(param);
  }, [reload]);
  const getOrderLessors = async (infor) => {
    try {
      if (user?.id) {
        setLoading(true);
        const response = await getHistoryOrdersLessor(user?.id, infor);
        
        if (response?.content) {
          setData(response.content);
        }
      }
    } catch (err) {
      console.error(err);
    }finally {
      setLoading(false); // Set loading to false when the request is complete
    }
  };

  return (
    <>


      <SearchInfor
        setReload={setReload}
        reload={reload}
        setFromDate={setFromDate}
        setToDate={setToDate}
      />
      <HistoryStoreRentedOrder orderList={data} setReload={setReload} reload={reload} setView={setView} loading={loading} />
    </>

  )
}

export default HistoryRentalOrderStore