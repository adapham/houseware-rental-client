import React, { useEffect, useState } from "react";

import Box from "@mui/material/Box";

import { getOrderLessee, getOrderLessor } from "../../redux/apiRequestProduct";
import { useSelector } from "react-redux";
import RentalOrderTableStore from "./RentalOrderTableStore";
import SeachInforStore from "./SeachInforStore";


const RentalOrderManagementStore = () => {
 
  const user = useSelector((state) => state?.auth?.loadUser?.user);
 
  const [fromDate, setFromDate] = useState();
  
  const [toDate, setToDate] = useState();
 
  const [reload,setReload] =useState(false);
  const [data,setData]=useState([]);
  const [status,setStatus]=useState([]);
  const sortByStatus = (orders) => {
    const statusOrder = {
      NEW: 1,
      PENDING: 2,
      RENTED: 3,
      CANCEL: 4,
      DONE: 5,
    };
  
    return orders.sort((a, b) => statusOrder[a.status] - statusOrder[b.status]);
  };
  useEffect(() => {
    const param = {
      dateFrom: fromDate,
      dateTo: toDate,
    };
    
    if (fromDate === null || toDate === null) {
      param = {};
    }
    getOrderLessors(param,status);
  }, [reload,status]);
  const getOrderLessors = async (infor,status) => {
    if (user?.id) {
      var response = null;
      response =await getOrderLessor(user?.id, infor,status);
      if (response) {
        if(response.content){
          setData(sortByStatus(response.content));
        }
        
      }
    }
  };


  return (

    <div>
      <Box sx={{ width: "100%", typography: "body1" }}>
      <SeachInforStore setStatus={setStatus} setReload={setReload} reload={reload}  setFromDate={setFromDate} setToDate={setToDate} />
              <RentalOrderTableStore orderList={data} status={status} setReload={setReload} reload={reload} />
      </Box>
    </div>
  );
};

export default RentalOrderManagementStore;
