import React, { useState, useEffect } from "react";
import { Col, Row } from "antd"
import Stack from "@mui/material/Stack";
import Button from "@mui/material/Button";
import General from "./General";
import httpRequest from "../../redux/Req/apiConfig";
import { toast, ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { useDispatch } from "react-redux";

import { TextField, InputAdornment, IconButton } from '@mui/material';

import VisibilityIcon from '@mui/icons-material/Visibility';
import VisibilityOffIcon from '@mui/icons-material/VisibilityOff';
const PersonalInfor = ({ user }) => {
    const [password, setPassword] = useState("");
    const [newPassword, setNewPassword] = useState("");
    const [confirmedPassword, setConfirmPassword] = useState("");
    const [showCPassword, setShowCPassword] = useState(false);
    const [showNPassword, setShowNPassword] = useState(false);
    const [showOPassword, setShowOPassword] = useState(false);
  const handleToggleCPasswordVisibility = () => {
    setShowCPassword((prev) => !prev);
  };
  const handleToggleNPasswordVisibility = () => {
    setShowNPassword((prev) => !prev);
  };
  const handleToggleOPasswordVisibility = () => {
    setShowOPassword((prev) => !prev);
  };
    const [errNewPassword, setErrNewPassword] = useState("");
    const [errPassword, setErrPassword] = useState();
    const [errConfirmPassword, setErrConfirmPassword] = useState();
    const dispatch = useDispatch();
    const [checkPassword, setCheckPassword] = useState(false)

    useEffect(() => {
        setCheckPassword(user?.emailVerified)
    }, [user]);

    const handlePost = (e) => {
        e.preventDefault();
        if(password != "" && newPassword !="" && confirmedPassword !=""){
            const objPassword = {
                "passwordRequest": {
                    oldPassword: password ? password : null,
                    newPassword: newPassword,
                    reNewPassword: confirmedPassword
                }
            }
    
            const url = 'api/users/' + user.id;
    
            httpRequest.putToken(url, objPassword)
                .then(response => {
                    if (response.status && response.status != 200) {
                        const errorMessage = response?.data?.message || "Cập nhật mật khẩu thất bại";
                        toast(errorMessage);
                    } else {
                        toast("Cập nhật mật khẩu thành công");
                    }
                })
                .catch(error => {
                    // Xử lý lỗi trong trường hợp không thể gọi hàm `httpRequest.putToken`
                    toast("Hệ thống đang bảo trì");
                });
        }else{
            toast.warn("Vui lòng điền đầy đủ thông tin", {
                position: "top-right",
                autoClose: 800,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
                theme: "light",
              });
        }
        
    };
    return (
        <div>
            <General user={user} />
            <fieldset className="border-2 rounded-lg shadow shadow-[#dde8da] border-[#dde8da] mt-5 p-4">
                {checkPassword ? (
                    <>
                        <legend className="text-xl">Tạo mật khẩu mới</legend>
                        <Row className="mt-2 mb-4" gutter={16}>

                            <Col className="" span={8}>

                                <TextField
                                    margin="normal"
                                    label="Mật khẩu mới"
                                    variant="outlined"
                                    onChange={(e) => {
                                        setNewPassword(e.target.value);
                                        setErrNewPassword("");
                                    }}
                                    value={newPassword}
                                    type="text"
                                    className="w-full"
                                    error={!!errNewPassword}
                                    helperText={errNewPassword}
                                />
                            </Col>
                            <Col className="" span={8}>
                                <TextField
                                    margin="normal"
                                    label="Xác nhận mật khẩu"
                                    variant="outlined"
                                    onChange={(e) => {
                                        setConfirmPassword(e.target.value);
                                        setErrConfirmPassword("");
                                    }}
                                    value={confirmedPassword}
                                    type="text"
                                    className="w-full"
                                    error={!!errConfirmPassword}
                                    helperText={errConfirmPassword}
                                />
                            </Col>
                        </Row>
                    </>
                ) : (
                    <>
                        <legend className="text-xl">Thay đổi mật khẩu</legend>
                        <Row className="mt-2 mb-4" gutter={16}>
                            <Col className="" span={8}>
                                <TextField
                                    margin="normal"
                                    label="Mật khẩu cũ"
                                    variant="outlined"
                                    onChange={(e) => {
                                        const trimmedValue = e.target.value.trim();
                                        setPassword(trimmedValue);
                                        setErrPassword('');
                                      }}
                                      
                                    value={password}
                                    type={showCPassword ? 'text' : 'password'}
                                    className="w-full"
                                    error={!!errPassword}
                                    helperText={errPassword}
                                    InputProps={{
                                        endAdornment: (
                                            <InputAdornment position="end">
                                                <IconButton onClick={handleToggleCPasswordVisibility} edge="end">
                                                    {showCPassword ? <VisibilityIcon /> : <VisibilityOffIcon />}
                                                </IconButton>
                                            </InputAdornment>
                                        ),
                                    }}
                                />


                            </Col>
                            <Col className="" span={8}>

                                <TextField
                                    margin="normal"
                                    label="Mật khẩu mới"
                                    variant="outlined"
                                    onChange={(e) => {
                                        const trimmedValue = e.target.value.trim();
                                        setNewPassword(trimmedValue);
                                        setErrNewPassword("");
                                      }}
                                    value={newPassword}
                                    type={showNPassword ? 'text' : 'password'}
                                    className="w-full"
                                    error={!!errNewPassword}
                                    helperText={errNewPassword}
                                    InputProps={{
                                        endAdornment: (
                                            <InputAdornment position="end">
                                                <IconButton onClick={handleToggleNPasswordVisibility} edge="end">
                                                    {showNPassword ? <VisibilityIcon /> : <VisibilityOffIcon />}
                                                </IconButton>
                                            </InputAdornment>
                                        ),
                                    }}
                                />
                            </Col>
                            <Col className="" span={8}>
                                <TextField
                                    margin="normal"
                                    label="Xác nhận mật khẩu"
                                    variant="outlined"
                                    onChange={(e) => {
                                        const trimmedValue = e.target.value.trim();
                                        setConfirmPassword(trimmedValue);
                                        setErrConfirmPassword("");
                                      }}
                                      
                                    value={confirmedPassword}
                                    type={showOPassword ? 'text' : 'password'}
                                    className="w-full"
                                    error={!!errConfirmPassword}
                                    helperText={errConfirmPassword}
                                    InputProps={{
                                        endAdornment: (
                                            <InputAdornment position="end">
                                                <IconButton onClick={handleToggleOPasswordVisibility} edge="end">
                                                    {showOPassword ? <VisibilityIcon /> : <VisibilityOffIcon />}
                                                </IconButton>
                                            </InputAdornment>
                                        ),
                                    }}
                                />
                            </Col>
                        </Row>
                    </>
                )}

                <Stack className="flex justify-end m-5 text-gray-900" direction="row" spacing={2}>
                    <Button
                        className="text-gray-900"
                        border={1}
                        variant="contained"
                        onClick={handlePost}
                        style={{
                            width: "200px",
                            backgroundColor: "#dde8da",
                            margin: "10px",
                            color: "black"
                        }}
                    >
                        Cập nhật mật khẩu
                    </Button>
                    <ToastContainer />
                </Stack>
            </fieldset>
        </div>
    );
};
export default PersonalInfor;
