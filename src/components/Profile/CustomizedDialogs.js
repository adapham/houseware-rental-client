import * as React from 'react';
import Button from '@mui/material/Button';
import { styled } from '@mui/material/styles';
import Dialog from '@mui/material/Dialog';
import DialogTitle from '@mui/material/DialogTitle';
import DialogContent from '@mui/material/DialogContent';
import DialogActions from '@mui/material/DialogActions';
import IconButton from '@mui/material/IconButton';
import CloseIcon from '@mui/icons-material/Close';
import Alert from '@mui/material/Alert';
import TextField from "@mui/material/TextField";
import { convertMoney } from '../../utils/formatConfig';
import { getDetailStore, getRechargeAndWithdraw, getTransferMoney } from '../../redux/apiRequestProduct';
import { FormControl, FormControlLabel, FormLabel, Grid, Radio, RadioGroup } from '@mui/material';
import { toast } from 'react-toastify';
import { useSelector } from 'react-redux';
import { GiFalloutShelter } from 'react-icons/gi';
import { Modal } from 'antd';

const BootstrapDialog = styled(Dialog)(({ theme }) => ({
    '& .MuiDialogContent-root': {
        padding: theme.spacing(2),
    },
    '& .MuiDialogActions-root': {
        padding: theme.spacing(1),
    },
}));

const CustomizedDialogs = ({ isModalOpen, closeModal, title, label, balances, id, transfer, storeId, totalIncome, setDataStore, openModal }) => {
    const [selectedShippingOption, setSelectedShippingOption] = React.useState('standard');
    const [recharge, setRecharge] = React.useState(100000);
    const [alert, setAlert] = React.useState(false);
    const [openC, setOpenC] = React.useState(false);
    const user = useSelector((state) => state?.auth?.loadUser?.user);
    const handleMoney = async (type) => {
        const infor = {
            typePayment: type,
            userId: id,
            money: recharge
        }
        const response = await getRechargeAndWithdraw(infor, type);
        if (response) {
            setAlert(false);
            const colonIndex = response.indexOf(':');
            const link = response.substring(colonIndex + 1);
            window.location.href = link
            closeModal();
        } else {
            setAlert(true);
        }
    }
    const handleSendRequest = () => {
        if (!transfer) {
            if (label === "Số tiền nạp") {
                handleMoney("CREDIT");
            } else {
                handleMoney("DEBIT");
            }
        } else {
            transferMoney({ money: recharge })
        }

    }
    const getDetailInforStore = async (userID) => {
        const response = await getDetailStore(userID);
        if (response) {

            setDataStore(response);

        }
    }
    const transferMoney = async (param) => {
        const response = await getTransferMoney(storeId, param);
        if (response) {
            getDetailInforStore(user?.id);
            closeModal();
            toast.success("Chuyển tiền thành công", {
                position: "top-right",
                autoClose: 800,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
                theme: "light",
            });
        }
    }
    const handleChange = (event) => {
        setSelectedShippingOption(event.target.value);
    };
    const hideModalC = (status) => {
        setOpenC(false);

        if (status) {
            handleSendRequest()
        } else {
            openModal()
        }
    };

    const priceDefaults = [100000, 200000, 500000, 1000000, 2000000, 5000000];
    return (
        <React.Fragment>
            <BootstrapDialog
                onClose={closeModal}
                aria-labelledby="customized-dialog-title"
                open={isModalOpen}
            >
                <div className='flex justify-between items-center'>
                    <DialogTitle sx={{ m: 0, p: 2 }} id="customized-dialog-title">
                        {title}
                    </DialogTitle>
                    <IconButton
                        aria-label="close"
                        onClick={closeModal}
                        sx={{
                            position: 'absolute',
                            right: 16,
                            top: 16,
                            color: (theme) => theme.palette.grey[500],
                        }}
                    >
                        <CloseIcon />
                    </IconButton>
                </div>

                <DialogContent dividers>
                    <Alert severity="success">
                        {totalIncome && (<h1 className='text-xl font-medium'>Số dư khả dụng: <span>{convertMoney(totalIncome)}đ</span></h1>)}
                        {!totalIncome && (<h1 className='text-xl font-medium'>Số dư khả dụng: <span>{convertMoney(balances)}</span></h1>)}

                    </Alert>
                    {alert && (<p>Số dư không khả dụng</p>)}
                    <div className="flex flex-row flex-wrap grid grid-cols-3 mt-10">
                        {priceDefaults.map((price, index) => (
                            <p
                                key={index}
                                className={`hover:bg-[#f1f6f0] hover:border-[#adc5a0] text-center text-base border-2 border-gray-300 rounded-lg p-2 cursor-pointer m-1 ${recharge === price ? 'bg-green-500' : ''}`}
                                onClick={() => setRecharge(price)}
                            >
                                {convertMoney(price)}
                            </p>
                        ))}
                    </div>
                    <TextField
                        margin="normal"
                        //label={label}
                        variant="outlined"
                        value={recharge}
                        type="number"
                        className="w-full mt-10"
                        onChange={(e) => {
                            let inputValue = parseInt(e.target.value, 10);
                            
                            if (isNaN(inputValue)) {
                              // Handle non-numeric input, you may choose to ignore or handle it differently
                              inputValue = 0; // Set to a default value if needed
                            }
                        
                            // Apply minimum and maximum limits
                            inputValue = Math.max(10000, Math.min(inputValue, 100000000));
                        
                            setRecharge(inputValue);
                          }}
                        InputProps={{
                            endAdornment: <span>VND</span>,
                        }}
                    />

                    <FormControl component="fieldset">
                        {!transfer && (
                            <>
                                <FormLabel component="legend">Phương thức thanh toán</FormLabel>
                                <RadioGroup
                                    aria-label="shipping"
                                    name="shipping"
                                    value={selectedShippingOption}
                                    onChange={handleChange}
                                >
                                    <FormControlLabel
                                        value="standard"
                                        control={<Radio />}
                                        label="Thanh toán qua ví VNPay"
                                    />
                                    {/* <FormControlLabel
                                        value="express"
                                        control={<Radio />}
                                        label="Quét mã QR Momo"
                                    /> */}
                                </RadioGroup>
                            </>
                        )}

                    </FormControl>
                </DialogContent>


                <DialogActions>
                    {transfer ? (
                        <Button autoFocus onClick={() => { setOpenC(true); closeModal(); }}>
                            Chuyển tiền
                        </Button>
                    ) : (
                        <Button autoFocus onClick={handleSendRequest}>
                            Gửi yêu cầu
                        </Button>
                    )}

                </DialogActions>

            </BootstrapDialog>
            <Modal
                title="Xác nhận chuyển tiền"
                open={openC}
                footer={null}

                cancelText="Quay lại"
                onCancel={() => hideModalC(false)}
            >

                <div className="flex justify-end mt-5">

                    <Button
                        className="mt-2  bg-[#1975D2] text-white "
                        onClick={() => hideModalC(true)}
                    >
                        Có
                    </Button>
                    <Button className="mt-2  bg-[#1975D2] text-white " onClick={() => hideModalC(false)}>
                        Không
                    </Button>
                </div>
            </Modal>
        </React.Fragment>
    );
}

export default CustomizedDialogs;
