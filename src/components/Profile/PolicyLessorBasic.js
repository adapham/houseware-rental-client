import * as React from 'react';
import {styled} from '@mui/material/styles';
import ArrowForwardIosSharpIcon from '@mui/icons-material/ArrowForwardIosSharp';
import MuiAccordion from '@mui/material/Accordion';
import MuiAccordionSummary from '@mui/material/AccordionSummary';
import MuiAccordionDetails from '@mui/material/AccordionDetails';
import Typography from '@mui/material/Typography';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemText from '@mui/material/ListItemText';

const Accordion = styled((props) => (
    <MuiAccordion disableGutters elevation={0} square {...props} />
))(({theme}) => ({
    border: `1px solid ${theme.palette.divider}`,
    '&:not(:last-child)': {
        borderBottom: 0,
    },
    '&:before': {
        display: 'none',
    },
}));

const AccordionSummary = styled((props) => (
    <MuiAccordionSummary
        expandIcon={<ArrowForwardIosSharpIcon sx={{fontSize: '0.9rem'}}/>}
        {...props}
    />
))(({theme}) => ({
    backgroundColor:
        theme.palette.mode === 'dark'
            ? 'rgba(255, 255, 255, .05)'
            : 'rgba(0, 0, 0, .03)',
    flexDirection: 'row-reverse',
    '& .MuiAccordionSummary-expandIconWrapper.Mui-expanded': {
        transform: 'rotate(90deg)',
    },
    '& .MuiAccordionSummary-content': {
        marginLeft: theme.spacing(1),
    },
}));

const AccordionDetails = styled(MuiAccordionDetails)(({theme}) => ({
    padding: theme.spacing(2),
    borderTop: '1px solid rgba(0, 0, 0, .125)',
}));

export default function PolicyLessorBasic() {
    const [expanded, setExpanded] = React.useState('panel1');

    const handleChange = (panel) => (event, newExpanded) => {
        setExpanded(newExpanded ? panel : false);
    };

    return (
        <div>
            <Accordion expanded={expanded === 'panel1'} onChange={handleChange('panel1')}>
                <AccordionSummary aria-controls="panel1d-content" id="panel1d-header">
                    <Typography>1. Quy định về đăng cho thuê sản phẩm trên ứng dụng Housewares Rental</Typography>
                </AccordionSummary>
                <AccordionDetails>
                    <Typography>
                        <List sx={{
                            width: '100%',
                            bgcolor: 'background.paper',
                            position: 'relative',
                            overflow: 'auto',
                            maxHeight: 300,
                            '& ul': {padding: 0},
                        }}
                              subheader={<li/>}
                        >
                            <li>
                                <ul>
                                    <ListItem>
                                        <ListItemText
                                            primary="1. Chủ tiệm có trách nhiệm cung cấp thông tin chính xác để mở cửa hàng trên app, bao gồm: thông tin sản phẩm, quy định cho thuê, địa chỉ giao nhận hàng,... Trong trường hợp có thay đổi về các thông tin trên sau khi cửa hàng đã tạo trên app, Chủ tiệm có trách nhiệm cập nhật trên ứng dụng."/>
                                    </ListItem>
                                    <ListItem>
                                        <ListItemText
                                            primary="2. Chủ tiệm được quyền đăng các sản phẩm lên Housewares Rental nhằm mục đích kinh doanh. Tuy nhiên, NGHIÊM CẤM Chủ tiệm đăng tải những sản phẩm được liệt kê trong mục #1 ở phần Chính sách & Quy định Chi tiết."/>
                                    </ListItem>
                                    <ListItem>
                                        <ListItemText
                                            primary="3. Chủ tiệm có trách nhiệm phản hồi tin nhắn, đơn hàng của khách trong vòng 24h hoặc trước khi kỳ thuê bắt đầu, tùy điều kiện nào đến trước. Chủ tiệm có quyền từ chối cho khách thuê bằng cách hủy đơn hàng ở bước “Chờ Chủ tiệm xác nhận”. Mọi đơn hàng hủy sau khi đơn hàng đã được xác nhận sẽ được ghi nhận vào tỷ lệ hủy đơn hàng tháng."/>
                                    </ListItem>
                                    <ListItem>
                                        <ListItemText
                                            primary="4. Mọi trao đổi với khách hàng phải được diễn ra trên app Housewares Rental. Nếu có trao đổi diễn ra bên ngoài app thì khi có sự cố xảy ra, Housewares Rental không cam kết sẽ hỗ trợ xử lý giải quyết. Quyết định của Housewares Rental là quyết định cuối cùng sau khi cân nhắc cẩn thận quyền lợi của Chủ tiệm và Khách thuê."/>
                                    </ListItem>
                                    <ListItem>
                                        <ListItemText>
                                            <p>5. Với mỗi đơn hàng hoàn tất qua app, Housewares Rental sẽ thu phí nền tảng là 9%
                                                dựa trên giá trị đơn hàng:</p>
                                            <p> - Cho tất cả đơn hàng đối với Chủ tiệm cá nhân</p>
                                            <p> - Chỉ cho những đơn hàng đến từ Housewares Rental đối với Chủ tiệm chuyên nghiệp.
                                                Quy trình tất toán sẽ diễn ra từ ngày 10 - ngày 20 hàng tháng.</p>
                                        </ListItemText>
                                    </ListItem>
                                </ul>
                            </li>
                        </List>
                    </Typography>
                </AccordionDetails>
            </Accordion>
        </div>
    );
}
