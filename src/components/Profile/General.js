import React, { useEffect, useMemo, useState } from "react";
import { Col, Row } from "antd";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import { DatePicker } from "@mui/x-date-pickers/DatePicker";
import dayjs from "dayjs";
import TextField from "@mui/material/TextField";
import { Button } from "@material-ui/core";
import {
  getDistrictByProvince,
  getProvince,
  getUser,
  getWardByDistrict,
} from "../../redux/apiRequestProduct";
import { Stack } from "@mui/material";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import Select from "@mui/material/Select";
import FormControl from "@mui/material/FormControl";
import { regexEmail, regexPhoneNumber } from "../../constants/constantsParam";
import { useDispatch } from "react-redux";
import httpRequest from "../../redux/Req/apiConfig";
import { toast, ToastContainer } from "react-toastify";
import { loadUserSuccess } from "../../redux/Slide/authSlide";

const General = ({ user, check, setLoadChat, loadChat }) => {
  const dispatch = useDispatch();
  const today = new Date();
  const [ward, setWard] = useState();
  const [district, setDistrict] = useState();
  const [province, setProvince] = useState();
  const [clientName, setClientName] = useState("");
  const [phoneNumber, setPhoneNumber] = useState("");
  const [gender, setGender] = useState("MALE");
  const [email, setEmail] = useState("");
  const [birthDate, setBirthDate] = useState("");
  const [wardSelect, setWardSelect] = useState("");
  const [districtSelect, setDistrictSelect] = useState("");
  const [provinceSelect, setProvinceSelect] = useState("");
  const [addressDetail, setAddressDetail] = useState("");
  const [errAddressDetail, setErrAddressDetail] = useState("");
  const [errClientName, setErrClientName] = useState("");
  const [errPhoneNumber, setErrPhoneNumber] = useState("");
  const [errEmail, setErrEmail] = useState("");
  const [err, setErr] = useState("");
  const [userD, setUserD] = useState([]);
  const [reload, setReload] = useState(false);

  useEffect(() => {
    if (user?.id) {
      getUpdateUser(user?.id);
    }
  }, [reload]);

  const getProvinces = async () => {
    const response = await getProvince();
    if (response) setProvince(response);
  };

  const getDistrict = async (provinceId) => {
    const response = await getDistrictByProvince(provinceId);
    if (response) {
      setDistrict(response);
    }
  };
  const getWard = async (districtId) => {
    const response = await getWardByDistrict(districtId);
    if (response) {
      setWard(response);
    }
  };

  const getUpdateUser = async (userId) => {
    const response = await getUser(userId);
    if (response) {
      setUserD(response);
      setClientName(response.name);
      setEmail(response.email);
      setPhoneNumber(response.phone);
      setGender(response.gender);
      setAddressDetail(response.address);
      setBirthDate(response.dob ? dayjs(response.dob) : null);
      setProvinceSelect(
        province?.filter((item) => item.name == response?.province)[0]?.id
      );
      setDistrictSelect(
        district?.filter((item) => item.name == response?.district)[0]?.id
      );
      setWardSelect(ward?.filter((item) => item.name == response?.ward)[0]?.id);
    }
  };

  const handlePost = (e) => {
    e.preventDefault();

    const objUser = {
      infoRequest: {
        name: clientName,
        phone: phoneNumber,
        ward: ward?.filter((item) => item.id == wardSelect)?.[0]?.name,
        district: district?.filter((item) => item.id == districtSelect)?.[0]
          ?.name,
        province: province?.filter((item) => item.id == provinceSelect)?.[0]
          ?.name,
        address: addressDetail,
        dob: birthDate ? dayjs(birthDate).valueOf() : null,
        gender: gender,
        facebook: user?.facebook,
        zalo: user?.zalo,
        imageUrl: user.imageUrl,
        email: email,
      },
    };

    const url = "api/users/" + user?.id;
    httpRequest
      .putToken(url, objUser)
      .then((response) => {
        if (response.status && response.status != 200) {
          const errorMessage =
            response?.data?.message || "Cập nhật thông tin thất bại";
          toast(errorMessage);
          setErr(errorMessage);
        } else {
          dispatch(loadUserSuccess(objUser.infoRequest));
          toast.success("Cập nhật thông tin thành công", {
            position: "top-right",
            autoClose: 2000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
            theme: "light",
          });
          setReload(!reload);
        }
      })
      .catch((error) => {
        toast("Hệ thống đang bảo trì");
      });
  };

  useEffect(() => {
    getProvinces();
  }, []);
  useEffect(() => {
    if (userD?.province) {
      var idProvince = province?.filter(
        (item) => item.name == userD?.province
      )[0]?.id;
      setProvinceSelect(idProvince);
    }
  }, [province,userD]);
  useEffect(() => {
    if (provinceSelect) {
      getDistrict(provinceSelect);
    }
  }, [provinceSelect]);
  useEffect(() => {
    var idDistrict = district?.filter((item) => item.name == userD?.district)[0]
      ?.id;
    if (userD?.district) {
      setDistrictSelect(idDistrict);
    }
  }, [district]);
  useEffect(() => {
    var idWar = ward?.filter((item) => item.name == userD?.ward)[0]?.id;
    if (userD?.ward) {
      setWardSelect(idWar);
    }
  }, [ward]);
  useEffect(() => {
    if (districtSelect) {
      getWard(districtSelect);
    }
  }, [districtSelect]);

  const handleBlurPhone = () => {
    const regexMatch = regexPhoneNumber.test(phoneNumber);
    if (!regexMatch) {
      setErrPhoneNumber("Vui lòng nhập số điện thoại hợp lệ");
    } else {
      setErrPhoneNumber("");
    }
  };

  const handleBlurEmail = () => {
    const regexMatch = regexEmail.test(email);
    if (!regexMatch) {
      setErrEmail("Vui lòng nhập email hợp lệ");
    } else {
      setErrEmail("");
    }
  };

  const ITEM_HEIGHT = 48;
  const ITEM_PADDING_TOP = 1;
  const MenuProps = {
    PaperProps: {
      style: {
        maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
        width: 250,
      },
    },
  };

  return (
    <>
      <fieldset className="border-2 rounded-lg shadow shadow-[#dde8da] border-[#dde8da] mt-5 p-4">
        {!check && <legend className="text-xl">Thay đổi thông tin</legend>}
        <Row className="mt-2 flex justify-between" gutter={16}>
          <Col className="" span={12}>
            <TextField
              margin="normal"
              label="Họ và tên"
              variant="outlined"
              onChange={(e) => {
                setClientName(e.target.value);
                setErrClientName("");
              }}
              value={clientName}
              type="text"
              className="w-full"
              InputProps={{
                readOnly: check,
              }}
              error={!!errClientName}
              helperText={errClientName}
            />
          </Col>
          <Col className="" span={12}>
            <FormControl sx={{ mt: 2 }} className="w-full">
              <InputLabel id="demo-select-small-label-gender">
                Giới Tính
              </InputLabel>
              <Select
                labelId="demo-select-small-label-gender"
                id="demo-select-small"
                value={gender}
                onChange={(e) => setGender(e.target.value)}
                label="Giới Tính"
                inputProps={{ readOnly: check }}
              >
                <MenuItem value={"MALE"}>Nam</MenuItem>
                <MenuItem value={"FEMALE"}>Nữ</MenuItem>
                <MenuItem value={"OTHER"}>Khác</MenuItem>
              </Select>
            </FormControl>
          </Col>
        </Row>
        <Row className="mt-4 mb-4 flex justify-between" gutter={16}>
          <Col className="" span={12}>
            <TextField
              margin="normal"
              label="Số điện thoại"
              variant="outlined"
              onChange={(e) => {
                setPhoneNumber(e.target.value);
                setErrPhoneNumber("");
              }}
              onBlur={handleBlurPhone}
              value={phoneNumber}
              type="text"
              className="w-full"
              InputProps={{
                readOnly: check,
              }}
              error={!!errPhoneNumber}
              helperText={errPhoneNumber}
            />
          </Col>
          <Col className="" span={12}>
            <TextField
              margin="normal"
              label="Email"
              variant="outlined"
              onChange={(e) => {
                setEmail(e.target.value);
                setErrEmail("");
              }}
              onBlur={handleBlurEmail}
              value={email}
              type="text"
              className="w-full"
              InputProps={{
                readOnly: check,
              }}
              error={!!errEmail}
              helperText={errEmail}
            />
          </Col>
        </Row>
        <Row className=" mb-4 mt-4 flex justify-center" gutter={16}>
          <Col className="mt-4" span={12}>
            <LocalizationProvider dateAdapter={AdapterDayjs}>
              {birthDate ? (
                <DatePicker
                  label="Ngày sinh"
                  className="w-full"
                  readOnly={check ? true : false}
                  value={dayjs(birthDate)}
                  renderInput={(params) => (
                    <TextField
                      {...params}
                      variant="outlined"
                      className="w-full"
                      placeholder="Chọn ngày sinh"
                    />
                  )}
                  onChange={(e) => setBirthDate(e)}
                  shouldDisableDate={(date) => date > today}
                />
              ) : (
                <DatePicker
                  label="Ngày sinh"
                  className="w-full"
                  readOnly={check ? true : false}
                  // value={dayjs(birthDate)}
                  renderInput={(params) => (
                    <TextField
                      {...params}
                      variant="outlined"
                      className="w-full"
                      placeholder="Chọn ngày sinh"
                    />
                  )}
                  onChange={(e) => setBirthDate(e)}
                  shouldDisableDate={(date) => date > today}
                />
              )}
            </LocalizationProvider>
          </Col>
          <Col className="w-full" span={12}>
            {useMemo(() => {
              return (
                <>
                  <FormControl sx={{ mt: 2 }} className="w-full">
                    <InputLabel id="demo-select-small-label-province">
                      Tỉnh/Thành phố
                    </InputLabel>
                    <Select
                      labelId="demo-select-small-label-province"
                      id="demo-select-small"
                      value={provinceSelect}
                      label="Tỉnh/Thành phố"
                      onChange={(e) => setProvinceSelect(e.target.value)}
                      MenuProps={MenuProps}
                    >
                      {Array.isArray(province) &&
                        province.map((item) => (
                          <MenuItem key={item.id} value={item?.id}>
                            {item.name}
                          </MenuItem>
                        ))}
                    </Select>
                  </FormControl>
                </>
              );
            }, [province, provinceSelect])}
          </Col>
        </Row>
        <Row className="mt-4 mb-4 flex justify-between" gutter={16}>
          <Col className="" span={12}>
            {useMemo(() => {
              return (
                <>
                  <FormControl sx={{ mt: 2 }} className="w-full">
                    <InputLabel id="demo-select-small-label-district">
                      Quận/Huyện
                    </InputLabel>
                    <Select
                      labelId="demo-select-small-label-district"
                      id="demo-select-small"
                      value={districtSelect}
                      label="Quận/Huyện"
                      onChange={(e) => setDistrictSelect(e.target.value)}
                      MenuProps={MenuProps}
                    >
                      {Array.isArray(district) &&
                        district.map((item) => (
                          <MenuItem key={item.id} value={item?.id}>
                            {item.name}
                          </MenuItem>
                        ))}
                    </Select>
                  </FormControl>
                </>
              );
            }, [district, districtSelect])}
          </Col>
          <Col className="" span={12}>
            {useMemo(() => {
              return (
                <>
                  <FormControl sx={{ mt: 2 }} className="w-full">
                    <InputLabel id="demo-select-small-label-ward">
                      Xã/Phường
                    </InputLabel>
                    <Select
                      labelId="demo-select-small-label-ward"
                      id="demo-select-small"
                      value={wardSelect}
                      label="Xã/Phường"
                      onChange={(e) => setWardSelect(e.target.value)}
                      MenuProps={MenuProps}
                    >
                      {/* <MenuItem value={-1}>
                                <em>Chọn Xã/Phường</em>
                              </MenuItem> */}
                      {Array.isArray(ward) &&
                        ward.map((item) => (
                          <MenuItem key={item.id} value={item?.id}>
                            {item.name}
                          </MenuItem>
                        ))}
                    </Select>
                  </FormControl>
                </>
              );
            }, [ward, wardSelect])}
          </Col>
        </Row>
        <Row className="mt-4 mb-4 flex justify-between">
          <Col className="" span={12}>
            <TextField
              margin="normal"
              label="Địa chỉ"
              variant="outlined"
              onChange={(e) => {
                setAddressDetail(e.target.value);
                setErrAddressDetail("");
              }}
              value={addressDetail}
              type="text"
              className="w-full"
              InputProps={{
                readOnly: check,
              }}
              error={!!errAddressDetail}
              helperText={errAddressDetail}
            />
          </Col>
        </Row>

        {!check && (
          <Stack className="flex justify-end m-5" direction="row" spacing={2}>
            <Button
              onClick={handlePost}
              type="submit"
              variant="contained"
              style={{
                width: "200px",
                backgroundColor: "#dde8da",
                margin: "10px",
              }}
            >
              Lưu
            </Button>
            <ToastContainer></ToastContainer>
          </Stack>
        )}
      </fieldset>
    </>
  );
};
export default General;
