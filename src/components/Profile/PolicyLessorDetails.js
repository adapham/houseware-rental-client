import * as React from 'react';
import {styled} from '@mui/material/styles';
import ArrowForwardIosSharpIcon from '@mui/icons-material/ArrowForwardIosSharp';
import MuiAccordion from '@mui/material/Accordion';
import MuiAccordionSummary from '@mui/material/AccordionSummary';
import MuiAccordionDetails from '@mui/material/AccordionDetails';
import Typography from '@mui/material/Typography';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemText from '@mui/material/ListItemText';
import ListSubheader from '@mui/material/ListSubheader';

const Accordion = styled((props) => (
    <MuiAccordion disableGutters elevation={0} square {...props} />
))(({theme}) => ({
    border: `1px solid ${theme.palette.divider}`,
    '&:not(:last-child)': {
        borderBottom: 0,
    },
    '&:before': {
        display: 'none',
    },
}));

const AccordionSummary = styled((props) => (
    <MuiAccordionSummary
        expandIcon={<ArrowForwardIosSharpIcon sx={{fontSize: '0.9rem'}}/>}
        {...props}
    />
))(({theme}) => ({
    backgroundColor:
        theme.palette.mode === 'dark'
            ? 'rgba(255, 255, 255, .05)'
            : 'rgba(0, 0, 0, .03)',
    flexDirection: 'row-reverse',
    '& .MuiAccordionSummary-expandIconWrapper.Mui-expanded': {
        transform: 'rotate(90deg)',
    },
    '& .MuiAccordionSummary-content': {
        marginLeft: theme.spacing(1),
    },
}));

const AccordionDetails = styled(MuiAccordionDetails)(({theme}) => ({
    padding: theme.spacing(2),
    borderTop: '1px solid rgba(0, 0, 0, .125)',
}));

export default function PolicyLessorDetails() {
    const [expanded, setExpanded] = React.useState();

    const handleChange = (panel) => (event, newExpanded) => {
        setExpanded(newExpanded ? panel : false);
    };

    return (
        <div>
            <Accordion expanded={expanded === 'panel1'} onChange={handleChange('panel1')}>
                <AccordionSummary aria-controls="panel1d-content" id="panel1d-header">
                    <Typography>1. Quy định về đăng cho thuê sản phẩm trên ứng dụng Housewares Rental</Typography>
                </AccordionSummary>
                <AccordionDetails>
                    <Typography>
                        <List sx={{
                            width: '100%',
                            bgcolor: 'background.paper',
                            position: 'relative',
                            overflow: 'auto',
                            maxHeight: 300,
                            '& ul': {padding: 0},
                        }}
                              subheader={<li/>}
                        >
                            <li>
                                <ul>
                                    <ListSubheader>{`Chủ tiệm được quyền đăng các sản phẩm lên Housewares Rental nhằm mục đích kinh doanh. Tuy nhiên, NGHIÊM CẤM Chủ tiệm đăng tải những sản phẩm được liệt kê có nội dung sau đây`}</ListSubheader>
                                    <ListItem>
                                        <ListItemText
                                            primary="a. Phản động, chống phá, bài xích tôn giáo, khiêu dâm, bạo lực, đi ngược lại thuần phong mỹ tục, truyền thống và văn hóa Việt Nam;"/>
                                    </ListItem>
                                    <ListItem>
                                        <ListItemText
                                            primary="b. Đăng thông tin rác, phá rối hay làm mất uy tín của các dịch vụ do Housewares Rental cung cấp;"/>
                                    </ListItem>
                                    <ListItem>
                                        <ListItemText
                                            primary="c. Xúc phạm, khích bác đến người khác dưới bất kỳ hình thức nào;"/>
                                    </ListItem>
                                    <ListItem>
                                        <ListItemText
                                            primary="d. Tuyên truyền về những thông tin mà pháp luật nghiêm cấm như: sử dụng heroin, thuốc lắc, giết người, cướp của,vv (VD: sản phẩm in hình lá cần sa, shisha);"/>
                                    </ListItem>
                                    <ListItem>
                                        <ListItemText
                                            primary="e. Khuyến khích, quảng cáo cho việc sử dụng các sản phẩm độc hại (VD: thuốc lá, rượu, cần sa);"/>
                                    </ListItem>
                                    <ListItem>
                                        <ListItemText
                                            primary="f. Các sản phẩm văn hóa đồi trụy (băng đĩa, sách báo, vật phẩm);"/>
                                    </ListItem>
                                    <ListItem>
                                        <ListItemText
                                            primary="g. Tài liệu bí mật quốc gia, bí mật nhà nước, bí mật kinh doanh, bí mật cá nhân;"/>
                                    </ListItem>
                                    <ListItem>
                                        <ListItemText primary="h. Con người và/hoặc các bộ phận của cơ thể con người;"/>
                                    </ListItem>
                                    <ListItem>
                                        <ListItemText
                                            primary="i. Những sản phẩm có tính chất phân biệt chủng tộc, xúc phạm đến dân tộc hoặc quốc gia nào đó;"/>
                                    </ListItem>
                                    <ListItem>
                                        <ListItemText
                                            primary="j. Hạn chế tối đa những sản phẩm mang tính cá nhân (như hình cá nhân, hình ảnh của gia đình, hình ảnh của con cái);"/>
                                    </ListItem>
                                    <ListItem>
                                        <ListItemText
                                            primary="k. Vi phạm quyền sở hữu trí tuệ và/hoặc bất kỳ nhãn hiệu hàng hóa nào của bất kỳ bên thứ ba nào;"/>
                                    </ListItem>
                                    <ListItem>
                                        <ListItemText
                                            primary="m. Các sản phẩm nằm trong Danh sách sản phẩm bị cấm/hạn chế của Housewares Rental."/>
                                    </ListItem>
                                </ul>
                            </li>
                        </List>
                    </Typography>
                </AccordionDetails>
            </Accordion>
            <Accordion expanded={expanded === 'panel2'} onChange={handleChange('panel2')}>
                <AccordionSummary aria-controls="panel2d-content" id="panel2d-header">
                    <Typography>2. Quy Trình Tất Toán Thưởng và Phí Nền Tảng</Typography>
                </AccordionSummary>
                <AccordionDetails>
                    <Typography>
                        <List sx={{
                            width: '100%',
                            bgcolor: 'background.paper',
                            position: 'relative',
                            overflow: 'auto',
                            maxHeight: 300,
                            '& ul': {padding: 0},
                        }}
                              subheader={<li/>}
                        >
                            <li>
                                <ul>
                                    <ListSubheader>{`Với mỗi đơn hàng hoàn tất qua app, Housewares Rental sẽ thu phí nền tảng là 9% dựa trên giá trị đơn hàng:`}</ListSubheader>
                                    <ListItem>
                                        <ListItemText
                                            primary="a. Cho tất cả đơn hàng đối với Chủ tiệm cá nhân"/>
                                    </ListItem>
                                    <ListItem>
                                        <ListItemText
                                            primary="b. Chỉ cho những đơn hàng đến từ Housewares Rental đối với Chủ tiệm chuyên nghiệp."/>
                                    </ListItem>
                                    <ListItem>
                                        <ListItemText
                                            primary="c. Đối với mỗi đơn hàng có áp dụng khuyến mãi từ Housewares Rental, Housewares Rental sẽ tất toán phần khuyến mãi cho Chủ tiệm sau khi đơn hàng hoàn tất."/>
                                    </ListItem>
                                </ul>
                                <p className='font-semibold'>Ví dụ</p>
                                <ul>
                                    <ListSubheader><span>Đơn 1: khách thanh toán qua app, phí thuê 100K, có áp khuyến mãi 25K từ Housewares Rental.</span></ListSubheader>
                                    <ListItem>
                                        <ListItemText
                                            primary="a. Giá trị đơn thuê: 100,000đ"/>
                                    </ListItem>
                                    <ListItem>
                                        <ListItemText
                                            primary="b. Khách trả: 75,000đ qua app (100K – 25K khuyến mãi)"/>
                                    </ListItem>
                                    <ListItem>
                                        <ListItemText
                                            primary="c. Housewares Rental thu: 75,000đ"/>
                                    </ListItem>
                                    <ListItem>
                                        <ListItemText
                                            primary="d. Khi đơn hàng hoàn tất, Housewares Rental sẽ trả Chủ tiệm = 91,000đ (100K – 9K phí nền tảng)"/>
                                    </ListItem>
                                </ul>
                                <ul>
                                    <ListSubheader>{`Đơn 2: khách thanh toán trực tiếp cho Chủ tiệm, phí thuê 100K, có áp khuyến mãi 25K từ Housewares Rental`}</ListSubheader>
                                    <ListItem>
                                        <ListItemText
                                            primary="a. Giá trị đơn thuê: 100,000đ"/>
                                    </ListItem>
                                    <ListItem>
                                        <ListItemText
                                            primary="b. Chủ tiệm thu từ khách: 75,000đ (100K – 25K khuyến mãi)"/>
                                    </ListItem>
                                    <ListItem>
                                        <ListItemText
                                            primary="c. Khi đơn hàng hoàn tất, Housewares Rental sẽ trả Chủ tiệm = 16,000đ (25K khuyến mãi – 9K phí nền tảng)"/>
                                    </ListItem>
                                </ul>
                            </li>
                        </List>
                    </Typography>
                </AccordionDetails>
            </Accordion>
            <Accordion expanded={expanded === 'panel3'} onChange={handleChange('panel3')}>
                <AccordionSummary aria-controls="panel3d-content" id="panel3d-header">
                    <Typography>3. Quy Trình Giải Quyết Tranh Chấp, Khiếu Nại</Typography>
                </AccordionSummary>
                <AccordionDetails>
                    <Typography>
                        <List sx={{
                            width: '100%',
                            bgcolor: 'background.paper',
                            position: 'relative',
                            overflow: 'auto',
                            maxHeight: 300,
                            '& ul': {padding: 0},
                        }}
                              subheader={<li/>}
                        >
                            <li>
                                <ul>
                                    {/* <ListSubheader>{`Chủ tiệm được quyền đăng các sản phẩm lên Housewares Rental nhằm mục đích kinh doanh. Tuy nhiên, NGHIÊM CẤM Chủ tiệm đăng tải những sản phẩm được liệt kê có nội dung sau đây`}</ListSubheader> */}
                                    <p className='font-medium'>Khi phát sinh tranh chấp hoặc khiếu nại, Housewares Rental khuyến
                                        khích giải pháp thương lượng, hòa giải giữa các bên để đạt được sự đồng thuận về
                                        phương án giải quyết. Nếu hai bên không thể thương lượng với nhau và yêu cầu
                                        Housewares Rental đứng ra giải quyết vụ việc. Quyết định của Housewares Rental là quyết định cuối
                                        cùng. Tranh chấp hoặc khiếu nại sẽ được Housewares Rental xử lý theo trình tự sau:</p>
                                    <ListItem>
                                        <ListItemText
                                            primary="Bước 1: Để tạo khiếu nại tới Housewares Rental, khách thuê hoặc Chủ tiệm cần nhấn nút “Báo cáo đơn hàng” ngay trong đơn hàng chi tiết trong ứng dụng. Hệ thống sẽ ghi nhận khiếu nại này của Khách thuê hoặc Chủ tiệm."/>
                                    </ListItem>
                                    <ListItem>
                                        <ListItemText
                                            primary="Bước 2: Bộ phận giải quyết khiếu nại của Housewares Rental sẽ tiếp nhận các yêu cầu của người khiếu nại/các bên tranh chấp. Housewares Rental sẽ có biện pháp cụ thể hỗ trợ người khiếu nại/các bên tranh chấp để giải quyết tùy theo tính chất và mức độ của vụ việc."/>
                                    </ListItem>
                                    <ListItem>
                                        <ListItemText
                                            primary="Bước 3: Trong trường hợp nằm ngoài khả năng và thẩm quyền của Sàn Giao Dịch Thương Mại Điện Tử, Housewares Rental sẽ yêu cầu các bên tranh chấp đưa vụ việc ra giải quyết tại cơ quan nhà nước có thẩm quyền theo quy định của pháp luật. Housewares Rental tôn trọng và nghiêm túc thực hiện các quy định của pháp luật về bảo vệ quyền lợi của khách hàng (người tiêu dùng). Các Chủ tiệm trên sàn Giao Dịch Thương Mại Điện Tử Housewares Rental cần cung cấp đầy đủ, chính xác, trung thực và chi tiết các thông tin/tài liệu liên quan đến sản phẩm. Mọi hành vi lừa đảo, gian lận trong kinh doanh đều bị lên án và phải chịu hoàn toàn trách nhiệm trước pháp luật."/>
                                    </ListItem>
                                </ul>
                            </li>
                        </List>
                    </Typography>
                </AccordionDetails>
            </Accordion>
            <Accordion expanded={expanded === 'panel4'} onChange={handleChange('panel4')}>
                <AccordionSummary aria-controls="panel4d-content" id="panel4d-header">
                    <Typography>4. Tiêu Chí Của Cửa Hàng</Typography>
                </AccordionSummary>
                <AccordionDetails>
                    <Typography>
                        <List sx={{
                            width: '100%',
                            bgcolor: 'background.paper',
                            position: 'relative',
                            overflow: 'auto',
                            maxHeight: 300,
                            '& ul': {padding: 0},
                        }}
                              subheader={<li/>}
                        >
                            <li>
                                <ul>
                                    {/* <ListSubheader>{`Chủ tiệm được quyền đăng các sản phẩm lên Housewares Rental nhằm mục đích kinh doanh. Tuy nhiên, NGHIÊM CẤM Chủ tiệm đăng tải những sản phẩm được liệt kê có nội dung sau đây`}</ListSubheader> */}
                                    <p className='font-medium'>Chủ tiệm cần duy trì hoạt động tốt cho tài khoản cửa hàng
                                        của mình nhằm đem đến trải nghiệm tốt nhất cho khách hàng khi thuê trên
                                        website.</p>
                                    <p className='font-medium'>Do đó, các cửa hàng không đáp ứng được tiêu chuẩn theo
                                        quy định và chính sách của Housewares Rental có thể bị phạt vì ảnh hưởng đến trải nghiệm
                                        khách hàng.</p>
                                    <p className='font-medium'>Các thông số thuộc tiêu chuẩn chất lượng bao gồm nhưng
                                        không giới hạn:</p>
                                    <ListItem>
                                        <ListItemText
                                            primary="a. Chỉ số tín nhiệm (đánh giá của khách hàng)"/>
                                    </ListItem>
                                    <ListItem>
                                        <ListItemText
                                            primary="b. Chỉ số phản hồi tin nhắn"/>
                                    </ListItem>
                                    <ListItem>
                                        <ListItemText
                                            primary="c. Chỉ số xác nhận đơn hàng"/>
                                    </ListItem>
                                    <span>Tùy vào mức độ vi phạm của từng chỉ số, Housewares Rental sẽ có hình thức xử lý tương ứng từ cảnh cáo đến các chế tài.</span>
                                </ul>
                            </li>
                        </List>
                    </Typography>
                </AccordionDetails>
            </Accordion>
        </div>
    );
}
