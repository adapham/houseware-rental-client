import * as React from "react";
import Paper from "@mui/material/Paper";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TablePagination from "@mui/material/TablePagination";
import TableRow from "@mui/material/TableRow";
import { useState } from "react";
import { convertToDate } from "../../utils/formatConfig";
import { FcApproval } from "react-icons/fc";
import { FcCancel } from "react-icons/fc";
import { Tooltip } from "antd";
import { IconButton } from "@mui/material";
import { FaFileExport } from "react-icons/fa";
import { FcDocument } from "react-icons/fc";
import { exportTransaction } from "../../redux/apiRequestProduct";
import { useSelector } from "react-redux";
import { toast } from "react-toastify";
export default function HistoryProduct({ transactionData, check,type,toDate,fromDate }) {
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const user = useSelector((state) => state?.auth?.loadUser?.user);
  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };
  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };
  const getExport = async () => {
    const response = await exportTransaction(user?.id, type,toDate||"",fromDate||"");
    if (response.status==200) {
      toast.success("Xuất báo cáo thành công", {
        position: "top-right",
        autoClose: 800,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "light",
      });
    }else{
      toast.error("Xuất báo cáo thất bại", {
          position: "top-right",
          autoClose: 800,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
          theme: "light",
        });
    }
  };
  const handleExport = () => {
    if(user?.id ){
        getExport()
    }
  };
  var columns = [
    { id: "code", label: "Mã giao dịch", minWidth: 100 },
    { id: "date", label: "Thời gian", minWidth: 100 },
    {
      id: "bank",
      label: "Ngân hàng",
      minWidth: 100,
      align: "right",
      style: {
        fontWeight: "bold",
      },
    },
    {
      id: "paymentContent",
      label: "Nội dung thanh toán",
      minWidth: 100,
      align: "center",
      style: {
        fontWeight: "bold",
      },
    },
    {
      id: "money",
      label: "Số tiền",
      minWidth: 100,
      align: "center",
      style: {
        fontWeight: "bold",
      },
    },
    {
      id: "status",
      label: "Trạng thái",
      minWidth: 170,
      align: "center",
      style: {
        fontWeight: "bold",
      },
    },
    {
      id: "action",
      label: (
        <>
          Xuất báo cáo:
          <Tooltip title="Xuất Báo cáo" arrow onClick={handleExport}>
            <IconButton>
              <FcDocument style={{ fontSize: "30px" }} />
            </IconButton>
          </Tooltip>
        </>
      ),
      minWidth: 100,
      align: "center",
      style: {
        fontWeight: "bold",
      },
    },
  ];
  if (check == 3 || check == 4) {
    columns = columns.filter((column) => column.id !== "bank");
  }

  function createData(code, date, bank, paymentContent, money, status, action) {
    return { code, date, bank, paymentContent, money, status, action };
  }

  const rows =
    Array.isArray(transactionData) && transactionData.length > 0
      ? transactionData.map((item) => {
          if (item && item.code) {
            return createData(
              item.code,
              convertToDate(item.createdTime),
              item.bank,
              item.desciption,
              item.totalAmount,
              item.status == "SUCCESS" ? "Thành công" : "Thất bại"
            );
          }
          return null;
        })
      : [];
  return (
    <>
      <Paper sx={{ width: "100%", overflow: "hidden" }}>
        <TableContainer sx={{ maxHeight: 440 }}>
          <Table stickyHeader aria-label="sticky table">
            <TableHead>
              <TableRow>
                {columns.map((column) => (
                  <TableCell
                    key={column.id}
                    align={column.align}
                    style={{ minWidth: column.minWidth }}
                  >
                    {column.label}
                  </TableCell>
                ))}
              </TableRow>
            </TableHead>
            <TableBody>
              {rows
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((row) => {
                  return (
                    <TableRow
                      hover
                      role="checkbox"
                      tabIndex={-1}
                      key={row.code}
                    >
                      {columns.map((column) => {
                        const value = row[column.id];
                        return (
                          <TableCell key={column.id} align={column.align}>
                            {column.id === "status" ? (
                              <div className="flex items-center justify-center">
                                {value === "Thành công" ? (
                                  <button className="flex items-center space-x-1 text-green-500">
                                    <FcApproval className="h-5 w-5" />
                                    <span>{value}</span>
                                  </button>
                                ) : (
                                  <button className="flex items-center space-x-1 text-red-500">
                                    <FcCancel className="h-5 w-5" />
                                    <span>{value}</span>
                                  </button>
                                )}
                              </div>
                            ) : column.format && typeof value === "number" ? (
                              column.format(value)
                            ) : (
                              value
                            )}
                          </TableCell>
                        );
                      })}
                    </TableRow>
                  );
                })}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[10, 25, 100]}
          component="div"
          count={rows.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onPageChange={handleChangePage}
          onRowsPerPageChange={handleChangeRowsPerPage}
        />
      </Paper>
    </>
  );
}
