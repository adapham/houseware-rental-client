import { SearchOutlined } from "@ant-design/icons";
import React, { useState } from "react";

import { Button, Col, DatePicker, Row } from "antd";
import dayjs from "dayjs";

const SearchInfor = ({ reload, setReload, setFromDate, setToDate }) => {
  const [from, setFrom] = useState();
  const [to, setTo] = useState();
  const { RangePicker } = DatePicker;
  const handleSearch = () => {
    console.log("Search");
    setFromDate(from);
    setToDate(to);
    setReload(!reload);
  };
  const handleRangeChange = (value) => {
    if (value && value.length > 0) {
      const startDate = value[0];
      const endDate = value[1];

      setFrom(dayjs(startDate).valueOf());
      setTo(dayjs(endDate).valueOf());
    } else {
      const startDate = null;
      const endDate = null;

      setFrom(dayjs(startDate).valueOf());
      setTo(dayjs(endDate).valueOf());
    }
  };

  return (
    <div>
      <fieldset className="border-2 rounded-lg shadow shadow-[#dde8da] border-[#dde8da] mt-5 p-4 mb-5">
        <legend className="text-xl">Thông tin tìm kiếm</legend>
        <Row>
          <Col span={14}>
            <RangePicker
              placeholder={["Ngày bắt đầu", "Ngày kết thúc"]}
              style={{ width: "70%" }}
              size="large"
              onChange={handleRangeChange}
            />
          </Col>
          <Col span={10}>
            <Button
              type="primary"
              icon={<SearchOutlined />}
              style={{
                backgroundColor: "blue",
                color: "white",
              }}
              labelColor="black"
              size="large"
              onClick={handleSearch}
            >
              Search
            </Button>
          </Col>
        </Row>
        {/* <Divider></Divider> */}
      </fieldset>
    </div>
  );
};

export default SearchInfor;
