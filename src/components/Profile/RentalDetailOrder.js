import * as React from "react";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import { convertMoney, convertToDate } from "../../utils/formatConfig";
import Accordion from "@mui/material/Accordion";
import AccordionDetails from "@mui/material/AccordionDetails";
import AccordionSummary from "@mui/material/AccordionSummary";
import Typography from "@mui/material/Typography";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import { List, ListItem } from "@mui/material";
import { BsFacebook, BsFillTelephoneFill } from "react-icons/bs";
import { Button, Col, Row } from "antd";
import { SiZalo } from "react-icons/si";
import { useNavigate } from "react-router-dom";
function monneyFormat(num) {
  return `${convertMoney(parseFloat(num))}đ`;
}

function priceRow(price, depositePrice) {
  return price + depositePrice;
}

function createRow(product, price, depositePrice, quantity, rentalDate, total) {
  //const total = priceRow(price, depositePrice);
  return { product, price, depositePrice, quantity, rentalDate, total };
}

function subtotal(items) {

  return items
    .map(({ total }) => Number(total)) // Convert total to a number
    .reduce((sum, i) => sum + i, 0);
}

export default function RentalDetailOrder({ dataDetail, status,userIdChoose,setLoadChat,loadChat }) {
  
  const navigate = useNavigate();
  const rows = Array.isArray(dataDetail)
    ? dataDetail.map((item, index) =>
        createRow(
          <div class="flex items-center mr-3" key={index}>
            <img
              src={item.imageText[0]}
              alt="iMac Front Image"
              class="h-8 w-auto mr-3"
            />
            {item.productName}
          </div>,
          `${item.priceProduct}`,
          `${item.depositPrice}`,
          `${item.quantity}`,
          `${
            status === "DONE"
              ? `${convertToDate(item?.createdTime)} - ${convertToDate(
                  item?.expiredDate
                )}`
              : `${item.rentalPeriod} tháng`
          }`,
          `${item.price}`
        )
      )
    : [];

  const invoiceSubtotal = subtotal(rows);
  const [expanded, setExpanded] = React.useState(false);
  const [expanded2, setExpanded2] = React.useState(false);
  const handleChange = (panel) => (event, isExpanded) => {
    setExpanded(isExpanded ? panel : false);
  };

  const handleChange2 = (panel) => (event, isExpanded) => {
    setExpanded2(isExpanded ? panel : false);
  };

  return (
    <>
      <TableContainer component={Paper}>
        <Table sx={{ minWidth: 700 }} aria-label="spanning table">
          <TableHead>
            <TableRow>
              <TableCell align="center">Tên sản phẩm</TableCell>
              <TableCell align="center">Giá thuê</TableCell>
              <TableCell align="center">Giá cọc</TableCell>
              <TableCell align="center">Số lượng</TableCell>
              <TableCell align="center">Thời gian thuê</TableCell>
              <TableCell align="center">Tiền thuê</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {rows.map((row) => (
              <TableRow key={row.product}>
                <TableCell>{row.product}</TableCell>
                <TableCell align="center">{monneyFormat(row.price)}</TableCell>
                <TableCell align="center">
                  {monneyFormat(row.depositePrice)}
                </TableCell>
                <TableCell align="center">{row.quantity}</TableCell>
                <TableCell align="center">{row.rentalDate}</TableCell>
                <TableCell align="center">{monneyFormat(row.total)}</TableCell>
              </TableRow>
            ))}

            <TableRow>
              <TableCell ></TableCell>
              <TableCell ></TableCell>
              <TableCell ></TableCell>
              <TableCell ></TableCell>
              <TableCell size="large" align="right" colSpan={1}>
                Tổng tiền đi thuê:
              </TableCell>
              <TableCell align="center">
                {monneyFormat(invoiceSubtotal)}
              </TableCell>
            </TableRow>
          </TableBody>
        </Table>
      </TableContainer>
      <Row gutter={16} className="mt-3">
        <Col span={12}>
          <Accordion
            expanded={expanded === "panel"}
            onChange={handleChange("panel")}
          >
            <AccordionSummary
              expandIcon={<ExpandMoreIcon />}
              aria-controls="panel1bh-content"
              id="panel1bh-header"
              className="flex justify-between w-full"
            >
              <Typography
                sx={{
                  width: "70%",
                  flexShrink: 0,
                  fontSize: "18px",
                  paddingLeft: "20px",
                }}
              >
                Thông tin liên hệ
              </Typography>
              <Typography sx={{ color: "text.secondary" }}>
                xem chi tiết
              </Typography>
            </AccordionSummary>
            <AccordionDetails>
              <Typography>
                <List>
                  <ListItem disablePadding>
                    <div className="flex justify-between w-full">
                      <div>
                        Địa chỉ:
                        <span className="font-bold">
                          {" "}
                          {dataDetail?.[0]?.address}
                        </span>
                      </div>
                      <div>
                        <div className="flex justify-center items-center">
                          <div className="m-1 ml-3 flex justify-center items-center">
                            <BsFillTelephoneFill style={{ fontSize: "24px" }} />
                            <div className="ml-2">{dataDetail?.[0]?.phone}</div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </ListItem>
                </List>
              </Typography>
            </AccordionDetails>
          </Accordion>
        </Col>
        <Col span={12}>
          <Accordion
            expanded={expanded2 === "panel1"}
            onChange={handleChange2("panel1")}
          >
            <AccordionSummary
              expandIcon={<ExpandMoreIcon />}
              aria-controls="panel1bh-content"
              id="panel1bh-header"
              className="flex justify-between w-full"
            >
              <Typography
                sx={{
                  width: "70%",
                  flexShrink: 0,
                  fontSize: "18px",
                  paddingLeft: "20px",
                }}
              >
                Thông tin đơn hàng
              </Typography>
              <Typography sx={{ color: "text.secondary" }}>
                xem chi tiết
              </Typography>
            </AccordionSummary>
            <AccordionDetails>
              <Typography>
                <List>
                  <ListItem disablePadding>
                    <div className="w-full">
                      <div>
                        Địa chỉ: <span className="font-bold">
                          {dataDetail?.[0]?.addressShip}
                        </span>                      
                      </div>
                      <div>
                        Email: <span className="font-bold">
                          {dataDetail?.[0]?.email}
                        </span>                      
                      </div>
                      <div>
                        Họ và Tên: <span className="font-bold">
                          {dataDetail?.[0]?.fullName}
                        </span>                      
                      </div>
                      <div>
                        Số điện thoại: <span className="font-bold">
                          {dataDetail?.[0]?.phone}
                        </span>                      
                      </div>
                      
                    </div>
                  </ListItem>
                </List>
              </Typography>
            </AccordionDetails>
          </Accordion>
        </Col>
      </Row>
      <div className="flex justify-end">
        <Button className="mt-2  bg-[#1975D2] text-white "
        onClick={()=>{
          navigate('/profile',{state:userIdChoose});
          setLoadChat(!loadChat)
          }}>
           Liên hệ
        </Button>
      </div>
    </>
  );
}
