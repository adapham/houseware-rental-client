import * as React from "react";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import { convertMoney, convertToDate } from "../../utils/formatConfig";
import { List, ListItem } from "@mui/material";
import { BsFacebook, BsFillTelephoneFill } from "react-icons/bs";
import Accordion from "@mui/material/Accordion";
import AccordionDetails from "@mui/material/AccordionDetails";
import AccordionSummary from "@mui/material/AccordionSummary";
import Typography from "@mui/material/Typography";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import { Button, Col, Modal, Row } from "antd";
import {
  getOrderDone,
} from "../../redux/apiRequestProduct";

import {  useSelector } from "react-redux";
import { toast } from "react-toastify";
import { useNavigate } from "react-router-dom";

function monneyFormat(num) {
  return `${convertMoney(parseFloat(num))}đ`;
}

function priceRow(price, depositePrice) {
  return price + depositePrice;
}

function createRow(product, price, depositePrice, quantity, rentalDate, total) {
  //const total = priceRow(price, depositePrice);
  return { product, price, depositePrice, quantity, rentalDate, total };
}

function subtotal(items) {
  return items
    .map(({ total }) => Number(total)) // Convert total to a number
    .reduce((sum, i) => sum + i, 0);
}

function HistoryStoreRentalDetail({ dataDetail, setOpen,reload,setReload ,status,countDay}) {
  const navigate = useNavigate();
  const [openC, setOpenC] = React.useState(false);
  const [expanded, setExpanded] = React.useState(false);
  const handleChange = (panel) => (event, isExpanded) => {
    setExpanded(isExpanded ? panel : false);
  };

  const rows = [
    createRow(
      <div class="flex items-center">
        <img
          src={dataDetail.imageText[0]}
          alt="Image"
          class="h-8 w-auto mr-3"
        />
        {dataDetail.productName}
      </div>,
      `${dataDetail.priceProduct}`,
      `${dataDetail.depositPrice}`,
      `${dataDetail.quantity}`,
      `${`${dataDetail.rentalPeriod} tháng`}`,
      `${dataDetail.price}`
    ),
  ];
  const handleAccept = () => {
    getDone(dataDetail?.orderId);
    setReload(!reload);
  };
  const getDone = async (id) => {
    const response = await getOrderDone(id);
    if (response) {
       setOpen(false);
       toast.success("Đã nhận lại hàng", {
        position: "top-right",
        autoClose: 800,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "light",
      });
    }else if(response.status===404){
         toast.error("lỗi không thể xác nhận", {
            position: "top-right",
            autoClose: 2000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
            theme: "light",
          });
    }
  };
  const hideModalC = (status) => {
    setOpenC(false);

    if (status) {
      handleAccept();
    }
  };
  return (
    <TableContainer className="p-5" component={Paper}>
      <Table sx={{ minWidth: 700 }} aria-label="spanning table">
        <TableHead>
          <TableRow>
            <TableCell align="center">Tên sản phẩm</TableCell>
            <TableCell align="center">Giá thuê</TableCell>
            <TableCell align="center">Giá cọc</TableCell>
            <TableCell align="center">Số lượng</TableCell>
            <TableCell align="center">Thời gian thuê</TableCell>
            <TableCell align="center">Tiền thuê</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map((row) => (
            <TableRow key={row.product}>
              <TableCell>{row.product}</TableCell>
              <TableCell align="center">{monneyFormat(row.price)}</TableCell>
              <TableCell align="center">
                {monneyFormat(row.depositePrice)}
              </TableCell>
              <TableCell align="center">{row.quantity}</TableCell>
              <TableCell align="center">{row.rentalDate}</TableCell>
              <TableCell align="center">{monneyFormat(row.total)}</TableCell>
            </TableRow>
          ))}

          {/* <TableRow>
            <TableCell size="large" align="right" colSpan={1}>
              Tổng tiền đi thuê:
            </TableCell>
            <TableCell align="left">{monneyFormat(invoiceSubtotal)}</TableCell>
            <TableCell size="large" align="right" colSpan={1}></TableCell>
          </TableRow> */}
        </TableBody>
      </Table>
      <Row gutter={16} className="mt-3">
        <Col span={15}>
          <Accordion
            expanded={expanded === "panel1"}
            onChange={handleChange("panel1")}
          >
            <AccordionSummary
              expandIcon={<ExpandMoreIcon />}
              aria-controls="panel1bh-content"
              id="panel1bh-header"
              className="flex justify-between w-full"
            >
              <Typography
                sx={{ width: "80%", flexShrink: 0, fontSize: "18px",paddingLeft:"20px" }}
                
              >
                Thông tin liên hệ
              </Typography>
              <Typography sx={{ color: "text.secondary" }}>
                xem chi tiết
              </Typography>
            </AccordionSummary>
            <AccordionDetails>
              <Typography>
                <List>
                  <ListItem disablePadding>
                    <div className="flex justify-between w-full">
                      <div>Địa chỉ:<span className="font-bold"> {dataDetail?.address}</span></div>
                      <div>
                        <div className="flex justify-center items-center">
                          
                          <div className="m-1 ml-3 flex justify-center items-center">
                            <BsFillTelephoneFill style={{ fontSize: "24px" }} />
                            <div className="ml-2">{dataDetail?.phone}</div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </ListItem>
                </List>
              </Typography>
            </AccordionDetails>
          </Accordion>
        </Col>
        <Col span={3}>
          <Button  className="mt-2  bg-[#1975D2] text-white " 
          onClick={()=>navigate('/profile',{state:dataDetail?.userId})}>Liên hệ</Button>
        </Col>
      </Row>
      {((status == "RETURN")||(status=="RENTED" && countDay <=0 )) && (
        <div className="flex justify-end my-4">
        <div className="mx-5 border">
          <Button className="bg-[#1975D2] text-white "  onClick={()=>setOpenC(true)}>
            ĐÃ NHẬN LẠI HÀNG
          </Button>
        </div>
      </div>
      )}
      <Modal
                    title="Xác nhận đã nhận lại hàng"
                     open={openC}
                    footer={null}

                    cancelText="Quay lại"
                    onCancel={() => hideModalC(false)}
                >

                    <div className="flex justify-end mt-5">

                        <Button
                            className="mt-2  bg-[#1975D2] text-white "
                            onClick={() => hideModalC(true)}
                        >
                            Có
                        </Button>
                        <Button className="mt-2  bg-[#1975D2] text-white " onClick={() => hideModalC(false)}>
                            Không
                        </Button>
                    </div>
                </Modal>
    </TableContainer>
  );
}

export default HistoryStoreRentalDetail;
