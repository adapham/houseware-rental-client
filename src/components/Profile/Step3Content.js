import React from 'react';
import PolicyLessorDetails from "./PolicyLessorDetails";
import PolicyLessorBasic from "./PolicyLessorBasic";
import {Typography} from "@material-ui/core";
import Checkbox from '@mui/material/Checkbox';
import FormGroup from '@mui/material/FormGroup';
import FormControlLabel from '@mui/material/FormControlLabel';

function Step3Content({setChecked}) {
    const handleChange = (event) => {
        setChecked(event.target.checked);
      };
    return (
        <div className="mt-10">
            <Typography
                variant="h4"
                className="font-bold py-3 mb-4 text-left text-success fw-light"
            >
                Chính Sách & Điều Kiện Cơ Bản
            </Typography>
            <PolicyLessorBasic/>
            <Typography
                variant="h4"
                className="font-bold py-3 mb-4 text-left text-success fw-light"
            >
                Chính Sách & Quy Định Chi Tiết
            </Typography>
            <PolicyLessorDetails/>

            <FormGroup>
                <FormControlLabel control={<Checkbox onChange={handleChange}/>} label="Đồng ý với mọi chính sách và quy định"/>
            </FormGroup>
        </div>
    );
}

export default Step3Content;