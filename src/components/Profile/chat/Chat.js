import React, {useEffect, useState, useRef} from "react";
import {Button, message} from "antd";
import SendIcon from '@mui/icons-material/Send';
import {
    getAllUsersBySenderId,
    findChatMessages,
    findChatMessage,
    getCurrentUser,
} from "../../../utils/ApiUtil";

import ScrollToBottom from "react-scroll-to-bottom";
import "./Chat.css";
import {useNavigate} from "react-router-dom";
import { isArray, isEmpty } from "lodash";

var stompClient = null;
const Chat = ({userId}) => {
    const [currentUser, setCurrentUser] = useState("");
    const [text, setText] = useState("");
    const [contacts, setContacts] = useState([]);
    const [activeContact, setActiveContact] = useState(null); //Lấy từ redux

    const [messages, setMessages] = useState("");
    const navigate = useNavigate();
    const [imageUser, setImageUser] = useState();

    const imageDefault = "https://inkythuatso.com/uploads/thumbnails/800/2023/03/6-anh-dai-dien-trang-inkythuatso-03-15-26-36.jpg";
    // const user = useSelector((state) => state?.auth?.loadUser?.user);

    const BASE_URL = process.env.REACT_APP_BASE_URL

    const renderMessageContent = (content) => {
        // Use a regular expression to match URLs
        const urlRegex = /(https?:\/\/[^\s]+)/g;
        // Extract URLs from the content
        const urls = content.match(urlRegex);
      
        // If there are URLs in the content, replace them with links
        if (urls && urls.length > 0) {
          const contentWithoutUrls = content.replace(urlRegex, '');
          
          return (
            <>
              {contentWithoutUrls}
              {urls.map((url, index) => (
                <a key={index} href={url} target="_blank" rel="noopener noreferrer">
                  {url}
                </a>
              ))}
            </>
          );
        } else {
          // If there are no URLs, render the content as plain text
          return content;
        }
    };

    function checkImagePath(inputString) {
        return inputString.endsWith("image/") ? imageDefault : inputString;
    }
    console.log("activeContact", activeContact)
    useEffect(() => {
        const connectData = async() => {
            if (localStorage.getItem("access-token") === null) {
                // props.history.push("/login");
                navigate("/signin");
            }
            await loadCurrentUser();
            await connect();
            await loadContacts();
        }
        connectData();
    }, []);

    useEffect(() => {
        if(!activeContact) return;
        findAllChatMessage(activeContact.id, currentUser.id);
        loadContacts();
    }, [activeContact, currentUser]);

    const findAllChatMessage = async (activeId, currentId) => {
        const msgs = await findChatMessages(activeId, currentId);
        setMessages(msgs)
    }

    const loadCurrentUser = async () => {
        const data = await getCurrentUser();
        console.log('====> currentUser: ',data);
        setCurrentUser(data)
        const imageUrl = data?.imageUrl ? checkImagePath(data?.imageUrl) :imageDefault;
        setImageUser(imageUrl);
    }

    const connect = () => {
        const Stomp = require("stompjs");
        var SockJS = require("sockjs-client");
        
        SockJS = new SockJS(BASE_URL + "/ws");
        stompClient = Stomp.over(SockJS);
        stompClient.connect({}, onConnected, onError);
    };

    const activeContactRef = useRef(activeContact);

    useEffect(() => {
        activeContactRef.current = activeContact;
    }, [activeContact]);

    const onConnected = async () => {
        const data = await getCurrentUser();
        console.log('====> currentUser: ',data);
        setCurrentUser(data)
        console.log("connected User: ", data?.id);
        stompClient.subscribe(
            "/user/" + data?.id + "/queue/messages",
            onMessageReceived
        );
    };

    const onError = (err) => {
        console.log(err);
    };

    const onMessageReceived = (msg) => {
        const notification = JSON.parse(msg.body);
        // console.log('ActiveContact AND Notification: ', activeContact, notification);
        console.log('ActiveContact AND Notification: ', activeContactRef.current, notification);
        // if (activeContact.id === notification.senderId) {
        //     findChatMessage(notification.id).then((message) => {
        //         setMessages((preState) => ([...preState, message]));
        //     });
        // } else {
        //     message.info("Received a new message from " + notification.senderName);
        // }

        if (activeContactRef.current && activeContactRef.current.id === notification.senderId) {
            findChatMessage(notification.id).then((message) => {
                setMessages((preState) => ([...preState, message]));
            });
        }
        // else {
        //     message.info("Received a new message from " + notification.senderName);
        // }
        // loadContacts();
    };

    const sendMessage = (msg) => {
        if (msg.trim() !== "") {
            const message = {
                senderId: currentUser.id,
                recipientId: activeContact.id,
                senderName: currentUser.username,
                recipientName: activeContact.username,
                content: msg,
                timestamp: new Date(),
            };
            console.log('====> Message Send: ', message);
            stompClient.send("/app/chat", {}, JSON.stringify(message));

            const newMessages = [...messages];
            console.log('====> NewMsg: ', newMessages, message);
            newMessages.push(message);
            setMessages(newMessages);
        }
    };


    const loadContacts = async () => {
        const data = await getCurrentUser();
        const users = await getAllUsersBySenderId(data.id);
        if (users) {
            const userWithUserId = userId ? users.find(user => user.id === userId) : false;
            setContacts(users);
            if (!activeContact && users?.length > 0) {
                if(userWithUserId){
  
                    setActiveContact(userWithUserId);
                }else{
                    setActiveContact(users[0]);
                }
            }else {
                setActiveContact(activeContact);
            }
        }
    };

    console.log("activeContactactiveContactactiveContactactiveContact", activeContact)


    return (
        <div id="frame">
            <div id="sidepanel">
                <div id="profile">
                    <div className="wrap">
                        <img
                            alt="User Avatar"
                            id="profile-img"
                            className="online"
                            src={imageUser ? imageUser : imageDefault}
                        />
                        <p>{currentUser?.name ? currentUser?.name : currentUser?.username}</p>
                    </div>
                </div>
                <div id="search"/>
                <div id="contacts">
                    <ul>
                        {isArray(contacts) && contacts?.map((contact) => (
                            <li
                                onClick={() => setActiveContact(contact)}
                                className={
                                    activeContact && contact.id === activeContact.id
                                        ? "contact active"
                                        : "contact"
                                }
                                key={contact.id}
                            >
                                <div className="wrap">
                                    <span className="contact-status online"></span>
                                    <img
                                        alt="avatar"
                                        id={contact.id}
                                        className="online"
                                        src={contact?.imageUrl ? contact?.imageUrl : imageDefault}
                                        style={{ width: '40px', height: '40px',cursor: 'pointer', borderRadius: '50%',objectFit: 'cover', objectPosition: 'center', }}
                                    />
                                    <div className="meta">
                                        <p className="name">{contact?.name ? contact?.name : contact?.username}</p>
                                        {contact?.isDelivered && (
                                            <p className="preview">
                                                Tin nhắn mới
                                            </p>
                                            )
                                        }
                                    </div>
                                </div>
                            </li>
                        ))}
                    </ul>
                </div>
            </div>
            <div className="content">
                <div className="contact-profile">
                    <img 
                        alt=""
                        src={activeContact && activeContact?.imageUrl ? activeContact?.imageUrl : imageDefault}
                         style={{ width: '50px', height: '50px',cursor: 'pointer', borderRadius: '50%',objectFit: 'cover', objectPosition: 'center' }}
                    />
                    
                    <p>{activeContact && activeContact?.name ? activeContact?.name : activeContact?.username}</p>
                </div>
                <ScrollToBottom className="messages">
                    <ul>
                        {isArray(messages) && messages?.map((msg, index) => (
                              <li key={index} className={msg.senderId === currentUser.id ? "sent" : "replies"}>
                                {msg.senderId !== currentUser.id && (
                                  <img src={activeContact?.imageUrl ? activeContact?.imageUrl : imageDefault} alt="" style={{ width: '50px', height: '50px',cursor: 'pointer', borderRadius: '50%',objectFit: 'cover', objectPosition: 'center', }} />
                                )}
                                <p>{renderMessageContent(msg.content)}</p>
                              </li>
                        ))}
                    </ul>
                </ScrollToBottom>
                <div className="message-input mt-10">
                    <div className="wrap">
                        <input
                            name="user_input"
                            size="large"
                            placeholder="Write your message..."
                            value={text}
                            onChange={(event) => setText(event.target.value)}
                            onKeyPress={(event) => {
                                if (event.key === "Enter") {
                                    sendMessage(text);
                                    setText("");
                                }
                            }}
                        />

                        <Button
                            icon={<i aria-hidden="true"><SendIcon /></i>}
                            onClick={() => {
                                sendMessage(text);
                                setText("");
                            }}
                        />
                    </div>
                </div>
            </div>
        </div>
    );
};
export default Chat;
