import * as React from "react";

import { convertMoney, convertToDate } from "../../utils/formatConfig";
import { BsEyeFill } from "react-icons/bs";
import { FcUndo } from "react-icons/fc";
import { Button, Modal, Table, Tooltip } from "antd";
import IconButton from "@mui/material/IconButton";

import { useState } from "react";
import {
  getOrderDelete,
  getReport,
} from "../../redux/apiRequestProduct";
import { toast } from "react-toastify";
import {
  FcCheckmark,
  FcClock,
  FcCancel,
  FcInfo,
  FcApproval,
} from "react-icons/fc";

import { FcOnlineSupport } from "react-icons/fc";
import { Radio, Space } from "antd";
import TextArea from "antd/es/input/TextArea";
import HistoryStoreRentalDetail from "./HistoryStoreRentalDetail";
import { useSelector } from "react-redux";
import LoadingScreen from "../../pages/examples/loading-screen";
function HistoryStoreRentedOrder({ orderList, setReload, reload,loading }) {
  const [valueOpRe, setValueOpRe] = useState();

  const [openD, setOpenD] = useState(false);
  const [openR, setOpenR] = useState(false);
  const [idDelete, setIdDelete] = useState();
  const [descriptionR, setDescriptionR] =useState();
  const [description, setDescription] = useState("");
  const user = useSelector((state) => state?.auth?.loadUser?.user);
  const [status,setStatus]=useState();
  const [countDay,setCountDay]=useState();
  const showModalD = () => {
    setOpenD(true);
  };
  const convertStatus = (value) => {
    let icon, color;

    switch (value) {
      case "NEW":
        icon = <FcInfo />;
        color = "text-yellow-500";
        return (
          <span className={`font-bold ${color} flex items-center`}>
            {icon} Chờ xác nhận
          </span>
        );

      case "PENDING":
        icon = <FcClock />;
        color = "text-blue-500";
        return (
          <span className={`font-bold ${color} flex items-center`}>
            {icon} Đang giao
          </span>
        );

      case "RENTED":
        icon = <FcCheckmark />;
        color = "text-green-500";
        return (
          <span className={`font-bold ${color} flex items-center`}>
            {icon} Đang thuê
          </span>
        );

      case "CANCEL":
        icon = <FcCancel />;
        color = "text-red-500";
        return (
          <span className={`font-bold ${color} flex items-center`}>
            {icon} Đã huỷ
          </span>
        );

      case "DONE":
        icon = <FcApproval />;
        color = "text-purple-500";
        return (
          <span className={`font-bold ${color} flex items-center`}>
            {icon} Đã hoàn thành
          </span>
        );
        case "RETURN":
          icon = <FcUndo />;
          color = "text-blue-500";
          return (
            <span className={`font-bold ${color} flex items-center`}>
              {icon} Chờ trả hàng
            </span>
          );

      default:
        return value;
    }
  };
  const convertStatusOrder = {
    NEW: 1,
    PENDING: 2,
    RENTED: 3,
    CANCEL: 4,
    DONE: 5,
    RETURN: 6,
  };
  const hideModalR = (status) => {
    
    const value = valueOpRe == 1 ? description : valueOpRe;
   if(status){
    getReportRequest(orderList[0]?.orderId,value);
   }
    setOpenR(false)
    setDescription(null);
    setValueOpRe(null)

  };
  const getReportRequest = async (id,content)=>{
    const responnse = await getReport(id,user?.id,content,"ORDER_DETAIL");
    if(responnse){
      setOpenR(false);
      toast.success("Báo cáo thành công!", {
        position: "top-right",
        autoClose: 2000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "light",
      });
    }
  }
  const hideModalD = (status) => {
    setOpenD(false);
    if (status) {
      const value = descriptionR;
      getDelete(idDelete, value);
      setDescriptionR("")
    }
    setReload(!reload);
  };
  const [open, setOpen] = useState(false);
 
  const onClose = () => {
    setOpen(false);
  };
  const handleGetDetail = (data) => {
    setCountDay(data?.timeDay);
    setStatus(data?.status)
    setDataDetail(data);
    setOpen(true);
    
  };
  const handleReport = () => {
    setOpenR(true);
  };
  
  const onChange = (e) => {

    setValueOpRe(e.target.value);
  };
 
  const getDelete = async (id, content) => {
    const response = await getOrderDelete(id, content);
    if (response) {
      toast.success("Huỷ sản đơn hàng thành công", {
        position: "top-right",
        autoClose: 2000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "light",
      });
    }
  };
  
 
  const [dataDetail, setDataDetail] = useState([]);
  const columns = [
    { 
      title: "STT", 
      dataIndex: "code", 
      key: "code", 
      minWidth: 5 
    },
    { 
      title: "Tên sản phẩm", 
      dataIndex: "productName", 
      key: "productName", 
      minWidth: 100 ,
      render: (value,record) => (
        <div class="flex items-center mr-3" key={record.id}>
          <img
            src={record.imageText[0]}
            alt="Image"
            class="h-8 w-auto mr-3"
          />
          {record.productName}
        </div>
      ),
    },
    {
      title: "Số ngày còn lại",
      dataIndex: "timeDay",
      key: "timeDay",
      minWidth: 50,
      sorter: (a, b) => a.timeDay - b.timeDay,
      align: "center",
      render: (value) => (
        <span style={{ fontWeight: "bold" }}>{value}</span>
      ),
    },
    {
      title: "Thời gian thuê",
      dataIndex: "rentTime",
      key: "rentTime",
      minWidth: 80,
      align: "center",
      render: (value, record) => {
        const createdTime = convertToDate(record.createdTime);
        const expiredDate = convertToDate(record.expiredDate);
    
        if (createdTime && expiredDate) {
          const formattedCreatedTime = createdTime.toLocaleString();
          const formattedExpiredDate = expiredDate.toLocaleString();
    
          return (
            <span style={{ fontWeight: "bold" }}>
              {`${formattedCreatedTime} - ${formattedExpiredDate}`}
            </span>
          );
        } else {
          return <span style={{ fontWeight: "bold" }}>-</span>;
        }
      },
    },
    
    
    {
      title: "Tổng số tiền",
      dataIndex: "price",
      key: "price",
      minWidth: 80,
      align: "center",
      sorter: (a, b) => a.price - b.price,
      render: (value) => (
        <span style={{ fontWeight: "bold" }}>{convertMoney(value)} VND</span>
      ),
    },
    {
      title: "Trạng thái",
      dataIndex: "status",
      key: "status",
      minWidth: 80,
      sorter: (a, b) => convertStatusOrder[a.status] - convertStatusOrder[b.status],
      align: "center",
      render: (value) => (
        <span style={{ fontWeight: "bold" }}>{convertStatus(value)}</span>
      ),
    },
    {
      title: "Thao tác",
      dataIndex: "action",
      key: "action",
      minWidth: 130,
      align: "center",
      render: (value,rows) => (
        <div className="flex justify-center">
          {rows.status == "RETURN" ? (
            <div className="px-1">
              <Tooltip title="Báo cáo" arrow>
                <IconButton>
                  <FcOnlineSupport
                    style={{ fontSize: "20px" }}
                    onClick={() => handleReport(rows)}
                  />
                </IconButton>
              </Tooltip>
            </div>
          ): <></>}
            
            <div className="px-1">
              <Tooltip title="Xem chi tiết" arrow>
                <IconButton>
                  <BsEyeFill
                    style={{ fontSize: "20px" }}
                    onClick={()=>handleGetDetail(rows)}
                  />
                </IconButton>
              </Tooltip>
            </div>
          </div>
      ),
    },
  ];
  if (loading) {
    return <LoadingScreen />;
  }

  

  return (
    <>
      <Table
          columns={columns}
          dataSource={orderList}
          pagination={{
            defaultPageSize: 8,
            showSizeChanger: true,
            pageSizeOptions: ["8","10", "20", "30"],
          }}/>

      <Modal
        title="Chi tiết sản phẩm"
        centered
        open={open}
        footer={null}
        width={1200}
        height={800}
        onCancel={onClose}
      >
        {dataDetail && (
          <HistoryStoreRentalDetail dataDetail={dataDetail} setOpen={setOpen} reload={reload}
           setReload={setReload} status={status} countDay={countDay}/>
        )}
      </Modal>
      <Modal
        title="Huỷ đơn hàng"
        open={openD}
        footer={null}
        okText="Xác nhận"
        cancelText="Quay lại"
        onCancel={hideModalD}
      >
        <p>Bạn có chắc muốn huỷ order không?</p>
        <TextArea
                      onChange={(e) => setDescriptionR(e.target.value)}
                      placeholder="Điền lý do ở dây"
                      value={descriptionR}
                    ></TextArea>
        <div className="flex justify-end mt-5">
          <Button
            className="bg-[#1975D2] text-white "
            onClick={() => hideModalD(true)}
          >
            Đồng ý
          </Button>
          <Button className="bg-[#1975D2] text-white " onClick={() => hideModalD(false)}>
            Huỷ bỏ
          </Button>
        </div>
      </Modal>
      <Modal
        title="Báo cáo vi phạm"
        open={openR}
        footer={null}
        okText="Xác nhận"
        cancelText="Quay lại"
        onCancel={()=>hideModalR(false)}
      >
        <Radio.Group onChange={onChange} value={valueOpRe}>
          <Space direction="vertical">
            <Radio value={"Khách thuê không đồng ý trả sản phẩm"}>
              Khách thuê không đồng ý trả sản phẩm
            </Radio>
            <Radio value={"Sản phẩm nhận lại bị hư hỏng"}>
              Sản phẩm nhận lại bị hư hỏng
            </Radio>
            <Radio value={"Sản phẩm nhận không đúng như đơn hàng"}>
              Sản phẩm nhận không đúng như đơn hàng
            </Radio>
           
            {/* <Radio value={"Không thống nhất được phí thuê đặt cọc"}>
              Không thống nhất được phí thuê đặt cọc
            </Radio> */}
            <Radio value={1}>
              Lý do khác
              {valueOpRe === 1 ? (
                <TextArea
                  onChange={(e) => setDescription(e.target.value)}
                  placeholder="Điền lý do ở dây"
                  value={description}
                ></TextArea>
              ) : null}
            </Radio>
          </Space>
        </Radio.Group>
        <div className="flex justify-end mt-5">
          {(description ||valueOpRe ) ?(
            <Button
            className="mt-2  bg-[#1975D2] text-white "
             onClick={() => hideModalR(true)}
           >
             Gửi
           </Button>
          ):(
            <Button
           className="mt-2  bg-[#1975D2] text-white "
           disabled
          >
            Gửi
          </Button>
          )}
          
        </div>
      </Modal>
      
    </>
  );
}

export default HistoryStoreRentedOrder;
