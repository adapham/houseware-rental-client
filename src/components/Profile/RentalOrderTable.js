import * as React from "react";

import { convertMoney, convertToDate } from "../../utils/formatConfig";
import { BsEyeFill } from "react-icons/bs";
import { GiCancel } from "react-icons/gi";
import { Button, Modal, Table, Tooltip } from "antd";
import IconButton from "@mui/material/IconButton";

import RentalDetailOrder from "./RentalDetailOrder";
import { useState } from "react";
import { getOrderApprove, getOrderDelete, getOrderSuccess, getReport } from "../../redux/apiRequestProduct";

import { FcCheckmark, FcClock, FcCancel, FcInfo, FcApproval } from 'react-icons/fc';
import { useSelector } from "react-redux";
import { FcApprove } from "react-icons/fc";

import TextArea from "antd/es/input/TextArea";
import { toast } from "react-toastify";
import dayjs from "dayjs";

export default function RentalOrderTable({ orderList, type, setReload, reload,setLoadChat,loadChat,dataTemp,idOrder }) {
  const [open, setOpen] = useState((dataTemp && idOrder) ? true :false);
  const [dataDetail, setDataDetail] = useState(dataTemp?.[0]?.orderManageDetailResps);
  const user = useSelector((state) => state?.auth?.loadUser?.user);
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const [openD, setOpenD] = useState(false);
  const [openR, setOpenR] = useState(false);
  const [descriptionR, setDescriptionR] = useState("");
  const [idDelete, setIdDelete] = useState();
  const [userIdChoose,setUserIdChoose] = useState();
  const [openC, setOpenC] = useState(false);
  const showModalD = () => {
    setOpenD(true);
  };
  
  const hideModalR = (status) => {

    const value = "Không nhận được sản phẩm thuê";
    setOpenR(false)
    if (status) {
      getReportRequest(orderList[0]?.id, value);
    }

    
  };
  
  const getReportRequest = async (id, content) => {
    const responnse = await getReport(id, user?.id, content,"ORDER");
    if (responnse) {
      setOpenR(false);
      toast.success("Báo cáo thành công!", {
        position: "top-right",
        autoClose: 2000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "light",
      });
    }
  }
  const convertStatus = (value) => {
    let icon, color;

    switch (value) {
      case "NEW":
        icon = <FcInfo />;
        color = "text-yellow-500";
        return (
          <span className={`font-bold ${color} flex items-center`}>
            {icon} Xác nhận
          </span>
        );

      case "PENDING":
        icon = <FcClock />;
        color = "text-blue-500";
        return (
          <span className={`font-bold ${color} flex items-center`}>
            {icon} Đang giao
          </span>
        );
      case "CONFIRM":
        icon = <FcApprove />;
        color = "text-pink-500";
        return (
          <span className={`font-bold ${color} flex items-center`}>
            {icon} Chờ xác nhận
          </span>
        );
      case "RENTED":
        icon = <FcCheckmark />;
        color = "text-green-500";
        return (
          <span className={`font-bold ${color} flex items-center`}>
            {icon} Hoàn tất
          </span>
        );

      case "CANCEL":
        icon = <FcCancel />;
        color = "text-red-500";
        return (
          <span className={`font-bold ${color} flex items-center`}>
            {icon} Đã huỷ
          </span>
        );

      case "DONE":
        icon = <FcApproval />;
        color = "text-purple-500";
        return (
          <span className={`font-bold ${color} flex items-center`}>
            {icon} Đã hoàn thành
          </span>
        );

      default:
        return value;
    }
  };
  const hideModalD = (status) => {

    setOpenD(false);
    if (status) {
      const value = descriptionR;
      getDelete(idDelete, value);
      setDescriptionR("");
    }
    setReload(!reload)
  };
  
  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };
  const onClose = () => {
    setOpen(false);
  };
  
  const handleGetDetail = (data) => {
    setDataDetail(data);
    setOpen(true);
    setUserIdChoose(data[0].userId);
    
  };
  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };
  const getApprove = async (id) => {
    const response = await getOrderApprove(id);
    if (response) {
      setReload(!reload)
      toast.success('Xác nhận đơn hàng', {
        position: "top-right",
        autoClose: 2000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "light",
      });
    }
  }
  const getDelete = async (id, content) => {
    const response = await getOrderDelete(id, content);
    if (response) {
      // toast.success('Huỷ sản đơn hàng thành công', {
      //   position: "top-right",
      //   autoClose: 2000,
      //   hideProgressBar: false,
      //   closeOnClick: true,
      //   pauseOnHover: true,
      //   draggable: true,
      //   progress: undefined,
      //   theme: "light",
      // });
    }
  }
  const getSuccess = async (id, userId) => {
    const response = await getOrderSuccess(id, userId);
    if (response) {
      setOpen(false);
      setReload(!reload)
      toast.success('Xác nhận đơn', {
        position: "top-right",
        autoClose: 2000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "light",
      });
    }
  }

  const handleApprove = () => {

    setOpenC(true);
    
  }
  const hideModalC = (id, status) => {
    setOpenC(false)
    if (status == "NEW") {
      getApprove(id);
    } else if (status == "PENDING") {
      getSuccess(id, user?.id);
    } else if (status == "CONFIRM") {
      getSuccess(id, user?.id);
    }

  };
  const handleDelete = (id) => {
    setIdDelete(id);
    showModalD(id)

  }
  
  const handleReport = () => {
    setOpenR(true);
    setOpen(false);
  }
  
  const columns = [
    {
      title: "Mã đơn hàng",
      dataIndex: "code",
      key: "code",
      minWidth: 100,
    },
    {
      title: "Ngày tạo đơn",
      dataIndex: "createdTime",
      key: "createdTime",
      minWidth: 100,
      align: "right",
      sorter: (a, b) =>
        dayjs(a.createdTime).unix() - dayjs(b.createdTime).unix(),
      render: (value) => dayjs(value).format("DD-MM-YYYY"),
    },
    {
      title: "Tổng tiền",
      dataIndex: "totalprice",
      key: "totalprice",
      minWidth: 100,
      align: "center",
      sorter: (a, b) => a.totalprice - b.totalprice,
      render: (value) => (
        <span style={{ fontWeight: "bold" }}>{convertMoney(value)} VND</span>
      ),
    },
    {
      title: "Số sản phẩm",
      dataIndex: "orderManageDetailResps",
      key: "orderManageDetailResps",
      minWidth: 100,
      align: "center",
      render: (value) => (
        value?.length
      ),
    },
    {
      title: "Trạng thái",
      dataIndex: "status",
      key: "status",
      minWidth: 100,
      align: "center",
      render: (value) => (
        convertStatus(value)
      ),
    },
    {
      title: "Hành động",
      dataIndex: "action",
      key: "action",
      minWidth: 170,
      align: "center",
      render: (value, rows) => (
        <>
          {rows.status === "NEW" && (
            <Tooltip title="Huỷ đơn hàng" arrow>
              <IconButton>
                <GiCancel
                  style={{ fontSize: "20px" }}
                  onClick={() => handleDelete(rows.id, rows.status)}
                />
              </IconButton>
            </Tooltip>
          )}
          <span className="mx-2">
            {/* Code for the second icon button */}
          </span>
          <Tooltip title="Xem chi tiết" arrow>
            <IconButton>
              <BsEyeFill
                style={{ fontSize: "20px" }}
                onClick={() => handleGetDetail(rows.orderManageDetailResps)}
              />
            </IconButton>
          </Tooltip>
        </>
      ),
    },
  ];
  

    
  return (
    <>
      <Table
          columns={columns}
          dataSource={orderList}
          pagination={{
            defaultPageSize: 8,
            showSizeChanger: true,
            pageSizeOptions: ["8","10", "20", "30"],
          }}/>

      <Modal
        title="Chi tiết đơn hàng"
        centered
        open={open}
        footer={null}
        width={1000}
        height={800}
        onCancel={onClose}
      >
        {dataDetail && <RentalDetailOrder dataDetail={dataDetail} type={type} userIdChoose={userIdChoose} setLoadChat={setLoadChat} loadChat={loadChat} dataTemp={dataTemp}/>}
        <div className="flex justify-end mt-5">
          {(type === "CONFIRM") && (
            <>
              <Button
                className="mt-2  bg-[#1975D2] text-white "
                onClick={() => handleApprove()}

              >
                Đã nhận hàng
              </Button>
              <Button
                className="mt-2  bg-[#1975D2] text-white "
                onClick={handleReport}

              >
                Không nhận được hàng
              </Button>
              {/* <Button
                variant="contained"
                key="ok"
                type="primary"
                className="bg-[#1976d2] mt-2"
                size="large"
                style={{ marginRight: "60px" }}
              >
                Liên hệ
              </Button> */}
            </>

          )}

        </div>
      </Modal>
      <Modal
        title="Huỷ đơn hàng"
        open={openD}
        footer={null}
        okText="Xác nhận"
        cancelText="Quay lại"
        onCancel={() => hideModalD(false)}
      >
        <p>Bạn có chắc muốn huỷ order không?</p>
        <TextArea
          onChange={(e) => setDescriptionR(e.target.value)}
          placeholder="Điền lý do ở dây"
          value={descriptionR}
        ></TextArea>
        <div className="flex justify-end mt-5">
          {descriptionR?.length > 0 ? (
            <Button
            className="bg-[#1975D2] text-white "
              onClick={() => hideModalD(true)}
            >
              Có
            </Button>
          ) : (
            <Button className="bg-[#1975D2] text-white " disabled >
              Có
            </Button>
          )}
          <Button className="bg-[#1975D2] text-white " onClick={() => hideModalD(false)}>
            Không
          </Button>
        </div>
      </Modal>
      <Modal
        title="Xác nhận không nhận được hàng"
        open={openR}
        footer={null}
        okText="Xác nhận"
        cancelText="Quay lại"
        onCancel={() => hideModalR(false)}
      >
        <p>Bạn không nhận được sản phẩm thuê đúng không?</p>
        <div className="flex justify-end mt-5">
          <Button
            className="mt-2  bg-[#1975D2] text-white "
            onClick={() => hideModalR(true)}
          >
             Xác nhận
          </Button>
        </div>
      </Modal>
      <Modal
        title="Bạn đã nhận hàng thành công"
        open={openC}
        footer={null}
        okText="Xác nhận"
        cancelText="Quay lại"
        onCancel={() => setOpenC(false)}
      >
            <p>Quý khách nên kiểm tra sản phẩm cẩn thận và chụp hình/quay phim lại quá trình kiểm hàng để hạn chế rủi ro.</p>
	          <div className="flex justify-end mt-5">           
	              <Button
	                className="mt-2  bg-[#1975D2] text-white "
	                onClick={() => hideModalC(orderList[0]?.id, type)}
	              >
	                Có
	              </Button>
	            <Button  className="mt-2  bg-[#1975D2] text-white " onClick={() => setOpenC(false)}>
	              Không
	            </Button>
	          </div>
	        </Modal>
    </>
  );
}
