import {
  BankOutlined,
  DatabaseOutlined,
  HistoryOutlined,
  HomeOutlined,
  MessageOutlined,
  PullRequestOutlined,
  UnorderedListOutlined,
  UserOutlined,
} from "@ant-design/icons";
import { Layout, Menu } from "antd";
import { useNavigate } from "react-router-dom";
const { Sider } = Layout;

const Sidebar = ({ collapsed = false, setCollapsed }) => {
  const navigate = useNavigate();

  const menuItem = [
    {
      label: "Trang chủ",
      icon: <HomeOutlined />,
      onClick: () => {
        navigate("/");
      },
    },
    {
      label: "Người dùng",
      icon: <UserOutlined />,
      onClick: () => {
        navigate("/admin/user");
      },
    },
    {
      label: "Yêu cầu",
      icon: <PullRequestOutlined />,
      onClick: () => {
        navigate("/admin/request");
      },
    },

    {
      label: "Cửa hàng",
      icon: <BankOutlined />,
      onClick: () => {
        navigate("/admin/store");
      },
    },
    {
      label: "Danh mục",
      icon: <UnorderedListOutlined />,
      onClick: () => {
        navigate("/admin/category");
      },
    },
    {
      label: "Sản phẩm",
      icon: <DatabaseOutlined />,
      onClick: () => {
        navigate("/admin/product");
      },
    },
    {
      label: "Lịch sử giao dịch",
      icon: <HistoryOutlined />,
      onClick: () => {
        navigate("/admin/history-payment");
      },
    },
    {
      label: "Tin nhắn",
      icon: <MessageOutlined />,
      onClick: () => {
        navigate("/admin/message");
      },
    },
  ];

  return (
    <Sider theme="light" trigger={null} collapsible collapsed={collapsed}>
      <Menu
        theme="light"
        mode="inline"
        defaultSelectedKeys={[1]}
        items={menuItem}
      />
    </Sider>
  );
};

export default Sidebar;
