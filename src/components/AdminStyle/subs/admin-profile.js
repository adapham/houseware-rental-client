import { UserOutlined } from "@ant-design/icons";
import { Avatar, Dropdown } from "antd";
import React, { memo } from "react";
import { HiLogout } from "react-icons/hi";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { loadUserSuccess } from "../../../redux/Slide/authSlide";

function AdminProfile() {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const state = useSelector((state) => state?.auth?.loadUser?.user);
  console.log("current user", state);
  const { name = "", username = "", imageUrl } = state;

  const handleToDetailAdminPage = () => {
    navigate("/");
  };
  function handlerLogout() {
    localStorage.removeItem("access-token");
    localStorage.removeItem("user");
    navigate("/");
    dispatch(loadUserSuccess(null));
  }
  const items = [
    {
      key: 1,
      label: (
        <div className="font-semibold" onClick={handleToDetailAdminPage}>
          {name}
        </div>
      ),
    },
    {
      key: 2,
      label: (
        <div className="font-semibold" onClick={handleToDetailAdminPage}>
          <div
            className="flex items-center mr-10 cursor-pointer"
            onClick={handlerLogout}
          >
            <HiLogout style={{ fontSize: "30px" }} className="mr-2" />
            <span>Thoát</span>
          </div>
        </div>
      ),
    },
  ];
  return (
    <div className="h-full  px-2 rounded-md">
      <Dropdown
        menu={{
          items,
        }}
        placement="bottomLeft"
      >
        <div className="h-full flex items-center gap-4">
          <div className="font-semibold ">{username}</div>
          <Avatar
            src={imageUrl}
            shape="square"
            size="large"
            icon={<UserOutlined />}
          />
        </div>
      </Dropdown>
    </div>
  );
}

export default memo(AdminProfile);
