import { Button } from "antd";
import React, { memo } from "react";
import { useNavigate } from "react-router-dom";

function BackButton() {
  const navigate = useNavigate();
  const handleGoback = () => {
    navigate(-1);
    return;
  };
  return <Button onClick={handleGoback}>Trở về</Button>;
}

export default memo(BackButton);
