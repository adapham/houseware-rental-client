import { QueryClient, QueryClientProvider } from "@tanstack/react-query";
import React, { useState } from "react";

import { Button, Layout, Typography, theme } from "antd";
import Sidebar from "./subs/side-bar";

import {
  HomeOutlined,
  MenuFoldOutlined,
  MenuUnfoldOutlined,
} from "@ant-design/icons";
import { ToastContainer } from "react-toastify";
import { RecoilRoot } from "recoil";
import AdminProfile from "./subs/admin-profile";


const { Header, Sider, Content } = Layout;
const { Text } = Typography;
const AdminStyle = ({ component, title = "Trang chủ" }) => {
  const queryClient = new QueryClient({
    defaultOptions: {
      queries: {
        refetchOnWindowFocus: false,
      },
    },
  });

  const {
    token: { colorBgContainer },
  } = theme.useToken();

  const [collapsed, setCollapsed] = useState(false);

  return (
    <>
      <ToastContainer
        position="top-right"
        autoClose={3000}
        hideProgressBar={true}
        newestOnTop={true}
        closeOnClick={true}
        rtl={false}
        draggable={false}
        theme="colored"
      />
      <QueryClientProvider client={queryClient}>
        <RecoilRoot>
          <Layout>
            <Sidebar collapsed={collapsed} />
            <Layout>
              <Header
                style={{
                  padding: 0,
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "space-between", // Added to align items at the ends
                }}
                className="bg-[#dde8da]"
              >
                <div
                  style={{ display: "flex", alignItems: "center", gap: "16px" }}
                >
                  <Button
                    type="text"
                    icon={
                      collapsed ? <MenuUnfoldOutlined /> : <MenuFoldOutlined />
                    }
                    onClick={() => setCollapsed(!collapsed)}
                    style={{
                      fontSize: "16px",
                      width: "64px",
                      height: "64px",
                      // color: "#FFF",
                    }}
                  />
                  <Text
                    style={{
                      fontSize: "16px",
                      lineHeight: "64px",
                      // color: "#FFF",
                      fontWeight: 700,
                    }}
                  >
                    <HomeOutlined /> / {title}
                  </Text>
                </div>

                <AdminProfile />
              </Header>

              <Content
                style={{
                  minHeight: "calc(100vh - 56px)",
                  margin: "8px",
                  padding: "16px",
                  background: colorBgContainer,
                }}
              >
                {component}
              </Content>
            </Layout>
          </Layout>
        </RecoilRoot>
      </QueryClientProvider>
    </>
  );
};

export default AdminStyle;
