import { DatePicker } from "antd";
import dayjs from "dayjs";
import { isNull } from "lodash";
import queryString from "query-string";
import React, { memo, useEffect, useRef } from "react";
import { useLocation, useSearchParams } from "react-router-dom";

const { RangePicker } = DatePicker;

function AdminDateTimePicker() {
  const [searchParams, setSearchParams] = useSearchParams();
  const location = useLocation();

  const ref = useRef();

  useEffect(() => {
    const urlParams = new URLSearchParams(window.location.search);
    const fromDateParam = urlParams.get("fromDate");
    const toDateParam = urlParams.get("toDate");

    const fromDateDayjs = fromDateParam
      ? dayjs.unix(parseInt(fromDateParam, 10))
      : null;
    const toDateDayjs = toDateParam
      ? dayjs.unix(parseInt(toDateParam, 10))
      : null;

    ref.current.value = [fromDateDayjs, toDateDayjs];
  }, []);
  const onChange = async (dates) => {
    const currentQuery = queryString.parse(location.search);

    if (isNull(dates)) {
      setSearchParams({
        ...currentQuery,
        type: searchParams.get("type"),
      });
      return;
    }
    if (dates && dates.length === 2) {
      const [fromDate, toDate] = dates.map((date) => dayjs(date).unix());
      setSearchParams({
        ...currentQuery,
        fromDate,
        toDate,
        type: searchParams.get("type"),
      });
    }
  };

  return (
    <RangePicker
      placeholder={["Ngày bắt đầu", "Ngày kết thúc"]}
      ref={ref}
      onChange={onChange}
    />
  );
}

export default memo(AdminDateTimePicker);
