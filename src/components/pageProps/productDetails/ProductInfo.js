import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { addToCart } from "../../../redux/Slide/orebiSlice";
import TextField from "@mui/material/TextField";
import MuiInput from "@mui/material/Input";
import Stack from "@mui/material/Stack";
import Paper from "@mui/material/Paper";
import { styled } from "@mui/material/styles";
import { addProductToCard, getProductsInCard } from "../../../redux/apiRequestProduct";

import { FaStar, FaHeart, FaCheckCircle, FaRegGem } from "react-icons/fa";
import { toast } from "react-toastify";
import { Typography } from "antd";
const Input = styled(MuiInput)`
  width: 70px;
`;
const ProductInfo = ({ productInfo,storeData }) => {

  const user = useSelector((state) => state?.auth?.loadUser?.user);
  const months = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
  const dispatch = useDispatch();
  const [quantity, setQuantity] = React.useState(1);
  const [selectedMonth, setSelectedMonth] = useState(1);
  const [productDb, setProductDb] = useState([]);
  const products = useSelector((state) => state.orebiReducer?.products);
  const { Title } = Typography;
  const handleChangeMonth = (month) => {
    const value = month.target.value === "" ? 1 : Number(month.target.value);
    if(value > 36){
      setSelectedMonth(36);
      toast.warning("Tháng thuê không được vượt quá 36 tháng", {
        position: "top-right",
        autoClose: 800,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "light",
      });
    }else{
      setSelectedMonth(value);
    }

    
  };
  const handleInputChange = (event) => {
    const value = event.target.value === "" ? 1 : Number(event.target.value);
    if(value > productInfo?.quantity){
      setQuantity(productInfo?.quantity);
      toast.warning(`Số sản phẩm chỉ còn: ${productInfo?.quantity}`, {
        position: "top-right",
        autoClose: 2000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "light",
      });
    }else{
      setQuantity(value);
    }
    
  };
 
  const getProductInCard = async (id) => { 
    const response = await getProductsInCard(id);
    if (response) {
      setProductDb(response);
    }
  };
  useEffect(()=>{
    if(user?.id){
      getProductInCard(user?.id);
    }
  },[user?.id])
  useEffect(()=>{
    if(productDb && productInfo){
      const result = (productDb.filter((item) => item?.productId === productInfo?.id));
      if(result?.[0]){
        const rentalPeriod = result?.[0]?.rentalPeriod;
        const quantity = result?.[0]?.quantity;
        setSelectedMonth(rentalPeriod);
        setQuantity(quantity);
        
      }else{
        setQuantity(products?.[0]?.quantity || 1);
        setSelectedMonth(products?.[0]?.rentalPeriod || 1);
      }
      
    }
  },[productDb,productInfo])
  
  const DemoPaper = styled(Paper)(({ theme }) => ({
    width: 120,
    // height: 120,
    padding: theme.spacing(2),
    ...theme.typography.body2,
    textAlign: "center",
  }));
  
 
  
  const handleAddToCard = () => {
    
      if (user?.id) {
        if (user?.storeId !== productInfo?.storeId) {
        addItemsToCart({
          title: productInfo?.title,
          imageText: productInfo?.imageTextUri[0],
          price: productInfo?.price,
          depositPrice: productInfo.depositPrice,
          rentalPeriod: selectedMonth,
          productId: productInfo?.id,
          userId: user?.id,
          quantity: quantity,
          storeId: productInfo?.storeId,
          storeName:storeData?.title,
        });
        }else{
          toast.error("Chủ shop không thể mua sản phẩm của shop mình", {
            position: "top-right",
            autoClose: 2000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
            theme: "light",
          });
        }
        
      } else {
        dispatch(
          addToCart({
            productId: productInfo?.id,
            title: productInfo?.title,
            quantity: quantity,
            storeId:storeData?.storeId,
            storeName:storeData?.title,
            imageText: productInfo?.imageTextUri[0],
            rentalPeriod: selectedMonth,
            price: productInfo?.price,
            depositPrice: productInfo?.depositPrice,
            
            
          })
        );
        toast.success("Thêm sản phẩm thành công!", {
          position: "top-right",
          autoClose: 2000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
          theme: "light",
        });
      }
    
  };
  const addItemsToCart = async (product) => {
    const response = await addProductToCard(product);
    if (response) {
      toast.success(" Thêm sản phẩm thành công!", {
        position: "top-right",
        autoClose: 2000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "light",
      });
      dispatch(
        addToCart({
          productId: productInfo?.id,
          title: productInfo?.title,
          quantity: quantity,
          period: 1,
          imageText: productInfo?.imageTextUri[0],
          rentalPeriod: selectedMonth,
          price: productInfo?.price,
          depositPrice: productInfo?.depositPrice,
          colors: productInfo?.color,
          storeId: productInfo?.storeId,
          storeName:storeData?.title,
        })
      );
    }
  };
  const handleMonthClick = (month) => {
    setSelectedMonth(month);
  };
 
  const getStatusLabel = () => {
    switch (productInfo?.status) {
      case "MINT":
        return {
          label: "Mới",
          banner: "Mới",
          icon: <FaStar className="text-yellow-500" style={{fontSize:'25px'}} />,
        };
      case "GOOD":
        return {
          label: "Tốt",
          banner: "Tốt",
          icon: <FaHeart className="text-red-500" style={{fontSize:'25px'}} />,
        };
      case "VERY_GOOD":
        return {
          label: "Rất tốt",
          banner: "Rất tốt",
          icon: <FaCheckCircle className="text-green-500" style={{fontSize:'25px'}}  />,
        };
      case "NEAR_MINT":
        return {
          label: "Như mới",
          banner: "Như mới",
          icon: <FaRegGem className="text-blue-500" style={{fontSize:'25px'}} />,
        };
      default:
        return {
          label: "",
          banner: "",
          icon: <></>,
        };
    }
  };

  const statusLabel = getStatusLabel();
  
  return (
    <div className="flex flex-col gap-5">
      
      <div className="flex">
      <div className="mr-10">
       
        <Title level={3}>{productInfo?.title}</Title>
      </div>
      <div className="status-banner bg-[#dde8da] text-[#57ad3f] text-center px-6 py-1 rounded-full inline-block">
            {statusLabel.banner}
      </div>
      </div>
      <div className="flex flex-row flex-wrap ">
        {months.map((month, index) => (
          <p
            key={index}
            className={`hover:bg-[#f1f6f0] hover:border-[#adc5a0] text-center text-base border-2  border-gray-300 rounded-lg p-2 cursor-pointer m-2 ${selectedMonth === month
              ? "border-[#57ad3f] text-[#57ad3f] bg-[#dde8da]"
              : ""
              }`}
            // style={{ width: 'fit-content' }}
            onClick={() => handleMonthClick(month)}
          >
            {month} tháng
          </p>
        ))}
      </div>
      <div className="ml-2">
        <TextField
          className="w-36"
          id="outlined-number"
          type="number"
          label="Số tháng thuê"
          value={selectedMonth}
          onChange={(event) => handleChangeMonth(event)}
          size="small"
          inputProps={{
            min: 1,
            max: 100,
            type: "number",
          }}
        />
      </div>

      <Stack direction="row" className="flex justify-between mx-2" spacing={2}>
        <DemoPaper style={{ width: "300px" }} square={false}>
          <div className="text-xl font-semibold">Tiền thuê hàng tháng</div>
          <div className="text-base font-normal mt-2">
            {(productInfo?.price * selectedMonth * quantity).toLocaleString()}{" "}
            VND
          </div>
        </DemoPaper>
        <DemoPaper style={{ width: "300px" }} square>
          <div className="text-xl font-semibold">Tiền cọc có thể hoàn lại</div>
          <div className="text-base font-normal mt-2">
            {(productInfo?.depositPrice * quantity).toLocaleString()} VND
          </div>
        </DemoPaper>
      </Stack>

      <div className="text-xl font-medium ml-2">
        <span>
          Thể Loại:{" "}
          <span className="text-base font-medium ml-2">
            {productInfo?.category?.categoryName}
          </span>
        </span>
      </div>

      <div className="flex items-center ml-2">
        <div className="">
          {/* <span >Số lượng</span> */}
          <TextField
            className="w-24"
            id="outlined-number"
            type="number"
            label="Số Lượng"
            value={quantity}
            onChange={(event) => handleInputChange(event)}
            size="small"
            inputProps={{
              min: 1,
              
              type: "number",
            }}
          />
        </div>
        {productInfo?.quantity == 0 ? (
          <button
            className="py-2 ml-10 px-10 border-solid w-[40%] border-2 border-[#e74c3c] text-[#e74c3c] bg-[#bdc3c7] rounded font-semibold uppercase duration-300"
          >
            Hết hàng
          </button>
        ) : (
          <button
            onClick={handleAddToCard}
            className="py-2 ml-10 px-10 border-solid w-[40%] border-2 border-[#57ad3f] text-[#57ad3f] bg-[#dde8da] rounded font-semibold uppercase duration-300"
          >
            Thuê ngay
          </button>
        )}
      </div>
    </div>
  );
};

export default ProductInfo;
