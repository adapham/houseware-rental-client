import React, { useEffect, useRef, useState } from 'react'
import { Box, Modal, Typography } from "@mui/material";

  import {
    GoogleMap,
    Marker,
    DirectionsRenderer,
    useJsApiLoader,
    Autocomplete,
  } from '@react-google-maps/api';
export  const ViewMap=({handleClose,open,destination})=> {
    const [map, setMap] = useState(null);
  const [directionsResponse, setDirectionsResponse] = useState(null);
  const [distance, setDistance] = useState('');
  const [duration, setDuration] = useState('');
  const originRef = useRef(null);
  const destinationRef = useRef(null);
  const [currentLocation, setCurrentLocation] = useState(null);
  const [nearbyLocations, setNearbyLocations] = useState([]);
  const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 800,
    height:600,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    
  };
  const { isLoaded } = useJsApiLoader({
    googleMapsApiKey: "AIzaSyAeRIWhAjT1qeUcJ_sAQOmmQGBs9RPrj7M",
    libraries: ['places'],
  });
  useEffect(() => {
    navigator.geolocation.getCurrentPosition((position) => {
      setCurrentLocation({ lat: position.coords.latitude, lng: position.coords.longitude });
    });
  }, []);

  useEffect(() => {
    if (isLoaded) {
      if (currentLocation) {
        const directionsService = new window.google.maps.DirectionsService();
        const directionsRequest = {
          origin: currentLocation,
          destination: destination,
          travelMode: window.google.maps.TravelMode.DRIVING,
        };
       
        directionsService.route(directionsRequest, (response, status) => {
          console.log('Directions response status:', status);
          if (status === window.google.maps.DirectionsStatus.OK) {
            setDirectionsResponse(response);
            console.log(response,"hoang");
          } else {
            console.error( status,"hoang");
          }
        });
        
      }
    }
  }, [currentLocation, isLoaded]);


  const center = currentLocation || { lat: 21.028511, lng: 105.804817 };

  async function calculateRoute() {
    if (!originRef.current.value || !destinationRef.current.value) {
      return;
    }

    const directionsService = new window.google.maps.DirectionsService();
    const results = await directionsService.route({
      origin: originRef.current.value,
      destination: destinationRef.current.value,
      travelMode: window.google.maps.TravelMode.DRIVING,
    });
    
    setDirectionsResponse(results);
    setDistance(results.routes[0].legs[0].distance.text);
    setDuration(results.routes[0].legs[0].duration.text);
  }

 

  if (!isLoaded) {
    return <div>Loading...</div>;
  }
  return (
    <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        
      <Box sx={style}>
      <Box position='absolute' left={0} top={0} right={0} bottom={0} display='flex' justifyContent='center' alignItems='center'>
        {/* Google Map Box */}
        <Box
          width='100%'
          height='100%'
          maxWidth='800px'
          maxHeight='800px'
          borderRadius='lg'
          overflow='hidden'
        >
          <GoogleMap
            center={center}
            zoom={15}
            mapContainerStyle={{ width: '100%', height: '100%' }}
            options={{
              zoomControl: false,
              streetViewControl: false,
              mapTypeControl: false,
              fullscreenControl: false,
            }}
            onLoad={map => setMap(map)}
          >
            {currentLocation && <Marker position={currentLocation} />}
            {directionsResponse && (
              <DirectionsRenderer directions={directionsResponse} />
            )}

            {nearbyLocations.map(location => (
              <Marker
                key={location.id}
                position={{ lat: location.lat, lng: location.lng }}
                title={location.name}
              />
            ))}
          </GoogleMap>
        </Box>
      </Box>
        </Box>
      </Modal>
  )
}
export default ViewMap;
