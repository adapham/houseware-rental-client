import React, { useEffect, useState } from "react";
import { BsGridFill } from "react-icons/bs";


import { useDispatch } from "react-redux";
import { updateFilter } from "../../../redux/Slide/productSlice";
import { FormControl, InputLabel, MenuItem, Select } from "@mui/material";


const ProductBanner = ({ itemsPerPageFromBanner,pageable }) => {
  const dispatch = useDispatch();
  const [selected, setSelected] = useState("BEST_SELLER");
  useEffect(() => {
    dispatch(updateFilter({ sortby: selected }));
  }, [dispatch, selected]);
  const [girdViewActive, setGridViewActive] = useState(true);
  const [listViewActive, setListViewActive] = useState(false);
  const itemsPerPageOptions = [12, 24, 36, 48];
  const [itemsPerPage, setItemsPerPage] = useState(12);

  useEffect(()=>{
    const pageSelect = pageable?.pageSize;
      if(pageSelect){
        setItemsPerPage(pageSelect)
      }
  },[pageable])

  useEffect(() => {
    const gridView = document.querySelector(".gridView");


    gridView.addEventListener("click", () => {
      setListViewActive(false);
      setGridViewActive(true);
    });

  }, [girdViewActive, listViewActive]);
  const handleChange = (event) => {
    setSelected(event.target.value);
  };
  return (
    <div className="w-full flex flex-col md:flex-row md:items-center justify-between">
      <div className="flex items-center gap-4">
        <span
          className={`${girdViewActive
              ? "bg-primeColor text-white"
              : "border-[1px] border-gray-300 text-[#737373]"
            } w-8 h-8 text-lg flex items-center justify-center cursor-pointer gridView`}
        >
          <BsGridFill />
        </span>
     
      </div>
      <div className="flex items-center gap-2 md:gap-6 mt-4 md:mt-0">
        <div className="flex items-center gap-2 text-base text-[#767676] relative">

          <FormControl variant="outlined" size="small">
            <InputLabel id="sort-label">Sắp xếp theo</InputLabel>
            <Select
              labelId="sort-label"
              id="sort-select"
              value={selected}
              onChange={handleChange}
              label="Sắp xếp theo"
              className="w-32 md:w-52 border-[1px] border-gray-200 py-1 px-4 cursor-pointer text-primeColor text-base block dark:placeholder-gray-400"
            >
              <MenuItem value="BEST_SELLER">Thuê nhiều nhất</MenuItem>
              <MenuItem value="NEW_ARRIVAL">Mới nhất</MenuItem>
            </Select>
          </FormControl>

        </div>
        <div className="flex items-center gap-2 text-[#767676] relative">
        
          <FormControl variant="outlined" size="small">
            <InputLabel id="items-per-page-label">Hiển thị</InputLabel>
            <Select
              labelId="items-per-page-label"
              id="items-per-page-select"
              value={itemsPerPage}
              onChange={(e) => itemsPerPageFromBanner(+e.target.value)}
              label="Hiển thị"
              className="w-25  border-[1px] border-gray-200 py-1 px-4 cursor-pointer text-primeColor text-base block dark:placeholder-gray-400"
            >
              {itemsPerPageOptions.map((option) => (
                <MenuItem key={option} value={option}>
                  {option}
                </MenuItem>
              ))}
            </Select>
          </FormControl>
         
        </div>
      </div>

    </div>
  );
};

export default ProductBanner;
