import React from "react";
import { BiCaretDown } from "react-icons/bi";

const NavTitle = ({ title, icons }) => {
  return (
    <div className="flex items-center justify-between pb-4">
      {icons ? (
        <>
          <div className="flex flex-col">
            <h3 className="text-3xl font-bold font-semibold text-primeColor ">{title}</h3> 
           
          </div>
          {icons && <BiCaretDown />}
        </>
      ) : (
        <>
          <div className="flex flex-col">
            <h3 className="text-3xl font-bold font-semibold text-primeColor ">{title}</h3> 
            
            
          </div>
          {icons && <BiCaretDown />}
        </>
      )}
    </div>
  );
};

export default NavTitle;
