import React, { useEffect } from "react";
import NavTitle from "./NavTitle";
import Button from '@mui/material/Button';
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';

import Slider from '@mui/material/Slider';
import MuiInput from '@mui/material/Input';

import { styled } from '@mui/material/styles';
import { useDispatch } from "react-redux";
import { updateFilter } from "../../../../redux/Slide/productSlice";
import { getMinMaxPrice } from "../../../../redux/apiRequestProduct";

const Input = styled(MuiInput)`
  width: 42px;
`;
const Price = () => {
  const dispatch = useDispatch();
  const [min, setMin] = React.useState();
  const [max, setMax] = React.useState();
  const [fromValue, setFromValue] = React.useState();
  const [toValue, setToValue] = React.useState();
  const handleSliderChange = (event, newValue) => {
    setFromValue(newValue[0]);
    setToValue(newValue[1]);
  };
  useEffect(() => {
    getMinMax();
    
  }, [])
  const getMinMax = async () => {
    const response = await getMinMaxPrice();

    if (response) {
      setMin(response.min / 1000);
      setMax(response.max / 1000);
      setFromValue(response.min / 1000);
      setToValue(response.max / 1000);
      dispatch(updateFilter({
        price: {
          from: response.min ,
          to: response.max ,
        },
      }))
    }
  }
  const handleInputChange = (event, type) => {
    const value = event.target.value === '' ? 0 : Number(event.target.value);
    if (type === 'from') {
      setFromValue(value);
    } else {
      setToValue(value);
    }
  };

  const handleBlur = (type) => {
    if (type === 'from') {
      if (fromValue < 0) {
        setFromValue(0);
      } else if (fromValue > toValue) {
        setFromValue(toValue);
      }
    } else {
      if (toValue > max) {
        setToValue(max);
      } else if (toValue < fromValue) {
        setToValue(fromValue);
      }
    }
  };
  return (
    <div className="w-full ">
      <div className="py-4 mx-4">
        <NavTitle title="Tìm kiếm theo giá" icons={false} />
        <h3 className="font-semibold text-primeColor text-sm  mb-2">Sử dụng thanh tìm kiếm hoặc nhập giá trị </h3>
            <h3 className="text-sm font-bold font-semibold text-primeColor mb-5 ">(Ví dụ:10 = 10.000đ)</h3> 
        <Box sx={{ width: 250 }}>
          <Grid container spacing={2} alignItems="center">
            <Grid item xs={12} md={12}>
              <Slider
                min={min}
                max={max}
                value={[fromValue, toValue]}
                onChange={handleSliderChange}
                aria-labelledby="range-slider"
                sx={{ color: '#57ad3f' }} // Set slider track color
              />
            </Grid>
            <Box sx={{ width: 300 }}>
    
                
                <Grid container spacing={2} alignItems="center" className="flex justify-between">
                  <Grid item xs={6} md={6}>
                    <div className="flex items-center">
                      <span className="mr-2">Từ: </span>
                      <Input
                        style={{ width: '80px', border: '1px solid #dde8da', borderRadius: '5px' }}
                        value={fromValue}
                        onChange={(event) => handleInputChange(event, 'from')}
                        onBlur={() => handleBlur('from')}
                        inputProps={{
                          min: min,
                          max: max,
                          type: 'number',
                          'aria-labelledby': 'range-slider-from',
                        }}
                      />
                    </div>
                  </Grid>
                  <Grid item xs={6} md={6}>
                    <div className="flex items-center">
                      <span className="mr-2">Đến:</span>
                      <Input
                   
                        style={{ width: '80px', border: '1px solid #dde8da', borderRadius: '5px' }}
                        value={toValue}
                        onChange={(event) => handleInputChange(event, 'to')}
                        onBlur={() => handleBlur('to')}
                        inputProps={{
                          min: min,
                          max: max,
                          type: 'number',
                          'aria-labelledby': 'range-slider-to',
                        }}
                      />
                    </div>
                  </Grid>
                </Grid>

            
            </Box>
            <Grid item xs={8} className="flex justify-end mt-2">
              <div className=" text-[#57ad3f] bg-[#dde8da] ">
                <Button variant="outlined" onClick={(e) => {
                  dispatch(updateFilter({
                    price: {
                      from: fromValue * 1000,
                      to: toValue * 1000,
                    },
                  }))
                }}>Lọc</Button>
              </div>
            </Grid>
          </Grid>
        </Box>
      </div>
    </div>

  );
};

export default Price;
