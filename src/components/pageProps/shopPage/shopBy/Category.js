import React, { useEffect, useState } from "react";
// import { FaPlus } from "react-icons/fa";
import { useCallback } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getCategories } from "../../../../redux/Slide/categorySlice";
import {
  updateFilter,
  updatePageble,
} from "../../../../redux/Slide/productSlice";
import { getAllCateory } from "../../../../redux/apiRequestProduct";
import NavTitle from "./NavTitle";
import { useLocation } from "react-router-dom";

const Category = () => {
  const dispatch = useDispatch();
  const categoryStore = useSelector((state) => state.category);
  const [highlightedCategoryId, setHighlightedCategoryId] = useState(0);
  const { categories } = categoryStore;
  const location = useLocation();
  const { id } = location?.state || {};

  const getAllCategories = useCallback(async () => {
    const response = await getAllCateory();

    if (response) {
      dispatch(getCategories(response));
    }
  }, [dispatch]);
  const handleFilter = (id) => {

    setHighlightedCategoryId(id);
    dispatch(updateFilter({ categoryId: id }));
    dispatch(updatePageble({ pageIndex: 1, pageSize: 12 }));
  };
  useEffect(()=>{
    dispatch(updateFilter({ categoryId: null }));
    if(id){
      setHighlightedCategoryId(id);
    }
 },[id])

  useEffect(() => {
    getAllCategories();
  }, [getAllCategories]);

  return (
    <div className="w-full border-2">
      <div className="py-4 mx-4">
        <NavTitle title="Danh mục" icons={false} />
        <div className="cursor-pointer  ">
          <ul className="flex flex-col gap-4 text-sm lg:text-base text-[#767676]">
            <li
              onClick={() => {
                dispatch(updateFilter({ categoryId: null }));
                setHighlightedCategoryId(0);
              }}
              className={
                highlightedCategoryId === 0
                  ? "border-b-[1px] border-b-[#57ad3f] rounded p-2 flex items-center justify-between bg-[#dde8da] hover:bg-[#f1f6f0]"
                  : "border-b-[1px] border-b-[#F0F0F0] p-2 flex items-center justify-between hover:bg-[#f1f6f0]"
              }
            >
              <span className="text-xl font-medium text-gray-900 tracking-wide">
                Tất cả sản phẩm
              </span>
            </li>
            {categories.map(({ id, categoryName }) => (
              <li
                key={id}
                onClick={() => handleFilter(id)}
                className={
                  highlightedCategoryId === id
                    ? "border-b-[1px] border-b-[#57ad3f] rounded p-2 flex items-center justify-between bg-[#dde8da] hover:bg-[#f1f6f0]"
                    : "border-b-[1px] border-b-[#F0F0F0] p-2 flex items-center justify-between hover:bg-[#f1f6f0]"
                }
              >
                <span className="text-xl  font-medium text-gray-900 tracking-wide">
                  {categoryName}
                </span>
              </li>
            ))}
          </ul>
        </div>
      </div>
    </div>
  );
};

export default Category;
