import React, { useState } from "react";
import { DatePicker, Space } from "antd";
import NavTitle from "./NavTitle";
import { convertDateFormToTimestamp } from "../../../../utils/formatConfig";
import { useDispatch } from "react-redux";
import { updateFilter } from "../../../../redux/Slide/productSlice";
const { RangePicker } = DatePicker;

const DateFromTo = () => {
  const dispatch = useDispatch();
  const [value, setValue] = useState({
    startDate: null,
    endDate: null,
  });
  
  const handleOnChange = (range) => {
    setValue(range);
    const dateFrom = value && value[0] && convertDateFormToTimestamp(value[0].$d);
    const dateTo = value && value[1] && convertDateFormToTimestamp(value[1].$d);
    const rentalDate = {from:dateFrom,to:dateTo};

    dispatch(updateFilter({rentalDate}))
  };
  

  
  return (
    <div className="w-full">
      <NavTitle title="Find by rental date" />
      <Space direction="vertical" size={12}>
        <RangePicker ovalue={value} onChange={handleOnChange}/>
      </Space>
    </div>
  );
};

export default DateFromTo;
