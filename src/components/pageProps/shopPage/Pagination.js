import React, { useState } from "react";
import ReactPaginate from "react-paginate";
import Product from "../../home/Products/Product";
import { useDispatch} from "react-redux";
import { updatePageble } from "../../../redux/Slide/productSlice";

function Items({ currentItems }) {
   
  return (
    <>
      {currentItems &&
        currentItems.map((item) => (
          <div key={item?.id} className="w-full">
            <Product
              _id={item?.id}
              img={item?.imageTextUri[0]}
              productName={item?.title}
              price={item?.price}
              description={item?.description}
              depositPrice={item?.depositPrice}
              badge={item?.createdTime}
              des={item?.description}
              categoryName={item?.categoryName}
              storeId={item?.storeId}
              quantity={item?.quantity}
              status={item?.status}
              storeName={item?.storeName}
            />
          </div>
        ))}
    </>
  );
  }

const Pagination = (props) => {
  const dispatch = useDispatch();


  const handlePageClick = (event) => {
    const pageIndex = event.selected;
    dispatch(
      updatePageble({
        pageIndex: pageIndex+1,
      })
    );
  };

  return (
    <div>
      <div className="grid grid-cols-1 md:grid-cols-2 xl:grid-cols-3 gap-10 mdl:gap-4 lg:gap-10">
        {/* <Items currentItems={currentItems} /> */}
        <Items currentItems={props?.products} />
      </div>
      <div className="flex flex-col mdl:flex-row justify-center mdl:justify-between items-center">
        <ReactPaginate
          nextLabel=""
          onPageChange={handlePageClick}
          pageRangeDisplayed={3}
          marginPagesDisplayed={2}
          pageCount={props?.totalPage}
          previousLabel=""
          pageLinkClassName="w-9 h-9 border-[1px] border-lightColor hover:border-gray-500 duration-300 flex justify-center items-center"
          pageClassName="mr-6"
          containerClassName="flex text-base font-semibold font-titleFont py-10"
          activeClassName="bg-black text-white"
        />

        <p className="text-base font-semibold text-lightText">
          Hiển thị trang {" "}
          {props?.pageable?.pageIndex === 1 ? 1 : props?.pageable?.pageIndex} - {" "}
          {props?.totalPage} / {props?.pageable?.pageSize} sản phẩm
        </p>

      </div>
    </div>
  );
};

export default Pagination;
