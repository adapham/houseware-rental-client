import * as React from "react";
import Box from "@mui/material/Box";
import Tab from "@mui/material/Tab";
import TabContext from "@mui/lab/TabContext";
import TabList from "@mui/lab/TabList";
import TabPanel from "@mui/lab/TabPanel";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import { Button, Col, List, Row } from "antd";
import { Avatar, IconButton, ListItem, Tooltip } from "@mui/material";
import Rating from "@mui/material/Rating";
import Typography from "@mui/material/Typography";
import TextArea from "antd/es/input/TextArea";
import { formatDate } from "../../../utils/formatConfig";
import {
  getCommentReview,
  getReviewDelete,
  getReviewEdit,
  getReviewsProduct,
} from "../../../redux/apiRequestProduct";
import { useSelector } from "react-redux";
import { AiOutlineEdit } from "react-icons/ai";
import { MdOutlineDeleteForever } from "react-icons/md";

import { Modal } from "antd/es";
import { toast } from "react-toastify";
export default function TabDetail({
  productInfo,
  reviewInfo,
  setCommentSuccess,
  commentSuccess,
  reload,
  setReload
}) {
  const user = useSelector((state) => state?.auth?.loadUser?.user);
  const idUser = user?.id;
  const [value, setValue] = React.useState("1");
  const [valueRate, setValueRate] = React.useState(5);
  const [description, setDescription] = React.useState();
  const [count,setCount]=React.useState(0);
  const [openD, setOpenD] = React.useState(false);
  const [openE, setOpenE] = React.useState(false);
  const [idReview, setIdReview] = React.useState();
  const [contentReview, setContentReview] = React.useState();
  const [starReview, setStarReview] = React.useState();
  const showModalD = () => {
    setOpenD(true);
  };
  const showModalE = () => {
    setOpenE(true);
  }
  const hideModalE = (status) => {
    setOpenE(false);
    if (status) {
      const inforReview = {
        productId: productInfo?.id,
        userId: idUser,
        content: contentReview,
        rating: starReview
      }
      getEdit(idReview, inforReview);

    }
  };
  const hideModalD = (status) => {
    setOpenD(false);
    if (status) {
      getDelete(idReview);

    }
  };
  const getDelete = async (id) => {
    const response = await getReviewDelete(id);
    if (response) {
      setReload(!reload)
      toast.success("Xoá bình luận thành công", {
        position: "top-right",
        autoClose: 2000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "light",
      });
    }
  };
  const getEdit = async (id, infor) => {
    const response = await getReviewEdit(id, infor);

    if (response.status != 400) {
      setReload(!reload)
      toast.success("Sửa bình luận thành công", {
        position: "top-right",
        autoClose: 2000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "light",
      });
     
    }else{
      toast.success("Không thể chỉnh sửa", {
        position: "top-right",
        autoClose: 2000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "light",
      });
    }

  };
  const handleDelete = (id) => {
    setIdReview(id);
    showModalD(id);
  };
  const handleChange = (event, newValue) => {
    setValue(newValue);
  };
  function createData(name, calories) {
    return { name, calories };
  }
  const handleComment = () => {
    const params = {
      productId: productInfo?.id,
      userId: user?.id,
      content: description,
      rating: valueRate,
    };
    reviewComment(params);
  };
  const rows = [
    createData("Kích thước", productInfo.dimensions),
    createData("Cân nặng", productInfo.weight),
    createData("Dung tích", productInfo.capacity),
    createData("Công suất", productInfo.wattage),
  ];

  const reviewComment = async (infor) => {
    const response = await getCommentReview(infor);
    if (response) {
      setCommentSuccess(!commentSuccess);
      setDescription("");
    }
  };
  const handleEdit = (id, productId, rating, content,count) => {
    setStarReview(rating);
    setContentReview(content);
    setCount(count);
    setIdReview(id);
    showModalE(id);
   
  };
  

  return (
    <div className="mb-10">
      <Box sx={{ width: "100%", typography: "body1" }}>
        <TabContext value={value}>
          <Box sx={{ borderBottom: 1, borderColor: "divider" }}>
            <TabList
              onChange={handleChange}
              aria-label="lab API tabs example"
              className="bg-[#f5f5f5]"
              value={value}
            >
              <Tab
                sx={{ width: "20%", flexShrink: 0, fontSize: "15px" }}
                label="Mô tả sản phẩm"
                value="1"
              />
              <Tab
                sx={{ width: "20%", flexShrink: 0, fontSize: "15px" }}
                label="Thông số kỹ thuật"
                value="2"
              />
              <Tab
                sx={{ width: "25%", flexShrink: 0, fontSize: "15px" }}
                label="Ý kiến nhận xét/Đánh giá"
                value="3"
              />
            </TabList>
          </Box>
          <TableContainer component={Paper}>
            <TabPanel value="1">
              <div
                dangerouslySetInnerHTML={{ __html: productInfo.description }}
              />
            </TabPanel>
          </TableContainer>

          <TableContainer component={Paper}>
            <TabPanel value="2">
              <Table sx={{ minWidth: 650 }} aria-label="simple table">
                <TableBody>
                  {rows.map((row, index) => (
                    <TableRow key={index} sx={{ " td": { border: 0 } }}>
                      <TableCell component="th" scope="row">
                        {row.name}
                      </TableCell>
                      <TableCell
                        component="th"
                        scope="row"
                        className=""
                        align="right"
                      >
                        {row.calories}
                      </TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </TabPanel>
          </TableContainer>

          <TableContainer component={Paper}>
            <TabPanel value="3">
              <Row gutter={100} className="w-full">
                <Col span={12}>
                  <Typography variant="h5" gutterBottom>
                    {reviewInfo?.content?.length} bình luận cho sản phẩm
                  </Typography>
                  <List>
                    {reviewInfo &&
                      reviewInfo?.content?.map((item) => (
                        <ListItem key={item?.id} disablePadding>
                          <Row gutter={10}>
                            <Col className="flex justify-center items-center mx-2">
                              <Avatar
                                alt="Remy Sharp"
                                src={item?.imageUrl}
                              />
                            </Col>
                            <Col>
                              <div>
                                <div className="flex justify-center items-center mx-2">
                                  <span className="font-bold text-base mr-2">
                                    {item?.userName}
                                  </span>
                                  {formatDate(item?.createdTime)}

                                  <Rating
                                    className="ml-2"
                                    name="read-only"
                                    value={item?.rating}
                                    readOnly
                                  />
                                  {/* {item?.userId === idUser && (
                                    <>
                                      <Tooltip title="Sửa bình luận" arrow>
                                        <IconButton>
                                          <AiOutlineEdit
                                            style={{ fontSize: "20px" }}
                                            onClick={() =>
                                              handleEdit(
                                                item.id,
                                                item?.productId,
                                                item?.rating,
                                                item?.content,
                                                item?.count
                                              )
                                            }
                                          />
                                        </IconButton>
                                      </Tooltip>
                                      <Tooltip title="Xoá bình luận" arrow>
                                        <IconButton>
                                          <MdOutlineDeleteForever
                                            style={{ fontSize: "20px" }}
                                            onClick={() =>
                                              handleDelete(item.id)
                                            }
                                          />
                                        </IconButton>
                                      </Tooltip>
                                    </>
                                  )} */}
                                </div>
                              </div>

                              <div className="mt-2">
                                <Typography gutterBottom>
                                  {item?.content}
                                </Typography>
                              </div>
                            </Col>
                          </Row>
                        </ListItem>
                      ))}
                  </List>
                </Col>
                {/* {user?.id && (
                  <Col span={12}>
                    <Typography variant="h4" gutterBottom>
                      Bình luận
                    </Typography>
                    <div className="flex justify-start">
                      <div>
                        <Typography gutterBottom>Điểm của bạn:</Typography>
                      </div>
                      <div>
                        <Rating
                          name="simple-controlled"
                          value={valueRate}
                          onChange={(event, newValue) => {
                            setValueRate(newValue);
                          }}
                        />
                      </div>
                    </div>
                    <TextArea
                      onChange={(e) => setDescription(e.target.value)}
                      placeholder="Bình luận ở đây"
                    ></TextArea>

                    <button
                      className="mt-5 w-40 h-10 duration-300 border-2 border-[#57ad3f] text-[#57ad3f] bg-[#dde8da] rounded"
                      onClick={handleComment}
                    >
                      Bình luận
                    </button>
                  </Col>
                )} */}
              </Row>
            </TabPanel>
          </TableContainer>
        </TabContext>
      </Box>
      <Modal
        title="Xoá bình luận"
        open={openD}
        footer={null}
        okText="Xác nhận"
        cancelText="Quay lại"
        onCancel={hideModalD}
      >
        <p>Bạn có chắc muốn xoá bình luận không?</p>
        <div className="flex justify-end mt-5">
          <Button className="bg-[#1975D2] text-white" onClick={() => hideModalD(true)}>
            OK
          </Button>
          <Button className="bg-[#1975D2] text-white" onClick={() => hideModalD(false)}>
            Cancel
          </Button>
        </div>
      </Modal>
      <Modal
        title="Chỉnh sửa bình luận"
        open={openE}
        footer={null}
        okText="Xác nhận"
        cancelText="Quay lại"
        onCancel={() => hideModalE(false)}
      >
        <div className="flex justify-start">
          <div>
            <Typography gutterBottom>Rating:</Typography>
          </div>
          <div>
            <Rating
              name="simple-controlled"
              value={starReview}
              onChange={(event, newValue) => {
                setStarReview(newValue);
              }}
            />
          </div>
        </div>
        <TextArea
          onChange={(e) => setContentReview(e.target.value)}
          placeholder="Nội Dung"
          value={contentReview}
        ></TextArea>

        <div className="flex justify-end mt-5">
          {count >=2 ?(
            <Button className="bg-[#1975D2] text-white" disabled>
            Chỉnh sửa
          </Button>
          ):(

            <Button className="bg-[#1975D2] text-white" onClick={() => hideModalE(true)}>
              Chỉnh sửa
            </Button>
          )}
          <Button className="bg-[#1975D2] text-white" onClick={() => hideModalE(false)}>
            Huỷ
          </Button>
        </div>
      </Modal>
    </div>
  );
}
