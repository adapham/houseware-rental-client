import {Col, Row} from "antd";
import React from "react";
import Accordion from "@mui/material/Accordion";
import AccordionDetails from "@mui/material/AccordionDetails";
import AccordionSummary from "@mui/material/AccordionSummary";
import Typography from "@mui/material/Typography";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import {List, ListItem} from "@mui/material";
import {BsFacebook, BsFillTelephoneFill} from "react-icons/bs";
import {FiMapPin} from "react-icons/fi";
import {SiZalo} from "react-icons/si";

import ViewMap from "../productDetails/ViewMap";

export default function ShortDetail({productInfo}) {

  const [expanded, setExpanded] = React.useState(false);
  const handleChange = (panel) => (event, isExpanded) => {
    setExpanded(isExpanded ? panel : false);
  };
  const handleClose = () => setOpen(false);
  const [open, setOpen] = React.useState(false);
  const handleOpen = () => setOpen(true);

  return (
    
    <Row gutter={16}>
      <Col span={12}>
        <Accordion
          expanded={expanded === "panel1"}
          onChange={handleChange("panel1")}
        >
          <AccordionSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls="panel1bh-content"
            id="panel1bh-header"
            className="flex justify-between"
          >
            <Typography sx={{ width: "80%", flexShrink: 0, fontSize: "18px" }}>
              Thông tin liên hệ
            </Typography>
            <Typography sx={{ color: "text.secondary" }}>
              xem chi tiết
            </Typography>
          </AccordionSummary>
          <AccordionDetails>
            <Typography>
              <List>
                  <ListItem disablePadding >
                  <div className="flex justify-between w-full">
                    <div>{productInfo?.store?.title}</div>
                    <div>
                      <div className="flex justify-center items-center">
                        <div div className="m-1 flex justify-center">
                          <a href="https://zalo.me/g/ndkiik950" target="_blank">
                                <SiZalo style={{ fontSize: "24px"}}/>
                          </a>
                        </div>
                        <div className="m-1 flex justify-center">
                          <a href="https://www.facebook.com/khacbao07" target="_blank">
                                <BsFacebook style={{ fontSize: "24px"}}/>
                          </a>
                        </div>
                        <div className="m-1 ml-3 flex justify-center items-center">
                          <BsFillTelephoneFill style={{ fontSize: "24px" }}/>
                          <div className="ml-2">{productInfo?.store?.phone}</div>
                        </div>
                      </div>
                    </div>
                  </div>
                </ListItem>

                
              </List>
            </Typography>
          </AccordionDetails>
        </Accordion>
      </Col>
      <Col span={12}>
        
          <Accordion expanded={expanded === "panel1"}>
            <AccordionSummary
              aria-controls="panel1bh-content"
              id="panel1bh-header"
              className="flex justify-between"
            >
              <div className="flex justify-between w-full">
                <Typography
                  sx={{ width: "80%", flexShrink: 0, fontSize: "15px" }}
                >
                  Địa chỉ: {productInfo?.store?.address}
                </Typography>
                <div className="flex">
                  <span className="m-1">
                    <FiMapPin />
                  </span>
                  <div onClick={handleOpen}>Xem bản đồ</div>
                </div>
              </div>
            </AccordionSummary>
          </Accordion>
        
      </Col>
      <ViewMap handleClose={handleClose} open={open} destination={"Hà Nội"}/>
    </Row>
  );
}
