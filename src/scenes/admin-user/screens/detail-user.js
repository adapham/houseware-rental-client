import { ExclamationCircleFilled } from "@ant-design/icons";
import { TextField } from "@material-ui/core";
import {
  Button,
  Collapse,
  Divider,
  Form,
  Input,
  Modal,
  Spin,
  Tooltip,
} from "antd";
import { isEmpty, isNull } from "lodash";
import React, { memo, useState } from "react";
import { FaLock, FaLockOpen } from "react-icons/fa";
import { useLocation } from "react-router-dom";
import BackButton from "../../../components/AdminStyle/subs/back-button";
import ErrorScreen from "../../../pages/examples/error-screen";
import LoadingScreen from "../../../pages/examples/loading-screen";
import { useChangeUserStatus } from "../admin-user.mutate";
import { useQueryUserDetail } from "../admin-user.query";
import ImagePreview from "../subs/image-preview";
const { confirm } = Modal;

function AdminDetailUser() {
  document.title = "Chi tiết người dùng";
  const location = useLocation();
  const id = location.state.id;

  const { data, isLoading, error } = useQueryUserDetail(id);
  const { status: currentStatus, id: userId, username } = data || {};
  const { mutate: changeStatus, isLoading: isLoadingChangeStatus } =
    useChangeUserStatus();

  const [open, setOpen] = useState(false);

  const handleBanUser = async (values) => {
    if (currentStatus === "UNBAN" || isEmpty(currentStatus)) {
      await changeStatus({
        id: userId,
        username: username,
        reasonReject: values.reasonReject,
      });
    }
    setOpen(false);
    return;
  };

  const handleUnbanUser = () => {
    if (currentStatus === "BAN") {
      confirm({
        title: "Bạn có muốn bỏ chặn người dùng này không?",
        content: "",
        icon: <ExclamationCircleFilled />,
        onOk: () => {
          changeStatus({
            id: userId,
            username: username,
            message: "Bỏ chặn người dùng",
          });
        },
        onCancel: () => { },
      });
    }
    return;
  };

  if (isLoading) {
    return <LoadingScreen />;
  }

  if (error) {
    return <ErrorScreen />;
  }

  return (
    <>
      <div className="flex flex-col items-center justify-center w-full ">
        <div className="w-3/4 flex flex-col items-start justify-center">
          <Divider orientation="left">Ảnh đại diện</Divider>
          <ImagePreview url={data.imageUrl} />
        </div>
        <div className="w-3/4 flex flex-col">
          <Divider orientation="left">Thông tin chung</Divider>
          <div className="w-full">
            <Collapse
              size="small"
              expandIconPosition="end"
              defaultActiveKey={["1"]}
              items={[
                {
                  key: "1",
                  label: (
                    <div className=" mx-1">
                      <span className="font-semibold">Tên đăng nhập: </span>
                      <span className=" text-blue-500 mx-2">
                        {data.username}
                      </span>
                    </div>
                  ),
                  children: (
                    <Form
                      initialValues={{
                        ...data,
                        labelGender:
                          data.gender === "MALE"
                            ? "Nam"
                            : data.gender === "FEMALE"
                              ? "Nữ"
                              : "Không xác định",
                      }}
                      layout="vertical"
                      disabled
                    >
                      <div className="w-full flex flex-col justify-center items-center">
                        <div className="w-full grid grid-cols-2 gap-2">
                          <Form.Item
                            label={
                              <div className="font-semibold">Họ và tên :</div>
                            }

                            name="name"
                          >
                            {/* <InputMUI placeholder={data.name} /> */}

                          {/* 
                            <Input placeholder={data.name} /> */}
                            <TextField

                              variant="outlined"
                              size="small"
                              type="text"
                              className="w-full"
                              placeholder={data.name}
                              disabled
                            />
                          </Form.Item>
                          <Form.Item
                            label={
                              <div className="font-semibold">Sinh nhật :</div>
                            }
                            name="dob"
                          >

                            <TextField

                              variant="outlined"
                              size="small"
                              type="text"
                              className="w-full"
                              placeholder={data.dob}
                              disabled
                            />
                          </Form.Item>
                          <Form.Item
                            label={
                              <div className="font-semibold">
                                Số điện thoại :
                              </div>
                            }
                            name="phone"
                          >

                            <TextField

                              variant="outlined"
                              size="small"
                              type="text"
                              className="w-full"
                              placeholder={data.phone}
                              disabled
                            />
                          </Form.Item>
                          <Form.Item
                            label={
                              <div className="font-semibold">Giới tính :</div>
                            }
                            name="labelGender"
                          >

                            <TextField

                              variant="outlined"
                              size="small"
                              type="text"
                              className="w-full"
                              placeholder={data.labelGender}
                              disabled
                            />
                          </Form.Item>
                        </div>
                      </div>
                    </Form>
                  ),
                },
              ]}
            />
          </div>

          <Divider orientation="left">Địa chỉ</Divider>
          <div className="w-full">
            <Collapse
              defaultActiveKey={["1"]}
              size="small"
              expandIconPosition="end"
              items={[
                {
                  key: "1",
                  label: <div className=""></div>,
                  children: (
                    <Form
                      initialValues={{
                        ...data,
                      }}
                      layout="vertical"
                      disabled
                    >
                      <div className="w-full flex flex-col justify-center items-center">
                        <div className="w-full grid grid-cols-2 gap-2">
                          <Form.Item
                            label={<div className="font-semibold">Địa chỉ</div>}
                            name="address"
                          >


                            <TextField

                              variant="outlined"
                              size="small"
                              type="text"
                              className="w-full"
                              placeholder={data.address}
                              disabled
                            />
                          </Form.Item>
                          <Form.Item
                            label={
                              <div className="font-semibold">Xã/Phường</div>
                            }
                            name="ward"
                          >

                            <TextField

                              variant="outlined"
                              size="small"
                              type="text"
                              className="w-full"
                              placeholder={data.ward}
                              disabled
                            />


                          </Form.Item>
                          <Form.Item
                            label={
                              <div className="font-semibold">Quận/Huyện :</div>
                            }
                            name="district"
                          >

                            <TextField

                              variant="outlined"
                              size="small"
                              type="text"
                              className="w-full"
                              placeholder={data.district}
                              disabled
                            />

                          </Form.Item>
                          <Form.Item
                            label={
                              <div className="font-semibold">
                                Tỉnh/Thành phố :
                              </div>
                            }
                            name="province"
                          >

                            <TextField

                              variant="outlined"
                              size="small"
                              type="text"
                              className="w-full"
                              placeholder={data.province}
                              disabled
                            />

                          </Form.Item>
                        </div>
                      </div>
                    </Form>
                  ),
                },
              ]}
            />
          </div>

          <Divider orientation="left">Liên kết mạng xã hội</Divider>
          <div className="w-full">
            <Collapse
              size="small"
              expandIconPosition="end"
              defaultActiveKey={["1"]}
              items={[
                {
                  key: "1",
                  label: <div className=""></div>,
                  children: (
                    <Form
                      initialValues={{
                        ...data,
                        isEmailVerified: data.emailVerified
                          ? "Đã xác thực"
                          : "Chưa xác thực",
                        labelFacebook: isNull(data.facebook)
                          ? "Chưa liên kết"
                          : data.facebook,
                        labelZalo: isNull(data.zalo)
                          ? "Chưa liên kết"
                          : data.zalo,
                      }}
                      layout="vertical"
                      disabled
                    >
                      <div className="w-full flex flex-col justify-center items-center">
                        <div className="w-full grid grid-cols-2 gap-2">
                          <Form.Item
                            label={<div className="font-semibold">Email :</div>}
                            name="email"
                          >

                            <TextField

                              variant="outlined"
                              size="small"
                              type="text"
                              className="w-full"
                              placeholder={data.email}
                              disabled
                            />

                          </Form.Item>
                          <Form.Item
                            label={
                              <div className="font-semibold">
                                Xác thực Email :
                              </div>
                            }
                            name="isEmailVerified"
                          >

                            <TextField

                              variant="outlined"
                              size="small"
                              type="text"
                              className="w-full"
                              placeholder={data.isEmailVerified}
                              disabled
                            />

                          </Form.Item>
                          <Form.Item
                            label={
                              <div className="font-semibold">Facebook :</div>
                            }
                            name="labelFacebook"
                          >

                            <TextField

                              variant="outlined"
                              size="small"
                              type="text"
                              className="w-full"
                              placeholder={data.labelFacebook}
                              disabled
                            />

                          </Form.Item>
                          {/* <Form.Item
                            label={<div className="font-semibold">Zalo :</div>}
                            name="labelZalo"
                          >

                            <TextField

                              variant="outlined"
                              size="small"
                              type="text"
                              className="w-full"
                              placeholder={data.labelZalo}
                              disabled
                            />

                          </Form.Item> */}
                        </div>
                      </div>
                    </Form>
                  ),
                },
              ]}
            />
          </div>
        </div>

        <div className="w-3/4 flex justify-center">
          <Divider orientation="left"></Divider>
        </div>
        <div className="w-full flex justify-center gap-2">
          <BackButton />
          {currentStatus === "UNBAN" && (
            <Tooltip title="Chặn người dùng này">
              <Button
                onClick={() => setOpen(true)}
                style={{
                  backgroundColor: "#ef4444", //red.500 - tailwind color
                }}
              >
                {isLoading ? <Spin /> : <FaLock />}
              </Button>
            </Tooltip>
          )}
          {currentStatus === "BAN" && (
            <Tooltip title="Bỏ chặn người dùng này">
              <Button
                onClick={handleUnbanUser}
                style={{
                  backgroundColor: "#22c55e", //green.500
                }}
              >
                {isLoading ? <Spin /> : <FaLockOpen />}
              </Button>
            </Tooltip>
          )}
        </div>
      </div>
      <Modal
        closeIcon={null}
        title="Vui lòng nhập lí do chặn người dùng!"
        open={open}
        footer={null}
      >
        <Form name="reject-store" onFinish={handleBanUser}>
          <Form.Item
            label=""
            name="reasonReject"
            rules={[
              {
                required: true,
                message: "Vui lòng nhập lí do chặn người dùng!",
              },
            ]}
          >
            <Input />
          </Form.Item>
          <div className="w-full flex justify-end gap-2">
            <Button
              style={{
                backgroundColor: "blue",
                color: "white",
              }}
              onClick={() => setOpen(false)}
            >
              Hủy bỏ
            </Button>
            <Form.Item>
              <Button
                style={{
                  backgroundColor: "red",
                  color: "white",
                }}
                htmlType="submit"
              >
                {isLoadingChangeStatus ? <Spin /> : "Xác nhận"}
              </Button>
            </Form.Item>
          </div>
        </Form>
      </Modal>
    </>
  );
}

export default memo(AdminDetailUser);
