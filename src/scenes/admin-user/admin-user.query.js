import { useQuery } from "@tanstack/react-query";
import API from "../../utils/api";

export const QUERY_ALL_USER_KEY = "QUERY_ALL_USER_KEY";

const getAllUsers = ({ keyword, pageIndex, pageSize }) => {
  return API.request({
    url: "/api/users",
    params: {
      keyword: keyword,
      pageIndex: pageIndex - 1,
      pageSize: pageSize,
    },
  }).then((res) => {
    return res;
  });
};

export const useQueryAllUsers = ({ keyword, pageIndex = 1, pageSize = 10 }) => {
  return useQuery({
    queryKey: [QUERY_ALL_USER_KEY, keyword, pageIndex, pageSize],
    queryFn: () => getAllUsers({ keyword, pageIndex, pageSize }),
  });
};

export const GET_USER_DETAIL_KEY = "GET_USER_DETAIL_KEY";

const getDetailUser = (params) => {
  const id = params.id;
  return API.request({
    url: `/api/users/${id}`,
  });
};

export const useQueryUserDetail = (id) => {
  return useQuery({
    queryKey: [GET_USER_DETAIL_KEY],
    queryFn: () => getDetailUser({ id }),
  });
};
