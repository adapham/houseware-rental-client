import { Input, Table } from "antd";
import React, { memo } from "react";

import { useLocation, useSearchParams } from "react-router-dom";
import { useQueryAllUsers } from "./admin-user.query";

import ErrorScreen from "../../pages/examples/error-screen";

import {
  CheckCircleOutlined,
  CloseCircleOutlined,
  QuestionCircleOutlined,
} from "@ant-design/icons";
import queryString from "query-string";
import Action from "./subs/action";

const { Search } = Input;

function AdminUser() {
  const location = useLocation();

  document.title = "Manage Users";

  const columns = [
    {
      title: "STT",
      dataIndex: "index",
      key: "index",
      align: "center",
      render: (text, record, index) => index + 1,
    },
    {
      title: "Tên đăng nhập",
      dataIndex: "username",
      key: "username",
      sorter: (a, b) => a.username.localeCompare(b.username),
    },
    {
      title: "Họ và tên",
      dataIndex: "name",
      key: "name",
      render: (value) => {
        return <div className="font-bold">{value}</div>;
      },
    },
    {
      title: "Email",
      dataIndex: "email",
      key: "email",
      // sorter: (a, b) => a?.name.localeCompare(b?.name),
    },
    {
      title: "Số điện thoại",
      align: "center",
      render: (value) => {
        return <div className="font-semibold font-mono">{value}</div>;
      },

      dataIndex: "phone",
      key: "phone",
    },
    {
      title: "Giới tính",
      dataIndex: "gender",
      align: "center",
      key: "gender",
      filters: [
        { text: "Nam", value: "MALE" },
        { text: "Nữ", value: "FEMALE" },
        { text: "Không xác định", value: "OTHER" },
      ],
      onFilter: (value, record) => record.gender === value,
      render: (value) => {
        return value === "MALE" ? (
          <div className="text-blue-500">Nam</div>
        ) : value === "FEMALE" ? (
          <div className="text-pink-500">Nữ</div>
        ) : (
          <div className="text-yellow-500">Không xác định</div>
        );
      },
    },
    {
      title: "Số lần vi phạm",
      dataIndex: "zalo",
      key: "zalo",
      align: "center",
      render: (value) => value ? value : 0,
    },
    {
      title: "Trạng thái",
      dataIndex: "status",
      align: "center",
      key: "status",
      filters: [
        { text: "Đang hoạt động", value: "UNBAN" },
        { text: "Đã chặn", value: "BAN" },
      ],
      onFilter: (value, record) => record.status === value,
      render: (value) => {
        const status = {
          BAN: (
            <div className="font-semibold text-red-500">
              <CloseCircleOutlined /> Đã chặn
            </div>
          ),
          UNBAN: (
            <div className="font-semibold text-green-500">
              <CheckCircleOutlined /> Đang hoạt động
            </div>
          ),
          null: (
            <div className="font-semibold text-yellow-500">
              <QuestionCircleOutlined /> Không xác định
            </div>
          ),
        };
        return status[value];
      },
    },
    {
      title: "Hành động",
      dataIndex: "",
      key: "",

      render: (value, rows) => <Action value={value} rows={rows} />,
    },
  ];

  const [searchParams, setSearchParams] = useSearchParams();

  const { data, isLoading, error } = useQueryAllUsers({
    keyword: searchParams.get("search"),
    pageIndex: searchParams.get("pageIndex") || 1,
  });

  const onSearch = (keywordSearch) => {
    const currentQuery = queryString.parse(location.search);

    setSearchParams({ ...currentQuery, search: keywordSearch });
  };

  if (error) {
    return <ErrorScreen />;
  }
  return (
    <div className="w-full flex flex-col gap-2">
      <div className="flex gap-4 items-center w-full">
        <Search
          style={{
            width: "300px",
          }}
          autoFocus
          placeholder="Tìm kiếm theo tên người dùng"
          onSearch={onSearch}
        />
      </div>
      <div className="">
        <Table
          columns={columns}
          dataSource={data?.content}
          loading={isLoading}
          pagination={{
            defaultPageSize: 10,
            showSizeChanger: false,
            total: data?.pageable.totalElements,
            onChange: (pageIndex) => {
              const currentQuery = queryString.parse(location.search);

              setSearchParams({
                ...currentQuery,
                pageIndex: pageIndex,
              });
            },
          }}
        />
      </div>
    </div>
  );
}

export default memo(AdminUser);
