import { useMutation, useQueryClient } from "@tanstack/react-query";
import { useNavigate } from "react-router-dom";
import { toast } from "react-toastify";
import API from "../../utils/api";
import { QUERY_ALL_USER_KEY } from "./admin-user.query";

const CHANGE_USER_STATUS_KEY = "CHANGE_USER_STATUS_KEY";

const changeStatus = (params) => {
  const { id, reasonReject } = params;
  return API.request({
    method: "PUT",
    url: `/api/users/ban/${id}`,
    params: {
      reasonReject,
    },
  });
};

export const useChangeUserStatus = () => {
  const queryClient = useQueryClient();
  const navigate = useNavigate();
  return useMutation({
    mutationKey: [CHANGE_USER_STATUS_KEY],
    mutationFn: changeStatus,
    onSuccess: (_, params) => {
      const { reasonReject, username } = params;

      queryClient.invalidateQueries([QUERY_ALL_USER_KEY]);
      toast.success(
        `${
          reasonReject ? "Chặn người dùng" : "Bỏ chặn người dùng"
        } ${username} thành công.`
      );
      navigate(-1);
    },
    onError: (_, params) => {
      const { reasonReject, username } = params;

      queryClient.invalidateQueries([QUERY_ALL_USER_KEY]);
      toast.success(
        `${
          reasonReject ? "Chặn người dùng" : "Bỏ chặn người dùng"
        } ${username} thất bại.`
      );
      navigate(-1);
    },
  });
};
