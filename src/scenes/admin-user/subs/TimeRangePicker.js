import { DatePicker } from "antd";
import * as React from "react";

const { RangePicker } = DatePicker;

export default function TimeRangePicker() {
  return (
    <div>
      <RangePicker
        onChange={(e) => {
          console.log(e);
        }}
      />
    </div>
  );
}
