import React, { memo } from "react";

import { Button, Tooltip } from "antd";

import { FaRegEye } from "react-icons/fa";
import { useNavigate } from "react-router-dom";

function Action({ _, rows }) {
  const navigate = useNavigate();

  const { id: userId } = rows;

  const handleGotoDetail = () => {
    navigate(`/admin/user/${userId}`, {
      state: {
        id: userId,
      },
    });
  };

  return (
    <div className="w-full flex justify-start gap-1">
      <Tooltip title="Xem chi tiết">
        <Button onClick={handleGotoDetail}>
          <FaRegEye />
        </Button>
      </Tooltip>
    </div>
  );
}

export default memo(Action);
