import { StarOutlined } from "@ant-design/icons";
import {
  Button,
  Collapse,
  Divider,
  Form,
  Input,
  Modal,
  Spin,
  Tooltip,
} from "antd";
import { memo, useState } from "react";
import ReactHtmlParser from "react-html-parser";
import { AiOutlineCheck, AiOutlineClose } from "react-icons/ai";
import { useLocation } from "react-router-dom";
import BackButton from "../../../components/AdminStyle/subs/back-button";
import ErrorScreen from "../../../pages/examples/error-screen";
import LoadingScreen from "../../../pages/examples/loading-screen";
import { formatToMoney } from "../../../utils/helper";
import { useQueryDetailProduct } from "../admin-product.query";
import SlideImage from "../subs/slide-image";

import { useApproveProduct, useCancelProduct } from "../admin-product.mutate";
import { TextField } from "@material-ui/core";
const { confirm } = Modal;
function AdminDetailProduct() {
  const location = useLocation();
  const { id, approve } = location.state;

  const STATUS = {
    MINT: "Mới",
    NEAR_MINT: "Gần như mới",
    VERY_GOOD: "Rất tốt",
    GOOD: "Tốt",
  };
  const [open, setOpen] = useState(false);
  const [openConfirm, setOpenConfirm] = useState(false);

  const { data, isLoading, error } = useQueryDetailProduct(id);

  const { mutate: approveProduct, isLoading: isLoadingApprove } =
    useApproveProduct();
  const { mutate: cancelProduct, isLoading: isLoadingCancel } =
    useCancelProduct();

  const handleApproveProduct = () => {
    approveProduct({ id });
  };

  const handleCancelProduct = async (values) => {
    await cancelProduct({ id, reasonReject: values.reasonReject });
    setOpen(false);
  };

  if (isLoading) {
    return <LoadingScreen />;
  }

  if (error) {
    return <ErrorScreen />;
  }

  return (
    <>
      <div className="flex flex-col items-center gap-4 w-full h-full">
        <div className="w-3/4">
          <Divider orientation="left">Thông tin chung</Divider>
        </div>
        <div className="w-3/4">
          <Collapse
            size="small"
            defaultActiveKey={["2", "3"]}
            expandIconPosition="end"
            items={[
              {
                key: "1",
                label: "Thông tin cửa hàng",
                children: (
                  <Form
                    initialValues={{
                      ...data.store,
                    }}
                    layout="vertical"
                    disabled
                  >
                    <div className="w-full flex flex-col justify-center items-center">
                      <div className="w-full grid grid-cols-2 gap-2">
                        <Form.Item
                          label={
                            <div className="font-semibold">Tên cửa hàng :</div>
                          }
                          name="title"
                        >
                          <TextField
                            variant="outlined"
                            size="small"
                            type="text"
                            className="w-full"
                            placeholder={data.store.title}
                            disabled
                          />
                        </Form.Item>
                        <Form.Item
                          label={
                            <div className="font-semibold">Đánh giá :</div>
                          }
                          name="rating"
                        >

                          <TextField
                            variant="outlined"
                            size="small"
                            type="text"
                            className="w-full"
                            placeholder={
                              data.store.rating || "Chưa có đánh giá"
                            }
                            disabled
                          />
                        </Form.Item>
                        <Form.Item
                          label={
                            <div className="font-semibold">Điện thoại :</div>
                          }
                          name="phone"
                        >
                          <TextField
                            variant="outlined"
                            size="small"
                            type="text"
                            className="w-full"
                            placeholder={data.store.phone}
                            disabled
                          />
                        </Form.Item>
                        <Form.Item
                          label={<div className="font-semibold">Địa chỉ :</div>}
                          name="address"
                        >
                          <TextField
                            variant="outlined"
                            size="small"
                            type="text"
                            className="w-full"
                            placeholder={data.store.address}
                            disabled
                          />
                        </Form.Item>
                      </div>
                      <div className="w-full grid grid-cols-1 gap-2">
                        <Form.Item
                          label={<div className="font-semibold">Mô tả :</div>}
                          name="description"
                        >
                          <Input.TextArea
                            placeholder={data.store.description}
                          />
                        </Form.Item>
                      </div>
                    </div>
                  </Form>
                ),
              },
              {
                key: "2",
                label: "Ảnh sản phẩm",
                children: (
                  <div className=" w-full flex flex-col justify-start">
                    <p className="font-semibold">Ảnh sản phẩm: </p>
                    <div>
                      <SlideImage data={data.imageTextUri} />
                    </div>
                  </div>
                ),
              },
              {
                key: "3",
                label: "Thông tin sản phẩm",
                children: (
                  <div className="w-full">
                    <Form
                      initialValues={{
                        ...data,
                        status: STATUS[data.status],
                        branch: data.category.categoryName,
                      }}
                      layout="vertical"
                      disabled
                    >
                      <div className="w-full flex flex-col justify-center items-center">
                        <div className="w-full grid grid-cols-3 gap-2">
                          <Form.Item
                            label={
                              <div className="font-semibold">
                                Tên sản phẩm :
                              </div>
                            }
                            name="title"
                          >
                            <TextField
                              variant="outlined"
                              size="small"
                              type="text"
                              className="w-full"
                              placeholder={data.title}
                              disabled
                            />
                          </Form.Item>
                          <Form.Item
                            label={
                              <div className="font-semibold">Trạng thái :</div>
                            }
                            name="status"
                          >
                            <TextField
                              variant="outlined"
                              size="small"
                              type="text"
                              className="w-full"
                              placeholder={STATUS[data.status]}
                              disabled
                            />
                          </Form.Item>
                          <Form.Item
                            label={
                              <div className="font-semibold">
                                Số <StarOutlined /> :
                              </div>
                            }
                            name="star"
                          >
                            <TextField
                              variant="outlined"
                              size="small"
                              type="text"
                              className="w-full"
                              placeholder={data.star}
                              disabled
                            />
                          </Form.Item>

                          <Form.Item
                            label={
                              <div className="font-semibold">Nhãn hiệu :</div>
                            }
                            name="model"
                          >
                            <TextField
                              variant="outlined"
                              size="small"
                              type="text"
                              className="w-full"
                              placeholder={data.model}
                              disabled
                            />
                          </Form.Item>
                          <Form.Item
                            label={
                              <div className="font-semibold">Danh mục :</div>
                            }
                            name="branch"
                          >
                            <TextField
                              variant="outlined"
                              size="small"
                              type="text"
                              className="w-full"
                              placeholder={data.branch}
                              disabled
                            />
                          </Form.Item>
                          <Form.Item
                            label={
                              <div className="font-semibold">Kích thước :</div>
                            }
                            name="dimensions"
                          >
                            <TextField
                              variant="outlined"
                              size="small"
                              type="text"
                              className="w-full"
                              placeholder={data.dimensions}
                              disabled
                            />
                          </Form.Item>
                        </div>
                        <div className="w-full grid grid-cols-1 gap-2">
                          <Form.Item
                            label={
                              <div className="font-semibold">
                                Mô tả sản phẩm :
                              </div>
                            }
                            name="description"
                          >
                            <Input.TextArea placeholder={data.description} />
                          </Form.Item>
                          <Form.Item
                            label={
                              <div className="font-semibold">
                                Tình trạng sản phẩm :
                              </div>
                            }
                            name="capacity"
                          >
                            <Input.TextArea placeholder={data.capacity} />
                          </Form.Item>
                        </div>
                      </div>
                    </Form>
                  </div>
                ),
              },
            ]}
          />
        </div>
        <div className="w-3/4">
          <Divider orientation="left">Quy định thuê</Divider>
        </div>
        <div className="w-3/4">
          <Collapse
            size="small"
            defaultActiveKey={[1]}
            expandIconPosition="end"
            items={[
              {
                key: 1,
                label: "",
                children: (
                  <div className="w-full">
                    <div>{ReactHtmlParser(data.regulations)}</div>
                  </div>
                ),
              },
            ]}
          />
        </div>
        <div className="w-3/4">
          <Divider />
        </div>
        <div className="w-3/4 flex">
          <div className="w-1/2 flex items-baseline gap-3">
            <p className="font-semibold">Giá thuê: </p>
            <div className="font-semibold text-blue-500 text-lg">
              {formatToMoney(data.price)}
            </div>
          </div>
          <div className="w-1/2 flex items-baseline gap-3">
            <p className="font-semibold">Đặt cọc: </p>
            <div className="font-semibold text-blue-500 text-lg">
              {formatToMoney(data.depositPrice)}
            </div>
          </div>
        </div>
        <div className="w-3/4">
          <Divider />
        </div>
        <div className="w-full flex justify-center gap-2">
          {(approve === "REFUSED" || approve === "WAIT_APPROVE") && (
            <Tooltip title="Phê duyệt">
              <Button
                onClick={() => setOpenConfirm(true)}
                style={{
                  backgroundColor: "#22c55e", //green.500
                }}
              >
                {isLoadingApprove ? <Spin /> : <AiOutlineCheck />}
              </Button>
            </Tooltip>
          )}
          {(approve === "APPROVED" || approve === "WAIT_APPROVE") && (
            <Tooltip title="Hủy phê duyệt">
              <Button
                onClick={() => setOpen(true)}
                style={{
                  backgroundColor: "#ef4444", //red.500
                }}
              >
                {isLoadingCancel ? <Spin /> : <AiOutlineClose />}
              </Button>
            </Tooltip>
          )}
          <BackButton />
        </div>
      </div>
      <Modal
        closeIcon={null}
        title="Nhập lý do từ chối sản phẩm"
        open={open}
        footer={null}
      >
        <Form name="reject-product" onFinish={handleCancelProduct}>
          <Form.Item
            label=""
            name="reasonReject"
            rules={[
              {
                required: true,
                message: "Vui lòng nhập lí do từ chối sản phẩm!",
              },
            ]}
          >
            <Input />
          </Form.Item>
          <div className="w-full flex justify-end gap-2">
            <Button
              style={{
                backgroundColor: "blue",
                color: "white",
              }}
              onClick={() => setOpen(false)}
            >
              Hủy bỏ
            </Button>
            <Form.Item>
              <Button
                style={{
                  backgroundColor: "red",
                  color: "white",
                }}
                htmlType="submit"
              >
                {isLoadingCancel ? <Spin /> : "Xác nhận"}
              </Button>
            </Form.Item>
          </div>
        </Form>
      </Modal>
      <Modal
        open={openConfirm}
        closeIcon={null}
        title="Bạn có muốn phê duyệt sản phẩm này không ?"
        footer={null}
      >
        <div className="w-full flex justify-end gap-2 pb-4">
          <Button
            style={{
              backgroundColor: "blue",
              color: "white",
            }}
            onClick={() => setOpenConfirm(false)}
          >
            Hủy bỏ
          </Button>

          <Button
            style={{
              backgroundColor: "green",
              color: "white",
            }}
            onClick={handleApproveProduct}
          >
            {isLoadingApprove ? <Spin /> : "Xác nhận"}
          </Button>
        </div>
      </Modal>
    </>
  );
}

export default memo(AdminDetailProduct);
