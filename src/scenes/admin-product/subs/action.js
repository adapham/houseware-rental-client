import { Button, Modal, Tooltip } from "antd";
import { memo } from "react";

import { FaRegEye } from "react-icons/fa";

import { useNavigate } from "react-router-dom";

const { confirm } = Modal;

function Action({ value, rows }) {
  const { id, approve } = rows;

  const navigate = useNavigate();

  const handleGotoDetail = () => {
    navigate(`/admin/product/detail/${id}`, {
      state: {
        id: id,
        approve: approve,
      },
    });
  };

  return (
    <>
      <div className="w-full gap-2 flex justify-center">
        <Tooltip title="Xem chi tiết">
          <Button onClick={handleGotoDetail}>
            <FaRegEye />
          </Button>
        </Tooltip>
      </div>
    </>
  );
}

export default memo(Action);
