import dayjs from "dayjs";

import { Input, Table } from "antd";
import React, { memo, useRef } from "react";
import { useLocation, useSearchParams } from "react-router-dom";

import ReactHtmlParser from "react-html-parser";

import ErrorScreen from "../../pages/examples/error-screen";

import Action from "./subs/action";

import {
  CheckCircleOutlined,
  CloseCircleOutlined,
  QuestionCircleOutlined,
} from "@ant-design/icons";
import queryString from "query-string";
import { formatToMoney } from "./../../utils/helper";
import { useQueryAllProduct } from "./admin-product.query";

const { Search } = Input;
function AdminProduct() {
  const location = useLocation();

  document.title = "Quản lý sản phẩm";

  const searchRef = useRef();

  const columns = [
    {
      title: "STT",
      dataIndex: "index",
      key: "index",
      align: "center",
      render: (text, record, index) => index + 1,
    },
    {
      width: "10%",
      title: "Ngày tạo",
      dataIndex: "createdTime",
      key: "createdTime",
      render: (value) => dayjs(value).format("DD-MM-YYYY"),
    },
    {
      width: "10%",
      title: "Tên sản phẩm",
      dataIndex: "title",
      key: "title",
      render: (value) => {
        return <div className="text-ellipsis font-bold">{value}</div>;
      },
    },
    {
      width: "10%",
      title: "Giá thuê",
      dataIndex: "depositPrice",
      key: "depositPrice",
      sorter: (a, b) => a.depositPrice - b.depositPrice,
      render: (value) => (
        <div className="text-ellipsis font-semibold text-blue-500">
          {formatToMoney(value)}
        </div>
      ),
      ellipsis: true,
    },
    {
      width: "25%",
      title: "Mô tả",
      dataIndex: "description",
      key: "description",
      render: (value) => (
        <div className="text-ellipsis line-clamp-2">
          {ReactHtmlParser(value)}
        </div>
      ),
      ellipsis: true,
    },
    {
      width: "10%",
      title: "Tình trạng",
      align: "center",
      dataIndex: "status",
      key: "status",
      filters: [
        { text: "Mới", value: "MINT" },
        { text: "Gần như mới", value: "NEAR_MINT" },
        { text: "Rất tốt", value: "VERY_GOOD" },
        { text: "Tốt", value: "GOOD" },
      ],
      onFilter: (value, record) => record.status === value,
      render: (value) => {
        const status = {
          MINT: (
            <div className="text-white bg-blue-600 p-1 rounded-md">Mới</div>
          ),
          NEAR_MINT: (
            <div className="text-white rounded-md bg-blue-400">Gần như mới</div>
          ),
          VERY_GOOD: (
            <div className="text-white rounded-md bg-yellow-500">Rất tốt</div>
          ),
          GOOD: <div className="text-white rounded-md bg-yellow-300">Tốt</div>,
        };
        return <div className="">{status[value]}</div>;
      },
    },
    {
      width: "15%",
      title: "Trạng thái phê duyệt",
      dataIndex: "approve",
      key: "approve",
      filters: [
        { text: "Chờ phê duyệt", value: "WAIT_APPROVE" },
        { text: "Đã phê duyệt", value: "APPROVED" },
        { text: "Đã từ chối", value: "REFUSED" },
      ],
      onFilter: (value, record) => record.approve === value,
      render: (value) => {
        const status = {
          WAIT_APPROVE: (
            <div className="text-yellow-500 font-semibold">
              <QuestionCircleOutlined />
              Chờ phê duyệt
            </div>
          ),
          APPROVED: (
            <div className="text-green-500 font-semibold">
              <CheckCircleOutlined /> Đã phê duyệt
            </div>
          ),
          REFUSED: (
            <div className="text-red-500 font-semibold">
              <CloseCircleOutlined /> Đã từ chối
            </div>
          ),
        };
        return (
          <div className="text-ellipsis line-clamp-2">{status[value]}</div>
        );
      },
    },
    {
      title: "Người phê duyệt",
      dataIndex: "approver",
      key: "approver",
      align: "center",
    },
    {
      // width: "20%",
      title: "Hành động",
      dataIndex: "",
      key: "",
      align: "center",
      render: (value, rows) => <Action value={value} rows={rows} />,
    },
  ];

  const [searchParams, setSearchParams] = useSearchParams();

  const { data, isLoading, error } = useQueryAllProduct({
    keyword: searchParams.get("search"),
    fromDate: searchParams.get("fromDate"),
    toDate: searchParams.get("toDate"),
    pageIndex: searchParams.get("pageIndex") || 1,
  });

  const onSearch = (keywordSearch) => {
    const currentQuery = queryString.parse(location.search);

    setSearchParams({ ...currentQuery, search: searchRef.current.input.value });
  };

  if (error) {
    return <ErrorScreen />;
  }

  return (
    <div className="w-full flex flex-col gap-2">
      <div className="flex gap-4 items-center w-full justify-between mb-2">
        <Search
          ref={searchRef}
          style={{
            width: "300px",
          }}
          autoFocus
          placeholder="Tìm kiếm theo tên danh mục"
          onSearch={onSearch}
        />
      </div>
      <div className="">
        <Table
          columns={columns}
          dataSource={data?.content}
          loading={isLoading}
          pagination={{
            defaultPageSize: 10,
            showSizeChanger: false,
            total: data?.pageable.totalElements,
            onChange: (pageIndex) => {
              const currentQuery = queryString.parse(location.search);
              setSearchParams({
                ...currentQuery,
                pageIndex: pageIndex,
              });
            },
          }}
        />
      </div>
    </div>
  );
}

export default memo(AdminProduct);
