import { useQuery } from "@tanstack/react-query";
import API from "../../utils/api";

export const QUERY_ALL_PRODUCT_KEY = "QUERY_ALL_PRODUCT_KEY";

const getAllProduct = ({ keyword, fromDate, toDate, pageIndex, pageSize }) => {
  return API.request({
    url: "/api/products/admin",
    params: {
      keyword: keyword,
      fromDate: fromDate,
      toDate: toDate,
      pageIndex: pageIndex - 1,
      pageSize: pageSize,
    },
  }).then((res) => {
    return res;
  });
};

export const useQueryAllProduct = ({
  keyword,
  fromDate,
  toDate,
  pageIndex = 1,
  pageSize = 10,
}) => {
  return useQuery({
    queryKey: [
      QUERY_ALL_PRODUCT_KEY,
      keyword,
      fromDate,
      toDate,
      pageIndex,
      pageSize,
    ],
    queryFn: () =>
      getAllProduct({ keyword, fromDate, toDate, pageIndex, pageSize }),
  });
};

export const QUERY_DETAIL_PRODUCT_KEY = "QUERY_DETAIL_PRODUCT_KEY";

const getDetailProduct = (id) => {
  return API.request({
    url: `/api/products/${id}`,
  }).then((res) => {
    return res;
  });
};

export const useQueryDetailProduct = (id) => {
  return useQuery({
    queryKey: [QUERY_DETAIL_PRODUCT_KEY, id],
    queryFn: () => getDetailProduct(id),
    enabled: !!id,
  });
};
