import { useMutation, useQueryClient } from "@tanstack/react-query";
import { useNavigate } from "react-router-dom";
import { toast } from "react-toastify";
import API from "../../utils/api";
import { QUERY_ALL_PRODUCT_KEY } from "./admin-product.query";

const APPROVE_PRODUCT_KEY = "APPROVE_PRODUCT_KEY";

const approveProduct = (params) => {
  const id = params.id;
  return API.request({
    method: "PUT",
    url: `/api/products/${id}/approve`,
  });
};

export const useApproveProduct = () => {
  const navigate = useNavigate();
  const queryClient = useQueryClient();
  return useMutation({
    mutationKey: [APPROVE_PRODUCT_KEY],
    mutationFn: approveProduct,
    onSuccess: (_, params) => {
      navigate(-1);
      queryClient.invalidateQueries([QUERY_ALL_PRODUCT_KEY]);
      toast.success(`Chấp nhận sản phẩm thành công.`);
    },
    onError: (_, params) => {
      navigate(-1);

      queryClient.invalidateQueries([QUERY_ALL_PRODUCT_KEY]);
      toast.error(`Chấp nhận sản phẩm thất bại.`);
    },
  });
};

//=====================================================

const CANCEL_PRODUCT_KEY = "CANCEL_PRODUCT_KEY";
const cancelProduct = (params) => {
  const { id, reasonReject } = params;
  return API.request({
    method: "PUT",
    url: `/api/products/${id}/refuse`,
    params: {
      reasonReject,
    },
  });
};

export const useCancelProduct = () => {
  const navigate = useNavigate();
  const queryClient = useQueryClient();
  return useMutation({
    mutationKey: [CANCEL_PRODUCT_KEY],
    mutationFn: cancelProduct,
    onSuccess: (_, params) => {
      navigate(-1);

      queryClient.invalidateQueries([QUERY_ALL_PRODUCT_KEY]);
      toast.success(`Hủy sản phẩm thành công.`);
    },
    onError: (_, params) => {
      navigate(-1);
      queryClient.invalidateQueries([QUERY_ALL_PRODUCT_KEY]);
      toast.error(`Hủy sản phẩm thất bại.`);
    },
  });
};
