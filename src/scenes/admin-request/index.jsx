import React, { memo, useEffect } from "react";

import { Input, Tabs } from "antd";
import { useLocation, useSearchParams } from "react-router-dom";

import TableReport from "./subs/table-report";
import TableRoleUp from "./subs/table-role-up";

import queryString from "query-string";
import AdminDateTimePicker from "../../components/admin-date-time-picker";

const { Search } = Input;

function AdminRequest() {
  const location = useLocation();

  document.title = "Quản lý yêu cầu/báo cáo";
  const [searchParams, setSearchParams] = useSearchParams();

  const tabs = [
    {
      label: `Đăng ký chủ tiệm`,
      key: "ROLE_UP",
      children: <TableRoleUp />,
    },
    {
      label: `Báo cáo`,
      key: "REPORT",
      children: <TableReport />,
    },
  ];

  const onSearch = (keywordSearch) => {
    const currentQuery = queryString.parse(location.search);

    setSearchParams({
      ...currentQuery,
      search: keywordSearch,
      type: searchParams.get("type"),
    });
  };

  const handleChangeType = (value) => {
    setSearchParams({
      type: value,
    });
  };

  // useEffect(() => {
  //   const validType = ["ROLE_UP", "REPORT"];
  //   const currentType = searchParams.get("type");
  //   if (!validType.includes(currentType)) {
  //     setSearchParams({
  //       type: "ROLE_UP",
  //     });
  //   }
  // }, [searchParams, setSearchParams]);

  useEffect(() => {
    setSearchParams({
      type: "ROLE_UP",
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div className="w-full flex flex-col gap-2">
      <div className="flex gap-4 items-center w-full justify-between mb-2">
        <Search
          style={{
            width: "300px",
          }}
          autoFocus
          placeholder="Tìm kiếm theo tên người dùng"
          onSearch={onSearch}
        />
        <AdminDateTimePicker />
      </div>

      <div className="flex flex-col gap-2 w-full mt-2">
        <Tabs onChange={handleChangeType} items={tabs} />
      </div>
    </div>
  );
}

export default memo(AdminRequest);
