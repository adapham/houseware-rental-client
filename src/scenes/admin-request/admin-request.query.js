import { useQuery } from "@tanstack/react-query";
import API from "../../utils/api";

export const QUERY_ALL_REQUEST_KEY = "QUERY_ALL_REQUEST_KEY";

const getAllRequest = ({
  keyword,
  fromDate,
  toDate,
  type,
  status,
  pageIndex,
  pageSize,
}) => {
  return API.request({
    url: "/api/requests/page",
    params: {
      status: status,
      keyword: keyword,
      fromDate: fromDate,
      toDate: toDate,
      type: type,
      pageIndex: pageIndex - 1,
      pageSize: pageSize,
    },
  }).then((res) => {
    return res;
  });
};

export const useQueryAllRequest = ({
  keyword,
  fromDate,
  toDate,
  type,
  status,
  pageIndex = 1,
  pageSize = 10,
}) => {
  return useQuery({
    queryKey: [
      QUERY_ALL_REQUEST_KEY,
      keyword,
      fromDate,
      toDate,
      type,
      status,
      pageIndex,
      pageSize,
    ],
    queryFn: () =>
      getAllRequest({
        keyword,
        fromDate,
        toDate,
        type,
        status,
        pageIndex,
        pageSize,
      }),
    enabled: !!type,
  });
};

export const QUERY_DETAIL_REQUEST = "QUERY_DETAIL_REQUEST";

const getDetailRequest = (id) => {
  return API.request({
    url: `/api/requests/${id}`,
  });
};

export const useQueryDetailRequest = ({ id }) => {
  return useQuery({
    queryKey: [QUERY_DETAIL_REQUEST, id],
    queryFn: () => getDetailRequest(id),
    enabled: !!id,
  });
};

export const QUERY_DETAIL_ROLE_UP_REQUEST = "QUERY_DETAIL_ROLE_UP_REQUEST";

const getDetailRoleUpRequest = (userId) => {
  return API.request({
    url: `/api/requests/${userId}/user`,
  });
};

export const useQueryRoleUpRequestDetail = ({ userId }) => {
  return useQuery({
    queryKey: [QUERY_DETAIL_ROLE_UP_REQUEST, userId],
    queryFn: () => getDetailRoleUpRequest(userId),
    enabled: !!userId,
  });
};

export const QUERY_DETAIL_REPORT_REQUEST = "QUERY_DETAIL_REPORT_REQUEST";

const getDetailReportRequest = (id) => {
  return API.request({
    url: `/api/requests/${id}/order-detail`,
  });
};

export const useQueryReportRequestDetail = ({ id }) => {
  return useQuery({
    queryKey: [QUERY_DETAIL_REPORT_REQUEST, id],
    queryFn: () => getDetailReportRequest(id),
    enabled: !!id,
  });
};
