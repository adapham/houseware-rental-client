import { Form, Input } from "antd";
import dayjs from "dayjs";
import React, { memo } from "react";
import { useLocation } from "react-router-dom";
import BackButton from "../../../components/AdminStyle/subs/back-button";
import ErrorScreen from "../../../pages/examples/error-screen";
import LoadingScreen from "../../../pages/examples/loading-screen";
import { useQueryDetailRequest } from "../admin-request.query";

function AdminDetailRequest() {
  const location = useLocation();
  const id = location.state.id;
  const type = location.state.type;

  const { data, isLoading, error } = useQueryDetailRequest({ id });

  const STATUS = {
    ACCEPT: "Đã xác thực",
    REFUSE: " Đã hủy cửa hàng",
    PROCESSING: " Đợi phê duyệt",
  };

  const onFinish = (values) => {
    console.log("current values", values);
  };

  const onFinishFailed = () => {
    console.log("on finish failed");
  };

  if (isLoading) {
    return <LoadingScreen />;
  }

  if (error) {
    return <ErrorScreen />;
  }

  return (
    <div className="flex flex-col gap-2 w-full">
      <div className="w-full flex justify-center p-4">
        <Form
          name="detail-request"
          layout="vertical"
          style={{
            width: "50%",
          }}
          initialValues={{
            ...data,
            createdTimeFormat: dayjs(data.createdTime).format("DD-MM-YYYY"),
            typeRequest:
              data.type === "ROLE_UP"
                ? "Đăng ký chủ tiệm"
                : data.type === "REPORT"
                ? "Báo cáo"
                : data.type,
            statusRequest: STATUS[data.status],
          }}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          autoComplete="off"
          disabled
        >
          <div className="w-full grid grid-cols-1">
            <Form.Item
              label={<div className="font-semibold">Người tạo yêu cầu :</div>}
              name="createdBy"
              rules={[
                {
                  required: true,
                  message: "Vui lòng nhập thông tin !",
                },
              ]}
            >
              <Input style={{ width: "100%" }} placeholder={data.createdBy} />
            </Form.Item>
            <Form.Item
              label={
                <div className="font-semibold">Thời gian tạo yêu cầu :</div>
              }
              name="createdTimeFormat"
              rules={[
                {
                  required: true,
                  message: "Vui lòng nhập thông tin !",
                },
              ]}
            >
              <Input placeholder={data.createdTimeFormat} />
            </Form.Item>
            <Form.Item
              label={<div className="font-semibold">Loại yêu cầu :</div>}
              name="typeRequest"
              rules={[
                {
                  required: true,
                  message: "Vui lòng nhập thông tin !",
                },
              ]}
            >
              <Input placeholder={data.type} />
            </Form.Item>
            {!!type && type === "ROLE_UP" && (
              <Form.Item
                label={
                  <div className="font-semibold">Trạng thái yêu cầu :</div>
                }
                name="statusRequest"
                rules={[
                  {
                    required: true,
                    message: "Vui lòng nhập thông tin !",
                  },
                ]}
              >
                <Input placeholder={data.statusRequest} />
              </Form.Item>
            )}
            {!!type && type === "REPORT" && (
              <Form.Item
                label={<div className="font-semibold">Nội dung yêu cầu :</div>}
                name="content"
                rules={[
                  {
                    required: true,
                    message: "Vui lòng nhập thông tin !",
                  },
                ]}
              >
                <Input placeholder={data.content} />
              </Form.Item>
            )}
          </div>
        </Form>
      </div>
      <div className="flex justify-center gap-1 p-4">
        <BackButton />
      </div>
    </div>
  );
}

export default memo(AdminDetailRequest);
