import { TextField } from "@material-ui/core";
import { Button, Divider, Form, Modal, Spin } from "antd";
import React, { memo } from "react";
import { useLocation } from "react-router-dom";
import BackButton from "../../../components/AdminStyle/subs/back-button";
import ErrorScreen from "../../../pages/examples/error-screen";
import LoadingScreen from "../../../pages/examples/loading-screen";
import { formatToMoney } from "../../../utils/helper";
import { useCancelOrder } from "../admin-request.mutate";
import { useQueryReportRequestDetail } from "../admin-request.query";
import SlideImage from "../subs/slide-image";

const { confirm } = Modal;

function AdminDetailReportRequest() {
  const location = useLocation();
  const { id, postId, userId, status } = location.state;
  console.log(location.state,"hoang");
  const { data, isLoading, error } = useQueryReportRequestDetail({ id });

  const { mutate: cancelOrder, isLoading: isLoadingCancelOrder } =
    useCancelOrder();

  const handleCancelOrder = () => {
    confirm({
      title: "Bạn có chắc muốn hủy đơn hàng này không? ",
      onOk: () => {
        cancelOrder({
          id,
          userId,
        });
      },
      onCancel: () => {
        console.log("Cancel - detail report request");
      },
    });
  };

  if (isLoading) {
    return <LoadingScreen />;
  }

  if (error) {
    return <ErrorScreen />;
  }

  return (
    <div className="flex flex-col items-center gap-2 w-full">
      <div className="w-3/4 flex justify-start">
        <Divider orientation="left">Ảnh sản phẩm</Divider>
      </div>
      <div className="w-3/4 flex justify-start">
        <SlideImage data={data?.product?.imageTextUri} />
      </div>

      <div className="w-3/4 flex justify-start">
        <Divider orientation="left">Chi tiết sản phẩm báo cáo</Divider>
      </div>
      <div className="w-3/4 flex justify-center gap-2">
        <Form
          name="detail-report-request-product"
          layout="vertical"
          style={{
            width: "100%",
          }}
          initialValues={{ ...data, title: data?.product?.title }}
          onFinish={() => {}}
          onFinishFailed={() => {}}
          autoComplete="off"
          disabled
        >
          <div className="w-full grid grid-cols-2 gap-2">
            <Form.Item
              label={<div className="font-semibold">Tên sản phẩm :</div>}
              name="title"
            >
              <TextField
                variant="outlined"
                size="small"
                type="text"
                className="w-full"
                placeholder={data?.title}
                disabled
              />
            </Form.Item>
          </div>
          <div className="w-full grid grid-cols-2 gap-2">
            <Form.Item
              label={<div className="font-semibold">Số lượng :</div>}
              name="quantity"
            >
              <TextField
                variant="outlined"
                size="small"
                type="text"
                className="w-full"
                placeholder={data?.quantity}
                disabled
              />
            </Form.Item>
            <Form.Item
              label={<div className="font-semibold">Số tháng thuê :</div>}
              name="rentalPeriod"
            >
              <TextField
                variant="outlined"
                size="small"
                type="text"
                className="w-full"
                placeholder={data?.rentalPeriod}
                disabled
              />
            </Form.Item>
          </div>
        </Form>
      </div>
      <div className="w-3/4 flex justify-start">
        <Divider orientation="left">Thông tin người báo cáo</Divider>
      </div>
      <div className="w-3/4 flex justify-center gap-2">
        <Form
          name="detail-report-request-user"
          layout="vertical"
          style={{
            width: "100%",
          }}
          initialValues={{
            ...data,
            username: data?.user?.name,
            userEmail: data?.user?.email,
            userPhone: data?.user?.phone,
          }}
          onFinish={() => {}}
          onFinishFailed={() => {}}
          autoComplete="off"
          disabled
        >
          <div className="w-full grid grid-cols-1 gap-2">
            <Form.Item
              label={<div className="font-semibold">Người báo cáo :</div>}
              name="username"
            >
              <TextField
                variant="outlined"
                size="small"
                type="text"
                className="w-full"
                placeholder={data?.username}
                disabled
              />
            </Form.Item>
          </div>
          <div className="w-full grid grid-cols-2 gap-2">
            <Form.Item
              label={<div className="font-semibold">Email :</div>}
              name="userEmail"
            >
              <TextField
                variant="outlined"
                size="small"
                type="text"
                className="w-full"
                placeholder={data?.userEmail}
                disabled
              />
            </Form.Item>
            <Form.Item
              label={<div className="font-semibold">Số điện thoại:</div>}
              name="userPhone"
            >
              <TextField
                variant="outlined"
                size="small"
                type="text"
                className="w-full"
                placeholder={data?.userPhone}
                disabled
              />
            </Form.Item>
          </div>
        </Form>
      </div>

      <div className="w-3/4 flex justify-start">
        <Divider orientation="left">Thông tin cửa hàng báo cáo</Divider>
      </div>

      <div className="w-3/4 flex justify-center gap-2">
        <Form
          name="detail-report-request-user"
          layout="vertical"
          style={{
            width: "100%",
          }}
          initialValues={{
            ...data,
            storeName: data?.store?.name,
            storePhone: data?.store?.phone,
          }}
          onFinish={() => {}}
          onFinishFailed={() => {}}
          autoComplete="off"
          disabled
        >
          <div className="w-full grid grid-cols-1 gap-2">
            <Form.Item
              label={<div className="font-semibold">Tên cửa hàng :</div>}
              name="storeName"
            >
              <TextField
                variant="outlined"
                size="small"
                type="text"
                className="w-full"
                placeholder={data?.storeName}
                disabled
              />
            </Form.Item>
            <Form.Item
              label={<div className="font-semibold">Số điện thoại :</div>}
              name="storePhone"
            >
              <TextField
                variant="outlined"
                size="small"
                type="text"
                className="w-full"
                placeholder={data?.storePhone}
                disabled
              />
            </Form.Item>
          </div>
        </Form>
      </div>

      <div className="w-3/4 flex justify-start">
        <Divider orientation="left"></Divider>
      </div>
      <div className="w-3/4 grid grid-cols-2 gap-2">
        <div className="w-1/2 flex items-baseline gap-3">
          <p className="font-semibold">Giá thuê: </p>
          <div className="font-semibold text-blue-500 text-lg">
            {formatToMoney(data?.price)}
          </div>
        </div>
        <div className="w-1/2 flex items-baseline gap-3">
          <p className="font-semibold">Đặt cọc: </p>
          <div className="font-semibold text-blue-500 text-lg">
            {formatToMoney(data?.depositPrice)}
          </div>
        </div>
      </div>
      <div className="w-3/4 flex justify-start">
        <Divider orientation="left"></Divider>
      </div>
      <div className="flex justify-center gap-1 p-4">
        {["PROCESSING"].includes(status) ? (
          <Button
            style={{
              backgroundColor: "red",
              color: "white",
            }}
            onClick={handleCancelOrder}
          >
            {isLoadingCancelOrder ? <Spin /> : "Hủy đơn hàng"}
          </Button>
        ) : null}
        <BackButton />
      </div>
    </div>
  );
}

export default memo(AdminDetailReportRequest);
