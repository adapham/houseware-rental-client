import { TextField } from "@material-ui/core";
import { Button, Divider, Form, Input, Modal, Spin, Tooltip } from "antd";
import TextArea from "antd/es/input/TextArea";
import React, { memo, useState } from "react";
import { AiOutlineCheck, AiOutlineClose } from "react-icons/ai";
import { useLocation } from "react-router-dom";
import BackButton from "../../../components/AdminStyle/subs/back-button";
import ErrorScreen from "../../../pages/examples/error-screen";
import LoadingScreen from "../../../pages/examples/loading-screen";
import { useChangeRequestStatus } from "../admin-request.mutate";
import { useQueryRoleUpRequestDetail } from "../admin-request.query";
import ImagePreview from "../subs/image-preview";

const { confirm } = Modal;

function AdminDetailRoleUpRequest() {
  const location = useLocation();
  const { userId, id, status } = location.state;

  const [open, setOpen] = useState(false);

  const { data, isLoading, error } = useQueryRoleUpRequestDetail({ userId });
  const { mutate: changeStatus, isLoading: isLoadingChangeStatus } =
    useChangeRequestStatus();
  const onFinish = (values) => {
    console.log("current values", values);
  };

  const handleApproveRequest = () => {
    confirm({
      title: "Bạn có chắc chắn muốn chấp nhận yêu cầu này không ?",
      content: "",
      onOk: () => {
        changeStatus({
          id: id,
          status: "ACCEPT",
          reason: "chấp nhận yêu cầu",
        });
      },
      onCancel: () => {},
    });
  };

  const handleRejectRequest = async (values) => {
    await changeStatus({
      id: id,
      status: "REFUSE",
      reason: values.reason,
    });

    setOpen(false);
  };

  const onFinishFailed = () => {
    console.log("on finish failed");
  };

  if (isLoading) {
    return <LoadingScreen />;
  }

  if (error) {
    return <ErrorScreen />;
  }
  return (
    <>
      <div className="flex flex-col items-center gap-2 w-full">
        <div className="w-3/4 flex justify-start">
          <Divider orientation="left">Ảnh cửa hàng</Divider>
        </div>
        <div className="w-3/4 flex justify-start">
          <ImagePreview url={data.imageTextUri[0]} />
        </div>
        <div className="w-3/4 flex justify-start">
          <Divider orientation="left">Thông tin cửa hàng</Divider>
        </div>
        <div className="w-3/4 flex justify-center gap-2">
          <Form
            name="detail-request"
            layout="vertical"
            style={{
              width: "100%",
            }}
            initialValues={{
              ...data,
            }}
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            autoComplete="off"
            disabled
          >
            <div className="w-full grid grid-cols-1">
              <Form.Item
                label={<div className="font-semibold">Tên cửa hàng :</div>}
                name="title"
              >
                <TextField
                  variant="outlined"
                  size="small"
                  type="text"
                  className="w-full"
                  placeholder={data.title}
                  disabled
                />
              </Form.Item>
              <Form.Item
                label={<div className="font-semibold">Mô tả :</div>}
                name="description"
              >
                <TextArea
                  placeholder="điền mô tả ở đây"
                  value={data.description}
                  disabled
                />
              </Form.Item>
            </div>
            <div className="w-full grid grid-cols-2 gap-2">
              <Form.Item
                label={<div className="font-semibold">Số điện thoại :</div>}
                name="phone"
              >
                <TextField
                  variant="outlined"
                  size="small"
                  type="text"
                  className="w-full"
                  placeholder={data.phone}
                  disabled
                />
              </Form.Item>
              <Form.Item
                label={<div className="font-semibold">Xã / Phường :</div>}
                name="ward"
              >
                <TextField
                  variant="outlined"
                  size="small"
                  type="text"
                  className="w-full"
                  placeholder={data.ward}
                  disabled
                />
              </Form.Item>
              <Form.Item
                label={<div className="font-semibold">Quận / Huyện :</div>}
                name="district"
              >
                <TextField
                  variant="outlined"
                  size="small"
                  type="text"
                  className="w-full"
                  placeholder={data.district}
                  disabled
                />
              </Form.Item>
              <Form.Item
                label={<div className="font-semibold">Tỉnh / Thành phố :</div>}
                name="province"
              >
                <TextField
                  variant="outlined"
                  size="small"
                  type="text"
                  className="w-full"
                  placeholder={data.province}
                  disabled
                />
              </Form.Item>
            </div>
          </Form>
        </div>
        <div className="w-3/4 flex justify-start">
          <Divider orientation="left"></Divider>
        </div>
        <div className="flex justify-center gap-1 p-4">
          {["PROCESSING"].includes(status) && (
            <>
              <Tooltip title="Chấp nhận yêu cầu">
                <Button
                  onClick={() => handleApproveRequest()}
                  style={{
                    backgroundColor: "#22c55e", //green.500
                  }}
                >
                  {isLoading ? <Spin /> : <AiOutlineCheck />}
                </Button>
              </Tooltip>
              <Tooltip title="Từ chối yêu cầu">
                <Button
                  onClick={() => setOpen(true)}
                  style={{
                    backgroundColor: "#ef4444", //red.500
                  }}
                >
                  {isLoading ? <Spin /> : <AiOutlineClose />}
                </Button>
              </Tooltip>
            </>
          )}

          <BackButton />
        </div>
      </div>
      <Modal
        closeIcon={null}
        title="Nhập lý do từ chối yêu cầu"
        open={open}
        footer={null}
      >
        <Form name="reject-request" onFinish={handleRejectRequest}>
          <Form.Item
            label=""
            name="reason"
            rules={[
              {
                required: true,
                message: "Vui lòng nhập lí do từ chối yêu cầu!",
              },
            ]}
          >
            <Input />
          </Form.Item>
          <div className="w-full flex justify-end gap-2">
            <Button
              style={{
                backgroundColor: "blue",
                color: "white",
              }}
              onClick={() => setOpen(false)}
            >
              Hủy bỏ
            </Button>
            <Form.Item>
              <Button
                style={{
                  backgroundColor: "red",
                  color: "white",
                }}
                htmlType="submit"
              >
                {isLoadingChangeStatus ? <Spin /> : "Xác nhận"}
              </Button>
            </Form.Item>
          </div>
        </Form>
      </Modal>
    </>
  );
}

export default memo(AdminDetailRoleUpRequest);
