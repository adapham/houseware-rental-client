import { useMutation, useQueryClient } from "@tanstack/react-query";
import { useNavigate } from "react-router-dom";
import { toast } from "react-toastify";
import API from "../../utils/api";
import {
  QUERY_ALL_REQUEST_KEY,
  QUERY_DETAIL_ROLE_UP_REQUEST,
} from "./admin-request.query";

const CHANGE_REQUEST_STATUS_KEY = "CHANGE_REQUEST_STATUS_KEY";

const changeStatus = (params) => {
  const { id, status, reason = "" } = params;
  return API.request({
    method: "PUT",
    url: `/api/requests/${id}`,
    params: {
      status: status,
      reason: reason,
    },
  });
};

export const useChangeRequestStatus = () => {
  const queryClient = useQueryClient();
  const navigate = useNavigate();
  return useMutation({
    mutationKey: [CHANGE_REQUEST_STATUS_KEY],
    mutationFn: changeStatus,
    onSuccess: (_, params) => {
      queryClient.invalidateQueries([QUERY_ALL_REQUEST_KEY]);
      queryClient.invalidateQueries([QUERY_DETAIL_ROLE_UP_REQUEST]);

      toast.success(`Cập nhật trạng thái yêu cầu thành công.`);
      navigate(-1);
    },
    onError: (_, params) => {
      queryClient.invalidateQueries([QUERY_ALL_REQUEST_KEY]);
      queryClient.invalidateQueries([QUERY_DETAIL_ROLE_UP_REQUEST]);

      toast.error(`Cập nhật trạng thái yêu cầu thất bại.`);
    },
  });
};

const CANCEL_ORDER = "CANCEL_ORDER";

const cancelOrder = (params) => {
  const { id, userId } = params;
  return API.request({
    method: "POST",
    url: `/api/requests/cancel/${id}`,
    params: {
      userId,
    },
  });
};

export const useCancelOrder = () => {
  const navigate = useNavigate();
  const queryClient = useQueryClient();
  return useMutation({
    mutationKey: [CANCEL_ORDER],
    mutationFn: cancelOrder,
    onSuccess: () => {
      navigate(-1);
      queryClient.invalidateQueries([QUERY_ALL_REQUEST_KEY]);
      queryClient.invalidateQueries([QUERY_DETAIL_ROLE_UP_REQUEST]);
      toast.success("Hủy đơn hàng thành công.");
      navigate(-1);
    },
    onError: (_, params) => {
      queryClient.invalidateQueries([QUERY_ALL_REQUEST_KEY]);
      queryClient.invalidateQueries([QUERY_DETAIL_ROLE_UP_REQUEST]);
      toast.error(`Hủy đơn hàng thất bại.`);
      navigate(-1);
    },
  });
};
