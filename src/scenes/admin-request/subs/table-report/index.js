import {
  CheckCircleOutlined,
  CloseCircleOutlined,
  QuestionCircleOutlined,
} from "@ant-design/icons";
import { Table } from "antd";
import dayjs from "dayjs";
import queryString from "query-string";
import React, { memo } from "react";
import { useLocation, useSearchParams } from "react-router-dom";
import ErrorScreen from "../../../../pages/examples/error-screen";
import { useQueryAllRequest } from "../../admin-request.query";
import Action from "./action";

function TableReport() {
  const location = useLocation();
  const [searchParams, setSearchParams] = useSearchParams();

  const columns = [
    {
      title: "STT",
      dataIndex: "index",
      key: "index",
      align: "center",
      render: (text, record, index) => index + 1,
    },
    {
      title: "Người tạo",
      dataIndex: "createdBy",
      key: "createdBy",
      render: (value) => {
        return <div className="font-bold">{value}</div>;
      },
    },
    {
      title: "Ngày tạo",
      dataIndex: "createdTime",
      key: "createdTime",
      sorter: (a, b) =>
        dayjs(a.createdTime).unix() - dayjs(b.createdTime).unix(),
      render: (value) => {
        return <div className="">{dayjs(value).format("DD-MM-YYYY")}</div>;
      },
    },
    {
      title: "Loại yêu cầu",
      dataIndex: "type",
      key: "type",
      render: () => {
        return <div className="text-green-500">Báo cáo</div>;
      },
    },
    {
      title: "Nội dung",
      dataIndex: "content",
      key: "content",
    },
    {
      title: "Trạng thái",
      dataIndex: "status",
      key: "status",
      filters: [
        { text: "Chờ duyệt", value: "PROCESSING" },
        { text: "Đã chấp nhận", value: "ACCEPT" },
        { text: "Đã từ chối", value: "REFUSE" },
      ],
      onFilter: (value, record) => {
        // const currentQuery = queryString.parse(location.search);
        // setSearchParams({ ...currentQuery });
        console.log("value", value, record);
        console.log("----", searchParams.get("pageIndex"));
        return record.status === value;
      },
      render: (value) => {
        if (value === "PROCESSING") {
          return (
            <div className="text-yellow-500 font-semibold">
              <QuestionCircleOutlined /> Chờ duyệt
            </div>
          );
        }
        if (value === "ACCEPT") {
          return (
            <div className="text-green-500 font-semibold">
              <CheckCircleOutlined /> Đã chấp nhận
            </div>
          );
        }
        if (value === "REFUSE") {
          return (
            <div className="text-red-500 font-semibold">
              <CloseCircleOutlined /> Đã từ chối
            </div>
          );
        }
      },
    },
    {
      title: "Hành động",
      dataIndex: "",
      key: "",
      align: "center",
      render: (value, rows) => <Action value={value} rows={rows} />,
    },
  ];

  const { data, isLoading, error } = useQueryAllRequest({
    keyword: searchParams.get("search"),
    fromDate: searchParams.get("fromDate"),
    toDate: searchParams.get("toDate"),
    type: searchParams.get("type"),
    status: searchParams.get("status"),
    pageIndex: searchParams.get("pageIndex") || 1,
  });

  if (error) {
    return <ErrorScreen />;
  }

  return (
    <Table
      columns={columns}
      dataSource={data?.content}
      loading={isLoading}
      pagination={{
        defaultPageSize: 10,
        showSizeChanger: false,
        total: data?.pageable.totalElements,
        onChange: (pageIndex) => {
          const currentQuery = queryString.parse(location.search);
          setSearchParams({ ...currentQuery, pageIndex: pageIndex });
        },
      }}
    />
  );
}

export default memo(TableReport);
