import { Button, Tooltip } from "antd";
import React, { memo } from "react";
import { useNavigate } from "react-router-dom";

import { LiaGiftsSolid } from "react-icons/lia";

function Action({ value, rows }) {
  const { id, postId, userId, status } = rows;
  const navigate = useNavigate();

  // const handleShowDetailRequest = () => {
  //   navigate(`/admin/request/detail/${id}`, {
  //     state: {
  //       id: id,
  //       type: "REPORT",

  //     },
  //   });
  // };

  const handleShowDetailReportRequest = () => {
    navigate(`/admin/request/report/${id}`, {
      state: {
        postId: postId,
        userId: userId,
        id: id,
        status: status,
      },
    });
  };

  return (
    <div className="w-full gap-2 flex justify-center">
      {/* <Tooltip title="Chi tiết yêu cầu báo cáo">
        <Button onClick={handleShowDetailRequest} style={{}}>
          <FaRegEye />
        </Button>
      </Tooltip> */}
      <Tooltip title="Chi tiết sản phẩm báo cáo">
        <Button onClick={handleShowDetailReportRequest} style={{}}>
          <LiaGiftsSolid />
        </Button>
      </Tooltip>
    </div>
  );
}

export default memo(Action);
