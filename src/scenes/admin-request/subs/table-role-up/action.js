import { Button, Tooltip } from "antd";
import React, { memo } from "react";

import { FaStoreAlt } from "react-icons/fa";
import { useNavigate } from "react-router-dom";

function Action({ value, rows }) {
  const { status, userId, id } = rows;
  const navigate = useNavigate();
  // const { mutate: changeStatus, isLoading } = useChangeRequestStatus();

  // const [open, setOpen] = useState(false);
  // const handleShowDetailRequest = () => {
  //   navigate(`/admin/request/detail/${id}`, {
  //     state: {
  //       id: id,
  //       type: "ROLE_UP",
  //     },
  //   });
  // };

  const handleShowDetailRoleUpRequest = () => {
    navigate(`/admin/request/role-up/${userId}`, {
      state: {
        userId: userId,
        id: id,
        status: status,
      },
    });
  };

  // const handleApproveRequest = () => {
  //   changeStatus({
  //     id: rows.id,
  //     status: "ACCEPT",
  //     reason: "chấp nhận yêu cầu",
  //   });
  // };

  // const handleRejectRequest = async (values) => {
  //   await changeStatus({
  //     id: rows.id,
  //     status: "REFUSE",
  //     reason: values.reason,
  //   });
  //   setOpen(false);
  // };

  // if (status === "PROCESSING") {
  //   return (
  //     <div className="w-full gap-2 flex justify-center">
  //       <Tooltip title="Chấp nhận yêu cầu">
  //         <Button
  //           onClick={() => handleApproveRequest()}
  //           style={{
  //             backgroundColor: "#22c55e", //green.500
  //           }}
  //         >
  //           {isLoading ? <Spin /> : <AiOutlineCheck />}
  //         </Button>
  //       </Tooltip>
  //       <Tooltip title="Từ chối yêu cầu">
  //         <Button
  //           onClick={() => setOpen(true)}
  //           style={{
  //             backgroundColor: "#ef4444", //red.500
  //           }}
  //         >
  //           {isLoading ? <Spin /> : <AiOutlineClose />}
  //         </Button>
  //       </Tooltip>
  //       {/* <Tooltip title="Chi tiết yêu cầu">
  //         <Button onClick={handleShowDetailRequest} style={{}}>
  //           <FaRegEye />
  //         </Button>
  //       </Tooltip> */}
  //       <Tooltip title="Chi tiết cửa hàng đăng kí">
  //         <Button onClick={handleShowDetailRoleUpRequest} style={{}}>
  //           <FaStoreAlt />
  //         </Button>
  //       </Tooltip>
  //       <Modal
  //         closeIcon={null}
  //         title="Nhập lý do từ chối yêu cầu"
  //         open={open}
  //         footer={null}
  //       >
  //         <Form name="reject-request" onFinish={handleRejectRequest}>
  //           <Form.Item
  //             label=""
  //             name="reason"
  //             rules={[
  //               {
  //                 required: true,
  //                 message: "Vui lòng nhập lí do từ chối yêu cầu!",
  //               },
  //             ]}
  //           >
  //             <Input />
  //           </Form.Item>
  //           <div className="w-full flex justify-end gap-2">
  //             <Button
  //               style={{
  //                 backgroundColor: "blue",
  //                 color: "white",
  //               }}
  //               onClick={() => setOpen(false)}
  //             >
  //               Hủy bỏ
  //             </Button>
  //             <Form.Item>
  //               <Button
  //                 style={{
  //                   backgroundColor: "red",
  //                   color: "white",
  //                 }}
  //                 htmlType="submit"
  //               >
  //                 {isLoading ? <Spin /> : "Xác nhận"}
  //               </Button>
  //             </Form.Item>
  //           </div>
  //         </Form>
  //       </Modal>
  //     </div>
  //   );
  // }

  return (
    <div className="w-full gap-2 flex justify-center">
      {/* <Tooltip title="Chi tiết yêu cầu">
        <Button onClick={handleShowDetailRequest} style={{}}>
          <FaRegEye />
        </Button>
      </Tooltip> */}
      <Tooltip title="Chi tiết cửa hàng đăng ký">
        <Button onClick={handleShowDetailRoleUpRequest} style={{}}>
          <FaStoreAlt />
        </Button>
      </Tooltip>
    </div>
  );
}

export default memo(Action);
