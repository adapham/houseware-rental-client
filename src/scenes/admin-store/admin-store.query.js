import { useQuery } from "@tanstack/react-query";
import API from "../../utils/api";

export const QUERY_ALL_STORE_KEY = "QUERY_ALL_STORE_KEY";

const getAllStore = ({ keyword, fromDate, toDate, pageIndex, pageSize }) => {
  return API.request({
    url: "/api/stores",
    params: {
      keyword: keyword,
      fromDate: fromDate,
      toDate: toDate,
      pageIndex: pageIndex - 1,
      pageSize: pageSize,
    },
  }).then((res) => {
    return res;
  });
};

export const useQueryAllStore = ({
  keyword,
  fromDate,
  toDate,
  pageIndex = 1,
  pageSize = 10,
}) => {
  return useQuery({
    queryKey: [
      QUERY_ALL_STORE_KEY,
      keyword,
      fromDate,
      toDate,
      pageIndex,
      pageSize,
    ],
    queryFn: () =>
      getAllStore({ keyword, fromDate, toDate, pageIndex, pageSize }),
  });
};

export const GET_DETAIL_STORE_KEY = "GET_DETAIL_STORE_KEY";

const getDetailStore = (id) => {
  return API.request({
    url: `/api/stores/${id}`,
  });
};

export const useQueryDetailStore = ({ id }) => {
  return useQuery({
    queryKey: [GET_DETAIL_STORE_KEY, id],
    queryFn: () => getDetailStore(id),
    enabled: !!id,
  });
};
