import { ExclamationCircleFilled } from "@ant-design/icons";
import { Button, Divider, Form, Input, Modal, Spin, Tooltip } from "antd";
import React, { memo, useState } from "react";
import { AiOutlineCheck, AiOutlineClose } from "react-icons/ai";
import { useLocation } from "react-router-dom";
import BackButton from "../../../components/AdminStyle/subs/back-button";
import ErrorScreen from "../../../pages/examples/error-screen";
import LoadingScreen from "../../../pages/examples/loading-screen";
import { useApproveStore, useCancelStore } from "../admin-store.mutate";
import { useQueryDetailStore } from "../admin-store.query";

const { confirm } = Modal;

function DetailStore() {
  const location = useLocation();
  const { id, status } = location.state;

  const { data, isLoading, error } = useQueryDetailStore({ id });

  const { mutate: approveStore, isLoading: isLoadingApprove } =
    useApproveStore();
  const { mutate: cancelStore, isLoading: isLoadingCancel } = useCancelStore();

  const [open, setOpen] = useState(false);

  const handleApproveStore = () => {
    confirm({
      title: "Bạn có muốn chấp nhận yêu cầu này không ?",
      content: "",
      icon: <ExclamationCircleFilled />,
      onOk: () => {
        approveStore({ id });
      },
      onCancel: () => {},
    });
  };

  const handleCancelStore = async (values) => {
    console.log("Lý do hủy cửa hàng :", values.reasonReject);
    await cancelStore({ id, reasonReject: values.reasonReject });
    setOpen(false);
  };

  if (isLoading) {
    return <LoadingScreen />;
  }

  if (error) {
    return <ErrorScreen />;
  }

  return (
    <>
      <div className="flex flex-col items-center gap-2 w-full">
        <div className="w-3/4 flex justify-start">
          <Divider orientation="left">Thông tin chi tiết cửa hàng</Divider>
        </div>
        <div className="w-3/4 flex justify-center gap-2">
          <Form
            name="detail-request"
            layout="vertical"
            style={{
              width: "100%",
            }}
            initialValues={{
              ...data,
            }}
            onFinish={() => {}}
            onFinishFailed={() => {}}
            autoComplete="off"
            disabled
          >
            <div className="w-full grid grid-cols-1">
              <Form.Item
                label={<div className="font-semibold">Tên cửa hàng :</div>}
                name="title"
              >
                <Input style={{ width: "100%" }} placeholder={data.title} />
              </Form.Item>
              <Form.Item
                label={<div className="font-semibold">Mô tả :</div>}
                name="description"
              >
                <Input.TextArea
                  style={{ width: "100%" }}
                  placeholder={data.description}
                />
              </Form.Item>
            </div>
            <div className="w-full grid grid-cols-2 gap-2">
              <Form.Item
                label={<div className="font-semibold">Số điện thoại :</div>}
                name="phone"
              >
                <Input style={{ width: "100%" }} placeholder={data.phone} />
              </Form.Item>
              <Form.Item
                label={<div className="font-semibold">Xã / Phường :</div>}
                name="ward"
              >
                <Input style={{ width: "100%" }} placeholder={data.ward} />
              </Form.Item>
              <Form.Item
                label={<div className="font-semibold">Quận / Huyện :</div>}
                name="district"
              >
                <Input style={{ width: "100%" }} placeholder={data.district} />
              </Form.Item>
              <Form.Item
                label={<div className="font-semibold">Tỉnh / Thành phố :</div>}
                name="province"
              >
                <Input style={{ width: "100%" }} placeholder={data.province} />
              </Form.Item>
            </div>
          </Form>
        </div>
        <div className="w-3/4 flex justify-start">
          <Divider orientation="left"></Divider>
        </div>
        <div className="flex justify-center gap-1 p-4">
          {status === "REFUSE" && (
            <Tooltip title="Cho phép hoạt động">
              <Button
                onClick={handleApproveStore}
                style={{
                  backgroundColor: "#22c55e", //green.500
                }}
              >
                {isLoadingApprove ? <Spin /> : <AiOutlineCheck />}
              </Button>
            </Tooltip>
          )}

          {status === "ACCEPT" && (
            <Tooltip title="Hủy cửa hàng">
              <Button
                onClick={() => {
                  setOpen(true);
                }}
                style={{
                  backgroundColor: "#ef4444", //red.500
                }}
              >
                {isLoadingCancel ? <Spin /> : <AiOutlineClose />}
              </Button>
            </Tooltip>
          )}
          {status === "PROGESSING" && (
            <>
              <Tooltip title="Chấp nhận cửa hàng">
                <Button
                  onClick={handleApproveStore}
                  style={{
                    backgroundColor: "#22c55e", //green.500
                  }}
                >
                  {isLoadingApprove ? <Spin /> : <AiOutlineCheck />}
                </Button>
              </Tooltip>
              <Tooltip title="Hủy cửa hàng">
                <Button
                  onClick={() => {
                    setOpen(true);
                  }}
                  style={{
                    backgroundColor: "#ef4444", //red.500
                  }}
                >
                  {isLoadingCancel ? <Spin /> : <AiOutlineClose />}
                </Button>
              </Tooltip>
            </>
          )}
          <BackButton />
        </div>
      </div>
      <Modal
        closeIcon={null}
        title="Vui lòng nhập lí do hủy cửa hàng!"
        open={open}
        footer={null}
      >
        <Form name="reject-store" onFinish={handleCancelStore}>
          <Form.Item
            label=""
            name="reasonReject"
            rules={[
              {
                required: true,
                message: "Vui lòng nhập lí do hủy cửa hàng!",
              },
            ]}
          >
            <Input />
          </Form.Item>
          <div className="w-full flex justify-end gap-2">
            <Button
              style={{
                backgroundColor: "blue",
                color: "white",
              }}
              onClick={() => setOpen(false)}
            >
              Hủy bỏ
            </Button>
            <Form.Item>
              <Button
                style={{
                  backgroundColor: "red",
                  color: "white",
                }}
                htmlType="submit"
              >
                {isLoadingCancel ? <Spin /> : "Xác nhận"}
              </Button>
            </Form.Item>
          </div>
        </Form>
      </Modal>
    </>
  );
}

export default memo(DetailStore);
