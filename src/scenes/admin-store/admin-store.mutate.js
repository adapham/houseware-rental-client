import { useMutation, useQueryClient } from "@tanstack/react-query";
import { useNavigate } from "react-router-dom";
import { toast } from "react-toastify";
import API from "../../utils/api";
import { GET_DETAIL_STORE_KEY, QUERY_ALL_STORE_KEY } from "./admin-store.query";

const APPROVE_STORE_KEY = "APPROVE_STORE_KEY";

const approveStore = (params) => {
  const id = params.id;
  return API.request({
    method: "PUT",
    url: `/api/stores/${id}/approve`,
  });
};

export const useApproveStore = () => {
  const navigate = useNavigate();
  const queryClient = useQueryClient();
  return useMutation({
    mutationKey: [APPROVE_STORE_KEY],
    mutationFn: approveStore,
    onSuccess: (_, params) => {
      queryClient.invalidateQueries([
        QUERY_ALL_STORE_KEY,
        GET_DETAIL_STORE_KEY,
      ]);
      navigate(-1);
      toast.success(`Chấp nhận cửa hàng thành công.`);
    },
    onError: (_, params) => {
      queryClient.invalidateQueries([
        QUERY_ALL_STORE_KEY,
        GET_DETAIL_STORE_KEY,
      ]);
      toast.error(`Chấp nhận cửa hàng thất bại.`);
    },
  });
};

//=====================================================

const CANCEL_STORE_KEY = "CANCEL_STORE_KEY";

const cancelStore = (params) => {
  const { id, reasonReject } = params;
  return API.request({
    method: "PUT",
    url: `/api/stores/${id}/cancel`,
    params: {
      reasonReject,
    },
  });
};

export const useCancelStore = () => {
  const navigate = useNavigate();
  const queryClient = useQueryClient();
  return useMutation({
    mutationKey: [CANCEL_STORE_KEY],
    mutationFn: cancelStore,
    onSuccess: (_, params) => {
      queryClient.invalidateQueries([
        QUERY_ALL_STORE_KEY,
        GET_DETAIL_STORE_KEY,
      ]);
      navigate(-1);
      toast.success(`Hủy cửa hàng thành công.`);
    },
    onError: (_, params) => {
      queryClient.invalidateQueries([
        QUERY_ALL_STORE_KEY,
        GET_DETAIL_STORE_KEY,
      ]);
      toast.error(`Hủy cửa hàng thất bại.`);
    },
  });
};
