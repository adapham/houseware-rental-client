import {
  CheckCircleOutlined,
  CloseCircleOutlined,
  QuestionCircleOutlined,
} from "@ant-design/icons";
import { Input, Table } from "antd";
import queryString from "query-string";
import React, { memo } from "react";
import { useLocation, useSearchParams } from "react-router-dom";
import ErrorScreen from "../../pages/examples/error-screen";
import { useQueryAllStore } from "./admin-store.query";
import Action from "./subs/action";
import ImagePreview from "./subs/image-preview";
const { Search } = Input;

function AdminStore() {
  const location = useLocation();

  document.title = "Quản lý cửa hàng";
  const columns = [
    {
      title: "STT",
      dataIndex: "index",
      key: "index",
      align: "center",
      render: (text, record, index) => index + 1,
    },

    {
      width: "20%",
      title: "Ảnh cửa hàng",
      dataIndex: "imageTextUri",
      key: "imageTextUri",
      align: "center",
      render: (url) => {
        return <ImagePreview url={url[0]} />;
      },
    },
    {
      title: "Tên cửa hàng",
      dataIndex: "title",
      key: "title",
      render: (value) => {
        return <div className="font-bold">{value}</div>;
      },
    },
    {
      title: "Địa chỉ",
      dataIndex: "address",
      key: "address",
      render: (value) => {
        return <div className="text-blue-500 text-ellipsis">{value}</div>;
      },
    },
    {
      align: "center",
      title: "Tổng sản phẩm đang có",
      dataIndex: "totalProduct",
      key: "totalProduct",
      sorter: (a, b) => a.totalProduct - b.totalProduct,
    },
    {
      align: "center",
      title: "Số lần vi phạm",
      dataIndex: "violations",
      key: "violations",
      sorter: (a, b) => a.violations - b.violations,
    },
    {
      align: "center",
      title: "Trạng thái cửa hàng",
      dataIndex: "status",
      key: "status",
      filters: [
        { text: "Đang hoạt động", value: "ACCEPT" },
        { text: "Tạm dừng kinh doanh", value: "REFUSE" },
      ],
      onFilter: (value, record) => record.status === value,
      render: (status) => {
        if (status === "ACCEPT") {
          return (
            <div className="text-green-500 font-semibold">
              <CheckCircleOutlined /> Đang hoạt động
            </div>
          );
        }
        if (status === "REFUSE") {
          return (
            <div className="text-red-500 font-semibold">
              <CloseCircleOutlined /> Tạm dừng kinh doanh
            </div>
          );
        }
        return (
          <div className="text-yellow-500 font-semibold">
            <QuestionCircleOutlined /> Chờ phê duyệt
          </div>
        );
      },
    },
    {
      title: "Hành động",
      dataIndex: "",
      key: "",
      align: "center",
      render: (value, rows) => <Action value={value} rows={rows} />,
    },
  ];

  const [searchParams, setSearchParams] = useSearchParams();
  const { data, isLoading, error } = useQueryAllStore({
    keyword: searchParams.get("search"),
    fromDate: searchParams.get("fromDate"),
    toDate: searchParams.get("toDate"),
  });

  const onSearch = (keywordSearch) => {
    const currentQuery = queryString.parse(location.search);
    setSearchParams({ ...currentQuery, search: keywordSearch });
  };

  if (error) {
    return <ErrorScreen />;
  }

  return (
    <div className="w-full flex flex-col gap-2">
      <div className="flex gap-4 items-center w-full justify-between mb-2">
        <Search
          style={{
            width: "300px",
          }}
          autoFocus
          placeholder="Tìm kiếm theo tên cửa hàng"
          onSearch={onSearch}
        />
      </div>
      <div className="">
        <Table
          columns={columns}
          dataSource={data?.content}
          loading={isLoading}
          pagination={{
            defaultPageSize: 10,
            showSizeChanger: false,
            total: data?.pageable.totalElements,
            onChange: (pageIndex) => {
              const currentQuery = queryString.parse(location.search);
              setSearchParams({
                ...currentQuery,
                pageIndex: pageIndex,
              });
            },
          }}
        />
      </div>
    </div>
  );
}

export default memo(AdminStore);
