import { Button, Tooltip } from "antd";
import React, { memo } from "react";

import { AiFillEye } from "react-icons/ai";
import { useNavigate } from "react-router-dom";

function Action({ value, rows }) {
  const { id: storeId, status } = rows;
  const navigate = useNavigate();

  const handleGotoDetail = () => {
    navigate(`/admin/store/${storeId}`, {
      state: {
        id: storeId,
        status: status,
      },
    });
  };

  return (
    <>
      <div className="w-full gap-2 flex justify-start">
        <Tooltip title="Xem chi tiết">
          <Button onClick={handleGotoDetail} style={{}}>
            <AiFillEye />
          </Button>
        </Tooltip>
      </div>
    </>
  );
}

export default memo(Action);
