import React, { memo } from "react";

import { Button, Input, Table } from "antd";
import { useLocation, useNavigate, useSearchParams } from "react-router-dom";

import dayjs from "dayjs";
import queryString from "query-string";
import ErrorScreen from "../../pages/examples/error-screen";
import { useQueryAllCategory } from "./admin-category.query";
import Action from "./subs/action";

const { Search } = Input;

function AdminCategory() {
  const location = useLocation();
  document.title = "Quản lý danh mục";
  const navigate = useNavigate();

  const columns = [
    {
      title: "STT",
      dataIndex: "index",
      key: "index",
      align: "center",
      render: (text, record, index) => index + 1,
    },
    {
      title: "Tên danh mục",
      dataIndex: "categoryName",
      key: "categoryName",
      render: (value) => {
        return <div className="font-bold">{value}</div>;
      },
    },
    {
      title: "Mô tả",
      dataIndex: "description",
      key: "description",
    },
    {
      title: "Ngày tạo",
      dataIndex: "createdTime",
      key: "createdTime",
      sorter: (a, b) =>
        dayjs(a.createdTime).unix() - dayjs(b.createdTime).unix(),
      render: (value) => dayjs(value).format("DD-MM-YYYY"),
    },
    {
      title: "Tổng sản phẩm",
      align: "center",
      dataIndex: "totalProduct",
      key: "totalProduct",
      sorter: (a, b) => a.totalProduct - b.totalProduct,
    },
    {
      title: "Hành động",
      dataIndex: "",
      key: "",
      align: "center",
      render: (value, rows) => <Action value={value} rows={rows} />,
    },
  ];

  const [searchParams, setSearchParams] = useSearchParams();

  const { data, isLoading, error } = useQueryAllCategory({
    keyword: searchParams.get("search"),
    fromDate: searchParams.get("fromDate"),
    toDate: searchParams.get("toDate"),
    pageIndex: searchParams.get("pageIndex") || 1,
  });

  const onSearch = (keywordSearch) => {
    const currentQuery = queryString.parse(location.search);
    setSearchParams({ ...currentQuery, search: keywordSearch });
  };

  const handleCreateCategory = () => {
    navigate("/admin/category/create");
  };

  if (error) {
    return <ErrorScreen />;
  }
  return (
    <div className="w-full flex flex-col gap-2">
      <div className="flex gap-4 items-center w-full justify-between mb-2">
        <Search
          style={{
            width: "300px",
          }}
          autoFocus
          placeholder="Tìm kiếm theo tên danh mục"
          onSearch={onSearch}
        />
        <Button style={{ fontWeight: "bold" }} onClick={handleCreateCategory}>
          Thêm danh mục
        </Button>
      </div>
      <div className="">
        <Table
          columns={columns}
          dataSource={data?.content}
          loading={isLoading}
          pagination={{
            defaultPageSize: 10,
            showSizeChanger: false,
            total: data?.pageable.totalElements,
            onChange: (pageIndex) => {
              const currentQuery = queryString.parse(location.search);

              setSearchParams({
                ...currentQuery,
                pageIndex: pageIndex,
              });
            },
          }}
        />
      </div>
    </div>
  );
}

export default memo(AdminCategory);
