import React, { memo, useState } from "react";

import { DeleteOutlined, EditOutlined } from "@ant-design/icons";
import { Button, Modal, Spin, Tooltip } from "antd";
import { useNavigate } from "react-router-dom";
import { useDeleteCategory } from "../admin-category.mutate";

const { confirm } = Modal;

function Action({ value, rows }) {
  const navigate = useNavigate();
  const categoryId = rows.id;
  const categoryName = rows.categoryName;
  const description = rows.description;

  const [openConfirm, setOpenConfirm] = useState(false);
  const [openReject, setOpenReject] = useState(false);

  const { mutate: deleteCategory, isLoading: isLoadingDelete } =
    useDeleteCategory();

  const handleEdit = () => {
    navigate(`/admin/category/edit/${categoryId}`, {
      state: {
        id: categoryId,
        categoryName,
        description,
      },
    });
  };

  const handleDelete = () => {
    deleteCategory(categoryId);
  };
  return (
    <>
      <div className="w-full flex justify-center items-center gap-2">
        <Tooltip title="Chỉnh sửa">
          <Button
            onClick={() => {
              setOpenConfirm(true);
            }}
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              color: "#FFF",
              fontWeight: "bold",
              backgroundColor: "#fdba74", //orange taiwin 300
            }}
          >
            <EditOutlined />
          </Button>
        </Tooltip>
        <Tooltip title="Xóa">
          <Button
            onClick={() => {
              setOpenReject(true);
            }}
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              color: "#FFF",
              fontWeight: "bold",
              backgroundColor: "red",
            }}
          >
            {isLoadingDelete ? <Spin /> : <DeleteOutlined />}
          </Button>
        </Tooltip>
      </div>
      <Modal
        open={openConfirm}
        closeIcon={null}
        title="Bạn có muốn chỉnh sửa danh mục này không ?"
        footer={null}
      >
        <div className="w-full flex justify-end gap-2 pb-4">
          <Button
            style={{
              backgroundColor: "blue",
              color: "white",
            }}
            onClick={() => setOpenConfirm(false)}
          >
            Hủy bỏ
          </Button>

          <Button
            style={{
              backgroundColor: "green",
              color: "white",
            }}
            onClick={handleEdit}
          >
            Xác nhận
          </Button>
        </div>
      </Modal>
      <Modal
        open={openReject}
        closeIcon={null}
        title="Bạn có muốn xóa danh mục này không ?"
        footer={null}
      >
        <div className="w-full flex justify-end gap-2 pb-4">
          <Button
            style={{
              backgroundColor: "blue",
              color: "white",
            }}
            onClick={() => setOpenReject(false)}
          >
            Hủy bỏ
          </Button>

          <Button
            style={{
              backgroundColor: "red",
              color: "white",
            }}
            onClick={() => {
              handleDelete();
              setOpenReject(false);
            }}
          >
            Xác nhận
          </Button>
        </div>
      </Modal>
    </>
  );
}

export default memo(Action);
