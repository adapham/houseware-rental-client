import { useMutation, useQueryClient } from "@tanstack/react-query";
import { useNavigate } from "react-router-dom";
import { toast } from "react-toastify";
import API from "../../utils/api";
import { QUERY_ALL_CATEGORIES_KEY } from "./admin-category.query";

const DELETE_CATEGORY_KEY = "DELETE_CATEGORY_KEY";

const deleteCategory = (id) => {
  return API.request({
    method: "DELETE",
    url: `/api/categories/${id}`,
  });
};

export const useDeleteCategory = () => {
  const queryClient = useQueryClient();

  return useMutation({
    mutationKey: [DELETE_CATEGORY_KEY],
    mutationFn: deleteCategory,
    onSuccess: (_, params) => {
      queryClient.invalidateQueries([QUERY_ALL_CATEGORIES_KEY]);
      toast.success(`Xoá danh mục thành công.`);
    },
    onError: (_, params) => {
      queryClient.invalidateQueries([QUERY_ALL_CATEGORIES_KEY]);
      toast.error(`Xóa danh mục thất bại.`);
    },
  });
};

const CREATE_CATEGORY_KEY = "CREATE_CATEGORY_KEY";

const createCategory = (params) => {
  return API.request({
    method: "POST",
    url: "/api/categories",
    params: params,
  });
};

export const useCreateCategory = () => {
  const navigate = useNavigate();
  return useMutation({
    mutationKey: [CREATE_CATEGORY_KEY],
    mutationFn: createCategory,
    onSuccess: (_, params) => {
      navigate("/admin/category");
      toast.success(`Tạo mới danh mục thành công.`);
    },
    onError: (_, params) => {
      toast.error(`Tạo mới danh mục thất bại.`);
    },
  });
};

const UPDATE_CATEGORY_KEY = "UPDATE_CATEGORY_KEY";

const updateCategory = (params) => {
  return API.request({
    method: "PUT",
    url: `/api/categories/${params.id}`,
    params: {
      categoryName: params.categoryName,
      description: params.description,
    },
  });
};

export const useUpdateCategory = () => {
  const navigate = useNavigate();
  return useMutation({
    mutationKey: [UPDATE_CATEGORY_KEY],
    mutationFn: updateCategory,
    onSuccess: (_, params) => {
      navigate("/admin/category");
      toast.success(`Cập nhật danh mục thành công.`);
    },
    onError: (_, params) => {
      toast.error(`Cập nhật danh mục thất bại.`);
    },
  });
};
