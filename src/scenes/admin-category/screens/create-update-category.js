import { Button, Form, Input, Modal } from "antd";
import { isNull } from "lodash";
import React, { memo } from "react";
import { useLocation } from "react-router-dom";
import BackButton from "../../../components/AdminStyle/subs/back-button";
import { useCreateCategory, useUpdateCategory } from "../admin-category.mutate";

const { confirm } = Modal;

function AdminCreateUpdateCategory() {
  const location = useLocation();
  const isCreate = isNull(location.state);

  const { mutate: createCategory } = useCreateCategory();
  const { mutate: updateCategory } = useUpdateCategory();

  const onFinish = (values) => {
    confirm({
      title: `Xác nhận ${isCreate ? "tạo mới" : "cập nhật"} danh mục?`,
      content: "",
      icon: null,
      onOk: () => {
        if (isCreate) {
          createCategory(values);
          return;
        }

        const id = location.state?.id;
        updateCategory({
          id: id,
          ...values,
        });
        return;
      },
      onCancel: () => {},
    });
  };

  const onFinishFailed = () => {
    console.log("on finish failed");
  };

  return (
    <div className="w-full justify-center items-center flex flex-col ">
      <div className="w-1/2">
        <Form
          name="create-update-category"
          layout="vertical"
          style={{
            width: "100%",
          }}
          initialValues={{
            ...location.state,
          }}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          autoComplete="off"
        >
          <Form.Item
            label={<div className="font-semibold">Tên danh mục :</div>}
            name="categoryName"
            rules={[
              {
                required: true,
                message: "Vui lòng nhập tên danh mục!",
              },
            ]}
          >
            <Input placeholder="Nhập tên danh mục tại đây" />
          </Form.Item>

          <Form.Item
            label={<div className="font-semibold">Mô tả danh mục :</div>}
            name="description"
            rules={[
              {
                required: true,
                message: "Vui lòng nhập mô tả",
              },
            ]}
          >
            <Input.TextArea placeholder="Nhập mô tả cửa hàng tại đây" />
          </Form.Item>

          <div className="w-full flex justify-center my-4 gap-2">
            <div>
              <Form.Item>
                <Button style={{}} htmlType="submit">
                  {isCreate ? "Tạo mới danh mục" : "Cập nhật danh mục"}
                </Button>
              </Form.Item>
            </div>
            <div>
              <BackButton />
            </div>
          </div>
        </Form>
      </div>
    </div>
  );
}

export default memo(AdminCreateUpdateCategory);
