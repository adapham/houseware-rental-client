import { useQuery } from "@tanstack/react-query";
import API from "../../utils/api";

export const QUERY_ALL_CATEGORIES_KEY = "QUERY_ALL_CATEGORIES_KEY";

const getAllCategories = ({
  keyword,
  fromDate,
  toDate,
  pageIndex,
  pageSize,
}) => {
  return API.request({
    url: "/api/categories/query",
    params: {
      keyword: keyword,
      fromDate: fromDate,
      toDate: toDate,
      pageIndex: pageIndex - 1,
      pageSize: pageSize,
    },
  }).then((res) => {
    return res;
  });
};

export const useQueryAllCategory = ({
  keyword,
  fromDate,
  toDate,
  pageIndex = 1,
  pageSize = 10,
}) => {
  return useQuery({
    queryKey: [
      QUERY_ALL_CATEGORIES_KEY,
      keyword,
      fromDate,
      toDate,
      pageIndex,
      pageSize,
    ],
    queryFn: () =>
      getAllCategories({ keyword, fromDate, toDate, pageIndex, pageSize }),
  });
};
