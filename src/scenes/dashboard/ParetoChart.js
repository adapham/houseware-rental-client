import React, { useEffect } from 'react';
import Highcharts from 'highcharts/highstock'; // Import highstock instead of highcharts
import exporting from 'highcharts/modules/exporting';
import exportData from 'highcharts/modules/export-data';
import { Button } from 'antd';
const BASE_URL = process.env.REACT_APP_BASE_URL;
exporting(Highcharts);
exportData(Highcharts);

const ParetoChart = () => {
  useEffect(() => {
    const fetchData = async () => {
      try {
        // Load the dataset
        const response = await fetch(
          `${BASE_URL}/api/admin/dashboard-revenue`
        );
        const data = await response.json();
        const inputObject = data?.revenue;
        
        const outputArray = [];
        
        for (const [timestamp, value] of Object.entries(inputObject)) {
          const timestampInMillis = parseInt(timestamp) * 1000;
          const dataPoint = [timestampInMillis, value];
          outputArray.push(dataPoint);
        }
        // Create the chart
        const chart = Highcharts.stockChart('container', {
          chart: {
            height: 400,
          },
          title: {
            text: 'Thống kê doanh thu',
          },
          
          rangeSelector: {
            selected: 1,
          },
          series: [
            {
              name: 'Tổng doanh thu',
              data: outputArray,
              type: 'area',
              threshold: null,
              tooltip: {
                valueDecimals: 0,
              },
            },
          ],
          responsive: {
            rules: [
              {
                condition: {
                  maxWidth: 500,
                },
                chartOptions: {
                  chart: {
                    height: 300,
                  },
                  subtitle: {
                    text: null,
                  },
                  navigator: {
                    enabled: false,
                  },
                },
              },
            ],
          },
        });

        // Event listeners for resizing
        document.getElementById('small').addEventListener('click', () => {
          chart.setSize(400);
        });

        document.getElementById('large').addEventListener('click', () => {
          chart.setSize(800);
        });

        document.getElementById('auto').addEventListener('click', () => {
          chart.setSize(null);
        });
      } catch (error) {
        console.error('Error fetching data:', error);
      }
    };

    fetchData();
  }, []); // Empty dependency array to run the effect only once

  return (
    <div>
      <div id="container" />
      <Button id="small">Small</Button>
      <Button id="large">Large</Button>
      <Button id="auto">Auto</Button>
    </div>
  );
};

export default ParetoChart;
