import { Box } from "@mui/material";
import StatisticStoreAdmin from "./StatisticStoreAdmin";
import { useEffect, useState } from "react";
import { getStaticAdmin, getStaticAdminIncome, getStaticCategoryAdmin, getStaticUserAdmin } from "../../redux/apiRequestProduct";

const Dashboard = () => {
  document.title = "Trang chủ admin";
  const [dataStatic,setDataStatic]= useState();
  const [staticIncome,setStaticIncome]=useState();
  const [categoryStatic,setCategoryStatic]=useState();
  const [statisUserAdmin,setStatisUserAdmin]=useState([]);
  const [typeIncome, setTypeIncome] = useState("ONE_MONTH");
  const [typeUserAdmin, setTypeUserAdmin] = useState("ONE_MONTH");
  const getCategoryAdminStatic = async ()=>{
    const response = await getStaticCategoryAdmin();
    if(response){
      setCategoryStatic(response);
    }
  }
  const getStaticIncome = async (type)=>{

    const response = await getStaticAdminIncome(type);
    if(response){
      setStaticIncome(response);
    }
  }
  useEffect(()=>{
    if (typeIncome) {
      getStaticIncome(typeIncome);
    }
  },[typeIncome])
  useEffect(() => {
    if (typeUserAdmin) {
      getUserStaticAdmin(typeUserAdmin);
    }
  }, [typeUserAdmin])
  const getDataStaticAdmin = async ()=>{

    const response = await getStaticAdmin();
    if(response){
      setDataStatic(response);
    }
  }
  useEffect(()=>{
    getDataStaticAdmin();
    getCategoryAdminStatic();
  },[])
  const getUserStaticAdmin = async (type)=>{
    const response = await getStaticUserAdmin( type);
    if (response) {
      setStatisUserAdmin(response);
    }
  }

  return <>
  <StatisticStoreAdmin dataStatic={dataStatic}
   setTypeIncome={setTypeIncome} 
   statisUserAdmin={statisUserAdmin}
   setTypeUserAdmin={setTypeUserAdmin}
  staticIncome={staticIncome}
  categoryStatic={categoryStatic} />
  </>
};

export default Dashboard;
