import React, { useEffect, useState } from 'react';
import Highcharts from 'highcharts';
import highcharts3d from 'highcharts/highcharts-3d'; // Import the highcharts-3d module
import exporting from 'highcharts/modules/exporting'; // Import the exporting module
import exportData from 'highcharts/modules/export-data'; // Import the export-data module
import { Select } from 'antd';

exporting(Highcharts);
exportData(Highcharts);


export default function CylinderChart({setTypeUserAdmin,statisUserAdmin}) {
    const chartHeight = 400;
    const [tickInterval,setTickInterval]=useState(5);
    const handleChange = (value) => {
      setTypeUserAdmin(value);
        switch (value) { 
          case 'ONE_WEEK':
            setTickInterval(2);
            break;
          case 'ONE_MONTH':
            setTickInterval(5);
            break;
          case 'ONE_YEAR':
            setTickInterval(100);
            break;
          default:
            setTickInterval(1);
            break;
        }
      };
  const chartConfig = {
    chart: {
      type: 'column', // Use 'column' chart type for a similar effect
      options: {
        enabled: true,
        alpha: 15,
        beta: 15,
        depth: 50,
        viewDistance: 25
      }
    },
    title: {
      text: 'Thống kê lượng người sử dụng ứng dụng'
    },
    xAxis: {
      categories: statisUserAdmin?.date,
      title: {
        text: 'Thời gian'
      },
      tickInterval: tickInterval,
      labels: {
        skew3d: true
      }
    },
    yAxis: {
      title: {
        margin: 20,
        text: 'Reported cases'
      },
      labels: {
        skew: true
      }
    },
    tooltip: {
      headerFormat: '<b>Age: {point.x}</b><br>'
    },
    plotOptions: {
      series: {
        depth: 25,
        colorByPoint: true
      }
    },
    series: [{
      data: statisUserAdmin?.user,
      name: 'Cases',
      showInLegend: false
    }],
    exporting: {
      enabled: true, // Enable exporting module
      buttons: {
        contextButton: {
          menuItems: ['downloadPNG', 'downloadCSV'], // Add downloadPNG and downloadCSV options
        },
      },
    },
  };

  useEffect(() => {
    Highcharts.chart('cylinderChart', chartConfig);
  }, [statisUserAdmin,tickInterval]);

  
  return (
    <div>
      <div>
      <Select
      defaultValue="ONE_MONTH"
      style={{
        width: 120,
      }}
      onChange={handleChange}
      options={[
        {
          value: 'ONE_DAY',
          label: 'Một ngày',
        },
        {
          value: 'THREE_DAY',
          label: 'Ba ngày',
        },
        {
          value: 'ONE_WEEK',
          label: 'Một tuần',
        },
        {
          value: 'ONE_MONTH',
          label: 'Một Tháng',
        },
        {
          value: 'ONE_YEAR',
          label: 'Một năm',
        },
      ]}
    />
      </div>

      <div style={{ height: chartHeight }} id="cylinderChart"></div>
    </div>
  );
}
