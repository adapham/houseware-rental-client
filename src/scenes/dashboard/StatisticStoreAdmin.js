import React from "react";

import { Card, Col, Row, Statistic } from "antd";
import { FcPaid } from "react-icons/fc";

import { FcCurrencyExchange } from "react-icons/fc";
import { FcFaq } from "react-icons/fc";

import { FcShop } from "react-icons/fc";
import PieChartAdmin from "./PieChartAdmin";
import StackAreaChart from "./StackAreaChart";
import { FcConferenceCall } from "react-icons/fc";
import ParetoChart from "./ParetoChart";
import CylinderChart from "./CylinderChart";
import SemiCircleDonut from "./SemiCircleDonut";

const StatisticStoreAdmin = ({ dataStatic, staticIncome, setTypeIncome, categoryStatic,statisUserAdmin,setTypeUserAdmin }) => {

  return (
    <>

      {dataStatic && (
        <div>
          <Row gutter={1} justify="space-between">
            <Col span={1 / 5} className="text-center" style={{ width: '250px' }}>
              <Card bordered={false}>
                <Statistic
                  title={
                    <>
                      <center>
                        <FcCurrencyExchange className="w-10 h-10" />
                      </center>
                      <span className="text-xg font-extrabold">Tổng doanh thu</span>
                    </>
                  }
                  //value={staticData?.totalRevenue}
                  precision={0}
                  value={dataStatic?.totalRevenue}
                  valueStyle={{
                    color: "#3f51b5",
                  }}
                  // prefix={<ArrowUpOutlined />}
                  suffix="đ"
                />
              </Card>
            </Col>

            <Col span={1 / 5} className="text-center" style={{ width: '250px' }}>
              <Card bordered={false}>
                <Statistic
                  title={
                    <>
                      <center>
                        <FcPaid className="w-10 h-10" />
                      </center>
                      <span className="text-xg font-extrabold">Tổng sản phẩm đang thuê</span>
                    </>
                  }
                  //value={(staticData?.totalOrderRented)}
                  precision={0}
                  value={dataStatic?.totalProduct}
                  valueStyle={{
                    color: "#3f8600",
                  }}
                  // prefix={<ArrowUpOutlined />}
                  suffix="Sản phẩm"
                />
              </Card>
            </Col>
            <Col span={1 / 5} className="text-center" style={{ width: '250px' }}>
              <Card bordered={false}>
                <Statistic
                  title={
                    <>
                      <center>
                        <FcConferenceCall className="w-10 h-10" />
                      </center>
                      <span className="text-xg font-extrabold">Số người dùng ứng dụng</span>
                    </>
                  }
                  //value={(staticData?.totalOrderRented)}
                  precision={0}
                  value={dataStatic?.totalUser}
                  valueStyle={{
                    color: "#3f5600",
                  }}
                  // prefix={<ArrowUpOutlined />}
                  suffix="Người dùng"
                />
              </Card>
            </Col>
            <Col span={1 / 5} className="text-center" style={{ width: '250px' }}>
              <Card bordered={false}>
                <Statistic
                  title={
                    <>
                      <center>
                        <FcShop className="w-10 h-10" />
                      </center>

                      <span className="text-xg font-extrabold">Tổng cửa hàng</span>
                    </>
                  }
                  //value={staticData?.totalProduct}
                  precision={0}
                  value={dataStatic?.totalStore}
                  valueStyle={{
                    color: "#00bcd4",
                  }}
                  // prefix={<ArrowUpOutlined />}
                  suffix="Cửa hàng"
                />
              </Card>
            </Col>
            <Col span={1 / 5} className="text-center" style={{ width: '250px' }}>
              <Card bordered={false}>
                <Statistic
                  title={
                    <>
                      <center>
                        <FcFaq className="w-10 h-10" />
                      </center>
                      <span className="text-xg font-extrabold">Tổng Yêu Cầu</span>
                    </>
                  }
                  //value={staticData?.totalTenant}
                  precision={0}
                  value={dataStatic?.totalRequest}
                  valueStyle={{
                    color: "#ffb74d",
                  }}
                  // prefix={<ArrowDownOutlined />}
                  suffix="Yêu cầu"
                />
              </Card>
            </Col>
          </Row>
          <Row className="mt-20">
            <Col span={12}>
              <PieChartAdmin dataStatic={dataStatic?.totalStoreMap} />
            </Col>
            <Col span={12}>
              <SemiCircleDonut categoryStatic={categoryStatic} />
            </Col>
            
          </Row>
          <Row>
          <Col span={12}>
              <StackAreaChart staticIncome={staticIncome} setTypeIncome={setTypeIncome} />
            </Col>
            <Col span={12}>
              <CylinderChart statisUserAdmin={statisUserAdmin}
   setTypeUserAdmin={setTypeUserAdmin}/>
            </Col>

          </Row>
          {/* <Row>
            <Col span={24}>
              <ParetoChart />
            </Col>
          </Row> */}
        </div>

      )}
    </>
  );
};
export default StatisticStoreAdmin;
