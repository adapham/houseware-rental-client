import React, { useEffect } from 'react';
import Highcharts from 'highcharts';

import exporting from 'highcharts/modules/exporting'; // Import the exporting module
import exportData from 'highcharts/modules/export-data'; // Import the export-data module


exporting(Highcharts);
exportData(Highcharts);

export default function PieChart({dataStatic}) {
  const chartConfig = {
    chart: {
      type: 'pie',
      animation: {
        duration: 2000,
      },
    },
    title: {
      text: 'Thống kê yêu cầu hệ thống',
      align: 'left',
    },
    tooltip: {
      pointFormat: ' <b>{point.percentage:.2f}%</b>', // Display 2 decimal places
    },
    accessibility: {
      point: {
        valueSuffix: '%',
      },
    },
    plotOptions: {
      pie: {
        allowPointSelect: true,
        borderWidth: 2,
        cursor: 'pointer',
        dataLabels: {
          enabled: true,
          format: '<b>{point.name}</b><br>{point.percentage:.2f}%', // Display 2 decimal places
          distance: 20,
        },
      },
    },
    series: [
      {
        enableMouseTracking: true, // Enable mouse tracking
        animation: {
          duration: 2000,
        },
        colorByPoint: true,
        data: [
          { name: 'Chờ duyệt', y: dataStatic?.PROCESSING },
          { name: 'Đã duyệt', y: dataStatic?.ACCEPT },
          { name: 'Từ chối', y: dataStatic?.REFUSE },
        ],
      },
    ],
    exporting: {
      enabled: true, // Enable exporting module
      buttons: {
        contextButton: {
          menuItems: ['downloadPNG', 'downloadCSV'], // Add downloadPNG and downloadCSV options
        },
      },
    },
  };

  useEffect(() => {
    Highcharts.chart('pieChart', chartConfig);
  }, []);

  return <div id="pieChart"></div>;
}
