import React, { useEffect, useState } from 'react'
import Highcharts from 'highcharts';


export default function SemiCircleDonut({categoryStatic}) {
   
      const chartConfig = {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: 0,
        plotShadow: false
    },
    title: {
        text: 'Thông kê tổng sản phẩm theo thể loại',
        align: 'center',
        verticalAlign: 'middle',
        y: 150
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    accessibility: {
        point: {
            valueSuffix: '%'
        }
    },
    plotOptions: {
        pie: {
            dataLabels: {
                enabled: true,
                distance: -50,
                style: {
                    fontWeight: 'bold',
                    color: 'white'
                }
            },
            startAngle: -90,
            endAngle: 90,
            center: ['50%', '75%'],
            size: '110%'
        }
    },
    series: [{
        type: 'pie',
        name: 'Tổng sản phẩm',
        innerSize: '50%',
        data: categoryStatic
    }]
    }
    
      useEffect(() => {
        Highcharts.chart('semiCircleDonut', chartConfig);
      }, []);
    

      return (
        <div  id="semiCircleDonut"></div>
      );
    }
