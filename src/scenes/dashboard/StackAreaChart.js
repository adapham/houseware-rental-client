import React, { useEffect, useState } from 'react'
import Highcharts from 'highcharts';
import { Select } from 'antd';

export default function StackAreaChart({staticIncome,setTypeIncome}) {
    const chartHeight = 400;
    const [tickInterval,setTickInterval]=useState(5);
   
    const handleChange = (value) => {
        setTypeIncome(value);
        switch (value) { 
          case 'ONE_WEEK':
            setTickInterval(2);
            break;
          case 'ONE_MONTH':
            setTickInterval(5);
            break;
          case 'ONE_YEAR':
            setTickInterval(100);
            break;
          default:
            setTickInterval(1);
            break;
        }
      };
      const chartConfig = {
        chart: {
            type: 'area',
            height: chartHeight
        },
        title: {
            text: 'Thống kê giao dịch',
            align: 'left'
        },
        xAxis: {
            categories: staticIncome?.[0]?.data, // Use the "data" property of the first element
            tickInterval: tickInterval,
            
            labels: {
                skew: true,
            },
        },
        yAxis: {
            title: {
                useHTML: true,
                text: 'Tổng tiền'
            }
        },
        tooltip: {
            shared: true,
            headerFormat: '<span style="font-size:12px"><b>{point.key}</b></span><br>'
        },
        plotOptions: {
            
            area: {
                stacking: 'normal',
                lineColor: '#666666',
                lineWidth: 1,
                marker: {
                    lineWidth: 1,
                    lineColor: '#666666'
                }
            }
        },
        series: [
            { name: "NẠP TIỀN", data: staticIncome?.[1]?.data.map(Number) },
            { name: "RÚT TIỀN", data: staticIncome?.[2]?.data.map(Number) },
            // { name: staticIncome[3].name, data: staticIncome[3].data.map(Number) },
            // { name: staticIncome[4].name, data: staticIncome[4].data.map(Number) }
        ]
    };
    
      useEffect(() => {
        Highcharts.chart('stackedArea', chartConfig);
      }, [staticIncome,setTypeIncome]);
    

      return (
        <div>
          <div>
          <Select
          defaultValue="ONE_MONTH"
          style={{
            width: 120,
          }}
          onChange={handleChange}
          options={[
            {
              value: 'ONE_DAY',
              label: 'Một ngày',
            },
            {
              value: 'THREE_DAY',
              label: 'Ba ngày',
            },
            {
              value: 'ONE_WEEK',
              label: 'Một tuần',
            },
            {
              value: 'ONE_MONTH',
              label: 'Một Tháng',
            },
            {
              value: 'ONE_YEAR',
              label: 'Một năm',
            },
          ]}
        />
          </div>
          <div style={{ height: chartHeight }} id="stackedArea"></div>
        </div>
      );
    }
