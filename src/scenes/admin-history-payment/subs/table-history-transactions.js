import { CheckCircleOutlined, CloseCircleOutlined } from "@ant-design/icons";
import { Table } from "antd";
import dayjs from "dayjs";
import queryString from "query-string";
import React, { memo } from "react";
import { useLocation, useSearchParams } from "react-router-dom";
import ErrorScreen from "../../../pages/examples/error-screen";
import { formatToMoney } from "../../../utils/helper";
import { useQueryAllHistoryTransactions } from "../admin-history-payment.query";

function TableHistoryTransactions({ type }) {
  const location = useLocation();

  const STATUS = {
    SUCCESS: (
      <div className="text-green-500 font-semibold">
        <CheckCircleOutlined /> Thành công
      </div>
    ),
    FAIL: (
      <div className="text-red-500 font-semibold">
        <CloseCircleOutlined /> Thất bại
      </div>
    ),
    // thiếu trường hơp progressing
  };

  const columns = [
    {
      title: "STT",
      dataIndex: "index",
      key: "index",
      align: "center",
      render: (text, record, index) => index + 1,
    },
    {
      title: "Mã giao dịch",
      dataIndex: "code",
      key: "code",
      render: (value) => {
        return <div className="">#{value}</div>;
      },
    },
    {
      title: "Ngày nạp",
      dataIndex: "createdTime",
      key: "createdTime",
      sorter: (a, b) =>
        dayjs(a.createdTime).unix() - dayjs(b.createdTime).unix(),
      render: (value) => {
        return <div className="">{dayjs(value).format("DD-MM-YYYY")}</div>;
      },
    },
    {
      title: "Mô tả",
      dataIndex: "desciption",
      key: "desciption",
      render: (value) => {
        return <div className="">{value}</div>;
      },
    },
    {
      title: "Phương thức", // cái này API chưa có, fix cứng là VNPAy
      dataIndex: "method",
      key: "method",
      align: "center",
      render: () => {
        return <div className="">VNPay</div>;
      },
    },
    {
      title: "Số tiền",
      dataIndex: "totalAmount",
      key: "totalAmount",
      align: "center",
      sorter: (a, b) => Number(a.totalAmount) - Number(b.totalAmount),
      render: (value) => {
        return <div className="">{formatToMoney(value)}</div>;
      },
    },
    {
      title: "Tài khoản nạp",
      dataIndex: "userName",
      key: "userName",
      align: "center",
      render: (value) => {
        return <div className="">{value}</div>;
      },
    },
    {
      title: "Trạng thái",
      dataIndex: "status",
      key: "status",
      filters: [
        { text: "Thành công", value: "SUCCESS" },
        { text: "Thất bại", value: "FAIL" },
      ],
      onFilter: (value, record) => record.status === value,
      render: (value) => {
        return <div className="">{STATUS[value]}</div>;
      },
    },
  ];

  const [searchParams, setSearchParams] = useSearchParams();

  const { data, isLoading, error } = useQueryAllHistoryTransactions({
    keyword: searchParams.get("search"),
    fromDate: searchParams.get("fromDate"),
    toDate: searchParams.get("toDate"),
    type: searchParams.get("type"),
    pageIndex: searchParams.get("pageIndex") || 1,
  });

  if (error) {
    return <ErrorScreen />;
  }

  return (
    <Table
      columns={columns}
      dataSource={data?.content}
      loading={isLoading}
      pagination={{
        defaultPageSize: 10,
        showSizeChanger: false,
        total: data?.pageable.totalElements,
        onChange: (pageIndex) => {
          const currentQuery = queryString.parse(location.search);

          setSearchParams({
            ...currentQuery,
            pageIndex: pageIndex,
          });
        },
      }}
    />
  );
}

export default memo(TableHistoryTransactions);
