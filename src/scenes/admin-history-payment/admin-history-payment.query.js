import { useQuery } from "@tanstack/react-query";
import API from "../../utils/api";

export const QUERY_ALL_HISTORY_TRANSACTION_KEY =
  "QUERY_ALL_HISTORY_TRANSACTION_KEY";

const getAllHistoryTransactions = ({
  keyword,
  fromDate,
  toDate,
  type,
  pageIndex,
  pageSize,
}) => {
  return API.request({
    url: "/api/transactions",
    params: {
      keyword: keyword,
      fromDate: fromDate,
      toDate: toDate,
      type: type,
      pageIndex: pageIndex - 1,
      pageSize: pageSize,
    },
  }).then((res) => {
    return res;
  });
};

export const useQueryAllHistoryTransactions = ({
  keyword,
  fromDate,
  toDate,
  type,
  pageIndex = 1,
  pageSize = 10,
}) => {
  return useQuery({
    queryKey: [
      QUERY_ALL_HISTORY_TRANSACTION_KEY,
      keyword,
      fromDate,
      toDate,
      type,
      pageIndex,
      pageSize,
    ],
    queryFn: () =>
      getAllHistoryTransactions({
        keyword,
        fromDate,
        toDate,
        type,
        pageIndex,
        pageSize,
      }),
    enabled: !!type,
  });
};
