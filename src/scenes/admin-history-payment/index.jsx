import { Input, Tabs } from "antd";
import React, { memo, useEffect } from "react";

import queryString from "query-string";
import { useLocation, useSearchParams } from "react-router-dom";
import AdminDateTimePicker from "../../components/admin-date-time-picker";
import TableHistoryTransactions from "./subs/table-history-transactions";

const { Search } = Input;

function AdminHistoryPayment() {
  const location = useLocation();

  document.title = "Lịch sử giao dịch";
  const [searchParams, setSearchParams] = useSearchParams();

  const tabs = [
    // {
    //   label: `Thanh toán`,
    //   key: "PAYMENT",
    //   children: <TableHistoryTransactions type="PAYMENT" />,
    // },
    {
      label: `Nạp tiền`,
      key: "RECHARGE",
      children: <TableHistoryTransactions type="RECHARGE" />,
    },
    {
      label: `Rút tiền`,
      key: "WITHDRAW_MONEY",
      children: <TableHistoryTransactions type="WITHDRAW_MONEY" />,
    },
  ];

  const onSearch = (keywordSearch) => {
    const currentQuery = queryString.parse(location.search);
    setSearchParams({ ...currentQuery, search: keywordSearch });
  };

  const handleChangeType = (value) => {
    const currentQuery = queryString.parse(location.search);
    setSearchParams({
      ...currentQuery,
      type: value,
    });
  };

  useEffect(() => {
    const validType = ["RECHARGE", "RECHARGE", "WITHDRAW_MONEY"];
    const currentType = searchParams.get("type");
    if (!validType.includes(currentType)) {
      setSearchParams({
        type: "RECHARGE",
      });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div className="w-full flex flex-col gap-2">
      <div className="flex gap-4 items-center w-full justify-between mb-2">
        <Search
          style={{
            width: "300px",
          }}
          autoFocus
          placeholder="Tìm kiếm theo tên người dùng"
          onSearch={onSearch}
        />
        <AdminDateTimePicker />
      </div>

      <div className="flex flex-col gap-2 w-full mt-2">
        <Tabs onChange={handleChangeType} items={tabs} />
      </div>
    </div>
  );
}

export default memo(AdminHistoryPayment);
