import * as React from 'react';
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import Link from '@mui/material/Link';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import {createTheme, ThemeProvider} from '@mui/material/styles';
import httpRequest from "../../redux/apiConfig";
import {useState} from "react";
import {useNavigate} from "react-router-dom";
import Alert from '@mui/material/Alert';
import Stack from '@mui/material/Stack';

const defaultTheme = createTheme();
const BASE_URL = process.env.REACT_APP_BASE_URL;

export default function ForgotPassword() {

    const [errEmail, setErrEmail] = useState("");
    const [email, setEmail] = useState("");
    const [check, setCheck] = useState(false);
    const [message, setMessage] = useState(false);
    const navigate = useNavigate();
    const EmailValidation = (email) => {
        return String(email)
            .toLowerCase()
            .match(/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i);
    };
    const handleSubmit = (event) => {
        event.preventDefault();
        if (!email) {
            setErrEmail("Nhập địa chỉ email của bạn");
        } else {
            if (!EmailValidation(email)) {
                setErrEmail("Email không không lệ!");
                console.log(errEmail)
                return;
            } else {
                const url = `${BASE_URL}/api/users/send-otp?email=` + email;
                httpRequest.get(url, {}, {})
                    .then(res => {
                        if (res?.data?.message) {
                            console.log(res?.data?.message)
                            setMessage(res?.data?.message)
                            setCheck(true);
                            return;
                        }
                        setCheck(true);
                        const messageSuccess =
                        "Nếu địa chỉ email này đã được đăng ký trong Hệ thống của chúng tôi, bạn sẽ nhận được một liên kết cài đặt mật khẩu của mình tại    " + email;
                        setMessage(messageSuccess);
                    });
            }
        }


    };

    const handleEmail = (e) => {
        setEmail(e.target.value);
        setErrEmail("");
    };
    return (

        <ThemeProvider theme={defaultTheme}>
            <Stack sx={{width: '100%'}} spacing={2}>
                {check ? (
                    <Alert variant="filled" severity="success">
                        {message}
                    </Alert>
                ) :(
                    <>
                    <div>

                    </div>
                    </>
                )}

            </Stack>
            <Container component="main" maxWidth="xs">
                <CssBaseline/>
                <Box
                    sx={{
                        marginTop: 8,
                        display: 'flex',
                        flexDirection: 'column',
                        alignItems: 'center',
                    }}
                >
                    <Avatar sx={{m: 1, bgcolor: 'secondary.main'}}>
                        <LockOutlinedIcon/>
                    </Avatar>
                    <Typography component="h1" variant="h5">
                        Quên mật khẩu
                    </Typography>
                    <Box component="form" noValidate sx={{mt: 3, minWidth: 400}}>
                        <Grid container spacing={1}>
                            <Grid item xs={24}>
                                <TextField
                                    required
                                    fullWidth
                                    id="email"
                                    label="Email"
                                    name="email"
                                    autoComplete="email"
                                    placeholder='Nhập địa chỉ email tại đây'
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    onChange={handleEmail}
                                    error={!!errEmail}
                                    helperText={errEmail}
                                />
                            </Grid>
                        </Grid>
                        <Button
                            type="submit"
                            variant="contained"
                            sx={{mt: 3, mb: 2}}
                            onClick={handleSubmit}
                        >
                            Gửi
                        </Button>
                        <Grid container justifyContent="flex-end">
                            <Grid item>
                                <Link href="/signin" variant="body2">
                                    Quay lại đăng nhập
                                </Link>
                            </Grid>
                        </Grid>
                    </Box>
                </Box>
            </Container>
        </ThemeProvider>
    );
}