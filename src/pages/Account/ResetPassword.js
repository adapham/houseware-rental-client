import * as React from 'react';
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import Link from '@mui/material/Link';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import {createTheme, ThemeProvider} from '@mui/material/styles';
import {useEffect, useState} from "react";
import httpRequest from "../../redux/apiConfig";
import {toast} from "react-toastify";
import {useNavigate} from "react-router-dom";

const defaultTheme = createTheme();
const BASE_URL = process.env.REACT_APP_BASE_URL;

export default function ResetPassword() {
    let key = '';
    const navigate = useNavigate();
    const handleSubmit = (event) => {
        event.preventDefault();
        const data = new FormData(event.currentTarget);
        const pass = data.get('password');
        const pass1 = data.get('rePassword');
        const objPassword =
            {
                keyEmail: key,
                newPassword: pass,
                reNewPassword: pass1
            }

        const url = `${BASE_URL}/api/users/reset-password`
        console.log(objPassword)
        httpRequest.put(url,objPassword,{}).then(res=>{
            if(res?.data?.message){
                toast(res.data.message)
            }else{
                toast("Lấy lại mật khẩu thành công!")
                navigate("/signin")
            }

        })
    };
    useEffect(() => {
        const urlParams = new URLSearchParams(window.location.search);
        key = urlParams.get('key');

    }, []);

    return (
        <ThemeProvider theme={defaultTheme}>
            <Container component="main" maxWidth="xs">
                <CssBaseline/>
                <Box
                    sx={{
                        marginTop: 8,
                        display: 'flex',
                        flexDirection: 'column',
                        alignItems: 'center',
                    }}
                >
                    <Avatar sx={{m: 1, bgcolor: 'secondary.main'}}>
                        <LockOutlinedIcon/>
                    </Avatar>
                    
                    <Typography component="h1" variant="h5">
                        Thay đổi mật khẩu mới
                    </Typography>
                    <Box component="form" noValidate onSubmit={handleSubmit} sx={{mt: 3, minWidth: 400}}>
                        <Grid container spacing={1}>
                            <Grid item xs={24}>
                                <TextField
                                    required
                                    fullWidth
                                    id="password"
                                    label="Mật khẩu "
                                    name="password"
                                    autoComplete="password"
                                    placeholder='********'
                                    type={"password"}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                />

                            </Grid>
                            <Grid item xs={24}>


                                <TextField
                                    required
                                    fullWidth
                                    id="rePassword"
                                    label="Nhập lại mật khẩu"
                                    name="rePassword"
                                    autoComplete="rePassword"
                                    placeholder='********'
                                    type={"password"}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}

                                />
                            </Grid>
                        </Grid>
                        <Button
                            type="submit"
                            variant="contained"
                            sx={{mt: 3, mb: 2}}
                        >
                            Xác nhận
                        </Button>
                        <Grid container justifyContent="flex-end">
                            <Grid item>
                            </Grid>
                        </Grid>
                    </Box>

                </Box>
            </Container>
        </ThemeProvider>
    );
}