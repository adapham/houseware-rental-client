import KeyboardBackspaceIcon from "@mui/icons-material/KeyboardBackspace";
import LockOutlinedIcon from "@mui/icons-material/LockOutlined";
import Avatar from "@mui/material/Avatar";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import CssBaseline from "@mui/material/CssBaseline";
import Grid from "@mui/material/Grid";
import Link from "@mui/material/Link";
import Paper from "@mui/material/Paper";
import TextField from "@mui/material/TextField";
import Typography from "@mui/material/Typography";
import { ThemeProvider, createTheme } from "@mui/material/styles";
import { Form } from "antd";
import React, { useEffect } from "react";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import { toast } from "react-toastify";
import { login } from "../../redux/Req/authRequest";
import { loadUserSuccess } from "../../redux/Slide/authSlide";
import getTokenPromise from "./UseAuth2";

const defaultTheme = createTheme();

export default function SignIn() {
  const BASE_URL = process.env.REACT_APP_BASE_URL;

  const dispatch = useDispatch();
  const navigate = useNavigate();

  // ============= Event Handler End here ===============
  const handleSignIn = (values) => {
    const newUser = {
      username: values.username,
      password: values.password,
    };

    login(newUser, dispatch).then((rep) => {
      console.log(rep);
      if (rep?.accessToken) {
        toast.success("Đăng nhập thành công", {
          position: "top-right",
          autoClose: 2000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
          theme: "light",
        });
        window.location.href = "/";
      } else {
        toast.error(rep?.data?.message);
      }
    });
  };

  const handleGoogleSignIn = (e) => {
    e.preventDefault();
    window.location.href = `${BASE_URL}/oauth2/authorization/google`;
  };
  const handleFacebookSignIn = (e) => {
    e.preventDefault();
    window.location.href = `${BASE_URL}/oauth2/authorization/facebook`;
  };

  useEffect(() => {
    const url = window.location.search;
    const searchParams = new URLSearchParams(url);
    let token = searchParams.get("token");
    if (token) {
      localStorage.setItem("access-token", token);
      const resUser = getTokenPromise("api/users/profile");
      resUser.then((response) => {
        console.log("responseOauth", response);
        if (response) {
          dispatch(loadUserSuccess(response));
          navigate("/");
        } else {
          localStorage.removeItem("access-token");
          localStorage.removeItem("user");
          navigate("/signin");
        }
      });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <ThemeProvider theme={defaultTheme}>
      <Grid container component="main" sx={{ height: "100vh" }}>
        <CssBaseline />
        <Grid
          item
          xs={false}
          sm={4}
          md={7}
          sx={{
            backgroundImage:
              "url(https://source.unsplash.com/random?wallpapers)",
            backgroundRepeat: "no-repeat",
            backgroundColor: (t) =>
              t.palette.mode === "light"
                ? t.palette.grey[50]
                : t.palette.grey[900],
            backgroundSize: "cover",
            backgroundPosition: "center",
          }}
        />
        <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
          <Box
            sx={{
              my: 8,
              mx: 4,
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
            }}
          >
            <Avatar sx={{ m: 1, bgcolor: "secondary.main" }}>
              <LockOutlinedIcon />
            </Avatar>
            <Typography component="h1" variant="h5">
              Đăng nhập
            </Typography>
            <div className="w-full mt-5">
              <Form
                style={{
                  width: "100%",
                }}
                name="login"
                onFinish={handleSignIn}
                autoComplete="off"
              >
                <div className="w-full grid grid-cols-1 gap-6 mt-2">
                  <Form.Item
                    label=""
                    name="username"
                    rules={[
                      {
                        required: true,
                        message: "Nhập username or email của bạn",
                      },
                    ]}
                  >
                    <TextField
                      required
                      fullWidth
                      label="Tên đăng nhập"
                      placeholder="Nhập tại đây"
                      InputLabelProps={{
                        shrinkOnSubmit: true,
                      }}
                     
                    />
                  </Form.Item>

                  <Form.Item
                    label=""
                    name="password"
                    rules={[
                      {
                        required: true,
                        message: "Nhập password của bạn",
                      },
                    ]}
                  >
                    <TextField
                      required
                      fullWidth
                      label="Mật khẩu"
                      type="password"
                      placeholder="Nhập tại đây"
                      InputLabelProps={{
                        shrinkOnSubmit: true,
                      }}
                    
                    />
                    
                  </Form.Item>
                  <Button type="submit" fullWidth variant="contained">
                    Đăng nhập
                  </Button>
                </div>

                <button
                  onClick={handleGoogleSignIn}
                  className="bg-red-500 hover:bg-red-600 text-white cursor-pointer w-full text-base font-medium h-10 rounded-md mt-2 duration-300"
                >
                  Login with Google
                </button>

                <button
                  onClick={handleFacebookSignIn}
                  className="bg-blue-500 hover:bg-blue-600 text-white cursor-pointer w-full text-base font-medium h-10 rounded-md mt-2 duration-300"
                >
                  Login with Facebook
                </button>
              </Form>

              <Grid container className="mt-5">
                <Grid item xs>
                  <Link href="/" variant="body2">
                    <Button
                      variant="contained"
                      startIcon={<KeyboardBackspaceIcon />}
                    >
                      Về trang chủ
                    </Button>
                  </Link>
                </Grid>
              </Grid>

              <Grid container className="mt-5">
                <Grid item xs>
                  <Link href="/forgotpassword" variant="body2">
                    Quên mật khẩu?
                  </Link>
                </Grid>
                <Grid item>
                  <Link href="/signup" variant="body2">
                    {"Nếu bạn chưa có tài khoản? Đăng ký ngay"}
                  </Link>
                </Grid>
              </Grid>
            </div>
          </Box>
        </Grid>
      </Grid>
    </ThemeProvider>
  );
}
