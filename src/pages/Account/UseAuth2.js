import {instanceCTDRequest} from "../../redux/apiConfig";

const BASE_URL = process.env.REACT_APP_BASE_URL

export default function getTokenPromise(url, params = {}) {
    return new Promise((resolve, reject) => {
        const accessToken = localStorage.getItem("access-token");
        instanceCTDRequest
            .get(`${BASE_URL}/` + url, {
                headers: { Authorization: `Bearer ${accessToken}` },
                params: params ? params : {},
            })
            .then(response => {
                if (response.data) {
                    resolve(response.data);
                } else {
                    resolve({});
                }
            })
    });
}