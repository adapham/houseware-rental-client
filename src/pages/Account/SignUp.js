import React, { useState } from "react";
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import Link from '@mui/material/Link';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import { useNavigate } from "react-router-dom";
import { register } from "../../redux/Req/authRequest";
import { useDispatch } from "react-redux";
import { toast } from 'react-toastify';
import { regexEmail } from "../../constants/constantsParam";
const defaultTheme = createTheme();

export default function SignUp() {
    // const handleSubmit = (event) => {
    //     event.preventDefault();
    //     const data = new FormData(event.currentTarget);
    //     console.log({
    //         email: data.get('email'),
    //         password: data.get('password'),
    //     });
    // };

    // ============= Initial State Start here =============
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [username, setUsername] = useState("");
    // ============= Initial State End here ===============
    const [errUsername, setErrUsername] = useState("");
    const [errEmail, setErrEmail] = useState("");
    const [errPassword, setErrPassword] = useState("");
    // ============= Error Msg End here ===================

    const dispatch = useDispatch();
    const navigate = useNavigate();

    const user = {
        username: username,
        password: password,
        email: email

    }

    const handleUsername = (e) => {
        const value = e.target.value.trim();
        setUsername(value);
        setErrUsername("");
    };

    const handleEmail = (e) => {
        const emailTrim = e.target.value.trim();
        setEmail(emailTrim);
      
        if (emailTrim === '') {
          setErrEmail("Vui lòng nhập địa chỉ");
        } else {
          const regexMatch = regexEmail.test(emailTrim);
          if (!regexMatch) {
            setErrEmail("Vui lòng nhập địa chỉ email hợp lệ");
          } else {
            setErrEmail("");
          }
        }
    };

    const handlePassword = (e) => {
        const value = e.target.value.trim();
        setPassword(value);
        setErrPassword("");
    };


    // ============= Event Handler End here ===============
    // ================= Email Validation start here =============

    const EmailValidation = (email) => {
        return String(email)
            .toLowerCase()
            .match(/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i);
    };

    const handleSignUp = (e) => {
        e.preventDefault();
        if (!username) {
            setErrUsername("Nhập username của bạn");

        } else {
            if (password.length > 20) {
                setErrUsername("username ít hơn ký tự 20 kí tự");
                return;
            }
        }
        if (!email) {
            setErrEmail("Nhập địa chỉ email của bạn");
        } else {
            if (!EmailValidation(email)) {
                setErrEmail("Email không không lệ!");
                return;
            }
        }
        if (!password) {
            setErrPassword("Tạo mật khẩu");
        } else {
            if (password.length < 6) {
                setErrPassword("Mật khẩu phải chứa ít nhất 6 ký tự");
                return;
            }
        }
        if (email != "" && username != "" && password != "" && errEmail?.trim()=="") {
            register(user, dispatch, navigate).then(res => {
                if (res?.status) {
                    toast(res.data.message)
                } else {
                    toast(res.message)
                    navigate("/signin")
                }
            });
        } else {
            toast("Vui lòng điền đủ thông tin")
        }

    };

    return (
        <ThemeProvider theme={defaultTheme}>
            <Container component="main" maxWidth="xs">
                <CssBaseline />
                <Box
                    sx={{
                        marginTop: 8,
                        display: 'flex',
                        flexDirection: 'column',
                        alignItems: 'center',
                    }}
                >
                    <Avatar sx={{ m: 1, bgcolor: 'secondary.main' }}>
                        <LockOutlinedIcon />
                    </Avatar>
                    <Typography component="h1" variant="h5">
                        Đăng ký
                    </Typography>
                    <Box
                        component="form"
                        noValidate
                        // onSubmit={handleSubmit} 
                        sx={{ mt: 3 }
                        }>
                        <Grid container spacing={2}>
                            <Grid item xs={12} sm={12}>
                                <TextField
                                    autoComplete="given-name"
                                    name="firstName"
                                    required
                                    fullWidth
                                    id="firstName"
                                    label="Tên đăng nhập"
                                    placeholder="Khachhang"
                                    autoFocus
                                    onChange={handleUsername}
                                    value={username}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    error={!!errUsername}
                                    helperText={errUsername}
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    required
                                    fullWidth
                                    id="email"
                                    label="Email"
                                    name="email"
                                    autoComplete="email"
                                    placeholder="khachhang@gmail.com"
                                    onChange={handleEmail}
                                    value={email}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    error={!!errEmail}
                                    helperText={errEmail}
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    required
                                    fullWidth
                                    name="password"
                                    label="Mật khẩu"
                                    type="password"
                                    id="password"
                                    autoComplete="new-password"
                                    placeholder="Nhập tại đây"
                                    onChange={handlePassword}
                                    value={password}
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    error={!!errPassword}
                                    helperText={errPassword}
                                />
                            </Grid>
                            {/* <Grid item xs={12}>
                                <FormControlLabel
                                    control={<Checkbox value="allowExtraEmails" color="primary"/>}
                                    label="I want to receive inspiration, marketing promotions and updates via email."
                                />
                            </Grid> */}
                        </Grid>
                        <Button
                            // type="submit"
                            onClick={handleSignUp}
                            fullWidth
                            variant="contained"
                            sx={{ mt: 3, mb: 2 }}
                        >
                            Đăng ký
                        </Button>
                        <Grid container justifyContent="flex-end">
                            <Grid item>
                                <Link href="/signin" variant="body2">
                                    Nếu bạn đã có tài khoản? Đăng nhập
                                </Link>
                            </Grid>
                        </Grid>
                    </Box>
                </Box>
            </Container>
        </ThemeProvider>
    );
}