import React from "react";
import PolicyLessorBasic from "../../components/Profile/PolicyLessorBasic";
import PolicyLessorDetails from "../../components/Profile/PolicyLessorDetails";
import {Typography} from "@material-ui/core";

const PoliCy = () => {
    return (
        <div className="max-w-container mx-auto px-4 mt-10 mb-10">
            <div className="pb-10">
                <Typography
                    variant="h4"
                    className="font-bold py-3 mb-4 text-left text-success fw-light"
                >
                    Chính Sách & Điều Kiện Cơ Bản
                </Typography>
                <PolicyLessorBasic/>
                <Typography
                    variant="h4"
                    className="font-bold py-3 mb-4 text-left text-success fw-light"
                >
                    Chính Sách & Quy Định Chi Tiết
                </Typography>
                <PolicyLessorDetails/>
            </div>
        </div>
    );
};

export default PoliCy;
