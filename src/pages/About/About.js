import React from 'react';
import { Card, Col, Layout, Row, Typography } from 'antd';
import Meta from 'antd/es/card/Meta';
import { useNavigate } from 'react-router-dom';
import image1 from '../../assets/images/about1.png'
import image2 from '../../assets/images/about2.png'
import image3 from '../../assets/images/about3.png'
import image4 from '../../assets/images/about4.png'
const { Header, Content } = Layout;
const { Title } = Typography;

const Faq = () => {
    const navigate = useNavigate();
    return (
        <div className="">
            <Layout className="min-h-screen">
                <center>

                <Header className="relative h-300px bg-cover" style={{
                    
                    backgroundImage: `url(${image1})`
                    , height: '300px', backgroundSize: 'cover', backgroundPosition: 'center', backgroundRepeat: 'no-repeat'
                }}>
                    <Content className="relative flex items-center justify-center text-center">
                        <div className="absolute top-1/2 left-1/2 transform -translate-x-1/2 -translate-y-1/2 " style={{ paddingTop: '300px' }}>
                            <Title level={1} style={{ color: '#fff' }}>
                                Sổ tay Chủ Tiệm
                            </Title>
                        </div>
                    </Content>
                </Header>
                </center>
                <Row gutter={32} className='fex justify-center mt-20'>
                    <Col span={24} md={12} lg={8} xl={6}>
                        <Card
                            onClick={() => navigate("/guideLessor")}
                            hoverable
                            style={{
                                width: '100%',
                                marginBottom: '20px',  // Margin between cards
                            }}
                            cover={<img alt="example" style={{ height: '250px' }} src={image2} />}
                        >
                            <Meta title="Hướng dẫn tính năng cho Chủ Tiệm" />
                        </Card>
                    </Col>
                    <Col span={24} md={12} lg={8} xl={6}>
                        <Card
                            onClick={() => navigate("/faq")}
                            hoverable
                            style={{
                                width: '100%',
                                marginBottom: '20px',  // Margin between cards
                            }}
                            cover={<img alt="example" style={{ height: '250px' }} src={image3} />}
                        >
                            <Meta title="Câu hỏi thường gặp" />
                        </Card>
                    </Col>
                </Row>
                <Row gutter={32} className='fex justify-center mt-10 mb-20'>
                    <Col span={12} md={12} lg={12} xl={12}>
                        <Card
                            onClick={() => navigate("/policy")}
                            hoverable
                            style={{
                                width: '100%',
                                marginBottom: '20px',  // Margin between cards
                            }}
                           
                            cover={<img alt="example" style={{ objectFit: 'cover', height: '250px', width: '100%' }}
                             src={image4} />}
                        >
                            <Meta title="Chính sách, quy định dành cho Chủ Tiệm" />
                        </Card>
                    </Col>
                </Row>

            </Layout>
        </div>
    );
};

export default Faq;
