import React from "react";
import { Typography } from "@material-ui/core";
import { Col, Row } from 'antd';

const GuideLessor = () => {
    return (
        <div className="max-w-container mx-auto px-4 mt-10 mb-10">
            <div className="pb-10">
                <Typography
                    variant="h4"
                    className="font-bold py-3 mb-4 text-center text-success fw-light"
                >
                    Hướng dẫn tính năng cho Chủ tiệm
                </Typography>
            </div>

            <Row className="mb-10 p-4">
                <Col span={10}>
                    <Typography
                        variant="h6"
                        className="font-bold py-3 mb-4 text-left text-success fw-light"
                    >
                        1. Tạo sản phẩm cho thuê
                    </Typography>
                    <div
                        className="elementor-element elementor-element-6514d37 elementor-widget elementor-widget-text-editor"
                        data-id="6514d37"
                        data-element_type="widget"
                        data-widget_type="text-editor.default"
                    >
                        <div className="elementor-widget-container">
                            <p className="text-base">Để có thể tạo sản phẩm cho thuê, Chủ Tiệm cần làm theo các bước sau:</p>
                            <ol className="ml-4 text-base">
                                <li>
                                    1. Truy cập vào tab <strong>Tài khoản.</strong>
                                </li>
                                <li>
                                    2. Bấm vào mục <strong>Xem cửa hàng.</strong>
                                </li>
                                <li>
                                    3. Chọn vào tab <strong>Quản lý sản phẩm.</strong>
                                </li>
                                <li>
                                    4. Chọn <strong>Thêm sản phẩm.</strong>
                                </li>
                                <li>
                                    5. Điền các thông tin về sản phẩm.
                                </li>
                                <li>
                                    6. Kiểm tra lại thông tin ở màn hình sản phẩm<strong>.</strong>
                                </li>
                                <li>
                                    7. Bấm <strong>Thêm sản phẩm mới.</strong>
                                </li>
                            </ol>
                        </div>
                    </div>
                </Col>
                <Col span={14}>
                    <iframe
                        title="Youtube Player"
                        width="560"
                        height="315"
                        src="https://www.youtube.com/embed/sCruRaNy6Us?autoplay=1"

                        allow="autoplay; encrypted-media"
                        allowFullScreen
                    />
                </Col>
            </Row>

            <Row className="mb-10 p-4">
                <Col span={10}>
                    <Typography
                        variant="h6"
                        className="font-bold py-3 mb-4 text-left text-success fw-light"
                    >
                        2. Quản lý đơn hàng cho thuê
                    </Typography>

                    <div
                        className="elementor-element text-base elementor-element-6514d37 elementor-widget elementor-widget-text-editor"
                        data-id="6514d37"
                        data-element_type="widget"
                        data-widget_type="text-editor.default"

                    >
                        <div className="elementor-widget-container">
                            <p>Chủ Tiệm có thể quản lý đơn hàng dễ dàng bằng cách:</p>
                            <ol className="ml-4">
                                <li>
                                    1. Truy cập vào tab <strong>Tài khoản.</strong>
                                </li>
                                <li>
                                    2. Bấm vào mục <strong>Xem cửa hàng.</strong>
                                </li>
                                <li>
                                    3. Chọn vào tab <strong>Quản lý đơn hàng cho thuê.</strong>
                                </li>
                            </ol>
                        </div>
                    </div>
                </Col>
                <Col span={14}>
                    <iframe
                        title="Youtube Player"
                        width="560"
                        height="315"
                        src="https://www.youtube.com/embed/AgkHLHOA0Yg?si=2QrVYFGHR3npm0oz"
                        allow="autoplay; encrypted-media"
                        allowFullScreen
                    />

                </Col>
            </Row>

            <Row className="mb-10 p-4">
                <Col span={10}>
                    <Typography
                        variant="h6"
                        className="font-bold py-3 mb-4 text-left text-success fw-light"
                    >
                        3. Quản lý sản phẩm cho thuê
                    </Typography>

                    <div
                        className="text-base elementor-element elementor-element-6514d37 elementor-widget elementor-widget-text-editor"
                        data-id="6514d37"
                        data-element_type="widget"
                        data-widget_type="text-editor.default"
                    >
                        <div className="elementor-widget-container">
                            <p>Chủ Tiệm có thể quản lý đơn hàng dễ dàng bằng cách:</p>
                            <ol className="ml-4">
                                <li>
                                    1. Truy cập vào tab <strong>Tài khoản.</strong>
                                </li>
                                <li>
                                    2. Bấm vào mục <strong>Xem cửa hàng.</strong>
                                </li>
                                <li>
                                    3. Chọn vào tab <strong>Quản lý sản phẩm cho thuê.</strong>
                                </li>
                            </ol>
                        </div>
                    </div>
                </Col>
                <Col span={14}>
                    <iframe
                        title="Youtube Player"
                        width="560"
                        height="315"
                        src="https://www.youtube.com/embed/Mqqk2miQEvE"
                        allow="autoplay; encrypted-media"
                        allowFullScreen
                    />

                </Col>
            </Row>

            <Row className="mb-10 p-4">
                <Col span={10}>
                    <Typography
                        variant="h6"
                        className="font-bold py-3 mb-4 text-left text-success fw-light"
                    >
                        4. Quản lý sản phẩm
                    </Typography>
                    <div
                        className="text-base elementor-element elementor-element-6514d37 elementor-widget elementor-widget-text-editor"
                        data-id="6514d37"
                        data-element_type="widget"
                        data-widget_type="text-editor.default"
                    >
                        <div className="elementor-widget-container">
                            <p>Chủ Tiệm có thể quản lý đơn hàng dễ dàng bằng cách:</p>
                            <ol className="ml-4">
                                <li>
                                    1. Truy cập vào tab <strong>Tài khoản.</strong>
                                </li>
                                <li>
                                    2. Bấm vào mục <strong>Xem cửa hàng.</strong>
                                </li>
                                <li>
                                    3. Chọn vào tab <strong>Quản lý sản phẩm.</strong>
                                </li>
                            </ol>
                        </div>

                        <div className="elementor-widget-container">
                            <p>Có 3 trạng thái sản phẩm:</p>
                            <ol className="ml-4">
                                <li>
                                    1. Đã duyệt: Những sản phẩm bạn đã đăng và thông qua sự kiểm duyệt của hệ thống, khách có thể thấy và thuê được.
                                </li>
                                <li>
                                    2. Chờ duyệt: Những sản phẩm bạn đăng có thể chưa đủ điều kiện cần hệ thống kiểm duyệt lại.
                                </li>
                                <li>
                                    3. Từ chối: Những sản phẩm bạn đăng vi phạm quy định của hệ thống
                                </li>
                            </ol>
                        </div>

                        <div className="elementor-widget-container">
                            <p>Có 1 số tính năng giúp bạn dễ dàng thao tác với đơn hàng:</p>
                            <ol className="ml-4">
                                <li>
                                    1. Xem chi tiết sản phẩm
                                </li>
                                <li>
                                    2. Tạo sản phẩm mới
                                </li>
                                <li>
                                    3. Chỉnh sửa sản phẩm
                                </li>
                                <li>
                                    4. Ẩn sản phẩm
                                </li>
                            </ol>
                        </div>
                    </div>
                </Col>
                <Col span={14}>
                    <iframe
                        title="Youtube Player"
                        width="560"
                        height="315"
                        src="https://www.youtube.com/embed/VJ-fdfNxjVY"
                        allow="autoplay; encrypted-media"
                        allowFullScreen
                    />

                </Col>
            </Row>

            <Row className="mb-10 p-4">
                <Col span={10}>
                    <Typography
                        variant="h6"
                        className="font-bold py-3 mb-4 text-left text-success fw-light"
                    >
                        5. Nhắn tin với khách
                    </Typography>

                    <div
                        className="text-base elementor-element elementor-element-6514d37 elementor-widget elementor-widget-text-editor"
                        data-id="6514d37"
                        data-element_type="widget"
                        data-widget_type="text-editor.default"
                    >
                        <div className="elementor-widget-container">
                            <p>Khi có đơn phát sinh và Chủ Tiệm muốn nhắn tin với khách, bạn có thể làm theo các bước sau:</p>
                            <ol className="ml-4">
                                <li>
                                    1. Truy cập vào tab <strong>Tài khoản.</strong>
                                </li>
                                <li>
                                    2. Bấm vào mục <strong>Xem cửa hàng.</strong>
                                </li>
                                <li>
                                    3. Chọn vào tab <strong>Quản lý sản phẩm cho thuê.</strong>
                                </li>
                                <li>
                                    4. Chọn đơn hàng bạn muốn nhắn tin với khách.
                                </li>
                                <li>
                                    5. Bấm vào nút <strong>Liên hệ.</strong>
                                </li>
                            </ol>
                        </div>
                    </div>
                </Col>
                <Col span={14}>
                    <iframe
                        title="Youtube Player"
                        width="560"
                        height="315"
                        src="https://www.youtube.com/embed/IrwDi9r7bHA"
                        allow="autoplay; encrypted-media"
                        allowFullScreen
                    />

                </Col>
            </Row>
        </div>
    );
};

export default GuideLessor;
