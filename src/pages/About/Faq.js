import * as React from 'react';
import {styled} from '@mui/material/styles';
import ArrowForwardIosSharpIcon from '@mui/icons-material/ArrowForwardIosSharp';
import MuiAccordion from '@mui/material/Accordion';
import MuiAccordionSummary from '@mui/material/AccordionSummary';
import MuiAccordionDetails from '@mui/material/AccordionDetails';
import Typography from '@mui/material/Typography';
import PersonIcon from '@mui/icons-material/Person';
import ReceiptLongIcon from '@mui/icons-material/ReceiptLong';
import LocalShippingIcon from '@mui/icons-material/LocalShipping';
import InfoIcon from '@mui/icons-material/Info';

const Accordion = styled((props) => (
    <MuiAccordion disableGutters elevation={0} square {...props} />
))(({theme}) => ({
    border: `1px solid ${theme.palette.divider}`,
    '&:not(:last-child)': {
        borderBottom: 0,
    },
    '&:before': {
        display: 'none',
    },
}));

const AccordionSummary = styled((props) => (
    <MuiAccordionSummary
        expandIcon={<ArrowForwardIosSharpIcon sx={{fontSize: '0.9rem'}}/>}
        {...props}
    />
))(({theme}) => ({
    backgroundColor:
        theme.palette.mode === 'dark'
            ? 'rgba(255, 255, 255, .05)'
            : 'rgba(0, 0, 0, .03)',
    flexDirection: 'row-reverse',
    '& .MuiAccordionSummary-expandIconWrapper.Mui-expanded': {
        transform: 'rotate(90deg)',
    },
    '& .MuiAccordionSummary-content': {
        marginLeft: theme.spacing(1),
    },
}));

const AccordionDetails = styled(MuiAccordionDetails)(({theme}) => ({
    padding: theme.spacing(2),
    borderTop: '1px solid rgba(0, 0, 0, .125)',
}));

const Faq = () => {
    const [expanded, setExpanded] = React.useState('panel1');

    const handleChange = (panel) => (event, newExpanded) => {
        setExpanded(newExpanded ? panel : false);
    };

    const [expanded1, setExpanded1] = React.useState('panel1');

    const handleChange1 = (panel) => (event, newExpanded) => {
        setExpanded1(newExpanded ? panel : false);
    };

    const [expanded2, setExpanded2] = React.useState('panel1');

    const handleChange2 = (panel) => (event, newExpanded) => {
        setExpanded2(newExpanded ? panel : false);
    };

    const [expanded3, setExpanded3] = React.useState('panel1');

    const handleChange3 = (panel) => (event, newExpanded) => {
        setExpanded3(newExpanded ? panel : false);
    };

    return (
        <div className="max-w-container mx-auto px-4 mt-10 mb-10">
            <div className="pb-10">
                <Typography
                    variant="h4"
                    className="font-bold py-3 mb-4 text-left text-success fw-light"
                >
                    <PersonIcon /> Tài khoản của tôi
                </Typography>

                <Accordion expanded={expanded === 'panel1'} onChange={handleChange('panel1')}>
                    <AccordionSummary aria-controls="panel1d-content" id="panel1d-header">
                        <Typography> <PersonIcon /> Làm thế nào để tôi tạo tài khoản trên website Houseware Rental?</Typography>
                    </AccordionSummary>
                    <AccordionDetails>
                        <Typography className="text-base">
                            <div className="text-lg">
                                <h2 className='font-semibold'>Để đăng ký tài khoản tại HWR, bạn vui lòng làm theo hướng dẫn sau:</h2>
                                <ol className="ml-4">
                                    <li>
                                        Bước 1: Truy cập vào website
                                    </li>
                                    <li>
                                        Bước 2: có 03 cách để bạn đăng ký tài khoản
                                    </li>
                                    <li>
                                        - Đăng ký bằng tài khoản Gmail. Hệ thống sẽ tự động kết nối tài khoản Gmail của bạn vào tài khoản người dùng tại website HWR
                                    </li>
                                    <li>
                                        - Đăng nhập qua tài khoản google. 
                                    </li>
                                    <li>
                                        - Đăng nhập qua tài khoản facebook. 
                                    </li>
                                </ol>
                            </div>
                        </Typography>
                    </AccordionDetails>
                </Accordion>
                <Accordion expanded={expanded === 'panel2'} onChange={handleChange('panel2')}>
                    <AccordionSummary aria-controls="panel2d-content" id="panel2d-header">
                        <Typography><PersonIcon /> Tại sao tôi nên đăng ký tài khoản tại Houseware Rental?</Typography>
                    </AccordionSummary>
                    <AccordionDetails>
                        <Typography className="text-base">
                            <div className="text-lg">
                                <h2 className='font-semibold'>Khi đăng ký tài khoản tại Houseware Rental, bạn sẽ:</h2>
                                <ol className="ml-4">
                                    <li>
                                        - Dễ dàng theo dõi tình trạng đơn hàng của mình khi đặt hàng.
                                    </li>
                                    <li>
                                        - Thao tác nhanh hơn khi đặt những đơn hàng tiếp theo. Hệ thống sẽ tự động lưu trữ và điền sẵn các thông tin cá nhân của bạn cho những đơn hàng sau.
                                    </li>
                                    <li>
                                        - Thường xuyên được cập nhật những thông tin sản phẩm hữu ích cũng như đơn hàng mới, sản phẩm mới, tin nhắn mới...
                                    </li>
                                    <li>
                                        - Dễ dàng thao tác các tính năng trên website HWR
                                    </li>
                                </ol>
                            </div>
                        </Typography>
                    </AccordionDetails>
                </Accordion>
                <Accordion expanded={expanded === 'panel3'} onChange={handleChange('panel3')}>
                    <AccordionSummary aria-controls="panel3d-content" id="panel3d-header">
                        <Typography><PersonIcon /> Làm thế nào để tôi đổi mật khẩu?</Typography>
                    </AccordionSummary>
                    <AccordionDetails>
                        <Typography className="text-base">
                            <div className="text-lg">
                                <h2 className='font-semibold'>Trường hợp bạn quên mật khẩu hoặc muốn đổi mật khẩu, vui lòng làm theo các bước sau:</h2>
                                <ol className="ml-4">
                                    <li>
                                        - Mở website HWR, vào màn hình Đăng nhập, chọn “Quên mật khẩu”
                                    </li>
                                    <li>
                                        - Hệ thống sẽ yêu cầu bạn cung cấp email.
                                    </li>
                                    <li>
                                        - Hệ thống sẽ gửi email đến địa chỉ bạn đã điền
                                    </li>
                                    <li>
                                        - Bạn vui lòng truy cập vào đường dẫn mà website HWR đã cung cấp để tiến hành đổi mật khẩu
                                    </li>
                                </ol>
                            </div>
                        </Typography>
                    </AccordionDetails>
                </Accordion>
                <Accordion expanded={expanded === 'panel4'} onChange={handleChange('panel4')}>
                    <AccordionSummary aria-controls="panel4d-content" id="panel4d-header">
                        <Typography><PersonIcon /> Làm thế nào để tôi quản lý thông tin trong tài khoản?</Typography>
                    </AccordionSummary>
                    <AccordionDetails>
                        <Typography>
                            <div className="text-lg">
                                <h2 className='font-semibold'>Quản lý thông tin cá nhân:</h2>
                                <ol className="ml-4">
                                    <li>
                                        - Mở website HWR, vào mục Tài khoản > Quản lý tài khoản
                                    </li>
                                    <li>
                                        - Bạn điền các thông tin muốn thay đổi
                                    </li>
                                    <li>
                                        - Bấm nút "Lưu" để hoàn thành
                                    </li>
                                </ol>
                            </div>
                        </Typography>
                    </AccordionDetails>
                </Accordion>
            </div>

            <div className="pb-10">
                <Typography
                    variant="h4"
                    className="font-bold py-3 mb-4 text-left text-success fw-light"
                >
                    <ReceiptLongIcon/> Đặt hàng và Thanh toán
                </Typography>

                <Accordion expanded={expanded1 === 'panel1'} onChange={handleChange1('panel1')}>
                    <AccordionSummary aria-controls="panel1d-content" id="panel1d-header">
                        <Typography><ReceiptLongIcon/> Hướng dẫn thuê sản phẩm</Typography>
                    </AccordionSummary>
                    <AccordionDetails>
                        <Typography>
                            <div className="text-lg">
                                <h2 className='font-semibold'>Bạn có thể thuê đồ trực tuyến tại HWR thông qua các bước cơ bản như sau:</h2>
                                <ol className="ml-4">
                                    <li>
                                        1. Tìm kiếm sản phẩm: Khách thuê có thể tìm kiếm sản phẩm:
                                    </li>
                                    <li className="ml-2">
                                        - Thông qua từ khoá bằng cách click vào biểu tượng  trên thanh menu ở Trang Chủ
                                    </li>
                                    <li className="ml-2">
                                        - Thông qua chức năng lọc sản phẩm theo danh mục hoặc giá thuê
                                    </li>
                                    <li>
                                        2. Xem thông tin chi tiết của sản phẩm bao gồm: tính năng, giá thuê và thông tin Chủ tiệm.
                                    </li>
                                    <li>
                                        3. Nếu muốn thuê sản phẩm nào, Khách thuê vui lòng chọn “Thuê ngay”.
                                    </li>
                                    <li>
                                        4. Tiếp tục thuê sản phẩm khác, nếu muốn.
                                    </li>
                                    <li>
                                        5. Kiểm tra Giỏ hàng.
                                    </li>
                                    <li>
                                        6. Chọn số lượng và thời gian thuê cho từng sản phẩm
                                    </li>
                                    <li>
                                        7. Bấm nút "Thanh Toán" để xác nhận tạo đơn hàng
                                    </li>
                                </ol>
                            </div>
                        </Typography>
                    </AccordionDetails>
                </Accordion>
                <Accordion expanded={expanded1 === 'panel2'} onChange={handleChange1('panel2')}>
                    <AccordionSummary aria-controls="panel2d-content" id="panel2d-header">
                        <Typography><ReceiptLongIcon/> Làm thế nào để tôi liên hệ với Chủ tiệm?</Typography>
                    </AccordionSummary>
                    <AccordionDetails>
                        <Typography>
                            <div className="text-lg">
                                <p>Sau khi Khách thuê đã tạo đơn hàng thuê, HWR sẽ gửi email đến Chủ tiệm để xác nhận đơn hàng và chuẩn bị sản phẩm. Sau đó. Chủ tiệm sẽ chủ động liên hệ với bạn qua số điện thoại để xác nhận thêm về cách thức giao nhận hàng, thời gian giao nhận hàng và các thông tin khác, nếu có.</p>
                                <h2 className='font-semibold'>Trong trường hợp Khách thuê muốn chủ động liên hệ với Chủ tiệm, bạn có thể vào "Quản lý đơn hàng" để lấy thông tin liên hệ của Chủ tiệm.</h2>
                            </div>
                            <div>
                                Image
                            </div>
                        </Typography>
                    </AccordionDetails>
                </Accordion>
                <Accordion expanded={expanded1 === 'panel3'} onChange={handleChange1('panel3')}>
                    <AccordionSummary aria-controls="panel3d-content" id="panel3d-header">
                        <Typography><ReceiptLongIcon/> Làm sao tôi biết sản phẩm còn hay hết cho thuê?</Typography>
                    </AccordionSummary>
                    <AccordionDetails>
                        <Typography>
                            <div className="text-lg">
                                <h2 className='font-semibold'>Khách thuê có thể nhận biết sản phẩm có đang được cho thuê hay không bằng các cách sau:</h2>
                                <ol className="ml-4">
                                    <li>
                                        - Mở website HWR, vào mục "Tài khoản"
                                    </li>
                                    <li>
                                        - Nhấn vào mục "Quản lý sản phẩm thuê"
                                    </li>
                                </ol>
                            </div>
                            <div>
                                Image
                            </div>
                        </Typography>
                    </AccordionDetails>
                </Accordion>
            </div>

            <div className="pb-10">
                <Typography
                    variant="h4"
                    className="font-bold py-3 mb-4 text-left text-success fw-light"
                >
                    <LocalShippingIcon /> Giao nhận hàng
                </Typography>

                <Accordion expanded={expanded2 === 'panel1'} onChange={handleChange2('panel1')}>
                    <AccordionSummary aria-controls="panel1d-content" id="panel1d-header">
                        <Typography><LocalShippingIcon /> Làm thế nào để tôi nhận hàng từ Chủ tiệm?</Typography>
                    </AccordionSummary>
                    <AccordionDetails>
                        <Typography>
                            <div className="text-lg">
                                <h2 className='font-semibold'>Sau khi Chủ tiệm xác nhận đơn hàng, Chủ tiệm sẽ chủ động trao đổi với Khách thuê về thời gian và phương thức giao nhận.</h2>
                                <ol className="ml-4">
                                    <li>
                                        - Thời gian giao hàng có thể trước hoặc trong ngày đầu tiên sau khi chủ tiệm xác nhận đang giao, tuỳ vào thoả thuận giữa Chủ tiệm và Khách thuê.
                                    </li>
                                    <li>
                                        - Sau khi người thuê xác nhận đã nhận được hàng, kỳ thuê được tính bắt đầu từ ngày nhận hàng.
                                    </li>
                                    <li>
                                        - Sau khi nhận hàng, HWR khuyến khích Khách thuê nên kiểm tra sản phẩm cẩn thận và chụp hình/quay phim lại quá trình kiểm hàng để hạn chế rủi ro.
                                    </li>
                                    <li>
                                        - Sau khi đã kiểm tra và hài lòng với chất lượng sản phẩm, Khách thuê có thể xác nhận bắt đầu kỳ thuê bằng cách vào trang đơn hàng chi tiết và click vào nút “Tôi đã nhận hàng”
                                    </li>
                                    <li>
                                        - Trong trường hợp không hài lòng với sản phẩm được nhận, Khách thuê có thể liên hệ trực tiếp với Chủ tiệm hoặc thông báo với website HWR bằng cách vào trang đơn hàng chi tiết và chọn “Báo cáo vi phạm”.
                                    </li>
                                </ol>
                            </div>
                            <div>
                                Image
                            </div>
                        </Typography>
                    </AccordionDetails>
                </Accordion>
                <Accordion expanded={expanded2 === 'panel2'} onChange={handleChange2('panel2')}>
                    <AccordionSummary aria-controls="panel2d-content" id="panel2d-header">
                        <Typography><LocalShippingIcon /> Tôi cần lưu ý điều gì khi nhận hàng?</Typography>
                    </AccordionSummary>
                    <AccordionDetails>
                        <Typography>
                            <div className="text-lg">
                                <h2 className='font-semibold'>Có 02 lưu ý Khách thuê cần thực hiện trước khi xác nhận bắt đầu kỳ thuê:</h2>
                                <ol className="ml-4">
                                    <li>
                                        1. Kiểm tra hàng trước khi xác nhận thuê sản phẩm
                                    </li>
                                    <li>
                                        Sau khi nhận hàng, HWR khuyến khích Khách thuê nên kiểm tra sản phẩm cẩn thận và chụp hình/quay phim lại quá trình kiểm hàng để hạn chế rủi ro.
                                    </li>
                                    <li>
                                        2. Lên hệ thống bấm xác nhận "Đã nhận hàng"
                                    </li>
                                    <li>
                                        - Sau khi đã kiểm tra và hài lòng với chất lượng sản phẩm, Khách thuê có thể xác nhận bắt đầu kỳ thuê bằng cách vào trang đơn hàng chi tiết và click vào nút “Tôi đã nhận hàng”
                                    </li>
                                    <li>
                                        - Trong trường hợp không hài lòng với sản phẩm được nhận, Khách thuê có thể liên hệ trực tiếp với Chủ tiệm hoặc thông báo với website HWR bằng cách vào trang đơn hàng chi tiết và chọn “Báo cáo vi phạm”.
                                    </li>
                                </ol>
                            </div>
                        </Typography>
                    </AccordionDetails>
                </Accordion>
                <Accordion expanded={expanded2 === 'panel3'} onChange={handleChange2('panel3')}>
                    <AccordionSummary aria-controls="panel3d-content" id="panel3d-header">
                        <Typography><LocalShippingIcon /> Tôi cần lưu ý điều gì khi trả hàng?</Typography>
                    </AccordionSummary>
                    <AccordionDetails>
                        <Typography>
                            <div className="text-lg">
                                <h2 className='font-semibold'>Có 02 lưu ý Khách thuê cần thực hiện khi trả hàng:</h2>
                                <ol className="ml-4">
                                    <li>
                                        1. Kiểm tra kỹ sản phẩm trước khi trả hàng cho Chủ tiệm
                                    </li>
                                    <li className="ml-2">
                                        - Đối với các hư hại, vết bẩn phổ thông, Chủ tiệm và HWR hoàn toàn hiểu đó là điều không tránh khỏi trong quá trình sử dụng và đã bao gồm trong phí thuê. Do đó, Chủ tiệm sẽ không thu thêm chi phí khắc phục (đã bao gồm trong phí thành viên)
                                    </li>
                                    <li className="ml-2">
                                        - Tuy nhiên đối với các hư hại, vết bẩn khó khắc phục, hoặc mất sản phẩm, Chủ tiệm sẽ thu thêm một khoản phí để khắc phục những thiệt hại này.
                                    </li>
                                    <li className="ml-2">
                                        - Khoản phí này được tính toán sau khi Chủ tiệm nhận lại và kiểm tra sản phẩm, chi tiết hư hại cũng như khoản phí khắc phục sẽ được thông báo đến Khách thuê trước khi thực hiện việc cấn trừ vào phí đặt cọc.
                                    </li>
                                    <li className="ml-2">
                                        - Khoản phí này sẽ tuỳ thuộc vào mức độ thiệt hại cũng như khả năng khắc phục tuy nhiên sẽ không lớn hơn giá trị thật của sản phẩm (giá bán lẻ – khấu hao)
                                    </li>
                                    <li>
                                        2. Nhận lại phần cọc sau khi đã trừ phí thuê và phí hư hỏng (nếu có)
                                    </li>
                                    <li className="ml-2">
                                        - Khách hàng cần lên hệ thống HWR để xác nhận Trả Hàng để nhận lại được tiền cọc tương ứng
                                    </li>
                                </ol>
                            </div>
                        </Typography>
                    </AccordionDetails>
                </Accordion>
            </div>

            <div className="pb-10">
                <Typography
                    variant="h4"
                    className="font-bold py-3 mb-4 text-left text-success fw-light"
                >
                    <InfoIcon /> Về Housewares Rental
                </Typography>

                <Accordion expanded={expanded2 === 'panel1'} onChange={handleChange2('panel1')}>
                    <AccordionSummary aria-controls="panel1d-content" id="panel1d-header">
                        <Typography><InfoIcon /> Housewares Rental là gì?</Typography>
                    </AccordionSummary>
                    <AccordionDetails>
                        <Typography>
                            <div className="text-lg">
                                <ol className="ml-4">
                                    <li>
                                        - Housewares Rental là nền tảng online giúp kết nối Chủ tiệm và Khách thuê trong việc thuê vật dụng. Housewares Rental ra đời với mong muốn hướng đến một lối sống tối giản, tiết kiệm và tiêu dùng bền vững.
                                    </li>
                                    <li>
                                        - Đến với Housewares Rental, bạn có thể chia sẻ những vật dụng chất lượng mà mình đang sở hữu để tối ưu hoá sử dụng vòng đời sản phẩm bằng cách đăng cho thuê.
                                    </li>
                                    <li>
                                        - Đồng thời cũng có thể tìm và thuê được những món đồ chỉ dùng trong thời gian ngắn cho những nhu cầu đặc biệt với chất lượng như mới cùng giá thuê vô cùng hấp dẫn.
                                    </li>
                                </ol>
                            </div>
                        </Typography>
                    </AccordionDetails>
                </Accordion>
                {/*<Accordion expanded={expanded2 === 'panel2'} onChange={handleChange2('panel2')}>*/}
                {/*    <AccordionSummary aria-controls="panel2d-content" id="panel2d-header">*/}
                {/*        <Typography><LocalShippingIcon /> Tại sao tôi nên thuê đồ tại Housewares Rental?</Typography>*/}
                {/*    </AccordionSummary>*/}
                {/*    <AccordionDetails>*/}
                {/*        <Typography>*/}
                {/*            <div className="text-lg">*/}
                {/*                <h2 className='font-semibold'>Có 02 lưu ý Khách thuê cần thực hiện trước khi xác nhận bắt đầu kỳ thuê:</h2>*/}
                {/*                <ol className="ml-4">*/}
                {/*                    <li>*/}
                {/*                        Housewares Rental ra đời từ chính nhu cầu cá nhân của nhóm các nhà sáng lập. Do nhu cầu di chuyển liên tục, và việc mua và vận chuyển rất tốn kém.*/}
                {/*                    </li>*/}
                {/*                    <li>*/}
                {/*                        Sau khi nhận hàng, HWR khuyến khích Khách thuê nên kiểm tra sản phẩm cẩn thận và chụp hình/quay phim lại quá trình kiểm hàng để hạn chế rủi ro.*/}
                {/*                    </li>*/}
                {/*                    <li>*/}
                {/*                        2. Lên hệ thống bấm xác nhận "Đã nhận hàng"*/}
                {/*                    </li>*/}
                {/*                    <li>*/}
                {/*                        - Sau khi đã kiểm tra và hài lòng với chất lượng sản phẩm, Khách thuê có thể xác nhận bắt đầu kỳ thuê bằng cách vào trang đơn hàng chi tiết và click vào nút “Tôi đã nhận hàng”*/}
                {/*                    </li>*/}
                {/*                    <li>*/}
                {/*                        - Trong trường hợp không hài lòng với sản phẩm được nhận, Khách thuê có thể liên hệ trực tiếp với Chủ tiệm hoặc thông báo với website HWR bằng cách vào trang đơn hàng chi tiết và chọn “Báo cáo vi phạm”.*/}
                {/*                    </li>*/}
                {/*                </ol>*/}
                {/*            </div>*/}
                {/*        </Typography>*/}
                {/*    </AccordionDetails>*/}
                {/*</Accordion>*/}
            </div>
        </div>
    );
};

export default Faq;
