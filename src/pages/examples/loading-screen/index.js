import { Spin } from "antd";
import React, { memo } from "react";

function LoadingScreen() {
  return (
    <div className="w-full h-full flex justify-center items-center">
      <Spin fullscreen tip="Loading" size="large"></Spin>
    </div>
  );
}

export default memo(LoadingScreen);
