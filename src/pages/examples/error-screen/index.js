/* eslint-disable jsx-a11y/anchor-is-valid */
import { Result } from "antd";
import React, { memo } from "react";
import BackButton from "../../../components/AdminStyle/subs/back-button";

function ErrorScreen() {
  return (
    <div className="w-full h-full flex justify-center items-center">
      <Result
        status="500"
        title="ERROR"
        subTitle="Đã có lỗi xảy ra, vui lòng thử lại!"
        extra={<BackButton />}
      />
    </div>
  );
}

export default memo(ErrorScreen);
