import { Button, Image } from "antd";
import React from "react";
import { Link, useNavigate } from "react-router-dom";
import NotFoundImage from "../../assets/404.svg";

const NotFound = () => {
  const navigate = useNavigate();
  // alert("hoan");
  return (
    <main className="w-full h-screen flex justify-center items-center">
      <section className="vh-100 d-flex align-items-center justify-center ">
        <div className="v-50% flex-col justify-center items-center ">
          <div className="w-50% flex justify-center">
            <Image
              src={NotFoundImage}
              className="img-fluid w-75"
              onClick={() => navigate("/")}
            />
          </div>

          <h1 className=" text-primary text-center my-8 text-lg text-blue-600">
            PAGE NOT FOUND
          </h1>

          <div className="flex justify-center my-8">
            <Button
              as={Link}
              variant="primary"
              className="animate-hover"
              onClick={() => navigate("/")}
            >
              Go back home
            </Button>
          </div>
        </div>
      </section>
    </main>
  );
};
export default NotFound;
