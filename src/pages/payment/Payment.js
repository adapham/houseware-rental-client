import React from "react";
import { Link, useLocation } from "react-router-dom";
import Breadcrumbs from "../../components/pageProps/Breadcrumbs";
import CheckoutProduct from "../../components/checkoutDetail/CheckoutProduct";


const Payment = () => {
  const location = useLocation();
  const userData = location.state; 

  return (
    <div className="max-w-container mx-auto px-4 py-20 xl:py-20" style={{marginBottom:'100px'}}>
      {/* <Breadcrumbs title="Thanh toán" /> */}
      <CheckoutProduct cartItems={userData}/>
      {/* <div className="pb-10">
        <p>Payment gateway only applicable for Production build.</p>
        <Link to="/">
          <button className="w-52 h-10 bg-primeColor text-white text-lg mt-4 hover:bg-black duration-300">
            Explore More
          </button>
        </Link>
      </div> */}
    </div>
  );
};

export default Payment;
