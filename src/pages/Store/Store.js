import React, { useEffect, useRef, useState } from 'react';
import MenuStore from '../../components/Store/Menu';
import { useLocation, useParams } from 'react-router-dom';

import HeaderStore from '../../components/Store/HeaderStore';
import ProductStore from '../../components/Store/ProductStore';
import OrderManagement from '../../components/Profile/OrderManagement';
import { getDetailStore, getStaticDashboard, getStaticDashbordTotalOrder, getStaticDashbordIncome, getStaticTotalRented, getStaticUserRented, getUserProfile } from '../../redux/apiRequestProduct';
import { useDispatch, useSelector } from 'react-redux';
import StatisticStore from '../../components/Profile/StatisticStore';
import HistoryRentalOrderStore from '../../components/Profile/HistoryRentalOrderStore';
import RentalOrderManagerStoreNew from '../../components/Profile/RentalOrderManagerStoreNew';
import httpRequest from '../../redux/apiConfig';
import { loadUserSuccess } from '../../redux/Slide/authSlide';

const Store = () => {
  const user = useSelector((state) => state?.auth?.loadUser?.user);
  const [page, setPage] = useState(1);
  const [reload, setReload] = useState(false);
  const [dataStore, setDataStore] = useState([]);
  const [storeId, setStoreID] = useState();
  const [staticData, setStaticData] = useState([]);
  const [staticDataTotalOrder, setStaticDataTotalOrder] = useState([]);
  const [staticDataIncome, setStaticDataIncome] = useState([]);
  const [staticTotalProductRented, setStaticTotalProductRented] = useState([]);
  const [statisUserRented,setStaticUserRented]=useState([]);
  const [typeIncome, setTypeIncome] = useState("ONE_MONTH");
  const [typeOrder, setTypeOrder] = useState("ONE_MONTH");
  const [typeUserRented, setTypeUserRented] = useState("ONE_MONTH");
  const dispatch = useDispatch();
const [selectKey,setSelectKey] = useState("")
  const param = useParams();
  const userId = param.idStore;
  const location = useLocation();
  const type = location?.state;
  const getDetailInforStore = async (userID) => {
    const response = await getDetailStore(userID);
    if (response) {
      setStoreID(response?.id);
      setDataStore(response);
    }
  }

  useEffect(()=>{
    refreshUser();
    if(type){
      setPage(3);
      setSelectKey("3")
    }
  },[location])

  const refreshUser = async ()=>{
    const response = await getUserProfile();
    if(response){
      dispatch(loadUserSuccess(response));
    localStorage.setItem("user", JSON.stringify(response));
    }
  }
  useEffect(() => {
    if (userId) {
      getDetailInforStore(userId);
      staticDashboard();
      getStaticTotalRentedProduct();
    }
  }, [userId, reload])
  useEffect(() => {
    if (typeIncome) {
      staticDashboardIncome(typeIncome);
    }

  }, [typeIncome])
  useEffect(() => {
    if (typeOrder) {
      staticDashboardTotalOrder(typeOrder);
    }

  }, [typeOrder])
  useEffect(() => {
    if (typeUserRented) {
      getUserRentedStatic(typeUserRented);
    }

  }, [typeUserRented])
  
  const staticDashboard = async () => {
    const response = await getStaticDashboard(user?.storeId);
    if (response) {
      setStaticData(response);
    }
  }
  const staticDashboardTotalOrder = async (type) => {
    const response = await getStaticDashbordTotalOrder(user?.storeId,type);
    if (response) {
      setStaticDataTotalOrder(response);
    }
  }
  const staticDashboardIncome = async (type) => {
    const response = await getStaticDashbordIncome(user?.storeId, type);
    if (response) {
      setStaticDataIncome(response);
    }
  }
  const getStaticTotalRentedProduct = async () => {
    const response = await getStaticTotalRented(user?.storeId);
    if (response) {
      setStaticTotalProductRented(response);
    }
  }
  const getUserRentedStatic = async (type)=>{
    const response = await getStaticUserRented(user?.storeId, type);
    if (response) {
      setStaticUserRented(response);
    }
  }
 
   
  return (

    <div className="max-w-container mx-auto px-4 mt-10 py-5 xl:py-5">
      {/* <Breadcrumbs title="About" prevLocation={prevLocation} /> */}
      <div className="pb-10 flex">
        {(user?.roleId === 2 && user?.storeId === storeId) &&
          <div className='mr-4'>
            <MenuStore setPage={setPage} setReload={setReload} reload={reload} selectKey={selectKey} setSelectKey={setSelectKey} />
          </div>
        }

        <div className='w-full'>
          {page == 1 && (
            <>
              <HeaderStore dataDetail={dataStore} setDataStore={setDataStore} storeId={storeId} />
              <ProductStore idStore={userId} /></>
          )}
          {
            page == 2 && (
              <OrderManagement />
            )
          }
          {
            page == 3 && (
              <RentalOrderManagerStoreNew typeNotify={type} location={location}/>
            )
          }
          {
            page == 4 && (
              <HistoryRentalOrderStore />
            )
          }
          {
            page == 5 && (
              <StatisticStore staticData={staticData}
                staticDataTotalOrder={staticDataTotalOrder}
                staticDataIncome={staticDataIncome}
                staticTotalProductRented={staticTotalProductRented}
                setPage={setPage}
                setTypeIncome={setTypeIncome}
                setTypeOrder={setTypeOrder}
                statisUserRented={statisUserRented}
                setTypeUserRented={setTypeUserRented} 
                setSelectKey={setSelectKey}
               />
            )
          }
        </div>
      </div>
    </div>
  );
};

export default Store;