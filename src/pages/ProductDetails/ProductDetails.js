import React, { useEffect, useState } from "react";
import { useLocation, useParams } from "react-router-dom";
import Breadcrumbs from "../../components/pageProps/Breadcrumbs";
import ProductInfo from "../../components/pageProps/productDetails/ProductInfo";
import ProductsOnSale from "../../components/pageProps/productDetails/ProductsOnSale";
import RelateProducts from "../../components/home/RelateProducts/RelateProducts";
import TabDetail from "../../components/pageProps/tabDescription/TabDetail";
import AccordionProduct from "../../components/pageProps/accordionDescription/AccordionProduct";
import ShortDetail from "../../components/pageProps/accordionDescription/ShortDetail";
import StoreInfo from "../../pages/ProductDetails/StoreInfo";
import { getDetailStore, getProductById, getReviewsProduct } from "../../redux/apiRequestProduct";

const ProductDetails = () => {
  const location = useLocation();
  const param = useParams();
  
  const idProduct = location?.state?.item ? location?.state?.item : param?._id;
  
  const [prevLocation, setPrevLocation] = useState("");
  const [productInfo, setProductInfo] = useState([]);
  const [reviewInfo, setReviewInfo] = useState([]);
  const [productId, setProductId] = useState(idProduct);
  const [commentSuccess,setCommentSuccess]=useState(false);
  const [reload,setReload]=useState(false)
  const [dataStore,setDataStore]=useState([]);
  useEffect(() => {
    setProductId(idProduct);
    setPrevLocation(idProduct);
  }, [location, productId]);
//console.log(location.state.item,"HIHIHIH")
  useEffect(() => {
    getProduct(productId);
    getReview(productId);
  }, [productId,commentSuccess,reload])

  const getProduct = async (id) => {
    const response = await getProductById(id);
    if (response) {
      setProductInfo(response);
    }
  };
  const getReview = async (id) => {
    if (id) {
      const review = await getReviewsProduct(id);
      if (review) {
        setReviewInfo(review);
      }
    }
  };
  const getDetailInforStore = async (userID)=>{
    const response =await getDetailStore(userID);
    if(response){
      setDataStore(response);
    }
  }
  useEffect(()=>{
    if(productInfo?.store?.userId){
      getDetailInforStore(productInfo?.store?.userId);
    }   
  },[productInfo?.store?.userId])

  return (
    <div className="w-full mx-auto border-b-[1px] border-b-gray-300">
      <div className="max-w-container mx-auto px-4 mb-10">
        <div className="xl:-mt-10 -mt-7 py-20 xl:py-20">
          {/* <Breadcrumbs title="" prevLocation={prevLocation} /> */}
        </div>
        <div className="w-full grid grid-cols-1 md:grid-cols-2 xl:grid-cols-6 gap-4 h-full -mt-5 xl:-mt-8 pb-10 bg-gray-100 p-4">
          <div className="h-full">
            <ProductsOnSale imageRelate={productInfo.imageTextUri} />
          </div>
          <div className="h-full xl:col-span-2">
            {productInfo && productInfo.imageTextUri && (
              <img
                className="w-full h-full object-cover"
                src={productInfo?.imageTextUri?.[0]}
                alt={productInfo?.title}
              />)}
          </div>
          <div className="h-full w-full md:col-span-2 xl:col-span-3 xl:p-14 flex flex-col gap-6 justify-center">
            <ProductInfo productInfo={productInfo}  storeData={productInfo?.store}/>
          </div>
        </div>
      </div>
      <div className="max-w-container mx-auto px-4 mb-10">
        <ShortDetail productInfo={productInfo} />
      </div>
      <div className="max-w-container mx-auto px-4">
        <AccordionProduct productInfo={productInfo?.regulations} />
      </div>
      {/* Thông tin chủ tiệm */}
      <div className="max-w-container mx-auto px-4">
        <StoreInfo  productInfo={productInfo?.store} storeData={dataStore} productId={idProduct}/>
      </div>
      <div className="mx-auto max-w-container px-4">
        <TabDetail productInfo={productInfo} reviewInfo={reviewInfo}
         setCommentSuccess={setCommentSuccess}
         commentSuccess={commentSuccess}
         reload={reload}
         setReload={setReload}
         />
      </div>
      <div className="mt-5 max-w-container mx-auto px-4">
        <RelateProducts categoryId={productInfo?.category?.id} />
      </div>
    </div>
  );
};

export default ProductDetails;
