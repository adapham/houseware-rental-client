import {Col, Row} from "antd";
import React from "react";
import Box from '@mui/material/Box';
import Stack from '@mui/material/Stack';
import Paper from '@mui/material/Paper';
import {styled} from '@mui/material/styles';
import Button from '@mui/material/Button';
import ChatOutlinedIcon from '@mui/icons-material/ChatOutlined';
import StorefrontOutlinedIcon from '@mui/icons-material/StorefrontOutlined';
import Grid from '@mui/material/Grid';
import Link from "antd/es/typography/Link";
import { useNavigate } from "react-router-dom";

const Item = styled(Paper)(({theme}) => ({
    backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
    ...theme.typography.body2,
    padding: theme.spacing(1),
    textAlign: 'center',
    color: theme.palette.text.secondary,
}));

export default function StoreInfo({productInfo,storeData,productId}) {
    const navigate = useNavigate();
    return (
        <Row gutter={16} className="mb-10">
            <Col span={24}>
                <Box sx={{width: '100%'}}>
                    <Stack spacing={2}>
                        <Item>
                            <div className="">
                                <div className="grid grid-cols-7 gap-4">
                                    <div className="flex col-span-2 w-full p-2 border-r-2">
                                        <img src={storeData?.imageTextUri?.[0]} alt=""
                                             className="self-center flex-shrink-0 w-24 h-24 border rounded-full md:justify-self-start dark:bg-gray-500 dark:border-gray-700"/>
                                        <div className="flex flex-col justify-around ml-4 w-full">
                                            <h4 className="text-lg font-semibold text-center md:text-left">{productInfo?.title}</h4>
                                            <div className="flex justify-between">
                                                <Button size="small" variant="outlined" startIcon={<ChatOutlinedIcon/>}
                                                onClick={()=>navigate('/profile',{state:productInfo?.userId})}>
                                                    Liên hệ
                                                </Button>
                                                <Button size="small" variant="outlined"
                                                        startIcon={<StorefrontOutlinedIcon/>}>
                                                    <Link href={`/store/${productInfo?.userId}`} >Xem cửa hàng</Link>
                                                </Button>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-span-5 flex">
                                        <Grid container>
                                            <Grid className="flex justify-start items-center" Grid item xs={2} sm={4}
                                                  md={4}>
                                                <p>Tổng sản phẩm: <span className="text-red-500 ml-10"> {storeData?.totalProduct}</span></p>
                                            </Grid>
                                            <Grid className="flex justify-start items-center" Grid item xs={2} sm={4}
                                                  md={4}>
                                                <p>Tỷ lệ xác nhận đơn: <span className="text-red-500 ml-10">{storeData?.orderReceivingRate}</span>
                                                </p>
                                            </Grid>
                                            <Grid className="flex justify-start items-center" Grid item xs={2} sm={4}
                                                  md={4}>
                                                <p>Đánh giá: <span className="text-red-500 ml-10">1k</span></p>
                                            </Grid>
                                            <Grid className="flex justify-start items-center" Grid item xs={2} sm={4}
                                                  md={4}>
                                                <p>Đã thuê: <span className="text-red-500 ml-10">{storeData?.totalPersonRented}</span></p>
                                            </Grid>
                                            
                                            <Grid className="flex justify-start items-center" Grid item xs={2} sm={4}
                                                  md={4}>
                                                <p>Tỷ lệ huỷ đơn: <span className="text-red-500 ml-10">{storeData?.orderCancellationRate}</span></p>
                                            </Grid>
                                            {/* <Grid className="flex justify-start items-center" Grid item xs={2} sm={4}
                                                  md={4}>
                                                <p>Theo dõi <span className="text-red-500 ml-10">18,7k</span></p>
                                            </Grid> */}
                                        </Grid>
                                    </div>
                                </div>
                            </div>
                        </Item>
                    </Stack>
                </Box>
            </Col>
        </Row>
    );
}
