import React, { useEffect, useState } from "react";
import { Button, Card, Col, Row } from "antd";
import { Container, Grid, Tab, Tabs, Typography } from "@material-ui/core";
import AvatarProfile from "./AvatarProfile";
import { FcAddRow, FcMoneyTransfer } from "react-icons/fc";
import { BsFillCartCheckFill } from "react-icons/bs";
import PersonalInfor from "../../components/Profile/PersonalInfor";
import General from "../../components/Profile/General";
import { fetchUser } from "../../redux/fetchData";
import { useDispatch, useSelector } from "react-redux";
import { loadUserSuccess } from "../../redux/Slide/authSlide";
import CustomizedDialogs from "../../components/Profile/CustomizedDialogs";
import { useLocation, useNavigate } from "react-router-dom";
import Link from "antd/es/typography/Link";
import { MdKeyboardArrowRight } from "react-icons/md";
import HorizontalLinearStepper from "../../components/Profile/HorizontalLinearStepper";

import ManageTransHistory from "../../components/Profile/ManageTransHistory";
import RentalOrderManagement from "../../components/Profile/RentalOrderManagement";
import { getOrderStatusLessee, getOrderStatusLessor, getStatusRequest, getTotalTransactions, getTransactionWithDraw, getUser } from "../../redux/apiRequestProduct";
import HistoryRented from "../../components/Profile/HistoryRented";
import { convertMoney } from "../../utils/formatConfig";
import { FcDebt } from "react-icons/fc";
import Chat from "../../components/Profile/chat/Chat"

const Profile = () => {
  const location = useLocation();
  const userId = location?.state;

  const [tabValue, setTabValue] = useState("account-general");
  const { Meta } = Card;
  const handleTabChange = (event, newValue) => {
    setTabValue(newValue);
  };
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const [loadChat, setLoadChat] = useState(false);
  const [view, setView] = useState(false);
  const [qldh, setQldh] = useState(true);
  const [qldhct, setQldhct] = useState(true);
  const user = useSelector((state) => state?.auth?.loadUser?.user);
  const [isModalOpen, setOpen] = useState(false);
  const [isModalOpen2, setOpen2] = useState(false);
  const [userD, setUserD] = useState([]);
  console.log(typeof(userId),"hoang");
  const [staticLessee, setStaticLessee] = useState();
  const [totalPrice, setTotalPrice] = useState(0);
  const [reload, setReload] = useState(false);
  const [totalWithDraw, setTotalWithDraw] = useState("");
  const [checkRequest, setCheckRequest] = useState();
  const [typeChoose, setTypeChoose] = useState("");
  useEffect(() => {
    if (userId && userId?.visual != "view" && typeof(userId)=='number' ) {
      setTabValue("message");
      setQldh(true); setQldhct(true);
    } else if (userId?.visual == "view") {
      setTabValue("account-general");
      setQldh(false);
    }else if(typeof(userId)=='string'){
      setTabValue("account-general");
      setQldh(false);
    }

  }, [loadChat,location])

  useEffect(() => {
    if (user?.id) {
      getTotalTransaction()
      getMyUser(user?.id);
    } else {
      const fetchData = async () => {
        const userData = await fetchUser();
        console.log("userData", userData);
        !userData && navigate("/signin");
        dispatch(loadUserSuccess(userData));

      };

      fetchData();
      setUserD(user)
    }
  }, [isModalOpen, isModalOpen2, reload, user]);

  const getTotalTransaction = async () => {
    const response = await getTotalTransactions(user?.id);
    if (response) {
      setTotalPrice(response);
    }
  }
  useEffect(() => {
    getTrasWithDraw(user?.id);
    getStaticLessee(user?.id);
    getCheckRequestLessor(user?.id);
  }, [reload])

  const getCheckRequestLessor = async (userId) => {
    const response = await getStatusRequest(userId);
    if (response) {

      setCheckRequest(response);
    }
  }
  const openDepositDialog = () => {
    setOpen(true);
  };
  const closeDepositDialog = () => {
    setOpen(false);
  };

  const openWithdrawDialog = () => {
    setOpen2(true);
  };
  const closeWithdrawDialog = () => {
    setOpen2(false);
  };
  const getMyUser = async (userId) => {
    const response = await getUser(userId);
    if (response) {
      setUserD(response);
    }
  };

  const getStaticLessee = async (userId) => {
    const response = await getOrderStatusLessee(userId);
    if (response) {
      setStaticLessee(response?.mapsData);
    }
  };
  const getTrasWithDraw = async (userId) => {
    const response = await getTransactionWithDraw(userId);
    if (response) {

      setTotalWithDraw(response);
    }
  };

  const handleChooseType = (type) => {
    setQldh(false);
    setTypeChoose(type);
  }

  return (
    <div className="max-w-container mx-auto px-4">
      <div className="pb-10">
        
          <Typography
            variant="h4"
            style={{width:'1500px'}}
            className="font-bold py-3 mb-4 text-center text-success fw-light bg-gradient-to-r from-green-400 to-blue-500 text-white rounded-md p-4"
          >
            Xin Chào Khách Hàng:{" "}
            <span className="font-bold">
              {userD?.name ? userD?.name : userD?.username}
            </span>
          </Typography>
          <Card className="overflow-hidden border-2 rounded-lg shadow shadow-[#dde8da] border-[#dde8da]" style={{width:'1500px'}}>
            <Grid container spacing={0}>
              <Grid item md={3}>
                <AvatarProfile user={userD} />
                <div className="flex justify-between m-10 ">
                  <Button className="bg-[#2980b9] text-white" onClick={openDepositDialog}>NẠP TIỀN</Button>
                  <Button className="bg-[#2980b9] text-white" onClick={openWithdrawDialog}>RÚT TIỀN</Button>
                </div>
    
                <div className="text-center flex justify-center"> {/* Add a wrapper div with text-center class */}
                  <Tabs
                    orientation="vertical"
                    variant="scrollable"
                    value={tabValue}
                    onChange={handleTabChange}
                  >
                    <Tab
                      onClick={() => { setQldh(true); setQldhct(true); setReload(!reload); setTypeChoose("") }}
                      label="Tổng Quan"
                      value="account-general"
                    />
                    <Tab
                      onClick={() => { setQldh(true); setQldhct(true); }}
                      label="Tin nhắn"
                      value="message"
                    />
                    <Tab
                      onClick={() => { setQldh(true); setQldhct(true); }}
                      label="Quản lý tài khoản"
                      value="account"
                    />
                    {(
                      <Tab
                        onClick={() => { setQldh(true); setQldhct(true); setView(false) }}
                        label="Quản lý sản phẩm thuê"
                        value="history-rented"
                      />
                    )}
                    <Tab
                      onClick={() => { setQldh(true); setQldhct(true); }}
                      label="Lịch Sử Giao Dịch"
                      value="history"
                    />
                    {userD.roleId === 1 && (checkRequest !== "ACCEPT") && (
                      <Tab
                        onClick={() => { setQldh(true); setQldhct(true); setReload(!reload) }}
                        label="Đăng ký cửa hàng"
                        value="request-account-form"
                      />
                    )}
                  </Tabs>
                </div>
                {userD.roleId === 2 && (
                  <div className="flex justify-center text-red-500">
                    <Link href={`/store/${user?.id}`} className="mr-5">
                      <Button className="w-auto border-none">
                        <span className="text-gray-500 font-medium">XEM CỬA HÀNG</span>
                      </Button>
                    </Link>
                  </div>
                )}
              </Grid>

              {(!qldh) && (
                <Grid item md={9}>
                  <RentalOrderManagement typeS={"qldh"} setLoadChat={setLoadChat} loadChat={loadChat} typeChoose={typeChoose} setTypeChoose={setTypeChoose} typeNotify={userId} reloadType={location} />
                </Grid>
              )}
              {(!qldhct) && (
                <Grid item md={9}>
                  <RentalOrderManagement typeS={"qldhct"} setLoadChat={setLoadChat} loadChat={loadChat} typeChoose={typeChoose} setTypeChoose={setTypeChoose}typeNotify={userId} reloadType={location} />
                </Grid>
              )}

              {qldh && qldhct && (
                <Grid item md={9}>
                  <div className="p-4">
                    {tabValue === "account-general" && (
                      <div>
                        <Row gutter={16}>
                          <Col span={8}>
                            <Card
                              bordered={false}
                              variant="outlined"
                              className="h-24 max-h-full border-2 text-lg font-semibold rounded-lg shadow shadow-[#dde8da] border-[#dde8da] cursor-pointer hover:bg-[#f1f6f0] hover:border-[#adc5a0]"
                            >
                              <Meta
                                avatar={
                                  <FcMoneyTransfer
                                    style={{ width: "50px", height: "50px" }}
                                  />
                                }
                                title={typeof totalPrice === 'object' ? "0 đ" : convertMoney(totalPrice) + " đ"}
                                description="Tổng tiền đã nạp"
                              />
                            </Card>
                          </Col>

                          <Col span={8}>
                            <Card
                              bordered={false}
                              variant="outlined"
                              className="h-24 max-h-full border-2 text-lg font-semibold rounded-lg shadow shadow-[#dde8da] border-[#dde8da]"
                            >
                              <Meta
                                avatar={
                                  <FcDebt
                                    style={{ width: "50px", height: "50px" }}
                                  />
                                }
                                title={typeof totalWithDraw === 'object' ? "0 đ" : convertMoney(totalWithDraw) + " đ"}
                                description="Tổng cọc nhận lại"
                              />
                            </Card>
                          </Col>

                          <Col span={8}>
                            <Card
                              bordered={false}
                              variant="outlined"
                              className="h-24 max-h-full border-2 text-lg font-semibold rounded-lg shadow shadow-[#dde8da] border-[#dde8da]"
                            >
                              <Meta
                                avatar={
                                  <FcAddRow
                                    style={{ width: "50px", height: "50px" }}
                                  />
                                }
                                title={userD?.money ? (userD?.money == 0.0 ? 0 : convertMoney(userD?.money)) + " đ" : 0 + "đ"}
                                description="Số dư còn lại"
                              />
                            </Card>
                          </Col>
                        </Row>

                        <fieldset className=" mt-5">
                          <div>
                            <button
                              onClick={() => setQldh(false)}
                              className="py-2 px-4 border-solid border-2 border-[#dde8da] text-[#57ad3f] bg-[#ecf1e9] 
                          hover:bg-[#f1f6f0] rounded-md font-semibold uppercase mb-2 duration-300 flex justify-between items-center w-full"
                            >
                              <span>Quản Lý đơn hàng thuê</span>
                              <MdKeyboardArrowRight className="text-2xl font-bold text-[#57ad3f]" />
                            </button>
                          </div>

                          <Row className="flex justify-between border-2 rounded-lg shadow shadow-[#dde8da] border-[#dde8da] mt-5 px-4 py-4">
                            <Col
                              className="flex justify-center border rounded-lg shadow shadow-[#dde8da]  border-gray-400"
                              span={4}
                            >
                              <Card
                                bordered={false}
                                style={{
                                  width: 150,
                                  textAlign: "center",
                                }}
                                onClick={() => handleChooseType("NEW")}
                                className="cursor-pointer"
                              >
                                <div
                                  style={{
                                    display: "flex",
                                    alignItems: "center",
                                    justifyContent: "center",
                                  }}
                                >
                                  <Meta
                                    avatar={
                                      <BsFillCartCheckFill
                                        style={{
                                          width: "30px",
                                          height: "30px",
                                        }}
                                      />
                                    }
                                  />
                                  <p
                                    style={{
                                      margin: "10px",
                                      marginRight: "48px",
                                    }}
                                    className="absolute top-1 right-2 bg-primeColor text-white text-xs w-4 h-4  rounded-full flex items-center justify-center font-semibold"
                                  >
                                    {staticLessee?.NEW}
                                  </p>
                                </div>
                                <p className="mt-5">Xác nhận</p>
                              </Card>
                            </Col>

                            <Col
                              className="flex justify-center border rounded-lg shadow shadow-[#dde8da] border-gray-400"
                              span={4}
                            >
                              <Card
                                onClick={() => handleChooseType("PENDING")}
                                bordered={false}
                                style={{
                                  width: 150,
                                  textAlign: "center",
                                }}
                                className="cursor-pointer"
                              >
                                <div
                                  style={{
                                    display: "flex",
                                    alignItems: "center",
                                    justifyContent: "center",
                                  }}
                                >
                                  <Meta
                                    avatar={
                                      <BsFillCartCheckFill
                                        style={{
                                          width: "30px",
                                          height: "30px",
                                        }}
                                      />
                                    }
                                  />
                                  <p
                                    style={{
                                      margin: "10px",
                                      marginRight: "48px",
                                    }}
                                    className="absolute top-1 right-2 bg-primeColor text-white text-xs w-4 h-4  rounded-full flex items-center justify-center font-semibold"
                                  >
                                    {staticLessee?.PENDING}
                                  </p>
                                </div>
                                <p className="mt-5">Đang giao</p>
                              </Card>
                            </Col>

                            <Col
                              className="flex justify-center border rounded-lg shadow shadow-[#dde8da] border-gray-400"
                              span={4}
                            >
                              <Card
                                onClick={() => handleChooseType("CONFIRM")}
                                bordered={false}
                                style={{
                                  width: 150,
                                  textAlign: "center",
                                }}
                                className="cursor-pointer"
                              >
                                <div
                                  style={{
                                    display: "flex",
                                    alignItems: "center",
                                    justifyContent: "center",
                                  }}
                                >
                                  <Meta
                                    avatar={
                                      <BsFillCartCheckFill
                                        style={{
                                          width: "30px",
                                          height: "30px",
                                        }}
                                      />
                                    }
                                  />
                                  <p
                                    style={{
                                      margin: "10px",
                                      marginRight: "48px",
                                    }}
                                    className="absolute top-1 right-2 bg-primeColor text-white text-xs w-4 h-4  rounded-full flex items-center justify-center font-semibold"
                                  >
                                    {staticLessee?.CONFIRM}
                                  </p>
                                </div>
                                <p className="mt-5">Chờ xác nhận</p>
                              </Card>
                            </Col>

                            <Col
                              className="flex justify-center border rounded-lg shadow shadow-[#dde8da] border-gray-400"
                              span={4}
                            >
                              <Card
                                onClick={() => handleChooseType("RENTED")}
                                bordered={false}
                                style={{
                                  width: 150,
                                  textAlign: "center",
                                }}
                                className="cursor-pointer"
                              >
                                <div
                                  style={{
                                    display: "flex",
                                    alignItems: "center",
                                    justifyContent: "center",
                                  }}
                                >
                                  <Meta
                                    avatar={
                                      <BsFillCartCheckFill
                                        style={{
                                          width: "30px",
                                          height: "30px",
                                        }}
                                      />
                                    }
                                  />
                                  <p
                                    style={{
                                      margin: "10px",
                                      marginRight: "48px",
                                    }}
                                    className="absolute top-1 right-2 bg-primeColor text-white text-xs w-4 h-4  rounded-full flex items-center justify-center font-semibold"
                                  >
                                    {staticLessee?.RENTED}
                                  </p>
                                </div>
                                <p className="mt-5">Hoàn tất</p>
                              </Card>
                            </Col>

                            <Col
                              className="flex justify-center border rounded-lg shadow shadow-[#dde8da] border-gray-400"
                              span={4}
                            >
                              <Card
                                onClick={() => handleChooseType("CANCEL")}
                                bordered={false}
                                style={{
                                  width: 150,
                                  textAlign: "center",
                                }}
                                className="cursor-pointer"
                              >
                                <div
                                  style={{
                                    display: "flex",
                                    alignItems: "center",
                                    justifyContent: "center",
                                  }}
                                >
                                  <Meta
                                    avatar={
                                      <BsFillCartCheckFill
                                        style={{
                                          width: "30px",
                                          height: "30px",
                                        }}
                                      />
                                    }
                                  />
                                  <p
                                    style={{
                                      margin: "10px",
                                      marginRight: "48px",
                                    }}
                                    className="absolute top-1 right-2 bg-primeColor text-white text-xs w-4 h-4  rounded-full flex items-center justify-center font-semibold"
                                  >
                                    {staticLessee?.CANCEL}
                                  </p>
                                </div>
                                <p className="mt-5">Đã hủy</p>
                              </Card>
                            </Col>
                          </Row>
                        </fieldset>



                        {user?.id && <General user={user} check={true}  />}
                      </div>
                    )}
                    {tabValue === "account" && <PersonalInfor user={user} />}
                    {tabValue === "message" && <Chat userId={userId} />}
                    {tabValue === "account-info" && (
                      <div>{/* Info Tab Content */}</div>
                    )}
                    {tabValue === "account-social-links" && (
                      <div>{/* Social Links Tab Content */}</div>
                    )}
                    {tabValue === "history" && <ManageTransHistory />}
                    {(tabValue === "request-account-form") && (
                      <HorizontalLinearStepper checkRequest={checkRequest} />
                    )}
                    {(tabValue === "history-rented") && (
                      <HistoryRented view={view} setView={setView} setLoadChat={setLoadChat} loadChat={loadChat} />
                    )}


                  </div>
                </Grid>
              )}
            </Grid>
          </Card>

          <div className="text-right mt-3"></div>
  
      </div>

      <CustomizedDialogs
        isModalOpen={isModalOpen}
        closeModal={closeDepositDialog}
        title={"Nạp tiền"}
        label={"Số tiền nạp"}
        balances={userD?.money ? userD.money.toLocaleString() + " đ" : 0}
        id={userD?.id}
      />

      <CustomizedDialogs
        isModalOpen={isModalOpen2}
        closeModal={closeWithdrawDialog}
        title={"Rút tiền"}
        label={"Số tiền rút"}
        balances={userD?.money ? userD.money.toLocaleString() + " đ" : 0}
        id={userD?.id}
      />
    </div>
  );
};

export default Profile;
