import React, { useState, useRef, useEffect } from 'react';
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import Modal from '@mui/material/Modal';
import TextField from '@mui/material/TextField';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import IconButton from '@mui/material/IconButton';
import CloseIcon from '@mui/icons-material/Close';
import Stack from '@mui/material/Stack';
import { FaFileContract } from 'react-icons/fa6';
import { getUpdateAvatar } from '../../redux/apiRequestProduct';
import { toast } from 'react-toastify';

// Create a custom Badge component using Chip
const Badge = ({ color, children }) => (
    <Button variant="contained" color={color} size="small">
      {children}
    </Button>
  );
  const imageDefault = "https://inkythuatso.com/uploads/thumbnails/800/2023/03/6-anh-dai-dien-trang-inkythuatso-03-15-26-36.jpg";
  function checkImagePath(inputString) {
    // Kiểm tra xem chuỗi có kết thúc bằng "image/" hay không
    return inputString.endsWith("image/") ? imageDefault : inputString;
}
export default function AvatarProfile({ user }) {
  const [imageUser, setImageUser] = useState();
  const [name, setName] = useState('');
  const [zalo, setZalo] = useState('');

  const [isOpen, setIsOpen] = useState(false);
  const profileImage = useRef(null);

  const openChooseImage = () => {
    profileImage.current.click();
  };
const getEditAvatar = async (infor)=>{

  const response = await getUpdateAvatar(user?.id,infor);
  if(response){
    toast.success('Ảnh đã được sửa', {
      position: "top-right",
      autoClose: 2000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
      theme: "light",
      });
  }
}
  const changeProfileImage = (event) => {
    console.log(event,"fileImgae");
    const ALLOWED_TYPES = ['image/png', 'image/jpeg', 'image/jpg'];
    const selected = event.target.files[0];
    console.log(selected,"IMAGEFILE");
    if (selected && ALLOWED_TYPES.includes(selected.type)) {
      const formData = new FormData();
    formData.append('image', selected);
      getEditAvatar(formData)
      let reader = new FileReader();
      reader.onloadend = () => setImageUser(reader.result);
      return reader.readAsDataURL(selected);
    }

    setIsOpen(true);
  };

  useEffect(() => {
    if (user) {
      // const imageUrl = checkImagePath(user?.imageUrl) ? user.imageUrl : "https://inkythuatso.com/uploads/thumbnails/800/2023/03/6-anh-dai-dien-trang-inkythuatso-03-15-26-36.jpg";
      const imageUrl = user?.imageUrl ? checkImagePath(user?.imageUrl) : "https://inkythuatso.com/uploads/thumbnails/800/2023/03/6-anh-dai-dien-trang-inkythuatso-03-15-26-36.jpg";

      setImageUser(imageUrl);
      setName(user?.name ? user?.name : user?.username);
      setZalo(user?.zalo);
    }
  }, [user]);

  return (
    <Box sx={{ display: 'flex', flexDirection: 'column', alignItems: 'center', py: 5, borderBottom: 1, borderColor: 'brand.light' }}>
      <Avatar
        sx={{ width: '200px', height: '200px', cursor: 'pointer', borderRadius: '50%',objectFit: 'cover', // or 'contain' depending on your preference
        objectPosition: 'center', }}
        alt="User Avatar"
        src={imageUser ? imageUser :imageDefault}
        onClick={openChooseImage}
      ></Avatar>
      <input
        hidden
        type="file"
        ref={profileImage}
        onChange={changeProfileImage}
      />
      <Modal
        open={isOpen}
        onClose={() => setIsOpen(false)}
      >
        <Box sx={{ position: 'absolute', top: '50%', left: '50%', transform: 'translate(-50%, -50%)', width: 300, bgcolor: 'background.paper', boxShadow: 24, p: 3, borderRadius: 2 }}>
          <Box display="flex" alignItems="center" justifyContent="space-between">
            <Typography variant="h6">Something went wrong</Typography>
            <IconButton onClick={() => setIsOpen(false)}>
              <CloseIcon />
            </IconButton>
          </Box>
          <Typography>File not supported!</Typography>
          <Stack direction="row" spacing={1} alignItems="center">
            <Typography color="text.secondary" fontSize="sm">
              Supported types:
            </Typography>
            <Badge color="success.main">PNG</Badge>
            <Badge color="success.main">JPG</Badge>
            <Badge color="success.main">JPEG</Badge>
          </Stack>
          <Button onClick={() => setIsOpen(false)}>Close</Button>
        </Box>
      </Modal>
      <Stack spacing={1}>
        <Typography variant="h5" color="brand.dark">
          {name}
        </Typography>
      </Stack>
     
    </Box>
  );
}
