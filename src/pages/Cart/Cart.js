import React, { useCallback, useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link, useNavigate } from "react-router-dom";
import { motion } from "framer-motion";
import Breadcrumbs from "../../components/pageProps/Breadcrumbs";
import { resetCart } from "../../redux/Slide/orebiSlice";
import { emptyCart } from "../../assets/images/index";
import ItemCard from "./ItemCard";
import {
  getProductsInCard,
  removeAllProductInCard,
} from "../../redux/apiRequestProduct";
import { toast } from "react-toastify";



function filterUniqueById(objects) {
  const uniqueIds = {};
  const result = [];

  for (const obj of objects) {
    const id = obj.id;

    // Kiểm tra xem id đã xuất hiện chưa
    if (!uniqueIds[id]) {
      // Nếu chưa xuất hiện, thêm giá trị vào mảng kết quả và đánh dấu id đã xuất hiện
      result.push(obj);
      uniqueIds[id] = true;
    }
  }

  return result;
}
const Cart = () => {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const user = useSelector((state) => state?.auth?.loadUser?.user);
  const products = useSelector((state) => state.orebiReducer?.products);
  const [productDb, setProductDb] = useState({});
  const [totalAmt, setTotalAmt] = useState("");
  const [shippingCharge, setShippingCharge] = useState("");
  const [selectedProducts, setSelectedProducts] = useState([]);
  const [productChoose, setProductChoose] = useState([]);
  const [isChecked, setIsChecked] = useState([]);
  const [reload, setReload] = useState(false);
  const [storeID, setStoreId] = useState("");
  const getProductInCard = async (id) => {
    const response = await getProductsInCard(id);
    if (response) {
      
      const storeDataObject = {};
      response.forEach((item) => {
        const { storeId } = item;
        if (storeDataObject[storeId]) {
          storeDataObject[storeId].push(item);
        } else {
          storeDataObject[storeId] = [item];
        }
      });
      setProductDb(storeDataObject);
    }
  };

  // Reload khi product thay đổi
  useEffect(() => {
    if (user?.id) {
      getProductInCard(user?.id);
    } else {
      const storeDataObject = {};
      // Lặp qua mảng yourData để phân loại theo storeId
      products.forEach((item) => {
        const { storeId } = item;
        // Nếu storeId đã tồn tại trong Object, thêm vào mảng tương ứng
        if (storeDataObject[storeId]) {
          storeDataObject[storeId].push(item);
        } else {
          // Nếu chưa tồn tại, tạo mảng mới với storeId là key
          storeDataObject[storeId] = [item];
        }
      });
      setProductDb(storeDataObject);
    }
  }, [user?.id, products]);

  useEffect(() => {

    let price = 0;
    productChoose.map((item) => {
      price +=
        item.price * item.quantity * item.rentalPeriod +
        item.depositPrice * item.quantity;
      return price;
    });
    setTotalAmt(price);

  }, [productChoose]);

  const handleDeleteAllProduct = async () => {
    if (user?.id) {
      deleteAllProductOnCart(user?.id);
    } else {
      dispatch(resetCart());
    }
  };
  const deleteAllProductOnCart = async (userId) => {
    const response = await removeAllProductInCard(userId);
    if (response) {
      //getProductInCard(user?.id);
      dispatch(resetCart());
      toast.success("xoá sản phẩm thành công", {
        position: "top-right",
        autoClose: 2000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "light",
      });
    }
  };

  useEffect(() => {
    if (totalAmt <= 200) {
      setShippingCharge(0);
    } else if (totalAmt <= 400) {
      setShippingCharge(0);
    } else if (totalAmt > 401) {
      setShippingCharge(0);
    }
  }, [totalAmt]);

  const handleCheckboxChange = (index, e) => {
    const check = e.target.checked;
    const idsDelete = productDb[index].filter(({isHidden})=>isHidden!=true).map(({ productId }) => productId);
    
    if (check) {
      setProductChoose([...productChoose, ...productDb[index]])
      setIsChecked([...isChecked, ...idsDelete]);
      setStoreId(productDb[index][0].storeId);

    } else {
      setProductChoose(
        productChoose.filter((item) => !idsDelete.includes(item.productId))
      );
      setIsChecked(isChecked.filter((item) => !idsDelete.includes(item)));
      if (productDb[index][0].storeId === storeID) {
        setStoreId("");
      }
    }
  };
  useEffect(() => {
    const product = [];
    Object.keys(productDb).map((storeIndex) => {
      productDb[storeIndex].map((item) => {
        product.push(item);
      })
    })
    setProductChoose([...product.filter(({ productId }) => isChecked.includes(productId))]);
  }, [isChecked, productDb])


  
  return (
<div className="container mx-auto px-4 py-20 xl:py-20" style={Object.entries(productDb).length > 0 ? {marginBottom: '160px'} : { marginBottom: '160px', marginTop: '160px' }}>
      {/* <Breadcrumbs title="" /> */}
      {Object.entries(productDb).length > 0 ? (
        <div className="pb-20">
          <div>
            <div className="relative overflow-x-auto shadow-md sm:rounded-lg">
              <table className="w-full border text-sm text-left text-gray-500 text-gray-400">
                <thead className="text-sm bg-gray-500 text-white font-semibold uppercase">
                  <tr>
                    <th scope="col" className="px-6 py-4"></th>
                    <th scope="col" className="px-6 py-4">
                      <span className="sr-only">Image</span>
                    </th>
                    <th scope="col" className="px-6 py-4">
                      <div className="flex items-center text-sm">Sản phẩm</div>
                    </th>
                    <th scope="col" className="px-6 py-4">
                      <div className="flex items-center text-sm">Giá thuê</div>
                    </th>
                    <th scope="col" className="px-6 py-4">
                      <div className="flex items-center text-sm">Giá cọc</div>
                    </th>
                    <th scope="col" className="px-6 py-4">
                      <div className="flex items-center text-sm">Số lượng</div>
                    </th>
                    <th scope="col" className="px-6 py-4">
                      <div className="flex items-center text-sm">
                        Tháng thuê
                      </div>
                    </th>
                    <th scope="col" className="px-6 py-4">
                      <div className="flex items-center text-sm">Tạm tính</div>
                    </th>
                    <th scope="col" className="px-6 py-4">
                      <span className="sr-only">Action</span>
                    </th>
                  </tr>
                </thead>
                <tbody>
                  {Object.keys(productDb).map((storeIndex) => (
                    <React.Fragment key={storeIndex}>
                      <tr>
                        <td colSpan={9} className="relative">
                          <input
                            id={`checkbox-table-${storeIndex}`}
                            type="checkbox"
                            onChange={(e) =>
                              handleCheckboxChange(storeIndex, e)
                            }
                            className="ml-5 mt-2 w-8 h-8 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 dark:focus:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600"
                          />
                          <p className="text-xl ml-2 text-black inline absolute mt-2">
                            {productDb[storeIndex][0]?.storeName}
                          </p>
                        </td>
                      </tr>
                      {productDb[storeIndex].map((item, index) => (
                        <ItemCard
                          key={index}
                          item={item}
                          setTotalAmt={setTotalAmt}
                          selectedProducts={selectedProducts}
                          setSelectedProducts={setSelectedProducts}
                          isChecked={isChecked}
                          setIsChecked={setIsChecked}
                          productChoose={productChoose}
                          setProductChoose={setProductChoose}
                          reload={reload}
                          setReload={setReload}
                          storeID={storeID}
                        />
                      ))}
                    </React.Fragment>
                  ))}
                  {/* {Array.isArray(productDb) &&
                    productDb.map((item) => (
                      <ItemCard
                        key={item.productId}
                        item={item}
                        setTotalAmt={setTotalAmt}
                      />
                    ))} */}
                </tbody>
              </table>
            </div>
          </div>

          <div className="flex justify-between mt-5">
            <Link to="/shop">
              <button
                className="py-2 px-10 border-solid border-2 border-[#b5d69f] text-[#57ad3f] bg-[#dde8da] 
                hover:bg-[#f1f6f0] rounded-md font-semibold uppercase mb-4 duration-300"
              >
                Tiếp tục thuê hàng
              </button>
            </Link>

            <button
              onClick={handleDeleteAllProduct}
              className="py-2 px-10 border-solid border-2 border-[#b5d69f] text-[#57ad3f] bg-[#dde8da] 
              hover:bg-[#f1f6f0] rounded-md font-semibold uppercase mb-4 duration-300"
            >
              Xóa tất cả sản phẩm
            </button>
          </div>
          {
            productChoose.length > 0 && (
              <div className="gap-4 flex justify-end mt-4">
                <div className="w-96 flex flex-col gap-4">
                  <div className="rounded-md">
                    <div className="text-xl font-semibold text-white text-right card-header bg-secondary border-0">
                      CỘNG GIỎ HÀNG
                    </div>
                    <div className="flex items-center justify-between border-[1px] border-gray-400 border-b-0 py-1.5 text-lg px-4 font-semibold">
                      Tạm tính
                      <span className="font-semibold tracking-wide font-titleFont">
                        {totalAmt ? totalAmt.toLocaleString() : 0} VND
                      </span>
                    </div>
                    <div className="flex items-center justify-between border-[1px] border-gray-400 border-b-0 py-1.5 text-lg px-4 font-semibold">
                      Giảm giá
                      <span className="font-semibold tracking-wide font-titleFont">
                        {shippingCharge} VND
                      </span>
                    </div>
                    <div className="flex items-center justify-between border-[1px] border-gray-400 py-1.5 text-lg px-4 font-semibold">
                      Tổng
                      <span className="font-bold tracking-wide text-lg font-titleFont">
                        {totalAmt + shippingCharge
                          ? (totalAmt + shippingCharge).toLocaleString()
                          : 0}{" "}
                        VND
                      </span>
                    </div>
                  </div>
                  <div className="flex justify-end">
                    <button onClick={() => navigate('/paymentgateway', { state: productChoose })}
                      className="py-2 px-10 border-solid border-2 border-[#b5d69f] text-[#57ad3f] bg-[#dde8da] 
                  hover:bg-[#f1f6f0] rounded-md font-semibold uppercase mb-4 duration-300"

                    >
                      Tiến hành thanh toán
                    </button>
                  </div>
                </div>
              </div>
            )
          }

        </div>
      ) : (
        <motion.div
          initial={{ y: 30, opacity: 0 }}
          animate={{ y: 0, opacity: 1 }}
          transition={{ duration: 0.4 }}
          className="flex flex-col mdl:flex-row justify-center items-center gap-4 pb-20"
        >
          <div>
            <img
              className="w-80 rounded-lg p-4 mx-auto"
              src={emptyCart}
              alt="emptyCart"
            />
          </div>
          <div className="max-w-[500px] p-4 py-8 bg-white flex gap-4 flex-col items-center rounded-md shadow-lg">
            <h1 className="font-titleFont text-xl font-bold uppercase">
              Chưa có sản phẩm nào trong giỏ hàng.
            </h1>
            {/* <p className="text-sm text-center px-10 -mt-2">
              Your Shopping cart lives to serve. Give it purpose - fill it with
              books, electronics, videos, etc. and make it happy.
            </p> */}
            <Link to="/shop">
              <button className="py-2 px-10 border-solid border-2 border-[#b5d69f] text-[#57ad3f] bg-[#dde8da] rounded-md font-semibold uppercase mb-4 duration-300">
                Tiếp tục thuê hàng
              </button>
            </Link>
          </div>
        </motion.div>
      )}
    </div>
  );
};

export default Cart;
