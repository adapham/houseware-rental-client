import React, { useEffect, useState } from "react";
import { ImCross } from "react-icons/im";
import { useDispatch, useSelector } from "react-redux";
import {
  deleteItem,
  decreaseQuantity,
  increaseQuantity,
  increasePeriod,
  decreasePeriod,
} from "../../redux/Slide/orebiSlice";

import {
  getProductById,
  removeProductInCard,
  updateProductInCard,
} from "../../redux/apiRequestProduct";
import { toast } from "react-toastify";
import { useNavigate } from "react-router-dom";

const ItemCard = ({
  item,
  isChecked,
  setIsChecked,
  productChoose,
  setProductChoose,
  reload,
  setReload,
}) => {
  const user = useSelector((state) => state?.auth?.loadUser?.user);
  const [itemCart, setItemCart] = useState(item);
  const [totalTmp, setTotalTmp] = useState();
  const products = useSelector((state) => state.orebiReducer?.products);
  const dispatch = useDispatch();
  const [productDb, setProductDb] = useState([]);
  const navigate = useNavigate();
  const getProduct = async (id) => {
    const response = await getProductById(id);
    if (response) {
      setProductDb(response);
    }
  };

  const handleDeleteCart = () => {
    if (user?.id) {
      deleteCart(item.id);
    } else {
      dispatch(deleteItem(item.productId));
    }
  };

  useEffect(() => {
    if (!user?.id) {
      setItemCart(item);

    }

  }, [item]);
  useEffect(() => {
    if (item?.productId) {
      getProduct(item?.productId);

    }
  }, [item?.productId]);

  useEffect(() => {
    const total =
      itemCart.price * itemCart.quantity * itemCart.rentalPeriod +
      itemCart.depositPrice * itemCart.quantity;

    setTotalTmp(total);
  }, [itemCart]);

  const handleUpdateProduct = (type) => {
    if (type === "increaseQuantity") {
      if (user?.id) {
        if (itemCart.quantity + 1 > productDb?.quantity) {

          editProduct(item.id, { quantity: productDb?.quantity }, type);
          toast.warning(`Số sản phẩm chỉ còn: ${productDb?.quantity}`, {
            position: "top-right",
            autoClose: 2000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
            theme: "light",
          });
        } else {
          editProduct(item.id, { quantity: itemCart.quantity + 1 }, type);
        }

      } else {

        if (itemCart.quantity + 1 > productDb?.quantity) {
          toast.warning(`Số sản phẩm chỉ còn: ${productDb?.quantity}`, {
            position: "top-right",
            autoClose: 2000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
            theme: "light",
          });
        } else {
          dispatch(increaseQuantity({ productId: itemCart.productId }));
        }
      }
    } else if (type === "decreaseQuantity") {
      if (itemCart.quantity <= 1) {
        return;
      }
      if (user?.id) {
        editProduct(item.id, { quantity: itemCart.quantity - 1 }, type);
      } else {
        dispatch(decreaseQuantity({ productId: itemCart.productId }));
      }
    } else if (type === "increasePeriod") {
      dispatch(increasePeriod({ productId: itemCart.productId }));
      if (user?.id) {
        editProduct(item.id, { rentalPeriod: itemCart.rentalPeriod + 1 });
      }
    } else if (type === "decreasePeriod") {
      if (itemCart.rentalPeriod <= 1) {
        return;
      }
      dispatch(decreasePeriod({ productId: item.productId }));
      if (user?.id) {
        editProduct(item.id, { rentalPeriod: itemCart.rentalPeriod - 1 });
      }
    }
    setReload(!reload);
  };
  const editProduct = async (cartId, product, type) => {
    const response = await updateProductInCard(cartId, product);
    if (response === 200) {
      setItemCart({ ...itemCart, ...product });
      if (type == "increaseQuantity") {
        dispatch(increaseQuantity({ productId: itemCart.productId }));
      } else {
        dispatch(decreaseQuantity({ productId: itemCart.productId }));
      }
    }
  };

  const deleteCart = async (id) => {
    const response = await removeProductInCard(id);
    if (response) {
      dispatch(deleteItem(item.productId));
      toast.success("Xóa sản phẩm thành công", {
        position: "top-right",
        autoClose: 2000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "light",
      });
    }
    setReload(!reload);
  };
  const handleCheckboxChange = (item, e) => {
    const check = e.target.checked;
    if (!check) {
      setIsChecked(isChecked.filter((id) => id !== item?.productId))
      setProductChoose(productChoose.filter(({ productId }) => productId !== item?.productId))
    } else {
      setIsChecked([...isChecked, item?.productId])
      setProductChoose([...productChoose, item])
    }
  };

  const handleProductDetails = (id) => {
    navigate(`/product/${id}`, {
      state: {
        item: id,
      },
    });
  };
  console.log(item, "hoang");
  return (
    <>
      <tr className="bg-white border-b text-sm">
        <td className="w-1/9 p-4 w-3">
          <div className="flex items-center">
            {item?.isHidden ? (
              <></>
            ) : (

              <input
                id={`checkbox-table-${item.productId}`}
                type="checkbox"
                checked={isChecked.includes(item?.productId)}
                onChange={(e) => handleCheckboxChange(item, e)}
                className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 dark:focus:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600"
              />
            )}


          </div>
        </td>
        <td className="px-2 py-2 w-1/9 font-semibold text-gray-900 w-24 h-24 whitespace-nowrap">
          <img className="w-24 h-24" src={item.imageText} alt="productImage" />
        </td>
        <td className="px-2 py-2 text-lg text-primeColor font-bold text-gray-900 whitespace-normal cursor-pointer"
          onClick={item?.isHidden ? undefined :  () => handleProductDetails(item?.productId)}>
          {item.title}
          {item?.isHidden && (<p style={{ color: 'red' }}>(Sản phẩm không khả dụng)</p>)}
        </td>

        <td className="px-2 py-2 w-1/9 font-semibold text-gray-900 whitespace-nowrap">
          {item.price ? item.price.toLocaleString() : 0} VND
        </td>
        <td className="px-2 py-2 w-1/9 font-semibold text-gray-900 whitespace-nowrap">
          {item.depositPrice ? item.depositPrice.toLocaleString() : 0} VND
        </td>
        <td className="px-2 py-2 w-1/9 font-semibold text-gray-900 whitespace-nowrap">
          <div className="flex items-center gap-6 text-lg">
            <span
              onClick={() => handleUpdateProduct("decreaseQuantity")}
              className="w-6 h-6 bg-gray-100 text-2xl flex items-center justify-center hover:bg-gray-300 cursor-pointer duration-300 border-[1px] border-gray-300 hover:border-gray-300"
            >
              -
            </span>
            <span>{itemCart.quantity <= 0 ? 0 : itemCart.quantity}</span>
            <span
              onClick={() => handleUpdateProduct("increaseQuantity")}
              className="w-6 h-6 bg-gray-100 text-2xl flex items-center justify-center hover:bg-gray-300 cursor-pointer duration-300 border-[1px] border-gray-300 hover:border-gray-300"
            >
              +
            </span>
          </div>
        </td>
        <td className="px-2 py-2 w-1/9 font-semibold text-gray-900 whitespace-nowrap item-center">
          <div className="flex items-center gap-6 text-lg">
            <span
              onClick={() => handleUpdateProduct("decreasePeriod")}
              className="w-6 h-6 bg-gray-100 text-2xl flex items-center justify-center hover:bg-gray-300 cursor-pointer duration-300 border-[1px] border-gray-300 hover:border-gray-300"
            >
              -
            </span>
            <span>
              {itemCart.rentalPeriod <= 0 ? 0 : itemCart.rentalPeriod}
            </span>
            <span
              onClick={() => handleUpdateProduct("increasePeriod")}
              className="w-6 h-6 bg-gray-100 text-2xl flex items-center justify-center hover:bg-gray-300 cursor-pointer duration-300 border-[1px] border-gray-300 hover:border-gray-300"
            >
              +
            </span>
          </div>
        </td>
        <td className="px-2 py-2 w-1/9 font-semibold text-gray-900 whitespace-nowrap">
          <span>{totalTmp ? totalTmp.toLocaleString() : 0} VND</span>
        </td>
        <td className="px-2 py-2 w-1/9 text-right font-semibold text-gray-900 whitespace-nowrap">
          <div>
            <ImCross
              onClick={handleDeleteCart}
              className="text-primeColor hover:text-red-500 duration-300 cursor-pointer"
            />
          </div>
        </td>
      </tr>
    </>
  );
};

export default ItemCard;
