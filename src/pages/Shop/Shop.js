import React, {useEffect, useState} from "react";
import Breadcrumbs from "../../components/pageProps/Breadcrumbs";
import Pagination from "../../components/pageProps/shopPage/Pagination";
import ProductBanner from "../../components/pageProps/shopPage/ProductBanner";
import ShopSideNav from "../../components/pageProps/shopPage/ShopSideNav";
import {useDispatch, useSelector} from "react-redux";
import {getAllProduct} from "../../redux/Req/apiRequestProduct";
import {updateFilter, updatePageble, updateProduct} from "../../redux/Slide/productSlice";


const  Shop = () => {
  const dispatch = useDispatch();
 
  const itemsPerPageFromBanner = (itemsPerPage) => {
    dispatch(updatePageble({ pageIndex: 1, pageSize: itemsPerPage }));
  };

  const [totalPage, setTotalPage] = useState(1);
  const produtStore = useSelector((state) => state?.product);

  const { pageable, filters, products } = produtStore;

  useEffect(() => {
    dispatch(updateFilter({userId:null}))
  }, []);
  useEffect(() => {
    getProducts();
  }, [pageable, filters]);

  const getProducts = async () => {
    const response = await getAllProduct({ ...filters, ...pageable });
     
    if (response) {
      dispatch(updateProduct(response.content));

      setTotalPage(response.totalPages);
    }
  };

  return (
    <div className="max-w-container mx-auto py-20 xl:py-20">
      {/* <Breadcrumbs title="Sản phẩm cho thuê" /> */}
      {/* ================= Products Start here =================== */}
      <div className="w-full h-full flex pb-20 gap-10">
        <div className="w-[20%] lgl:w-[25%] hidden mdl:inline-flex h-full">
          <ShopSideNav />
        </div>
        <div className="w-full mdl:w-[80%] lgl:w-[75%] h-full flex flex-col gap-10">
          <ProductBanner itemsPerPageFromBanner={itemsPerPageFromBanner} pageable={pageable} />

          <Pagination
            pageable={pageable}
            products={products}
            totalPage={totalPage}
          />
        </div>
      </div>
      {/* ================= Products End here ===================== */}
    </div>
  );
};

export default Shop;
