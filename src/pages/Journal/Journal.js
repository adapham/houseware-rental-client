import React, { useEffect, useState } from "react";
import { Link, useLocation } from "react-router-dom";
import Breadcrumbs from "../../components/pageProps/Breadcrumbs";

const Journal = () => {
  const location = useLocation();
  const [prevLocation, setPrevLocation] = useState("");
  useEffect(() => {
    setPrevLocation(location?.state?.data);
  }, [location]);
  return (
    <div className="max-w-container mx-auto px-4">
      <div className="flex justify-center items-center">
            <div className="2xl:mx-auto 2xl:container lg:px-20 lg:py-16 md:py-12 md:px-6 py-9 px-4 w-96 sm:w-auto">
                <div className="flex flex-col items-center justify-center">
                    <h1 className="text-4xl font-semibold leading-9 text-center text-gray-800">Blogs</h1>
                    <p className="text-base leading-normal text-center text-gray-600 mt-4 lg:w-1/2 md:w-10/12 w-11/12">Chúng mình đang thực hiện khảo sát về tâm lý thuê đồ</p>
                </div>
                <div className="lg:flex items-stretch md:mt-12 mt-8">
                    <div className="lg:w-1/2">
                        <div className="sm:flex items-center justify-between xl:gap-x-8 gap-x-6">
                            <div className="sm:w-1/2 relative">
                                <div>
                                    <p className="p-6 text-xs font-medium leading-3 text-white absolute top-0 right-0"></p>
                                    <div className="absolute bottom-0 left-0 p-6">
                                        <h2 className="text-xl font-semibold 5 text-white">Đặt cho mỗi đồng tiết kiệm một mục đích</h2>
                                        <p className="text-base leading-4 text-white mt-2">29 Thg 09</p>
                                        <div className="flex items-center mt-4 cursor-pointer text-white hover:text-gray-200 hover:underline">
                                            <p className="pr-2 text-sm font-medium leading-none" onClick={()=>{window.open("https://vietcetera.com/vn/dat-cho-moi-dong-tiet-kiem-mot-muc-dich", '_blank');}} >Read More</p>
                                            <svg className="fill-stroke" width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M5.75 12.5L10.25 8L5.75 3.5" stroke="currentColor" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
                                            </svg>
                                        </div>
                                    </div>
                                </div>
                                <img style={{ objectFit: 'cover', height: '250px', width: '100%' }} src="https://cdn-resize-img.vietcetera.com/_next/image?url=https%3A%2F%2Fimg.vietcetera.com%2Fuploads%2Fimages%2F28-sep-2020%2F0928-saving-feature.jpg&q=80&w=2048" className="w-full" alt="chair" />
                            </div>
                            <div className="sm:w-1/2 sm:mt-0 mt-4 relative">
                                <div>
                                    <p className="p-6 text-xs font-medium leading-3 text-white absolute top-0 right-0"></p>
                                    <div className="absolute bottom-0 left-0 p-6">
                                        <h2 className="text-xl font-semibold 5 text-white">Kinh nghiệm tiết kiệm tiền khi sống tự lập</h2>
                                        <p className="text-base leading-4 text-white mt-2">20 Thg 11</p>
                                        <div className="flex items-center mt-4 cursor-pointer text-white hover:text-gray-200 hover:underline">
                                            <p className="pr-2 text-sm font-medium leading-none"
                                            onClick={()=>{window.open("https://vietcetera.com/vn/kinh-nghiem-tiet-kiem-tien-khi-song-tu-lap", '_blank');}}
                                            >Read More</p>
                                            <svg className="fill-stroke" width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M5.75 12.5L10.25 8L5.75 3.5" stroke="currentColor" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
                                            </svg>
                                        </div>
                                    </div>
                                </div>
                                <img style={{ objectFit: 'cover', height: '250px', width: '100%' }} src="https://cdn-resize-img.vietcetera.com/_next/image?url=https%3A%2F%2Fimg.vietcetera.com%2Fwp-content%2Fuploads%2F2019%2F11%2Ftiet-kiem-tien-cho-ban-tre-featured-1.jpg&q=80&w=2048" className="w-full" alt="wall design" />
                            </div>
                        </div>
                        <div className="relative">
                            <div>
                                <p className="md:p-10 p-6 text-xs font-medium leading-3 text-white absolute top-0 right-0"></p>
                                <div className="absolute bottom-0 left-0 md:p-10 p-6">
                                    <h2 className="text-xl font-semibold 5 text-white">Có người mẹ làm ngân hàng đã dạy tôi điều gì về tiết kiệm?</h2>
                                    <p className="text-base leading-4 text-white mt-2">29 Thg 11</p>
                                    <div className="flex items-center mt-4 cursor-pointer text-white hover:text-gray-200 hover:underline">
                                        <p className="pr-2 text-sm font-medium leading-none"
                                        onClick={()=>{window.open("https://vietcetera.com/vn/co-nguoi-me-lam-ngan-hang-da-day-toi-dieu-gi-ve-tiet-kiem", '_blank');}}
                                        >Read More</p>
                                        <svg className="fill-stroke" width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M5.75 12.5L10.25 8L5.75 3.5" stroke="currentColor" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
                                        </svg>
                                    </div>
                                </div>
                            </div>
                            <img style={{ objectFit: 'cover', height: '250px', width: '100%' }} src="https://img.vietcetera.com/wp-content/uploads/2019/11/NGAN-HANG-02.jpg" alt="sitting place" className="w-full mt-8 md:mt-6 hidden sm:block" />
                            <img className="w-full mt-4 sm:hidden" src="https://img.vietcetera.com/wp-content/uploads/2019/11/NGAN-HANG-01.jpg" alt="sitting place" />
                        </div>
                    </div>
                    <div className="lg:w-1/2 xl:ml-8 lg:ml-4 lg:mt-0 md:mt-6 mt-4 lg:flex flex-col justify-between">
                        <div className="relative">
                            <div>
                                <p className="md:p-10 p-6 text-xs font-medium leading-3 text-white absolute top-0 right-0"></p>
                                <div className="absolute bottom-0 left-0 md:p-10 p-6">
                                    <h2 className="text-xl font-semibold 5 text-white">Disposable income ảnh hưởng thế nào đến chi tiêu?</h2>
                                    <p className="text-base leading-4 text-white mt-2">05 Thg 09</p>
                                    <div className="flex items-center mt-4 cursor-pointer text-white hover:text-gray-200 hover:underline">
                                        <p className="pr-2 text-sm font-medium leading-none"
                                        onClick={()=>{window.open("https://vietcetera.com/vn/disposable-income-la-gi-anh-huong-the-nao-den-chi-tieu", '_blank');}}
                                        >Read More</p>
                                        <svg className="fill-stroke" width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M5.75 12.5L10.25 8L5.75 3.5" stroke="currentColor" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
                                        </svg>
                                    </div>
                                </div>
                            </div>
                            <img style={{ objectFit: 'cover', height: '250px', width: '100%' }} src="https://cdn-resize-img.vietcetera.com/_next/image?url=https%3A%2F%2Fimg.vietcetera.com%2Fuploads%2Fimages%2F01-sep-2021%2F20210901-5sgiainghia-feature.jpg&q=80&w=2048" alt="sitting place" className="w-full sm:block hidden" />
                            <img className="w-full sm:hidden" src="https://cdn-resize-img.vietcetera.com/_next/image?url=https%3A%2F%2Fimg.vietcetera.com%2Fuploads%2Fimages%2F01-sep-2021%2F20210901-5sgiainghia-feature.jpg&q=80&w=2048" alt="sitting place" />
                        </div>
                        <div className="sm:flex items-center justify-between xl:gap-x-8 gap-x-6 md:mt-6 mt-4">
                            <div className="relative w-full">
                                <div>
                                    <p className="p-6 text-xs font-medium leading-3 text-white absolute top-0 right-0"></p>
                                    <div className="absolute bottom-0 left-0 p-6">
                                        <h2 className="text-xl font-semibold 5 text-white">Mua hàng sale có đồng nghĩa với tiết kiệm?</h2>
                                        <p className="text-base leading-4 text-white mt-2">17 Thg 3</p>
                                        <div className="flex items-center mt-4 cursor-pointer text-white hover:text-gray-200 hover:underline">
                                            <p className="pr-2 text-sm font-medium leading-none"
                                            onClick={()=>{window.open("https://vietcetera.com/vn/mua-hang-sale-co-dong-nghia-voi-tiet-kiem", '_blank');}}
                                            >Read More</p>
                                            <svg className="fill-stroke" width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M5.75 12.5L10.25 8L5.75 3.5" stroke="currentColor" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
                                            </svg>
                                        </div>
                                    </div>
                                </div>
                                <img style={{ objectFit: 'cover', height: '250px', width: '100%' }} src="https://cdn-resize-img.vietcetera.com/_next/image?url=https%3A%2F%2Fimg.vietcetera.com%2Fuploads%2Fimages%2F16-mar-2021%2Ftiendongtiendinhsale.jpg&q=80&w=2048" className="w-full" alt="chair" />
                            </div>
                            <div className="relative w-full sm:mt-0 mt-4">
                                <div>
                                    <p className="p-6 text-xs font-medium leading-3 text-white absolute top-0 right-0"></p>
                                    <div className="absolute bottom-0 left-0 p-6">
                                        <h2 className="text-xl font-semibold 5 text-white">Chúng ta nhận được tài sản gì từ cha mẹ?</h2>
                                        <p className="text-base leading-4 text-white mt-2">28 Thg 07</p>
                                        <div className="flex items-center mt-4 cursor-pointer text-white hover:text-gray-200 hover:underline">
                                            <p className="pr-2 text-sm font-medium leading-none"
                                            onClick={()=>{window.open("https://vietcetera.com/vn/chung-ta-nhan-duoc-tai-san-gi-tu-cha-me", '_blank');}}
                                            >Read More</p>
                                            <svg className="fill-stroke" width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M5.75 12.5L10.25 8L5.75 3.5" stroke="currentColor" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
                                            </svg>
                                        </div>
                                    </div>
                                </div>
                                <img style={{ objectFit: 'cover', height: '250px', width: '100%' }} src="https://cdn-resize-img.vietcetera.com/_next/image?url=https%3A%2F%2Fimg.vietcetera.com%2Fuploads%2Fimages%2F27-jul-2021%2F210726-taisandelai-feature.jpg&q=80&w=2048" className="w-full" alt="wall design" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  );
};

export default Journal;
