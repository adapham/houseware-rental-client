
import logoLight from "./logoLight.png";
import bannerImgOne from "./banner/bannerImgOne.avif";
import bannerImgTwo from "./banner/bannerImgTwo.avif";
import bannerImgThree from "./banner/bannerImgThree.avif";
import saleImgOne from "./sale/saleImgOne.png";
import saleImgTwo from "./sale/saleImgTwo.png";
import saleImgThree from "./sale/saleImgThree.png";
// ============== Products Start here ====================
// New Arrivals
import newArrOne from "./products/newArrival/newArrOne.jpeg";
import newArrTwo from "./products/newArrival/newArrTwo.jpeg";
import newArrThree from "./products/newArrival/newArrThree.jpeg";
import newArrFour from "./products/newArrival/newArrFour.jpeg";

// Best Sellers
import bestSellerOne from "./products/bestSeller/bestSellerOne.jpeg";
import bestSellerTwo from "./products/bestSeller/bestSellerTwo.jpeg";
import bestSellerThree from "./products/bestSeller/bestSellerThree.jpeg";
import bestSellerFour from "./products/bestSeller/bestSellerFour.jpeg";

// Special Offers
import spfOne from "./products/specialOffer/spfOne.jpeg";
import spfTwo from "./products/specialOffer/spfTwo.jpeg";
import spfThree from "./products/specialOffer/spfThree.jpeg";
import spfFour from "./products/specialOffer/spfFour.jpeg";

// Year Product
import productOfTheYear from "./products/productOfTheYear.jpeg";
// ============== Products End here ======================
import paymentCard from "./payment.png";
import emptyCart from "../images/emptyCart.png";

// Image Feature
import featureOne from "./theme/icon/feature-1.png"
import featureThree from "./theme/icon/feature-3.png"
import featureFour from "./theme/icon/feature-4.png"
import featureFive from "./theme/icon/feature-5.png"
import featureSix from "./theme/icon/feature-6.png"

export {

  logoLight,
  bannerImgOne,
  bannerImgTwo,
  bannerImgThree,
  saleImgOne,
  saleImgTwo,
  saleImgThree,
  // ===================== Products start here ============
  // New Arrivals
  newArrOne,
  newArrTwo,
  newArrThree,
  newArrFour,

  // Best Sellers
  bestSellerOne,
  bestSellerTwo,
  bestSellerThree,
  bestSellerFour,

  // Sprcial Offers
  spfOne,
  spfTwo,
  spfThree,
  spfFour,

  // Year Product
  productOfTheYear,
  // ===================== Products End here ==============
  paymentCard,
  emptyCart,

  // Image Feature
  featureOne,
  featureThree,
  featureFour,
  featureFive,
  featureSix,
};
