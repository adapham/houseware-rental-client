import axios from "axios";
import authHeader from "./header";

const BASE_URL = process.env.REACT_APP_BASE_URL;
const API_URL = `${BASE_URL}/api/user/`;

const getAdminBoard = () => {
    return axios.get(API_URL + "admin", {
        headers: authHeader() });
};


const userService = {
    getAdminBoard,
};

export default userService;

